
package ke.co.coop.GetAccountStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts}getAccountsOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAccountsOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Common/Service/CommonCollections/GetAccounts/1.0/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts")
    protected GetAccountsOutputType getAccountsOutput;

    /**
     * Gets the value of the getAccountsOutput property.
     * 
     * @return
     *     possible object is
     *     {@link GetAccountsOutputType }
     *     
     */
    public GetAccountsOutputType getGetAccountsOutput() {
        return getAccountsOutput;
    }

    /**
     * Sets the value of the getAccountsOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAccountsOutputType }
     *     
     */
    public void setGetAccountsOutput(GetAccountsOutputType value) {
        this.getAccountsOutput = value;
    }

}
