
package ke.co.coop.GetAccountStubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ke.co.coop.GetAccountStubs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAccountsInput_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", "getAccountsInput");
    private final static QName _Institution_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0", "Institution");
    private final static QName _Account_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0", "Account");
    private final static QName _GetAccountsOutput_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", "getAccountsOutput");
    private final static QName _HeaderReplyStatusDescription_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusDescription");
    private final static QName _HeaderReplyStatusDescriptionKey_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusDescriptionKey");
    private final static QName _HeaderReplyStatusCode_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusCode");
    private final static QName _HeaderReplyMessageID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageID");
    private final static QName _HeaderReplyElapsedTime_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ElapsedTime");
    private final static QName _HeaderReplyCorrelationID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "CorrelationID");
    private final static QName _GetAccountsOutputTypeAccountsOperationParametersBillerCode_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", "BillerCode");
    private final static QName _GetAccountsOutputTypeAccountsOperationParametersAccountId_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", "AccountId");
    private final static QName _GetAccountsOutputTypeAccountsOperationParametersServiceCode_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", "ServiceCode");
    private final static QName _CredentialsUsername_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Username");
    private final static QName _CredentialsRealm_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Realm");
    private final static QName _CredentialsPassword_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Password");
    private final static QName _GetAccountsOutputTypeOperationParametersInstitutionId_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", "InstitutionId");
    private final static QName _StatusMessageMessageDescription_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageDescription");
    private final static QName _StatusMessageMessageCode_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageCode");
    private final static QName _StatusMessageApplicationID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ApplicationID");
    private final static QName _StatusMessageMessageType_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageType");
    private final static QName _HeaderRequestCreationTimestamp_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "CreationTimestamp");
    private final static QName _HeaderRequestReplyTO_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ReplyTO");
    private final static QName _HeaderRequestFaultTO_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "FaultTO");
    private final static QName _GetAccountsInputTypeOperationParametersOperationDate_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", "OperationDate");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ke.co.coop.GetAccountStubs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link HeaderReply }
     * 
     */
    public HeaderReply createHeaderReply() {
        return new HeaderReply();
    }

    /**
     * Create an instance of {@link GetAccountsInputType }
     * 
     */
    public GetAccountsInputType createGetAccountsInputType() {
        return new GetAccountsInputType();
    }

    /**
     * Create an instance of {@link GetAccountsOutputType }
     * 
     */
    public GetAccountsOutputType createGetAccountsOutputType() {
        return new GetAccountsOutputType();
    }

    /**
     * Create an instance of {@link GetAccountsOutputType.Accounts }
     * 
     */
    public GetAccountsOutputType.Accounts createGetAccountsOutputTypeAccounts() {
        return new GetAccountsOutputType.Accounts();
    }

    /**
     * Create an instance of {@link AccountType }
     * 
     */
    public AccountType createAccountType() {
        return new AccountType();
    }

    /**
     * Create an instance of {@link InstitutionType }
     * 
     */
    public InstitutionType createInstitutionType() {
        return new InstitutionType();
    }

    /**
     * Create an instance of {@link DataInput }
     * 
     */
    public DataInput createDataInput() {
        return new DataInput();
    }

    /**
     * Create an instance of {@link DataOutput }
     * 
     */
    public DataOutput createDataOutput() {
        return new DataOutput();
    }

    /**
     * Create an instance of {@link HeaderRequest }
     * 
     */
    public HeaderRequest createHeaderRequest() {
        return new HeaderRequest();
    }

    /**
     * Create an instance of {@link Credentials }
     * 
     */
    public Credentials createCredentials() {
        return new Credentials();
    }

    /**
     * Create an instance of {@link HeaderReply.StatusMessages }
     * 
     */
    public HeaderReply.StatusMessages createHeaderReplyStatusMessages() {
        return new HeaderReply.StatusMessages();
    }

    /**
     * Create an instance of {@link StatusMessage }
     * 
     */
    public StatusMessage createStatusMessage() {
        return new StatusMessage();
    }

    /**
     * Create an instance of {@link GetAccountsInputType.OperationParameters }
     * 
     */
    public GetAccountsInputType.OperationParameters createGetAccountsInputTypeOperationParameters() {
        return new GetAccountsInputType.OperationParameters();
    }

    /**
     * Create an instance of {@link GetAccountsOutputType.OperationParameters }
     * 
     */
    public GetAccountsOutputType.OperationParameters createGetAccountsOutputTypeOperationParameters() {
        return new GetAccountsOutputType.OperationParameters();
    }

    /**
     * Create an instance of {@link GetAccountsOutputType.Accounts.OperationParameters }
     * 
     */
    public GetAccountsOutputType.Accounts.OperationParameters createGetAccountsOutputTypeAccountsOperationParameters() {
        return new GetAccountsOutputType.Accounts.OperationParameters();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAccountsInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", name = "getAccountsInput")
    public JAXBElement<GetAccountsInputType> createGetAccountsInput(GetAccountsInputType value) {
        return new JAXBElement<GetAccountsInputType>(_GetAccountsInput_QNAME, GetAccountsInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0", name = "Institution")
    public JAXBElement<InstitutionType> createInstitution(InstitutionType value) {
        return new JAXBElement<InstitutionType>(_Institution_QNAME, InstitutionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0", name = "Account")
    public JAXBElement<AccountType> createAccount(AccountType value) {
        return new JAXBElement<AccountType>(_Account_QNAME, AccountType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAccountsOutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", name = "getAccountsOutput")
    public JAXBElement<GetAccountsOutputType> createGetAccountsOutput(GetAccountsOutputType value) {
        return new JAXBElement<GetAccountsOutputType>(_GetAccountsOutput_QNAME, GetAccountsOutputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusDescription", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusDescription(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusDescription_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusDescriptionKey", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusDescriptionKey(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusDescriptionKey_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusCode", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusCode(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusCode_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageID", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyMessageID(String value) {
        return new JAXBElement<String>(_HeaderReplyMessageID_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ElapsedTime", scope = HeaderReply.class)
    public JAXBElement<Long> createHeaderReplyElapsedTime(Long value) {
        return new JAXBElement<Long>(_HeaderReplyElapsedTime_QNAME, Long.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CorrelationID", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyCorrelationID(String value) {
        return new JAXBElement<String>(_HeaderReplyCorrelationID_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", name = "BillerCode", scope = GetAccountsOutputType.Accounts.OperationParameters.class)
    public JAXBElement<String> createGetAccountsOutputTypeAccountsOperationParametersBillerCode(String value) {
        return new JAXBElement<String>(_GetAccountsOutputTypeAccountsOperationParametersBillerCode_QNAME, String.class, GetAccountsOutputType.Accounts.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", name = "AccountId", scope = GetAccountsOutputType.Accounts.OperationParameters.class)
    public JAXBElement<String> createGetAccountsOutputTypeAccountsOperationParametersAccountId(String value) {
        return new JAXBElement<String>(_GetAccountsOutputTypeAccountsOperationParametersAccountId_QNAME, String.class, GetAccountsOutputType.Accounts.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", name = "ServiceCode", scope = GetAccountsOutputType.Accounts.OperationParameters.class)
    public JAXBElement<String> createGetAccountsOutputTypeAccountsOperationParametersServiceCode(String value) {
        return new JAXBElement<String>(_GetAccountsOutputTypeAccountsOperationParametersServiceCode_QNAME, String.class, GetAccountsOutputType.Accounts.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Username", scope = Credentials.class)
    public JAXBElement<String> createCredentialsUsername(String value) {
        return new JAXBElement<String>(_CredentialsUsername_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Realm", scope = Credentials.class)
    public JAXBElement<String> createCredentialsRealm(String value) {
        return new JAXBElement<String>(_CredentialsRealm_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Password", scope = Credentials.class)
    public JAXBElement<String> createCredentialsPassword(String value) {
        return new JAXBElement<String>(_CredentialsPassword_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", name = "InstitutionId", scope = GetAccountsOutputType.OperationParameters.class)
    public JAXBElement<String> createGetAccountsOutputTypeOperationParametersInstitutionId(String value) {
        return new JAXBElement<String>(_GetAccountsOutputTypeOperationParametersInstitutionId_QNAME, String.class, GetAccountsOutputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageDescription", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageDescription(String value) {
        return new JAXBElement<String>(_StatusMessageMessageDescription_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageCode", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageCode(String value) {
        return new JAXBElement<String>(_StatusMessageMessageCode_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ApplicationID", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageApplicationID(String value) {
        return new JAXBElement<String>(_StatusMessageApplicationID_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageType", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageType(String value) {
        return new JAXBElement<String>(_StatusMessageMessageType_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CreationTimestamp", scope = HeaderRequest.class)
    public JAXBElement<XMLGregorianCalendar> createHeaderRequestCreationTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_HeaderRequestCreationTimestamp_QNAME, XMLGregorianCalendar.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ReplyTO", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestReplyTO(String value) {
        return new JAXBElement<String>(_HeaderRequestReplyTO_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "FaultTO", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestFaultTO(String value) {
        return new JAXBElement<String>(_HeaderRequestFaultTO_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CorrelationID", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestCorrelationID(String value) {
        return new JAXBElement<String>(_HeaderReplyCorrelationID_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", name = "OperationDate", scope = GetAccountsInputType.OperationParameters.class)
    public JAXBElement<XMLGregorianCalendar> createGetAccountsInputTypeOperationParametersOperationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetAccountsInputTypeOperationParametersOperationDate_QNAME, XMLGregorianCalendar.class, GetAccountsInputType.OperationParameters.class, value);
    }

}
