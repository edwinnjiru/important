
package ke.co.coop.GetAccountStubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for getAccountsInputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAccountsInputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperationParameters" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OperationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0}Institution" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAccountsInputType", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", propOrder = {
    "operationParameters",
    "institution"
})
public class GetAccountsInputType {

    @XmlElement(name = "OperationParameters")
    protected GetAccountsInputType.OperationParameters operationParameters;
    @XmlElement(name = "Institution", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0")
    protected InstitutionType institution;

    /**
     * Gets the value of the operationParameters property.
     * 
     * @return
     *     possible object is
     *     {@link GetAccountsInputType.OperationParameters }
     *     
     */
    public GetAccountsInputType.OperationParameters getOperationParameters() {
        return operationParameters;
    }

    /**
     * Sets the value of the operationParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAccountsInputType.OperationParameters }
     *     
     */
    public void setOperationParameters(GetAccountsInputType.OperationParameters value) {
        this.operationParameters = value;
    }

    /**
     * Gets the value of the institution property.
     * 
     * @return
     *     possible object is
     *     {@link InstitutionType }
     *     
     */
    public InstitutionType getInstitution() {
        return institution;
    }

    /**
     * Sets the value of the institution property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstitutionType }
     *     
     */
    public void setInstitution(InstitutionType value) {
        this.institution = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OperationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "operationDate"
    })
    public static class OperationParameters {

        @XmlElementRef(name = "OperationDate", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> operationDate;

        /**
         * Gets the value of the operationDate property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getOperationDate() {
            return operationDate;
        }

        /**
         * Sets the value of the operationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setOperationDate(JAXBElement<XMLGregorianCalendar> value) {
            this.operationDate = value;
        }

    }

}
