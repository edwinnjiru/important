
package ke.co.coop.GetAccountStubs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAccountsOutputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAccountsOutputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0}Institution" minOccurs="0"/>
 *         &lt;element name="OperationParameters" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="InstitutionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Accounts" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0}Account" minOccurs="0"/>
 *                   &lt;element name="OperationParameters" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AccountId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BillerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAccountsOutputType", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", propOrder = {
    "institution",
    "operationParameters",
    "accounts"
})
public class GetAccountsOutputType {

    @XmlElement(name = "Institution", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0")
    protected InstitutionType institution;
    @XmlElement(name = "OperationParameters")
    protected GetAccountsOutputType.OperationParameters operationParameters;
    @XmlElement(name = "Accounts")
    protected List<GetAccountsOutputType.Accounts> accounts;

    /**
     * Gets the value of the institution property.
     * 
     * @return
     *     possible object is
     *     {@link InstitutionType }
     *     
     */
    public InstitutionType getInstitution() {
        return institution;
    }

    /**
     * Sets the value of the institution property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstitutionType }
     *     
     */
    public void setInstitution(InstitutionType value) {
        this.institution = value;
    }

    /**
     * Gets the value of the operationParameters property.
     * 
     * @return
     *     possible object is
     *     {@link GetAccountsOutputType.OperationParameters }
     *     
     */
    public GetAccountsOutputType.OperationParameters getOperationParameters() {
        return operationParameters;
    }

    /**
     * Sets the value of the operationParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAccountsOutputType.OperationParameters }
     *     
     */
    public void setOperationParameters(GetAccountsOutputType.OperationParameters value) {
        this.operationParameters = value;
    }

    /**
     * Gets the value of the accounts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accounts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccounts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetAccountsOutputType.Accounts }
     * 
     * 
     */
    public List<GetAccountsOutputType.Accounts> getAccounts() {
        if (accounts == null) {
            accounts = new ArrayList<GetAccountsOutputType.Accounts>();
        }
        return this.accounts;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0}Account" minOccurs="0"/>
     *         &lt;element name="OperationParameters" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AccountId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BillerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "account",
        "operationParameters"
    })
    public static class Accounts {

        @XmlElement(name = "Account", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0")
        protected AccountType account;
        @XmlElement(name = "OperationParameters", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts")
        protected GetAccountsOutputType.Accounts.OperationParameters operationParameters;

        /**
         * Gets the value of the account property.
         * 
         * @return
         *     possible object is
         *     {@link AccountType }
         *     
         */
        public AccountType getAccount() {
            return account;
        }

        /**
         * Sets the value of the account property.
         * 
         * @param value
         *     allowed object is
         *     {@link AccountType }
         *     
         */
        public void setAccount(AccountType value) {
            this.account = value;
        }

        /**
         * Gets the value of the operationParameters property.
         * 
         * @return
         *     possible object is
         *     {@link GetAccountsOutputType.Accounts.OperationParameters }
         *     
         */
        public GetAccountsOutputType.Accounts.OperationParameters getOperationParameters() {
            return operationParameters;
        }

        /**
         * Sets the value of the operationParameters property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetAccountsOutputType.Accounts.OperationParameters }
         *     
         */
        public void setOperationParameters(GetAccountsOutputType.Accounts.OperationParameters value) {
            this.operationParameters = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AccountId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BillerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "accountId",
            "serviceCode",
            "billerCode"
        })
        public static class OperationParameters {

            @XmlElementRef(name = "AccountId", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", type = JAXBElement.class, required = false)
            protected JAXBElement<String> accountId;
            @XmlElementRef(name = "ServiceCode", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", type = JAXBElement.class, required = false)
            protected JAXBElement<String> serviceCode;
            @XmlElementRef(name = "BillerCode", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", type = JAXBElement.class, required = false)
            protected JAXBElement<String> billerCode;

            /**
             * Gets the value of the accountId property.
             * 
             * @return
             *     possible object is
             *     {@link JAXBElement }{@code <}{@link String }{@code >}
             *     
             */
            public JAXBElement<String> getAccountId() {
                return accountId;
            }

            /**
             * Sets the value of the accountId property.
             * 
             * @param value
             *     allowed object is
             *     {@link JAXBElement }{@code <}{@link String }{@code >}
             *     
             */
            public void setAccountId(JAXBElement<String> value) {
                this.accountId = value;
            }

            /**
             * Gets the value of the serviceCode property.
             * 
             * @return
             *     possible object is
             *     {@link JAXBElement }{@code <}{@link String }{@code >}
             *     
             */
            public JAXBElement<String> getServiceCode() {
                return serviceCode;
            }

            /**
             * Sets the value of the serviceCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link JAXBElement }{@code <}{@link String }{@code >}
             *     
             */
            public void setServiceCode(JAXBElement<String> value) {
                this.serviceCode = value;
            }

            /**
             * Gets the value of the billerCode property.
             * 
             * @return
             *     possible object is
             *     {@link JAXBElement }{@code <}{@link String }{@code >}
             *     
             */
            public JAXBElement<String> getBillerCode() {
                return billerCode;
            }

            /**
             * Sets the value of the billerCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link JAXBElement }{@code <}{@link String }{@code >}
             *     
             */
            public void setBillerCode(JAXBElement<String> value) {
                this.billerCode = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="InstitutionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "referenceNumber",
        "institutionId"
    })
    public static class OperationParameters {

        @XmlElement(name = "ReferenceNumber", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts")
        protected String referenceNumber;
        @XmlElementRef(name = "InstitutionId", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", type = JAXBElement.class, required = false)
        protected JAXBElement<String> institutionId;

        /**
         * Gets the value of the referenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferenceNumber() {
            return referenceNumber;
        }

        /**
         * Sets the value of the referenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferenceNumber(String value) {
            this.referenceNumber = value;
        }

        /**
         * Gets the value of the institutionId property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getInstitutionId() {
            return institutionId;
        }

        /**
         * Sets the value of the institutionId property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setInstitutionId(JAXBElement<String> value) {
            this.institutionId = value;
        }

    }

}
