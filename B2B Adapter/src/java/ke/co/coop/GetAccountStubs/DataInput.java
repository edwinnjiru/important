
package ke.co.coop.GetAccountStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts}getAccountsInput"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAccountsInput"
})
@XmlRootElement(name = "DataInput", namespace = "urn://co-opbank.co.ke/Banking/Common/Service/CommonCollections/GetAccounts/1.0/DataIO")
public class DataInput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getAccounts/1.0/CommonCollections.getAccounts", required = true)
    protected GetAccountsInputType getAccountsInput;

    /**
     * Gets the value of the getAccountsInput property.
     * 
     * @return
     *     possible object is
     *     {@link GetAccountsInputType }
     *     
     */
    public GetAccountsInputType getGetAccountsInput() {
        return getAccountsInput;
    }

    /**
     * Sets the value of the getAccountsInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAccountsInputType }
     *     
     */
    public void setGetAccountsInput(GetAccountsInputType value) {
        this.getAccountsInput = value;
    }

}
