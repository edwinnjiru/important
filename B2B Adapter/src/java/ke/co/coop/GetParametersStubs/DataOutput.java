
package ke.co.coop.GetParametersStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getParameters/1.0/CommonCollections.getParameters}getParametersOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getParametersOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Common/Service/CommonCollections/GetParameters/1.0/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/getParameters/1.0/CommonCollections.getParameters")
    protected GetParametersOutputType getParametersOutput;

    /**
     * Gets the value of the getParametersOutput property.
     * 
     * @return
     *     possible object is
     *     {@link GetParametersOutputType }
     *     
     */
    public GetParametersOutputType getGetParametersOutput() {
        return getParametersOutput;
    }

    /**
     * Sets the value of the getParametersOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetParametersOutputType }
     *     
     */
    public void setGetParametersOutput(GetParametersOutputType value) {
        this.getParametersOutput = value;
    }

}
