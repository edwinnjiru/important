
package ke.co.coop.NotifyStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify}notifyOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "notifyOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Common/Service/CommonCollections/Notify/1.0/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
    protected NotifyOutputType notifyOutput;

    /**
     * Gets the value of the notifyOutput property.
     * 
     * @return
     *     possible object is
     *     {@link NotifyOutputType }
     *     
     */
    public NotifyOutputType getNotifyOutput() {
        return notifyOutput;
    }

    /**
     * Sets the value of the notifyOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotifyOutputType }
     *     
     */
    public void setNotifyOutput(NotifyOutputType value) {
        this.notifyOutput = value;
    }

}
