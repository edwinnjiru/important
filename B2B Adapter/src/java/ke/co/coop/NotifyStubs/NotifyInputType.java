
package ke.co.coop.NotifyStubs;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for notifyInputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="notifyInputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperationParameters" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OperationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="B2BReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BFUBReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ChequeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PostingDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="ValueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="Account_CR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Account_Dr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Remitter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RemitterReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "notifyInputType", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify", propOrder = {
    "operationParameters"
})
public class NotifyInputType {

    @XmlElement(name = "OperationParameters")
    protected NotifyInputType.OperationParameters operationParameters;

    /**
     * Gets the value of the operationParameters property.
     * 
     * @return
     *     possible object is
     *     {@link NotifyInputType.OperationParameters }
     *     
     */
    public NotifyInputType.OperationParameters getOperationParameters() {
        return operationParameters;
    }

    /**
     * Sets the value of the operationParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotifyInputType.OperationParameters }
     *     
     */
    public void setOperationParameters(NotifyInputType.OperationParameters value) {
        this.operationParameters = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OperationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="B2BReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BFUBReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ChequeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PostingDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="ValueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="Account_CR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Account_Dr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Remitter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RemitterReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "operationDate",
        "b2BReference",
        "bfubReference",
        "amount",
        "paymentType",
        "chequeNumber",
        "postingDate",
        "valueDate",
        "accountCR",
        "accountDr",
        "remitter",
        "remitterReference"
    })
    public static class OperationParameters {

        @XmlElementRef(name = "OperationDate", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> operationDate;
        @XmlElement(name = "B2BReference", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        protected String b2BReference;
        @XmlElement(name = "BFUBReference", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        protected String bfubReference;
        @XmlElement(name = "Amount", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        protected BigDecimal amount;
        @XmlElement(name = "PaymentType", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        protected String paymentType;
        @XmlElement(name = "ChequeNumber", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        protected String chequeNumber;
        @XmlElement(name = "PostingDate", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar postingDate;
        @XmlElement(name = "ValueDate", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar valueDate;
        @XmlElement(name = "Account_CR", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        protected String accountCR;
        @XmlElement(name = "Account_Dr", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        protected String accountDr;
        @XmlElement(name = "Remitter", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        protected String remitter;
        @XmlElement(name = "RemitterReference", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify")
        protected String remitterReference;

        /**
         * Gets the value of the operationDate property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getOperationDate() {
            return operationDate;
        }

        /**
         * Sets the value of the operationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setOperationDate(JAXBElement<XMLGregorianCalendar> value) {
            this.operationDate = value;
        }

        /**
         * Gets the value of the b2BReference property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getB2BReference() {
            return b2BReference;
        }

        /**
         * Sets the value of the b2BReference property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setB2BReference(String value) {
            this.b2BReference = value;
        }

        /**
         * Gets the value of the bfubReference property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBFUBReference() {
            return bfubReference;
        }

        /**
         * Sets the value of the bfubReference property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBFUBReference(String value) {
            this.bfubReference = value;
        }

        /**
         * Gets the value of the amount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Sets the value of the amount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

        /**
         * Gets the value of the paymentType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaymentType() {
            return paymentType;
        }

        /**
         * Sets the value of the paymentType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaymentType(String value) {
            this.paymentType = value;
        }

        /**
         * Gets the value of the chequeNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChequeNumber() {
            return chequeNumber;
        }

        /**
         * Sets the value of the chequeNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChequeNumber(String value) {
            this.chequeNumber = value;
        }

        /**
         * Gets the value of the postingDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getPostingDate() {
            return postingDate;
        }

        /**
         * Sets the value of the postingDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setPostingDate(XMLGregorianCalendar value) {
            this.postingDate = value;
        }

        /**
         * Gets the value of the valueDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getValueDate() {
            return valueDate;
        }

        /**
         * Sets the value of the valueDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setValueDate(XMLGregorianCalendar value) {
            this.valueDate = value;
        }

        /**
         * Gets the value of the accountCR property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountCR() {
            return accountCR;
        }

        /**
         * Sets the value of the accountCR property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountCR(String value) {
            this.accountCR = value;
        }

        /**
         * Gets the value of the accountDr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountDr() {
            return accountDr;
        }

        /**
         * Sets the value of the accountDr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountDr(String value) {
            this.accountDr = value;
        }

        /**
         * Gets the value of the remitter property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemitter() {
            return remitter;
        }

        /**
         * Sets the value of the remitter property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemitter(String value) {
            this.remitter = value;
        }

        /**
         * Gets the value of the remitterReference property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRemitterReference() {
            return remitterReference;
        }

        /**
         * Sets the value of the remitterReference property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRemitterReference(String value) {
            this.remitterReference = value;
        }

    }

}
