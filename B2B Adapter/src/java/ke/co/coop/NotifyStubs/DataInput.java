
package ke.co.coop.NotifyStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify}notifyInput"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "notifyInput"
})
@XmlRootElement(name = "DataInput", namespace = "urn://co-opbank.co.ke/Banking/Common/Service/CommonCollections/Notify/1.0/DataIO")
public class DataInput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/notify/1.0/CommonCollections.notify", required = true)
    protected NotifyInputType notifyInput;

    /**
     * Gets the value of the notifyInput property.
     * 
     * @return
     *     possible object is
     *     {@link NotifyInputType }
     *     
     */
    public NotifyInputType getNotifyInput() {
        return notifyInput;
    }

    /**
     * Sets the value of the notifyInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotifyInputType }
     *     
     */
    public void setNotifyInput(NotifyInputType value) {
        this.notifyInput = value;
    }

}
