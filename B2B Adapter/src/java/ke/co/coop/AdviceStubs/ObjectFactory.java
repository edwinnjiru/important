
package ke.co.coop.AdviceStubs;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ke.co.coop.AdviceStubs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Cheque_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Cheque/1.0", "Cheque");
    private final static QName _Address_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Address/1.0", "Address");
    private final static QName _Institution_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0", "Institution");
    private final static QName _BusinessDetails_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/BusinessDetails/1.0", "BusinessDetails");
    private final static QName _AdviseOutput_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "adviseOutput");
    private final static QName _TaxInvoice_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/TaxInvoice/1.0", "TaxInvoice");
    private final static QName _Email_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Email/1.0", "Email");
    private final static QName _AdviseInput_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "adviseInput");
    private final static QName _Customer_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Customer/1.0", "Customer");
    private final static QName _CredentialsUsername_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Username");
    private final static QName _CredentialsRealm_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Realm");
    private final static QName _CredentialsPassword_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Password");
    private final static QName _AdviseInputTypeServiceCode_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "ServiceCode");
    private final static QName _StatusMessageMessageDescription_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageDescription");
    private final static QName _StatusMessageMessageCode_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageCode");
    private final static QName _StatusMessageApplicationID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ApplicationID");
    private final static QName _StatusMessageMessageType_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageType");
    private final static QName _AdviseInputTypeOperationParametersDepartmentCode_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "DepartmentCode");
    private final static QName _AdviseInputTypeOperationParametersPayerId_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "PayerId");
    private final static QName _AdviseInputTypeOperationParametersOperationDate_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "OperationDate");
    private final static QName _AdviseInputTypeOperationParametersSystemCode_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "SystemCode");
    private final static QName _AdviseInputTypeOperationParametersRegionCode_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "RegionCode");
    private final static QName _AdviseInputTypeOperationParametersBankCode_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "BankCode");
    private final static QName _AdviseInputTypeOperationParametersPayerName_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "PayerName");
    private final static QName _AdviseInputTypeOperationParametersStationCode_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "StationCode");
    private final static QName _AdviseInputTypeOperationParametersBankManagerId_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "BankManagerId");
    private final static QName _AdviseInputTypeOperationParametersChequesAmount_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "ChequesAmount");
    private final static QName _AdviseInputTypeOperationParametersBankTellerId_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "BankTellerId");
    private final static QName _AdviseInputTypeOperationParametersFullName_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "FullName");
    private final static QName _AdviseInputTypeOperationParametersBankTellerName_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "BankTellerName");
    private final static QName _AdviseInputTypeOperationParametersBankManagerName_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "BankManagerName");
    private final static QName _AdviseInputTypeOperationParametersBranchCode_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "BranchCode");
    private final static QName _AdviseInputTypeOperationParametersCustomerPIN_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "CustomerPIN");
    private final static QName _AdviseInputTypeOperationParametersCashAmount_QNAME = new QName("urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", "CashAmount");
    private final static QName _HeaderReplyStatusDescription_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusDescription");
    private final static QName _HeaderReplyStatusDescriptionKey_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusDescriptionKey");
    private final static QName _HeaderReplyStatusCode_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusCode");
    private final static QName _HeaderReplyMessageID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageID");
    private final static QName _HeaderReplyElapsedTime_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ElapsedTime");
    private final static QName _HeaderReplyCorrelationID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "CorrelationID");
    private final static QName _HeaderRequestCreationTimestamp_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "CreationTimestamp");
    private final static QName _HeaderRequestReplyTO_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ReplyTO");
    private final static QName _HeaderRequestFaultTO_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "FaultTO");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ke.co.coop.AdviceStubs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link HeaderReply }
     * 
     */
    public HeaderReply createHeaderReply() {
        return new HeaderReply();
    }

    /**
     * Create an instance of {@link EmailType }
     * 
     */
    public EmailType createEmailType() {
        return new EmailType();
    }

    /**
     * Create an instance of {@link AdviseInputType }
     * 
     */
    public AdviseInputType createAdviseInputType() {
        return new AdviseInputType();
    }

    /**
     * Create an instance of {@link HeaderRequest }
     * 
     */
    public HeaderRequest createHeaderRequest() {
        return new HeaderRequest();
    }

    /**
     * Create an instance of {@link Credentials }
     * 
     */
    public Credentials createCredentials() {
        return new Credentials();
    }

    /**
     * Create an instance of {@link HeaderReply.StatusMessages }
     * 
     */
    public HeaderReply.StatusMessages createHeaderReplyStatusMessages() {
        return new HeaderReply.StatusMessages();
    }

    /**
     * Create an instance of {@link StatusMessage }
     * 
     */
    public StatusMessage createStatusMessage() {
        return new StatusMessage();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link ChequeType }
     * 
     */
    public ChequeType createChequeType() {
        return new ChequeType();
    }

    /**
     * Create an instance of {@link DataInput }
     * 
     */
    public DataInput createDataInput() {
        return new DataInput();
    }

    /**
     * Create an instance of {@link DataOutput }
     * 
     */
    public DataOutput createDataOutput() {
        return new DataOutput();
    }

    /**
     * Create an instance of {@link AdviseOutputType }
     * 
     */
    public AdviseOutputType createAdviseOutputType() {
        return new AdviseOutputType();
    }

    /**
     * Create an instance of {@link InstitutionType }
     * 
     */
    public InstitutionType createInstitutionType() {
        return new InstitutionType();
    }

    /**
     * Create an instance of {@link TaxInvoiceType }
     * 
     */
    public TaxInvoiceType createTaxInvoiceType() {
        return new TaxInvoiceType();
    }

    /**
     * Create an instance of {@link BusinessDetailsType }
     * 
     */
    public BusinessDetailsType createBusinessDetailsType() {
        return new BusinessDetailsType();
    }

    /**
     * Create an instance of {@link CustomerType }
     * 
     */
    public CustomerType createCustomerType() {
        return new CustomerType();
    }

    /**
     * Create an instance of {@link EmailType.Attachments }
     * 
     */
    public EmailType.Attachments createEmailTypeAttachments() {
        return new EmailType.Attachments();
    }

    /**
     * Create an instance of {@link AdviseInputType.OperationParameters }
     * 
     */
    public AdviseInputType.OperationParameters createAdviseInputTypeOperationParameters() {
        return new AdviseInputType.OperationParameters();
    }

    /**
     * Create an instance of {@link AdviseInputType.Cheque }
     * 
     */
    public AdviseInputType.Cheque createAdviseInputTypeCheque() {
        return new AdviseInputType.Cheque();
    }

    /**
     * Create an instance of {@link AdviseInputType.PaymentDetails }
     * 
     */
    public AdviseInputType.PaymentDetails createAdviseInputTypePaymentDetails() {
        return new AdviseInputType.PaymentDetails();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChequeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Cheque/1.0", name = "Cheque")
    public JAXBElement<ChequeType> createCheque(ChequeType value) {
        return new JAXBElement<ChequeType>(_Cheque_QNAME, ChequeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddressType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Address/1.0", name = "Address")
    public JAXBElement<AddressType> createAddress(AddressType value) {
        return new JAXBElement<AddressType>(_Address_QNAME, AddressType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0", name = "Institution")
    public JAXBElement<InstitutionType> createInstitution(InstitutionType value) {
        return new JAXBElement<InstitutionType>(_Institution_QNAME, InstitutionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessDetailsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/BusinessDetails/1.0", name = "BusinessDetails")
    public JAXBElement<BusinessDetailsType> createBusinessDetails(BusinessDetailsType value) {
        return new JAXBElement<BusinessDetailsType>(_BusinessDetails_QNAME, BusinessDetailsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdviseOutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "adviseOutput")
    public JAXBElement<AdviseOutputType> createAdviseOutput(AdviseOutputType value) {
        return new JAXBElement<AdviseOutputType>(_AdviseOutput_QNAME, AdviseOutputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxInvoiceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/TaxInvoice/1.0", name = "TaxInvoice")
    public JAXBElement<TaxInvoiceType> createTaxInvoice(TaxInvoiceType value) {
        return new JAXBElement<TaxInvoiceType>(_TaxInvoice_QNAME, TaxInvoiceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Email/1.0", name = "Email")
    public JAXBElement<EmailType> createEmail(EmailType value) {
        return new JAXBElement<EmailType>(_Email_QNAME, EmailType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdviseInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "adviseInput")
    public JAXBElement<AdviseInputType> createAdviseInput(AdviseInputType value) {
        return new JAXBElement<AdviseInputType>(_AdviseInput_QNAME, AdviseInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Customer/1.0", name = "Customer")
    public JAXBElement<CustomerType> createCustomer(CustomerType value) {
        return new JAXBElement<CustomerType>(_Customer_QNAME, CustomerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Username", scope = Credentials.class)
    public JAXBElement<String> createCredentialsUsername(String value) {
        return new JAXBElement<String>(_CredentialsUsername_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Realm", scope = Credentials.class)
    public JAXBElement<String> createCredentialsRealm(String value) {
        return new JAXBElement<String>(_CredentialsRealm_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Password", scope = Credentials.class)
    public JAXBElement<String> createCredentialsPassword(String value) {
        return new JAXBElement<String>(_CredentialsPassword_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "ServiceCode", scope = AdviseInputType.class)
    public JAXBElement<String> createAdviseInputTypeServiceCode(String value) {
        return new JAXBElement<String>(_AdviseInputTypeServiceCode_QNAME, String.class, AdviseInputType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageDescription", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageDescription(String value) {
        return new JAXBElement<String>(_StatusMessageMessageDescription_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageCode", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageCode(String value) {
        return new JAXBElement<String>(_StatusMessageMessageCode_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ApplicationID", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageApplicationID(String value) {
        return new JAXBElement<String>(_StatusMessageApplicationID_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageType", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageType(String value) {
        return new JAXBElement<String>(_StatusMessageMessageType_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "DepartmentCode", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersDepartmentCode(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersDepartmentCode_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "PayerId", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersPayerId(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersPayerId_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "OperationDate", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<XMLGregorianCalendar> createAdviseInputTypeOperationParametersOperationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AdviseInputTypeOperationParametersOperationDate_QNAME, XMLGregorianCalendar.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "SystemCode", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersSystemCode(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersSystemCode_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "RegionCode", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersRegionCode(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersRegionCode_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "BankCode", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersBankCode(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersBankCode_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "PayerName", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersPayerName(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersPayerName_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "StationCode", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersStationCode(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersStationCode_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "BankManagerId", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersBankManagerId(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersBankManagerId_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "ChequesAmount", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<BigDecimal> createAdviseInputTypeOperationParametersChequesAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AdviseInputTypeOperationParametersChequesAmount_QNAME, BigDecimal.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "BankTellerId", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersBankTellerId(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersBankTellerId_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "FullName", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersFullName(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersFullName_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "BankTellerName", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersBankTellerName(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersBankTellerName_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "BankManagerName", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersBankManagerName(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersBankManagerName_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "BranchCode", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersBranchCode(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersBranchCode_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "CustomerPIN", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<String> createAdviseInputTypeOperationParametersCustomerPIN(String value) {
        return new JAXBElement<String>(_AdviseInputTypeOperationParametersCustomerPIN_QNAME, String.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", name = "CashAmount", scope = AdviseInputType.OperationParameters.class)
    public JAXBElement<BigDecimal> createAdviseInputTypeOperationParametersCashAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AdviseInputTypeOperationParametersCashAmount_QNAME, BigDecimal.class, AdviseInputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusDescription", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusDescription(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusDescription_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusDescriptionKey", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusDescriptionKey(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusDescriptionKey_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusCode", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusCode(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusCode_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageID", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyMessageID(String value) {
        return new JAXBElement<String>(_HeaderReplyMessageID_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ElapsedTime", scope = HeaderReply.class)
    public JAXBElement<Long> createHeaderReplyElapsedTime(Long value) {
        return new JAXBElement<Long>(_HeaderReplyElapsedTime_QNAME, Long.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CorrelationID", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyCorrelationID(String value) {
        return new JAXBElement<String>(_HeaderReplyCorrelationID_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CreationTimestamp", scope = HeaderRequest.class)
    public JAXBElement<XMLGregorianCalendar> createHeaderRequestCreationTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_HeaderRequestCreationTimestamp_QNAME, XMLGregorianCalendar.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ReplyTO", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestReplyTO(String value) {
        return new JAXBElement<String>(_HeaderRequestReplyTO_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "FaultTO", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestFaultTO(String value) {
        return new JAXBElement<String>(_HeaderRequestFaultTO_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CorrelationID", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestCorrelationID(String value) {
        return new JAXBElement<String>(_HeaderReplyCorrelationID_QNAME, String.class, HeaderRequest.class, value);
    }

}
