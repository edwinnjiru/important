
package ke.co.coop.AdviceStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for adviseOutputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="adviseOutputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/TaxInvoice/1.0}TaxInvoice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adviseOutputType", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", propOrder = {
    "taxInvoice"
})
public class AdviseOutputType {

    @XmlElement(name = "TaxInvoice", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/TaxInvoice/1.0")
    protected TaxInvoiceType taxInvoice;

    /**
     * Gets the value of the taxInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link TaxInvoiceType }
     *     
     */
    public TaxInvoiceType getTaxInvoice() {
        return taxInvoice;
    }

    /**
     * Sets the value of the taxInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxInvoiceType }
     *     
     */
    public void setTaxInvoice(TaxInvoiceType value) {
        this.taxInvoice = value;
    }

}
