
package ke.co.coop.AdviceStubs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TaxInvoiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaxInvoiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaxPaymentMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxPaymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxInvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentAdviceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PaymentReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalTaxAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxInvoiceReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CollectionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxComponent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxHead" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AmountPerTax" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TaxPeriod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxInvoiceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxInvoiceType", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/TaxInvoice/1.0", propOrder = {
    "taxPaymentMode",
    "taxPaymentType",
    "taxInvoiceNumber",
    "paymentCode",
    "paymentAdviceDate",
    "paymentReference",
    "totalTaxAmount",
    "taxInvoiceReference",
    "collectionDate",
    "currency",
    "taxCode",
    "taxComponent",
    "taxHead",
    "amountPerTax",
    "taxPeriod",
    "taxInvoiceDate"
})
public class TaxInvoiceType {

    @XmlElement(name = "TaxPaymentMode")
    protected String taxPaymentMode;
    @XmlElement(name = "TaxPaymentType")
    protected String taxPaymentType;
    @XmlElement(name = "TaxInvoiceNumber")
    protected String taxInvoiceNumber;
    @XmlElement(name = "PaymentCode")
    protected String paymentCode;
    @XmlElement(name = "PaymentAdviceDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paymentAdviceDate;
    @XmlElement(name = "PaymentReference")
    protected String paymentReference;
    @XmlElement(name = "TotalTaxAmount")
    protected BigDecimal totalTaxAmount;
    @XmlElement(name = "TaxInvoiceReference")
    protected String taxInvoiceReference;
    @XmlElement(name = "CollectionDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar collectionDate;
    @XmlElement(name = "Currency")
    protected String currency;
    @XmlElement(name = "TaxCode")
    protected String taxCode;
    @XmlElement(name = "TaxComponent")
    protected String taxComponent;
    @XmlElement(name = "TaxHead")
    protected String taxHead;
    @XmlElement(name = "AmountPerTax")
    protected BigDecimal amountPerTax;
    @XmlElement(name = "TaxPeriod")
    protected String taxPeriod;
    @XmlElement(name = "TaxInvoiceDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar taxInvoiceDate;

    /**
     * Gets the value of the taxPaymentMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxPaymentMode() {
        return taxPaymentMode;
    }

    /**
     * Sets the value of the taxPaymentMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxPaymentMode(String value) {
        this.taxPaymentMode = value;
    }

    /**
     * Gets the value of the taxPaymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxPaymentType() {
        return taxPaymentType;
    }

    /**
     * Sets the value of the taxPaymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxPaymentType(String value) {
        this.taxPaymentType = value;
    }

    /**
     * Gets the value of the taxInvoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxInvoiceNumber() {
        return taxInvoiceNumber;
    }

    /**
     * Sets the value of the taxInvoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxInvoiceNumber(String value) {
        this.taxInvoiceNumber = value;
    }

    /**
     * Gets the value of the paymentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentCode() {
        return paymentCode;
    }

    /**
     * Sets the value of the paymentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentCode(String value) {
        this.paymentCode = value;
    }

    /**
     * Gets the value of the paymentAdviceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentAdviceDate() {
        return paymentAdviceDate;
    }

    /**
     * Sets the value of the paymentAdviceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentAdviceDate(XMLGregorianCalendar value) {
        this.paymentAdviceDate = value;
    }

    /**
     * Gets the value of the paymentReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * Sets the value of the paymentReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentReference(String value) {
        this.paymentReference = value;
    }

    /**
     * Gets the value of the totalTaxAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalTaxAmount() {
        return totalTaxAmount;
    }

    /**
     * Sets the value of the totalTaxAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalTaxAmount(BigDecimal value) {
        this.totalTaxAmount = value;
    }

    /**
     * Gets the value of the taxInvoiceReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxInvoiceReference() {
        return taxInvoiceReference;
    }

    /**
     * Sets the value of the taxInvoiceReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxInvoiceReference(String value) {
        this.taxInvoiceReference = value;
    }

    /**
     * Gets the value of the collectionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCollectionDate() {
        return collectionDate;
    }

    /**
     * Sets the value of the collectionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCollectionDate(XMLGregorianCalendar value) {
        this.collectionDate = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the taxCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxCode() {
        return taxCode;
    }

    /**
     * Sets the value of the taxCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxCode(String value) {
        this.taxCode = value;
    }

    /**
     * Gets the value of the taxComponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxComponent() {
        return taxComponent;
    }

    /**
     * Sets the value of the taxComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxComponent(String value) {
        this.taxComponent = value;
    }

    /**
     * Gets the value of the taxHead property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxHead() {
        return taxHead;
    }

    /**
     * Sets the value of the taxHead property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxHead(String value) {
        this.taxHead = value;
    }

    /**
     * Gets the value of the amountPerTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountPerTax() {
        return amountPerTax;
    }

    /**
     * Sets the value of the amountPerTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountPerTax(BigDecimal value) {
        this.amountPerTax = value;
    }

    /**
     * Gets the value of the taxPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxPeriod() {
        return taxPeriod;
    }

    /**
     * Sets the value of the taxPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxPeriod(String value) {
        this.taxPeriod = value;
    }

    /**
     * Gets the value of the taxInvoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaxInvoiceDate() {
        return taxInvoiceDate;
    }

    /**
     * Sets the value of the taxInvoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaxInvoiceDate(XMLGregorianCalendar value) {
        this.taxInvoiceDate = value;
    }

}
