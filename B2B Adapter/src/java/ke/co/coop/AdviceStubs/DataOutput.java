
package ke.co.coop.AdviceStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise}adviseOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "adviseOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Common/Service/CommonCollections/Advise/1.0/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise")
    protected AdviseOutputType adviseOutput;

    /**
     * Gets the value of the adviseOutput property.
     * 
     * @return
     *     possible object is
     *     {@link AdviseOutputType }
     *     
     */
    public AdviseOutputType getAdviseOutput() {
        return adviseOutput;
    }

    /**
     * Sets the value of the adviseOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviseOutputType }
     *     
     */
    public void setAdviseOutput(AdviseOutputType value) {
        this.adviseOutput = value;
    }

}
