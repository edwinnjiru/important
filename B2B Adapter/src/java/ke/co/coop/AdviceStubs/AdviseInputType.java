
package ke.co.coop.AdviceStubs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for adviseInputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="adviseInputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperationParameters" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OperationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="SystemCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BankTellerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BankTellerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PayerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PayerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CustomerPIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BankManagerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BankManagerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BankCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RegionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="StationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CashAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="ChequesAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0}Institution" minOccurs="0"/>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/TaxInvoice/1.0}TaxInvoice" minOccurs="0"/>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Customer/1.0}Customer" minOccurs="0"/>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Email/1.0}Email" minOccurs="0"/>
 *         &lt;element name="Cheque" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Cheque/1.0}Cheque" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PaymentDetails" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/TaxInvoice/1.0}TaxInvoice" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adviseInputType", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", propOrder = {
    "operationParameters",
    "serviceCode",
    "institution",
    "taxInvoice",
    "customer",
    "email",
    "cheque",
    "paymentDetails"
})
public class AdviseInputType {

    @XmlElement(name = "OperationParameters")
    protected AdviseInputType.OperationParameters operationParameters;
    @XmlElementRef(name = "ServiceCode", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceCode;
    @XmlElement(name = "Institution", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0")
    protected InstitutionType institution;
    @XmlElement(name = "TaxInvoice", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/TaxInvoice/1.0")
    protected TaxInvoiceType taxInvoice;
    @XmlElement(name = "Customer", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Customer/1.0")
    protected CustomerType customer;
    @XmlElement(name = "Email", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Email/1.0")
    protected EmailType email;
    @XmlElement(name = "Cheque")
    protected AdviseInputType.Cheque cheque;
    @XmlElement(name = "PaymentDetails")
    protected AdviseInputType.PaymentDetails paymentDetails;

    /**
     * Gets the value of the operationParameters property.
     * 
     * @return
     *     possible object is
     *     {@link AdviseInputType.OperationParameters }
     *     
     */
    public AdviseInputType.OperationParameters getOperationParameters() {
        return operationParameters;
    }

    /**
     * Sets the value of the operationParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviseInputType.OperationParameters }
     *     
     */
    public void setOperationParameters(AdviseInputType.OperationParameters value) {
        this.operationParameters = value;
    }

    /**
     * Gets the value of the serviceCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceCode() {
        return serviceCode;
    }

    /**
     * Sets the value of the serviceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceCode(JAXBElement<String> value) {
        this.serviceCode = value;
    }

    /**
     * Gets the value of the institution property.
     * 
     * @return
     *     possible object is
     *     {@link InstitutionType }
     *     
     */
    public InstitutionType getInstitution() {
        return institution;
    }

    /**
     * Sets the value of the institution property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstitutionType }
     *     
     */
    public void setInstitution(InstitutionType value) {
        this.institution = value;
    }

    /**
     * Gets the value of the taxInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link TaxInvoiceType }
     *     
     */
    public TaxInvoiceType getTaxInvoice() {
        return taxInvoice;
    }

    /**
     * Sets the value of the taxInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxInvoiceType }
     *     
     */
    public void setTaxInvoice(TaxInvoiceType value) {
        this.taxInvoice = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerType }
     *     
     */
    public CustomerType getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerType }
     *     
     */
    public void setCustomer(CustomerType value) {
        this.customer = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link EmailType }
     *     
     */
    public EmailType getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailType }
     *     
     */
    public void setEmail(EmailType value) {
        this.email = value;
    }

    /**
     * Gets the value of the cheque property.
     * 
     * @return
     *     possible object is
     *     {@link AdviseInputType.Cheque }
     *     
     */
    public AdviseInputType.Cheque getCheque() {
        return cheque;
    }

    /**
     * Sets the value of the cheque property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviseInputType.Cheque }
     *     
     */
    public void setCheque(AdviseInputType.Cheque value) {
        this.cheque = value;
    }

    /**
     * Gets the value of the paymentDetails property.
     * 
     * @return
     *     possible object is
     *     {@link AdviseInputType.PaymentDetails }
     *     
     */
    public AdviseInputType.PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    /**
     * Sets the value of the paymentDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviseInputType.PaymentDetails }
     *     
     */
    public void setPaymentDetails(AdviseInputType.PaymentDetails value) {
        this.paymentDetails = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Cheque/1.0}Cheque" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cheque"
    })
    public static class Cheque {

        @XmlElement(name = "Cheque", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Cheque/1.0")
        protected List<ChequeType> cheque;

        /**
         * Gets the value of the cheque property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the cheque property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCheque().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChequeType }
         * 
         * 
         */
        public List<ChequeType> getCheque() {
            if (cheque == null) {
                cheque = new ArrayList<ChequeType>();
            }
            return this.cheque;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OperationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="SystemCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BankTellerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BankTellerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PayerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PayerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CustomerPIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BankManagerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BankManagerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BankCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RegionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="StationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CashAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="ChequesAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "operationDate",
        "systemCode",
        "branchCode",
        "bankTellerId",
        "bankTellerName",
        "payerId",
        "payerName",
        "customerPIN",
        "fullName",
        "bankManagerId",
        "bankManagerName",
        "bankCode",
        "departmentCode",
        "regionCode",
        "stationCode",
        "cashAmount",
        "chequesAmount"
    })
    public static class OperationParameters {

        @XmlElementRef(name = "OperationDate", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> operationDate;
        @XmlElementRef(name = "SystemCode", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> systemCode;
        @XmlElementRef(name = "BranchCode", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> branchCode;
        @XmlElementRef(name = "BankTellerId", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> bankTellerId;
        @XmlElementRef(name = "BankTellerName", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> bankTellerName;
        @XmlElementRef(name = "PayerId", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> payerId;
        @XmlElementRef(name = "PayerName", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> payerName;
        @XmlElementRef(name = "CustomerPIN", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> customerPIN;
        @XmlElementRef(name = "FullName", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> fullName;
        @XmlElementRef(name = "BankManagerId", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> bankManagerId;
        @XmlElementRef(name = "BankManagerName", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> bankManagerName;
        @XmlElementRef(name = "BankCode", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> bankCode;
        @XmlElementRef(name = "DepartmentCode", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> departmentCode;
        @XmlElementRef(name = "RegionCode", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> regionCode;
        @XmlElementRef(name = "StationCode", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<String> stationCode;
        @XmlElementRef(name = "CashAmount", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<BigDecimal> cashAmount;
        @XmlElementRef(name = "ChequesAmount", namespace = "urn://co-opbank.co.ke/Banking/Common/CommonDataModel/CommonCollections/advise/1.0/CommonCollections.advise", type = JAXBElement.class, required = false)
        protected JAXBElement<BigDecimal> chequesAmount;

        /**
         * Gets the value of the operationDate property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getOperationDate() {
            return operationDate;
        }

        /**
         * Sets the value of the operationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setOperationDate(JAXBElement<XMLGregorianCalendar> value) {
            this.operationDate = value;
        }

        /**
         * Gets the value of the systemCode property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSystemCode() {
            return systemCode;
        }

        /**
         * Sets the value of the systemCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSystemCode(JAXBElement<String> value) {
            this.systemCode = value;
        }

        /**
         * Gets the value of the branchCode property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getBranchCode() {
            return branchCode;
        }

        /**
         * Sets the value of the branchCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setBranchCode(JAXBElement<String> value) {
            this.branchCode = value;
        }

        /**
         * Gets the value of the bankTellerId property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getBankTellerId() {
            return bankTellerId;
        }

        /**
         * Sets the value of the bankTellerId property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setBankTellerId(JAXBElement<String> value) {
            this.bankTellerId = value;
        }

        /**
         * Gets the value of the bankTellerName property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getBankTellerName() {
            return bankTellerName;
        }

        /**
         * Sets the value of the bankTellerName property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setBankTellerName(JAXBElement<String> value) {
            this.bankTellerName = value;
        }

        /**
         * Gets the value of the payerId property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPayerId() {
            return payerId;
        }

        /**
         * Sets the value of the payerId property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPayerId(JAXBElement<String> value) {
            this.payerId = value;
        }

        /**
         * Gets the value of the payerName property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPayerName() {
            return payerName;
        }

        /**
         * Sets the value of the payerName property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPayerName(JAXBElement<String> value) {
            this.payerName = value;
        }

        /**
         * Gets the value of the customerPIN property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getCustomerPIN() {
            return customerPIN;
        }

        /**
         * Sets the value of the customerPIN property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setCustomerPIN(JAXBElement<String> value) {
            this.customerPIN = value;
        }

        /**
         * Gets the value of the fullName property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getFullName() {
            return fullName;
        }

        /**
         * Sets the value of the fullName property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setFullName(JAXBElement<String> value) {
            this.fullName = value;
        }

        /**
         * Gets the value of the bankManagerId property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getBankManagerId() {
            return bankManagerId;
        }

        /**
         * Sets the value of the bankManagerId property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setBankManagerId(JAXBElement<String> value) {
            this.bankManagerId = value;
        }

        /**
         * Gets the value of the bankManagerName property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getBankManagerName() {
            return bankManagerName;
        }

        /**
         * Sets the value of the bankManagerName property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setBankManagerName(JAXBElement<String> value) {
            this.bankManagerName = value;
        }

        /**
         * Gets the value of the bankCode property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getBankCode() {
            return bankCode;
        }

        /**
         * Sets the value of the bankCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setBankCode(JAXBElement<String> value) {
            this.bankCode = value;
        }

        /**
         * Gets the value of the departmentCode property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDepartmentCode() {
            return departmentCode;
        }

        /**
         * Sets the value of the departmentCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDepartmentCode(JAXBElement<String> value) {
            this.departmentCode = value;
        }

        /**
         * Gets the value of the regionCode property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRegionCode() {
            return regionCode;
        }

        /**
         * Sets the value of the regionCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRegionCode(JAXBElement<String> value) {
            this.regionCode = value;
        }

        /**
         * Gets the value of the stationCode property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getStationCode() {
            return stationCode;
        }

        /**
         * Sets the value of the stationCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setStationCode(JAXBElement<String> value) {
            this.stationCode = value;
        }

        /**
         * Gets the value of the cashAmount property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
         *     
         */
        public JAXBElement<BigDecimal> getCashAmount() {
            return cashAmount;
        }

        /**
         * Sets the value of the cashAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
         *     
         */
        public void setCashAmount(JAXBElement<BigDecimal> value) {
            this.cashAmount = value;
        }

        /**
         * Gets the value of the chequesAmount property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
         *     
         */
        public JAXBElement<BigDecimal> getChequesAmount() {
            return chequesAmount;
        }

        /**
         * Sets the value of the chequesAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
         *     
         */
        public void setChequesAmount(JAXBElement<BigDecimal> value) {
            this.chequesAmount = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/TaxInvoice/1.0}TaxInvoice" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "taxInvoice"
    })
    public static class PaymentDetails {

        @XmlElement(name = "TaxInvoice", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/TaxInvoice/1.0")
        protected List<TaxInvoiceType> taxInvoice;

        /**
         * Gets the value of the taxInvoice property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the taxInvoice property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTaxInvoice().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TaxInvoiceType }
         * 
         * 
         */
        public List<TaxInvoiceType> getTaxInvoice() {
            if (taxInvoice == null) {
                taxInvoice = new ArrayList<TaxInvoiceType>();
            }
            return this.taxInvoice;
        }

    }

}
