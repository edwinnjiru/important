
package ke.co.coop.ValidateStubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for validateInputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="validateInputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperationParameters" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Field1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0}Institution" minOccurs="0"/>
 *         &lt;element name="ServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "validateInputType", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate", propOrder = {
    "operationParameters",
    "institution",
    "serviceCode"
})
public class ValidateInputType {

    @XmlElement(name = "OperationParameters")
    protected ValidateInputType.OperationParameters operationParameters;
    @XmlElement(name = "Institution", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0")
    protected InstitutionType institution;
    @XmlElementRef(name = "ServiceCode", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceCode;

    /**
     * Gets the value of the operationParameters property.
     * 
     * @return
     *     possible object is
     *     {@link ValidateInputType.OperationParameters }
     *     
     */
    public ValidateInputType.OperationParameters getOperationParameters() {
        return operationParameters;
    }

    /**
     * Sets the value of the operationParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateInputType.OperationParameters }
     *     
     */
    public void setOperationParameters(ValidateInputType.OperationParameters value) {
        this.operationParameters = value;
    }

    /**
     * Gets the value of the institution property.
     * 
     * @return
     *     possible object is
     *     {@link InstitutionType }
     *     
     */
    public InstitutionType getInstitution() {
        return institution;
    }

    /**
     * Sets the value of the institution property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstitutionType }
     *     
     */
    public void setInstitution(InstitutionType value) {
        this.institution = value;
    }

    /**
     * Gets the value of the serviceCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceCode() {
        return serviceCode;
    }

    /**
     * Sets the value of the serviceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceCode(JAXBElement<String> value) {
        this.serviceCode = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Field1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "field1",
        "field2",
        "field3",
        "field4",
        "field5",
        "field6",
        "field7",
        "field8",
        "field9",
        "field10"
    })
    public static class OperationParameters {

        @XmlElement(name = "Field1", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field1;
        @XmlElement(name = "Field2", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field2;
        @XmlElement(name = "Field3", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field3;
        @XmlElement(name = "Field4", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field4;
        @XmlElement(name = "Field5", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field5;
        @XmlElement(name = "Field6", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field6;
        @XmlElement(name = "Field7", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field7;
        @XmlElement(name = "Field8", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field8;
        @XmlElement(name = "Field9", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field9;
        @XmlElement(name = "Field10", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field10;

        /**
         * Gets the value of the field1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField1() {
            return field1;
        }

        /**
         * Sets the value of the field1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField1(String value) {
            this.field1 = value;
        }

        /**
         * Gets the value of the field2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField2() {
            return field2;
        }

        /**
         * Sets the value of the field2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField2(String value) {
            this.field2 = value;
        }

        /**
         * Gets the value of the field3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField3() {
            return field3;
        }

        /**
         * Sets the value of the field3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField3(String value) {
            this.field3 = value;
        }

        /**
         * Gets the value of the field4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField4() {
            return field4;
        }

        /**
         * Sets the value of the field4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField4(String value) {
            this.field4 = value;
        }

        /**
         * Gets the value of the field5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField5() {
            return field5;
        }

        /**
         * Sets the value of the field5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField5(String value) {
            this.field5 = value;
        }

        /**
         * Gets the value of the field6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField6() {
            return field6;
        }

        /**
         * Sets the value of the field6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField6(String value) {
            this.field6 = value;
        }

        /**
         * Gets the value of the field7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField7() {
            return field7;
        }

        /**
         * Sets the value of the field7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField7(String value) {
            this.field7 = value;
        }

        /**
         * Gets the value of the field8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField8() {
            return field8;
        }

        /**
         * Sets the value of the field8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField8(String value) {
            this.field8 = value;
        }

        /**
         * Gets the value of the field9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField9() {
            return field9;
        }

        /**
         * Sets the value of the field9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField9(String value) {
            this.field9 = value;
        }

        /**
         * Gets the value of the field10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField10() {
            return field10;
        }

        /**
         * Sets the value of the field10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField10(String value) {
            this.field10 = value;
        }

    }

}
