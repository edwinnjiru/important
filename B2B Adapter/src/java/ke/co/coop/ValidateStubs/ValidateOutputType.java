
package ke.co.coop.ValidateStubs;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for validateOutputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="validateOutputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0}Institution" minOccurs="0"/>
 *         &lt;element name="OperationParameters" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Field1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Field20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FieldGuide" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="InReceipt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="InNarration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="InReceiptOrder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="InNarrationOrder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "validateOutputType", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate", propOrder = {
    "institution",
    "operationParameters",
    "fieldGuide"
})
public class ValidateOutputType {

    @XmlElement(name = "Institution", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Institution/1.0")
    protected InstitutionType institution;
    @XmlElement(name = "OperationParameters")
    protected ValidateOutputType.OperationParameters operationParameters;
    @XmlElement(name = "FieldGuide")
    protected List<ValidateOutputType.FieldGuide> fieldGuide;

    /**
     * Gets the value of the institution property.
     * 
     * @return
     *     possible object is
     *     {@link InstitutionType }
     *     
     */
    public InstitutionType getInstitution() {
        return institution;
    }

    /**
     * Sets the value of the institution property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstitutionType }
     *     
     */
    public void setInstitution(InstitutionType value) {
        this.institution = value;
    }

    /**
     * Gets the value of the operationParameters property.
     * 
     * @return
     *     possible object is
     *     {@link ValidateOutputType.OperationParameters }
     *     
     */
    public ValidateOutputType.OperationParameters getOperationParameters() {
        return operationParameters;
    }

    /**
     * Sets the value of the operationParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateOutputType.OperationParameters }
     *     
     */
    public void setOperationParameters(ValidateOutputType.OperationParameters value) {
        this.operationParameters = value;
    }

    /**
     * Gets the value of the fieldGuide property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldGuide property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldGuide().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValidateOutputType.FieldGuide }
     * 
     * 
     */
    public List<ValidateOutputType.FieldGuide> getFieldGuide() {
        if (fieldGuide == null) {
            fieldGuide = new ArrayList<ValidateOutputType.FieldGuide>();
        }
        return this.fieldGuide;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="InReceipt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="InNarration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="InReceiptOrder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="InNarrationOrder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "id",
        "code",
        "name",
        "length",
        "type",
        "description",
        "inReceipt",
        "inNarration",
        "inReceiptOrder",
        "inNarrationOrder"
    })
    public static class FieldGuide {

        @XmlElement(name = "Id", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String id;
        @XmlElement(name = "Code", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String code;
        @XmlElement(name = "Name", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String name;
        @XmlElement(name = "Length", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected BigInteger length;
        @XmlElement(name = "Type", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String type;
        @XmlElement(name = "Description", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String description;
        @XmlElement(name = "InReceipt", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String inReceipt;
        @XmlElement(name = "InNarration", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String inNarration;
        @XmlElement(name = "InReceiptOrder", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String inReceiptOrder;
        @XmlElement(name = "InNarrationOrder", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String inNarrationOrder;

        /**
         * Gets the value of the id property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setId(String value) {
            this.id = value;
        }

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the length property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getLength() {
            return length;
        }

        /**
         * Sets the value of the length property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setLength(BigInteger value) {
            this.length = value;
        }

        /**
         * Gets the value of the type property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Sets the value of the type property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Gets the value of the inReceipt property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInReceipt() {
            return inReceipt;
        }

        /**
         * Sets the value of the inReceipt property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInReceipt(String value) {
            this.inReceipt = value;
        }

        /**
         * Gets the value of the inNarration property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInNarration() {
            return inNarration;
        }

        /**
         * Sets the value of the inNarration property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInNarration(String value) {
            this.inNarration = value;
        }

        /**
         * Gets the value of the inReceiptOrder property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInReceiptOrder() {
            return inReceiptOrder;
        }

        /**
         * Sets the value of the inReceiptOrder property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInReceiptOrder(String value) {
            this.inReceiptOrder = value;
        }

        /**
         * Gets the value of the inNarrationOrder property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInNarrationOrder() {
            return inNarrationOrder;
        }

        /**
         * Sets the value of the inNarrationOrder property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInNarrationOrder(String value) {
            this.inNarrationOrder = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Field1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Field20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "field1",
        "field2",
        "field3",
        "field4",
        "field5",
        "field6",
        "field7",
        "field8",
        "field9",
        "field10",
        "field11",
        "field12",
        "field13",
        "field14",
        "field15",
        "field16",
        "field17",
        "field18",
        "field19",
        "field20"
    })
    public static class OperationParameters {

        @XmlElement(name = "Field1", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field1;
        @XmlElement(name = "Field2", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field2;
        @XmlElement(name = "Field3", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field3;
        @XmlElement(name = "Field4", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field4;
        @XmlElement(name = "Field5", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field5;
        @XmlElement(name = "Field6", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field6;
        @XmlElement(name = "Field7", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field7;
        @XmlElement(name = "Field8", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field8;
        @XmlElement(name = "Field9", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field9;
        @XmlElement(name = "Field10", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field10;
        @XmlElement(name = "Field11", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field11;
        @XmlElement(name = "Field12", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field12;
        @XmlElement(name = "Field13", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field13;
        @XmlElement(name = "Field14", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field14;
        @XmlElement(name = "Field15", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field15;
        @XmlElement(name = "Field16", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field16;
        @XmlElement(name = "Field17", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field17;
        @XmlElement(name = "Field18", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field18;
        @XmlElement(name = "Field19", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field19;
        @XmlElement(name = "Field20", namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
        protected String field20;

        /**
         * Gets the value of the field1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField1() {
            return field1;
        }

        /**
         * Sets the value of the field1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField1(String value) {
            this.field1 = value;
        }

        /**
         * Gets the value of the field2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField2() {
            return field2;
        }

        /**
         * Sets the value of the field2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField2(String value) {
            this.field2 = value;
        }

        /**
         * Gets the value of the field3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField3() {
            return field3;
        }

        /**
         * Sets the value of the field3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField3(String value) {
            this.field3 = value;
        }

        /**
         * Gets the value of the field4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField4() {
            return field4;
        }

        /**
         * Sets the value of the field4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField4(String value) {
            this.field4 = value;
        }

        /**
         * Gets the value of the field5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField5() {
            return field5;
        }

        /**
         * Sets the value of the field5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField5(String value) {
            this.field5 = value;
        }

        /**
         * Gets the value of the field6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField6() {
            return field6;
        }

        /**
         * Sets the value of the field6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField6(String value) {
            this.field6 = value;
        }

        /**
         * Gets the value of the field7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField7() {
            return field7;
        }

        /**
         * Sets the value of the field7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField7(String value) {
            this.field7 = value;
        }

        /**
         * Gets the value of the field8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField8() {
            return field8;
        }

        /**
         * Sets the value of the field8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField8(String value) {
            this.field8 = value;
        }

        /**
         * Gets the value of the field9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField9() {
            return field9;
        }

        /**
         * Sets the value of the field9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField9(String value) {
            this.field9 = value;
        }

        /**
         * Gets the value of the field10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField10() {
            return field10;
        }

        /**
         * Sets the value of the field10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField10(String value) {
            this.field10 = value;
        }

        /**
         * Gets the value of the field11 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField11() {
            return field11;
        }

        /**
         * Sets the value of the field11 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField11(String value) {
            this.field11 = value;
        }

        /**
         * Gets the value of the field12 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField12() {
            return field12;
        }

        /**
         * Sets the value of the field12 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField12(String value) {
            this.field12 = value;
        }

        /**
         * Gets the value of the field13 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField13() {
            return field13;
        }

        /**
         * Sets the value of the field13 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField13(String value) {
            this.field13 = value;
        }

        /**
         * Gets the value of the field14 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField14() {
            return field14;
        }

        /**
         * Sets the value of the field14 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField14(String value) {
            this.field14 = value;
        }

        /**
         * Gets the value of the field15 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField15() {
            return field15;
        }

        /**
         * Sets the value of the field15 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField15(String value) {
            this.field15 = value;
        }

        /**
         * Gets the value of the field16 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField16() {
            return field16;
        }

        /**
         * Sets the value of the field16 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField16(String value) {
            this.field16 = value;
        }

        /**
         * Gets the value of the field17 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField17() {
            return field17;
        }

        /**
         * Sets the value of the field17 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField17(String value) {
            this.field17 = value;
        }

        /**
         * Gets the value of the field18 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField18() {
            return field18;
        }

        /**
         * Sets the value of the field18 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField18(String value) {
            this.field18 = value;
        }

        /**
         * Gets the value of the field19 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField19() {
            return field19;
        }

        /**
         * Sets the value of the field19 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField19(String value) {
            this.field19 = value;
        }

        /**
         * Gets the value of the field20 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField20() {
            return field20;
        }

        /**
         * Sets the value of the field20 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField20(String value) {
            this.field20 = value;
        }

    }

}
