
package ke.co.coop.ValidateStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate}validateInput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validateInput"
})
@XmlRootElement(name = "DataInput", namespace = "urn://co-opbank.co.ke/Banking/Common/Service/CommonCollections/Validate/1.0/DataIO")
public class DataInput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Common/DataModel/CommonCollections/Validate/1.0/CommonCollections.validate")
    protected ValidateInputType validateInput;

    /**
     * Gets the value of the validateInput property.
     * 
     * @return
     *     possible object is
     *     {@link ValidateInputType }
     *     
     */
    public ValidateInputType getValidateInput() {
        return validateInput;
    }

    /**
     * Sets the value of the validateInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateInputType }
     *     
     */
    public void setValidateInput(ValidateInputType value) {
        this.validateInput = value;
    }

}
