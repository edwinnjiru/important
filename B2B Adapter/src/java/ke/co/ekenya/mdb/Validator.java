/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.mdb;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.soarequests.B2BNotify;

/**
 *
 * @author njiru
 */
public class Validator {

    Map<String, String> configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    String CorrelationID;

    public Validator(Map<String, String> configs, Map<String, String> xmlmap, String CorrelationID) {
        this.configs = configs;
        this.xmlmap = xmlmap;
        this.CorrelationID = CorrelationID;
    }

    public void process() {
        String biller = xmlmap.get("98").toLowerCase();
        try {
            switch (biller) {
                case "b2b_notify":
                    B2BNotify notify=new B2BNotify(configs, xmlmap, CorrelationID);
                    notify.doProcess();
                    break;
                default:
                    ESBLog lg = new ESBLog("InvalidTran", "Unhandled field98: " + xmlmap.toString());
                    lg.log();
                    break;
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("ValidationExceptions", "Error: " + sw.toString());
            el.logfile();
        }

    }

}
