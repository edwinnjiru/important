/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.mdb;

import commonj.work.Work;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Response_Codes_ISO87;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author njiru
 */
public class B2BProcessor implements Work {

    private Message message;
    Map<String, String> configs = new HashMap();

    /*JMS message containing the transaction fields*/
    /**
     * Class constructor
     *
     * @param message - JMS Object Message fetched from Request Queue
     */
    public B2BProcessor(Message message) {
        this.message = message;
        this.configs=B2B_MDB.configs;
    }

    @Override
    public void run() {
        HashMap<String, String> requestMap = new HashMap();
        try {
            if (message instanceof ObjectMessage) {
                requestMap = (HashMap) (((ObjectMessage) message).getObject());
                requestMap.put("CorrelationID", message.getJMSCorrelationID());
                ESBLog el22 = new ESBLog("RawRequests", "RequestMap to process:" + message);
                el22.log();
                
                Validator vdt=new Validator(configs, requestMap, message.getJMSCorrelationID());
                vdt.process();

            } else {
                 HashMap<String, String> messageMap = new HashMap();
                String msgStat = "FAILURE";

                messageMap.put("39", Response_Codes_ISO87.HOST_CONNECTION_DOWN);
                messageMap.put("48", "INVALID MESSAGE TYPE");
                messageMap.put("MSGSTAT", msgStat);
                messageMap.put("direction", "response");

                //Write failure response back to ESB response queue
                QueueWriter qw = new QueueWriter(B2B_MDB.configs.get("ADAPTOR_RESPONSE_QUEUE"));
                qw.sendObject(messageMap, messageMap.get("37"));

                //Log response sent back to ESB
                ESBLog el3 = new ESBLog("ReponsesToESB","Response sent back to ESB: "+ messageMap);
                el3.log();

            }

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("GeneralExceptions", "Error:" + sw.toString());
            el.log();
        }

    }

    @Override
    public void release() {
    }

    @Override
    public boolean isDaemon() {
        return false;
    }

}
