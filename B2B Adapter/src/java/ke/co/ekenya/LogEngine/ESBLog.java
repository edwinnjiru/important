package ke.co.ekenya.LogEngine;


import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SocketHandler;
import ke.co.ekenya.CommonOperations.QueueWriter;

public class ESBLog {

    private FileHandler fh = null;
    private String pathtologs = "/var/log/esblogs/B2B_Adapter/";
    
    private SocketHandler sh = null;
    private String directory = "B2B_Adapter";

    private String filename = null;
    private String msg = null;
    //private static final Logger logger = Logger.getLogger("");
    private Logger logger = null;
    public HashMap<String, String> logmap;

    public ESBLog(String filename, String msg) {
        this.filename = filename;
        this.msg = msg;
    }

    public ESBLog(String filename, HashMap map) {
        this.filename = filename;
        logmap = new HashMap(map);
        if (logmap.containsKey("RawMessage")) {
            logmap.put("RawMessage", "");
        }
        this.msg = logmap.toString();
    }

    public ESBLog(String msg) {
        this.filename = "exceptions";
        this.msg = msg;
    }
    
        public ESBLog(Exception e,String msg) {
        this.filename = "exceptions";
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString() +msg;
    }
    
    
 

    public void log() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
        String timestamp = format.format(new Date());
        String finalmessage = "###" + directory + "$$$" + filename + "###" + timestamp + "::" + msg;
        QueueWriter qw = new QueueWriter("jms/ESB_LOG_QUEUE");
        qw.send(finalmessage, "log");        
    }
    
    public String createDailyDirectory() {
        String Dailydirectory = "";
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        String daylog = format.format(new Date());
        Dailydirectory = pathtologs + daylog;
        new File(Dailydirectory).mkdir();
        return Dailydirectory + "/";
    }
    
    public void logfile() {
        logger = Logger.getLogger("");
        String daylog = anyDate("dd-MMM-yyyy");
        try {
            fh = new FileHandler(createDailyDirectory() + filename + "%g.log", 26000000, 20, true);
            fh.setFormatter(new EsbFormatter());
            logger.addHandler(fh);
            logger.setLevel(Level.FINE);
            logger.fine(msg);
            fh.close();
        } catch (Exception e) {
            //System.out.println("AT ESB Class ESBLog{ log() }: " + e.getMessage());
            e.printStackTrace();
        }

    }
    
    public String anyDate(String format) {
        String myDate = "";
        try {
            if ("".equals(format)) {
                format = "yyyy-MM-dd HH:mm:ss"; // default
            }
            LocalDateTime date = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            myDate = date.format(formatter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return myDate;
    }

}
