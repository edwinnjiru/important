/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.api;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.soarequests.B2BValidate;
import ke.co.ekenya.soarequests.B2BGetAccounts;
import ke.co.ekenya.soarequests.B2BGetParams;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author njiru
 */
public class RequestProcessor {

    String Request;
    static Map<String, String> responseMap = new HashMap();
    Map<String, String> configs = new HashMap();

    public RequestProcessor(String Request, Map<String, String> configs) {
        this.Request = Request;
        this.configs = configs;
    }

    public String process() {
        String response = "";
        Map<String, String> requestMap = new HashMap();

        try {
            JSONObject requestObject = new JSONObject(Request);
            requestMap = (new Utilities()).parseJSON(requestObject, requestMap);
            if (validatefields(requestMap)) {
                String type = requestMap.get("type").toLowerCase();
                switch (type) {
                    case "getaccounts":
                        B2BGetAccounts getAccounts = new B2BGetAccounts(configs, requestMap);
                        responseMap = getAccounts.doProcess();
                        break;
                    case "getparams":
                        B2BGetParams getParams = new B2BGetParams(configs, requestMap);
                        responseMap = getParams.doProcess();
                        break;
                    case "validate":
                        B2BValidate b2bValidate = new B2BValidate(configs, requestMap);
                        responseMap = b2bValidate.doProcess();
                        break;
                    default:
                        break;
                }

            }
            JSONObject responseObject = new JSONObject(responseMap);
            response = responseObject.toString();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions", "Error:" + sw.toString());
            el.log();
        }

        return response;
    }

    public static boolean validatefields(Map<String, String> requestMap) {
        boolean validity = true;
        try {
            if (!requestMap.containsKey("type")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Missing request type");
                validity = false;
                return validity;
            }

            if (("".equalsIgnoreCase(requestMap.get("type"))) || (!("getaccounts".equalsIgnoreCase(requestMap.get("type"))) && !("getparams".equalsIgnoreCase(requestMap.get("type"))) && !("validate".equalsIgnoreCase(requestMap.get("type"))))) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Invalid request type");
                validity = false;
                return validity;
            }

            String type = requestMap.get("type").toLowerCase();
            switch (type) {
                case "getaccounts":
                    if (!(requestMap.containsKey("accountnumber")) && !(requestMap.containsKey("businessname")) && !(requestMap.containsKey("businesscode"))) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Missing all get account parameters");
                        validity = false;
                        return validity;
                    }
                    if ("".equalsIgnoreCase(requestMap.get("accountnumber").trim()) && "".equalsIgnoreCase(requestMap.get("businessname").trim()) && "".equalsIgnoreCase(requestMap.get("businesscode").trim())) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Blank get account parameters");
                        validity = false;
                        return validity;
                    }
                    if (!(requestMap.containsKey("accountnumber"))) {
                        requestMap.put("accountnumber", "");
                    }
                    if (!(requestMap.containsKey("businessname"))) {
                        requestMap.put("businessname", "");
                    }
                    if (!(requestMap.containsKey("businesscode"))) {
                        requestMap.put("businesscode", "");
                    }
                    break;
                case "getparams":
                    if (!(requestMap.containsKey("servicecode"))) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Missing servicecode parameter");
                        validity = false;
                        return validity;
                    }
                    if ("".equalsIgnoreCase(requestMap.get("servicecode"))) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Empty servicecode parameter");
                        validity = false;
                        return validity;
                    }
                    break;
                case "validate":
                    if (!requestMap.containsKey("servicecode")) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Missing  compulsory servicecode tag");
                        validity = false;
                        return validity;
                    }
                    
                    if("".equalsIgnoreCase(requestMap.get("servicecode").trim())){
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Empty service code");
                        validity = false;
                        return validity;
                    }
                    
                    if (!(requestMap.containsKey("field1")) && !(requestMap.containsKey("field2")) && !(requestMap.containsKey("field3"))
                            && !(requestMap.containsKey("field4")) && !(requestMap.containsKey("field5")) && !(requestMap.containsKey("field6")) && !(requestMap.containsKey("field7"))
                            && !(requestMap.containsKey("field8")) && !(requestMap.containsKey("field9")) && !(requestMap.containsKey("field10"))) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Request is missing at least one field parameter");
                        validity = false;
                        return validity;
                    }

                    if ("".equalsIgnoreCase(requestMap.get("field1").trim()) && "".equalsIgnoreCase(requestMap.get("field2").trim())
                            && "".equalsIgnoreCase(requestMap.get("field3").trim()) && "".equalsIgnoreCase(requestMap.get("field4").trim()) && "".equalsIgnoreCase(requestMap.get("field5").trim())
                            && "".equalsIgnoreCase(requestMap.get("field6").trim()) && "".equalsIgnoreCase(requestMap.get("field7").trim()) && "".equalsIgnoreCase(requestMap.get("field8").trim())
                            && "".equalsIgnoreCase(requestMap.get("field9").trim()) && "".equalsIgnoreCase(requestMap.get("field10").trim())) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "All request fields are blank");
                        validity = false;
                        return validity;
                    }

                    if (!(requestMap.containsKey("field1"))) {
                        requestMap.put("field1", "");
                    }
                    if (!(requestMap.containsKey("field2"))) {
                        requestMap.put("field2", "");
                    }
                    if (!(requestMap.containsKey("field3"))) {
                        requestMap.put("field3", "");
                    }
                    if (!(requestMap.containsKey("field4"))) {
                        requestMap.put("field4", "");
                    }
                    if (!(requestMap.containsKey("field5"))) {
                        requestMap.put("field5", "");
                    }
                    if (!(requestMap.containsKey("field6"))) {
                        requestMap.put("field6", "");
                    }
                    if (!(requestMap.containsKey("field7"))) {
                        requestMap.put("field7", "");
                    }
                    if (!(requestMap.containsKey("field8"))) {
                        requestMap.put("field8", "");
                    }
                    if (!(requestMap.containsKey("field9"))) {
                        requestMap.put("field9", "");
                    }
                    if (!(requestMap.containsKey("field10"))) {
                        requestMap.put("field10", "");
                    }
                    break;
                default:
                    break;
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(ChannelRequest)", "Error:" + sw.toString());
            el.log();
            responseMap.put("Error", "true");
            responseMap.put("Message", "Exception occured");
            validity = false;
        }
        return validity;
    }

    /*public static void main(String[] args) {
        Map <String, String> requestMap=new HashMap();
        requestMap.put("type", "getaccounts");
        requestMap.put("accountnumber", "ffda");
        
        Boolean valid=validatefields(requestMap);
        JSONObject responseObject=new JSONObject(responseMap);
        System.out.println("valid: "+valid+" Resp map: "+responseObject.toString());
        System.out.println("new RequestMap: "+requestMap);
    }*/
}
