/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.api;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 * REST Web Service
 *
 * @author njiru
 */
@Path("b2bservices")
public class B2BResource {

    @Context
    private UriInfo context;
    public static Map<String, String> configs = new HashMap();

    /*
    *Read configs on start up
    *
    *
     */
    static {
        readPropertyFile(configs);
        ESBLog es12 = new ESBLog("readconfig", configs.toString());
        es12.log();
    }

    /**
     * Retrieves representation of an instance of ke.co.ekenya.api.B2BResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }


    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String postJson(String content) {
        String response = "";
        if (configs.isEmpty()) {
            readPropertyFile(configs);
        }

        try {
            RequestProcessor requestProcessor = new RequestProcessor(content, configs);
            response = requestProcessor.process();

            ESBLog es = new ESBLog("REST_Rqst&Resp", (new StringBuilder("Request: ")).append(content).append(" Response: ").append(response).toString());
            es.log();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es2 = new ESBLog("InitException", "Error: " + sw.toString());
            es2.log();
        }
        return response;
    }

    public static void readPropertyFile(Map configs) {
        InitialContext initialContext = null;
        try {
            initialContext = new InitialContext();
            NamingEnumeration<NameClassPair> list = initialContext.list("configs");
            while (list.hasMoreElements()) {
                String key = list.next().getName();
                String value = "";
                try {
                    value = (String) initialContext.lookup("configs/" + key);
                    configs.put(key.trim(), value.trim());
                } catch (Exception ex) {
                }
            }
            initialContext.close();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog(sw.toString());
            el.log();
        } finally {
            try {
                if (initialContext != null) {
                    initialContext.close();
                }
            } catch (Exception ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog(sw.toString());
                el.log();
            }
        }
    }

}
