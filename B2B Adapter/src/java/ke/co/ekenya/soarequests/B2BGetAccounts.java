/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.soarequests;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.xml.ws.BindingProvider;
import ke.co.coop.GetAccountStubs.AccountType;
import ke.co.coop.GetAccountStubs.DataInput;
import ke.co.coop.GetAccountStubs.GetAccountsInputType;
import ke.co.coop.GetAccountStubs.InstitutionType;
import ke.co.coop.GetAccountStubs.PortType;
import ke.co.coop.GetAccountStubs.ServiceStarter;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.messagehandlers.GetAccountsHandler;
import ke.co.ekenya.messagehandlers.GetAccountsResolver;

/**
 *
 * @author njiru
 */
public class B2BGetAccounts {

    Map<String, String> configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    Map<String, String> responseMap = new HashMap();

    public B2BGetAccounts(Map<String, String> configs, Map<String, String> xmlmap) {
        this.configs = configs;
        this.xmlmap = xmlmap;
    }

    public Map doProcess() {
        String response = "";
        try {
            DataInput dataInput = new DataInput();
            GetAccountsInputType.OperationParameters op_params = new GetAccountsInputType.OperationParameters();
            GetAccountsInputType inputType = new GetAccountsInputType();
            AccountType accountType = new AccountType();
            InstitutionType institutionType = new InstitutionType();

            if (!"".equals(xmlmap.get("accountnumber"))) {
                institutionType.setAccountNumber(xmlmap.get("accountnumber"));
            }

            if (!"".equals(xmlmap.get("businessname"))) {
                institutionType.setName(xmlmap.get("businessname"));
            }

            if (!"".equals(xmlmap.get("businesscode"))) {
                institutionType.setCode(xmlmap.get("businesscode"));
            }

            inputType.setInstitution(institutionType);
            dataInput.setGetAccountsInput(inputType);

            String proxypass = (new EncryptionUtils()).decrypt(configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + configs.get("B2B_WSDLS_PATH") + "Common_CommonCollections_getAccounts_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            GetAccountsHandler handler = new GetAccountsHandler(configs);
            GetAccountsResolver resolver = new GetAccountsResolver(handler);
            service.setHandlerResolver(resolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request with proxy username and password
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configs.get("SOA_USERNAME"));
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            //time-outs
            int connecttimeout = Integer.parseInt(configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.getAccounts(dataInput);
            response = handler.getMyXML();
            if (response == null || response.isEmpty() || response.trim().equalsIgnoreCase("")) {
                responseMap.put("Error", "true");
                responseMap.put("response", "Empty SOA Response");
                return responseMap;
            } else {
                breakDownSOAResponse(response);
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(B2BGetAccounts)", "Error:" + sw.toString());
            el.log();

            String message = "";
            String msg = ex.getMessage();
            if (msg.contains("SocketTimeoutException") && msg.contains("HTTP")) {
                message = "Connect timed out";
            } else if (msg.contains("SocketTimeoutException")) {
                message = "Read timed out";
            } else if (msg.contains("500")) {
                message = "Internal Server Error";
            } else {
                message = "Generic Error";
            }
            responseMap.put("Error", "true");
            responseMap.put("Message", message);
            return responseMap;
        }

        return responseMap;
    }

        private void breakDownSOAResponse(String SOAResp) {
        HashMap result = new HashMap();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAResp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("SOAP-ENV:Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("ns:HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("ns:StatusMessages");
            result.put("statusDescription", (String) headerReply.get("ns:StatusDescription"));
            result.put("statusCode", (String) headerReply.get("ns:StatusCode"));
            result.put("statusDescriptionKey", (String) headerReply.get("ns:StatusDescriptionKey"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                result.put("messageID", (String) headerReply.get("ns:MessageID"));

                //status messages breakdown for header
                HashMap statusMessage = (HashMap<String, Object>) statusMessages.get("ns:StatusMessage");
                result.put("messageType", (String) statusMessage.get("ns:MessageType"));
                result.put("applicationID", (String) statusMessage.get("ns:ApplicationID"));
                result.put("messageCode", (String) statusMessage.get("ns:MessageCode"));

                //BreakDown Of Body
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("SOAP-ENV:Body");
                HashMap<String, Object> dataOutput = (HashMap<String, Object>) body.get("ns0:DataOutput");
                HashMap<String, Object> accountsOutput = (HashMap<String, Object>) dataOutput.get("ns1:getAccountsOutput");
                HashMap<String, Object> accounts = (HashMap<String, Object>) accountsOutput.get("ns1:Accounts");

                HashMap<String, Object> operationParameters = (HashMap<String, Object>) accounts.get("ns1:OperationParameters");
                HashMap<String, Object> institution = (HashMap<String, Object>) accountsOutput.get("ns2:Institution");
                HashMap<String, Object> account = (HashMap<String, Object>) accounts.get("ns2:Account");
                System.out.println(operationParameters);

                responseMap.put("Error", "false");
                responseMap.put("type", "1");
                responseMap.put("AccountId", operationParameters.get("ns1:AccountId").toString());
                responseMap.put("ServiceCode", operationParameters.get("ns1:ServiceCode").toString());
                responseMap.put("BusinessCode", institution.get("ns2:Code").toString());
                responseMap.put("BusinessName", institution.get("ns2:Name").toString());
                responseMap.put("ProductId", account.get("ns2:ProductId").toString());
                responseMap.put("ProductCode", account.get("ns2:ProductCode").toString());
                responseMap.put("AccountName", account.get("ns2:AccountName").toString());
                responseMap.put("AccountNumber", account.get("ns2:AccountNumber").toString());
            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Account Details not found");
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(BreakdownAccounts)", "Exception occured while breaking down resonse.\nError:" + sw.toString());
            el.log();

            responseMap.put("Error", "true");
            responseMap.put("Message", "Breakdown error");
        }

    }

    private boolean checkinstance(Object obj, String cls)
            throws ClassNotFoundException {
        return Class.forName(cls).isInstance(obj);
    }

}
