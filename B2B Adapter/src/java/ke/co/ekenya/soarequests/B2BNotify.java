/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.soarequests;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import ke.co.coop.NotifyStubs.DataInput;
import ke.co.coop.NotifyStubs.NotifyInputType;
import ke.co.coop.NotifyStubs.ObjectFactory;
import ke.co.coop.NotifyStubs.PortType;
import ke.co.coop.NotifyStubs.ServiceStarter;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DBFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.messagehandlers.NotifyHandler;
import ke.co.ekenya.messagehandlers.NotifyResolver;

/**
 *
 * @author njiru
 */
public class B2BNotify {

    Map<String, String> configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    String CorrelationID;
    Map<String, String> responseMap = new HashMap();

    public B2BNotify(Map<String, String> configs, Map<String, String> xmlmap, String CorrelationID) {
        this.configs = configs;
        this.xmlmap = xmlmap;
        this.CorrelationID = CorrelationID;
    }

    public void doProcess() {
        String response = "";
        Date sendDate = new Date();
        try {
            DataInput dataInput = new DataInput();
            NotifyInputType inputType = new NotifyInputType();
            NotifyInputType.OperationParameters op_params = new NotifyInputType.OperationParameters();
            ObjectFactory objFactory = new ObjectFactory();

            op_params.setB2BReference(xmlmap.get("73"));
            op_params.setBFUBReference(xmlmap.get("37"));

            //amount
            BigDecimal amountVal = new BigDecimal(xmlmap.get("4"));
            op_params.setAmount(amountVal);

            op_params.setPaymentType("CASH");
            op_params.setChequeNumber("0");
            op_params.setAccountCR(xmlmap.get("103"));
            op_params.setAccountDr(xmlmap.get("102"));
            op_params.setRemitter(xmlmap.get("69"));
            op_params.setRemitterReference(xmlmap.get("72"));
            //opdate
            XMLGregorianCalendar gregOpDate = Utilities.stringToXMLGregorianCalendar(sendDate.toString());
            JAXBElement<XMLGregorianCalendar> gregOpDateJax = objFactory.createNotifyInputTypeOperationParametersOperationDate(gregOpDate);
            op_params.setOperationDate(gregOpDateJax);

            //posting date
            XMLGregorianCalendar gregPostingdate = Utilities.stringToXMLGregorianCalendar(sendDate.toString());
            op_params.setPostingDate(gregPostingdate);

            //value date
            XMLGregorianCalendar gregValueDate = Utilities.stringToXMLGregorianCalendar(sendDate.toString());
            op_params.setValueDate(gregValueDate);

            inputType.setOperationParameters(op_params);
            dataInput.setNotifyInput(inputType);

            String proxypass = (new EncryptionUtils()).decrypt(configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + configs.get("B2B_WSDLS_PATH") + "Common_CommonCollections_notify_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            NotifyHandler handler = new NotifyHandler(configs);
            NotifyResolver resolver = new NotifyResolver(handler);
            service.setHandlerResolver(resolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request with proxy username and password
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configs.get("SOA_USERNAME"));
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            //time-outs
            int connecttimeout = Integer.parseInt(configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.notify(dataInput);
            response = handler.getMyXML();
            if (response == null || response.isEmpty() || response.trim().equalsIgnoreCase("")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Generic error");
            } else {
                breakDownSOAResponse(response);
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(B2BGetAccounts)", "Error:" + sw.toString());
            el.log();

            String message = "";
            String msg = ex.getMessage();
            if (msg.contains("SocketTimeoutException") && msg.contains("HTTP")) {
                message = "Connect timed out";
            } else if (msg.contains("SocketTimeoutException")) {
                message = "Read timed out";
            } else if (msg.contains("500")) {
                message = "Internal Server Error";
            } else {
                message = "Generic Error";
            }
            responseMap.put("Error", "true");
            responseMap.put("Message", message);
            responseMap.put("status", "3");
        } finally {
            /**
             * ******Send response to ESB*************
             */
            if ("1".equalsIgnoreCase(responseMap.get("status"))) {
                //successful transaction
                xmlmap.put("39", "000");
                xmlmap.put("48", "SUCCESSFUL");
                responseMap.put("smscode", "B2B_001");
            } else {
                xmlmap.put("39", "713");
                xmlmap.put("48", responseMap.get("Message"));
                responseMap.put("smscode", "B2B_002");
            }
            QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
            boolean sent = qr.sendObject(xmlmap, CorrelationID);

            ESBLog lg = new ESBLog("ResponsetoESB", "Sent: " + sent + ", Map: " + xmlmap);
            lg.log();

            /**
             * ***********Insert Transaction*****************
             */
            xmlmap.put("status", responseMap.get("status"));
            xmlmap.put("Message", responseMap.get("Message"));
            xmlmap.put("smscode", responseMap.get("smscode"));
            int insert = (new DBFunctions()).insertBillPayments(xmlmap);
            if (insert == 1) {
                ESBLog ins = new ESBLog("SuccessfulInserts", "Insert: " + insert + ", Map: " + xmlmap);
                ins.log();
            } else {
                ESBLog ins = new ESBLog("FailedInserts", "Insert: " + insert + ", Map: " + xmlmap);
                ins.log();
            }
            
            /****************send sms****************/
            sendsms(xmlmap);
        }

    }

    private void breakDownSOAResponse(String SOAResp) {
        HashMap result = new HashMap();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAResp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("SOAP-ENV:Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("ns:HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("ns:StatusMessages");
            result.put("statusDescription", (String) headerReply.get("ns:StatusDescription"));
            result.put("statusCode", (String) headerReply.get("ns:StatusCode"));
            result.put("statusDescriptionKey", (String) headerReply.get("ns:StatusDescriptionKey"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                responseMap.put("Error", "false");
                responseMap.put("Message", "Transaction processed successfully");
                responseMap.put("status", "1");
            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Transaction failed");
                responseMap.put("status", "2");
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(BreakdownAccounts)", "Exception occured while breaking down resonse.\nError:" + sw.toString());
            el.log();

            responseMap.put("Error", "true");
            responseMap.put("Message", "Breakdown error");
            responseMap.put("status", "3");
        }

    }
    
    public void sendsms(Map<String, String> details) {
        String msg = "";
        Map<String, String> smsMap = new HashMap();
        boolean sent = false;
        
        try {
            Map<String, String> smsTemplate = (new DBFunctions()).getSMSTemplate(details.get("smscode"));
            msg = smsTemplate.get("SMSTEMPLATE");
            if (!"".equals(msg)) {
                msg = msg.replace("TRANREF", details.get("37"));
                msg = msg.replace("AMTR", details.get("4"));
                msg = msg.replace("INST", details.get("71"));
                msg = msg.replace("TRANREF", details.get("ref"));

                smsMap.put("PHONENUMBER", details.get("2"));
                smsMap.put("SMSMESSAGE", msg);
                smsMap.put("NotificationType", "SMS");
                smsMap.put("98", "SMS");
                smsMap.put("37", details.get("37"));
                smsMap.put("32", "B2B");
                smsMap.put("102", details.get("2"));
                smsMap.put("2", details.get("2"));

                QueueWriter queueWriter = new QueueWriter("jms/NOTIFICATIONS_QUEUE");
                sent = queueWriter.sendObject(smsMap, details.get("37"));
            } else {
                ESBLog es2 = new ESBLog("missingSmsTemplate", "Template code: " + details.get("smscode"));
                es2.log();
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog lg = new ESBLog("Exceptions(SendSms)", "Map: " + smsMap + ", error: " + sw.toString());
            lg.log();
        } finally {
            ESBLog es2 = new ESBLog("SendSms", "sent: " + sent + ", messageMap: " + smsMap);
            es2.log();
        }

    }

}
