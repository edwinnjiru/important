/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.soarequests;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.ws.BindingProvider;
import ke.co.coop.ValidateStubs.DataInput;
import ke.co.coop.ValidateStubs.ObjectFactory;
import ke.co.coop.ValidateStubs.PortType;
import ke.co.coop.ValidateStubs.ServiceStarter;
import ke.co.coop.ValidateStubs.ValidateInputType;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.messagehandlers.ValidateHandler;
import ke.co.ekenya.messagehandlers.ValidateResolver;

/**
 *
 * @author njiru
 */
public class B2BValidate {

    Map<String, String> configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    Map<String, String> responseMap = new HashMap();

    public B2BValidate(Map<String, String> configs, Map<String, String> xmlmap) {
        this.configs = configs;
        this.xmlmap = xmlmap;
    }

    public Map doProcess() {
        String response = "";
        try {
            DataInput dataInput = new DataInput();
            ValidateInputType inputType = new ValidateInputType();
            //InstitutionType institution = new InstitutionType();
            ValidateInputType.OperationParameters op_params = new ValidateInputType.OperationParameters();
            ObjectFactory objFactory = new ObjectFactory();

            if (!"".equals(xmlmap.get("field1"))) {
                op_params.setField1(xmlmap.get("field1"));
            }

            if (!"".equals(xmlmap.get("field2"))) {
                op_params.setField2(xmlmap.get("field2"));
            }

            if (!"".equals(xmlmap.get("field3"))) {
                op_params.setField3(xmlmap.get("field3"));
            }

            if (!"".equals(xmlmap.get("field4"))) {
                op_params.setField4(xmlmap.get("field4"));
            }

            if (!"".equals(xmlmap.get("field5"))) {
                op_params.setField5(xmlmap.get("field5"));
            }

            if (!"".equals(xmlmap.get("field6"))) {
                op_params.setField6(xmlmap.get("field6"));
            }

            if (!"".equals(xmlmap.get("field7"))) {
                op_params.setField7(xmlmap.get("field7"));
            }

            if (!"".equals(xmlmap.get("field8"))) {
                op_params.setField8(xmlmap.get("field8"));
            }

            if (!"".equals(xmlmap.get("field9"))) {
                op_params.setField9(xmlmap.get("field9"));
            }

            if (!"".equals(xmlmap.get("field10"))) {
                op_params.setField10(xmlmap.get("field10"));
            }

            if (!"".equals(xmlmap.get("servicecode"))) {
                JAXBElement<String> jaxServiceCode = objFactory.createValidateInputTypeServiceCode(xmlmap.get("servicecode"));
                inputType.setServiceCode(jaxServiceCode);
            }

            inputType.setOperationParameters(op_params);
            dataInput.setValidateInput(inputType);

            String proxypass = (new EncryptionUtils()).decrypt(configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + configs.get("B2B_WSDLS_PATH") + "Common_CommonCollections_validate_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            ValidateHandler handler = new ValidateHandler(configs);
            ValidateResolver resolver = new ValidateResolver(handler);
            service.setHandlerResolver(resolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request with proxy username and password
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configs.get("SOA_USERNAME"));
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            //time-outs
            int connecttimeout = Integer.parseInt(configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.validate(dataInput);
            response = handler.getMyXML();

            if (response.isEmpty() || response.trim().equalsIgnoreCase("")) {
                responseMap.put("Error", "true");
                responseMap.put("response", "Empty SOA Response");
                return responseMap;
            } else {
                breakDownSOAResponse(response);
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(B2BValidate)", "Error:" + sw.toString());
            el.log();

            String message = "";
            String msg = ex.getMessage();
            if (msg.contains("SocketTimeoutException") && msg.contains("HTTP")) {
                message = "Connect timed out";
            } else if (msg.contains("SocketTimeoutException")) {
                message = "Read timed out";
            } else if (msg.contains("500")) {
                message = "Internal Server Error";
            } else {
                message = "Generic Error";
            }
            responseMap.put("Error", "true");
            responseMap.put("Message", message);
            return responseMap;
        }
        return responseMap;

    }

    private void breakDownSOAResponse(String SOAResp) {
        HashMap result = new HashMap();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAResp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("SOAP-ENV:Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("ns:HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("ns:StatusMessages");
            result.put("statusDescription", (String) headerReply.get("ns:StatusDescription"));
            result.put("statusCode", (String) headerReply.get("ns:StatusCode"));
            result.put("statusDescriptionKey", (String) headerReply.get("ns:StatusDescriptionKey"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                result.put("messageID", (String) headerReply.get("ns:MessageID"));

                //status messages breakdown for header
                HashMap statusMessage = (HashMap<String, Object>) statusMessages.get("ns:StatusMessage");
                result.put("messageType", (String) statusMessage.get("ns:MessageType"));
                result.put("applicationID", (String) statusMessage.get("ns:ApplicationID"));
                result.put("messageCode", (String) statusMessage.get("ns:MessageCode"));

                //Breakdown body
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("SOAP-ENV:Body");
                HashMap<String, Object> dataOutput = (HashMap<String, Object>) body.get("ns0:DataOutput");
                HashMap<String, Object> validateOutput = (HashMap<String, Object>) dataOutput.get("ns1:validateOutput");
                HashMap<String, Object> operationParameters = (HashMap<String, Object>) validateOutput.get("ns1:OperationParameters");
                
                responseMap.put("Error", "false");
                responseMap.put("field1", operationParameters.get("ns1:Field1").toString());
                responseMap.put("field2", operationParameters.get("ns1:Field2").toString());
                responseMap.put("field3", operationParameters.get("ns1:Field3").toString());
                responseMap.put("field4", operationParameters.get("ns1:Field4").toString());
                responseMap.put("field5", operationParameters.get("ns1:Field5").toString());
                responseMap.put("field6", operationParameters.get("ns1:Field6").toString());
                responseMap.put("field7", operationParameters.get("ns1:Field7").toString());
                responseMap.put("field8", operationParameters.get("ns1:Field8").toString());
                responseMap.put("field9", operationParameters.get("ns1:Field9").toString());
                responseMap.put("field10",operationParameters.get("ns1:Field10").toString());
            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Entity does not exist");
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(BreakdownValidate)", "Exception occured while breaking down resonse.\nError:" + sw.toString());
            el.log();

            responseMap.put("Error", "true");
            responseMap.put("Message", "Breakdown error");
        }

    }

    private boolean checkinstance(Object obj, String cls)
            throws ClassNotFoundException {
        return Class.forName(cls).isInstance(obj);
    }

}
