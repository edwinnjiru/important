/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.soarequests;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.xml.ws.BindingProvider;
import ke.co.coop.GetParametersStubs.DataInput;
import ke.co.coop.GetParametersStubs.GetParametersInputType;
import ke.co.coop.GetParametersStubs.PortType;
import ke.co.coop.GetParametersStubs.ServiceStarter;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.messagehandlers.GetParamsHandler;
import ke.co.ekenya.messagehandlers.GetParamsResolver;

/**
 *
 * @author njiru
 */
public class B2BGetParams {

    Map<String, String> configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    Map<String, String> responseMap = new HashMap();

    public B2BGetParams(Map<String, String> configs, Map<String, String> xmlmap) {
        this.configs = configs;
        this.xmlmap = xmlmap;
    }
    
    public Map doProcess(){
        String response = "";
        try {
            DataInput dataInput=new DataInput();
            GetParametersInputType inputType= new GetParametersInputType();
            GetParametersInputType.OperationParameters op_params=new GetParametersInputType.OperationParameters();
            
            //op_params.setAccountId(xmlmap.get("servicecode"));
            op_params.setServiceCode(xmlmap.get("servicecode"));
            inputType.setOperationParameters(op_params);
            dataInput.setGetParametersInput(inputType);
            
            String proxypass = (new EncryptionUtils()).decrypt(configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + configs.get("B2B_WSDLS_PATH") + "Common_CommonCollections_getParameters_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service=new ServiceStarter(url);
            GetParamsHandler handler=new GetParamsHandler(configs);
            GetParamsResolver resolver=new GetParamsResolver(handler);
            service.setHandlerResolver(resolver);
            PortType port = service.getSOAPSpEventSpSource();
            
            //Binding request with proxy username and password
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configs.get("SOA_USERNAME"));
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            //time-outs
            int connecttimeout = Integer.parseInt(configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);
            
            port.getParameters(dataInput);
            response = handler.getMyXML();
            if (response == null || response.isEmpty() || response.trim().equalsIgnoreCase("")) {
                responseMap.put("Error", "true");
                responseMap.put("response", "Empty SOA Response");
                return responseMap;
            } else {
                breakDownSOAResponse(response);
            }
            
            
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(B2BGetParams)", "Error:" + sw.toString());
            el.log();

            String message = "";
            String msg = ex.getMessage();
            if (msg.contains("SocketTimeoutException") && msg.contains("HTTP")) {
                message = "Connect timed out";
            } else if (msg.contains("SocketTimeoutException")) {
                message = "Read timed out";
            } else if (msg.contains("500")) {
                message = "Internal Server Error";
            } else {
                message = "Generic Error";
            }
            responseMap.put("Error", "true");
            responseMap.put("Message", message);
            return responseMap;
        }
    
        return responseMap;
    }
    
    private void breakDownSOAResponse(String SOAResp) {
        HashMap result = new HashMap();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAResp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("SOAP-ENV:Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("ns:HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("ns:StatusMessages");
            result.put("statusDescription", (String) headerReply.get("ns:StatusDescription"));
            result.put("statusCode", (String) headerReply.get("ns:StatusCode"));
            result.put("statusDescriptionKey", (String) headerReply.get("ns:StatusDescriptionKey"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                result.put("messageID", (String) headerReply.get("ns:MessageID"));

                //status messages breakdown for header
                HashMap statusMessage = (HashMap<String, Object>) statusMessages.get("ns:StatusMessage");
                result.put("messageType", (String) statusMessage.get("ns:MessageType"));
                result.put("applicationID", (String) statusMessage.get("ns:ApplicationID"));
                result.put("messageCode", (String) statusMessage.get("ns:MessageCode"));

                //BreakDown Of Body
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("SOAP-ENV:Body");
                HashMap<String, Object> dataOutput = (HashMap<String, Object>) body.get("ns0:DataOutput");
                HashMap<String, Object> getParametersOutput = (HashMap<String, Object>) dataOutput.get("ns1:getParametersOutput");
                HashMap<String, Object> field = (HashMap<String, Object>) getParametersOutput.get("ns1:Field");
                HashMap<String, Object> fields = (HashMap<String, Object>) field.get("ns1:Fields");
                System.out.println(fields);

                responseMap.put("Error", "false");
                responseMap.put("id", fields.get("ns1:Id").toString());
                responseMap.put("code", fields.get("ns1:Code").toString());
                responseMap.put("name", fields.get("ns1:Name").toString());
                responseMap.put("length", fields.get("ns1:Length").toString());
                responseMap.put("type", fields.get("ns1:Type").toString());
            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Parameters Not Found");
            }
            
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(BreakdownAccounts)", "Exception occured while breaking down resonse.\nError:" + sw.toString());
            el.log();

            responseMap.put("Error", "true");
            responseMap.put("Message", "Breakdown error");
        }

    }
    
    private boolean checkinstance(Object obj, String cls)
            throws ClassNotFoundException {
        return Class.forName(cls).isInstance(obj);
    }

}
