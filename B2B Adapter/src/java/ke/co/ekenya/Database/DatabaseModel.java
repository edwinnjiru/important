/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Database;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.mdb.B2B_MDB;


/**
 *
 * @author Njiru
 */
public class DatabaseModel {
    
    public static Context ctx = null;
    public static javax.sql.DataSource ds;
    static Map<String, String> configs = new HashMap();
    
    public DatabaseModel() {
        configs = B2B_MDB.configs;
        if (configs.isEmpty()) {
            B2B_MDB.readPropertyFile(configs);
        }
    }
    
    public static Connection createConnection() {
        Connection con = null;
        try {

            //EBANK Schema
            Hashtable ht = new Hashtable();
            ht.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
            ht.put(Context.PROVIDER_URL, configs.get("PROVIDER_URL"));
            ctx = new InitialContext(ht);
            ds = (javax.sql.DataSource) ctx.lookup("jdbc/EBANK");
            con = ds.getConnection();
            
        } catch (NamingException | SQLException ex) {
            //System.out.println("create conn error: " + ex.getMessage());
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es = new ESBLog("DatabaseException", "Error creating connection error: " + sw.toString());
            es.log();
        }
        return con;
    }
    
        public static Connection createMvisaConnection() {
        Connection con = null;
        try {

            //MVISA Schema
            Hashtable ht = new Hashtable();
            ht.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
            ht.put(Context.PROVIDER_URL, configs.get("PROVIDER_URL"));
            ctx = new InitialContext(ht);
            ds = (javax.sql.DataSource) ctx.lookup("jdbc/ECONNECTESB");
            con = ds.getConnection();

        } catch (NamingException | SQLException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es = new ESBLog("DatabaseException", "Error creating 4.0 connection error: " + sw.toString());
            es.log();
        }
        return con;
    }

    
}
