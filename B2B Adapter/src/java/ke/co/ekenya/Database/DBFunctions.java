/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Database;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import ke.co.ekenya.LogEngine.ESBLog;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Njiru
 */
public class DBFunctions extends DatabaseModel {

    public DBFunctions() {
    }

    public int insertBillPayments(Map<String, String> xmlMap) {
        int insert = 0;
        ResultSet rs = null;
        String command = "{call SP_BILLPAYMENTS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        try (Connection con = createMvisaConnection();
                CallableStatement stmt = con.prepareCall(command);) {
            stmt.setString("V_PROVIDER", xmlMap.get("98"));
            stmt.setString("V_CHANNEL", xmlMap.get("32"));
            stmt.setString("V_AMOUNT", xmlMap.get("4"));
            stmt.setString("V_PROCODE", xmlMap.get("3"));
            stmt.setString("V_CUSTOMERNAME", xmlMap.get("75"));
            stmt.setString("V_ACCOUNTNUMBER", xmlMap.get("102"));
            stmt.setString("V_BILLNUMBER", xmlMap.get("72"));
            stmt.setString("V_PHONENUMBER", xmlMap.get("2"));
            stmt.setString("V_TRANSACTION_CODE", xmlMap.get("73"));
            stmt.setString("V_NARRATION", xmlMap.get("68"));
            stmt.setString("V_Msg", "");
            stmt.setString("V_TRANSACTION_ID", xmlMap.get("37"));
            stmt.setString("V_BILLDURATION", "");
            stmt.setString("V_BILLTYPE", "");
            stmt.setString("V_BILLREFERENCE", "");
            stmt.setString("V_REMITTER_NAME", xmlMap.get("69"));
            stmt.setString("V_ACC_CR", xmlMap.get("103"));
            stmt.setString("V_INSTITUTIONNAME", xmlMap.get("71"));
            stmt.setString("V_STATUS", xmlMap.get("status"));
            stmt.setString("V_RESPONSE", xmlMap.get("Message"));
            stmt.registerOutParameter("c_1", java.sql.Types.VARCHAR);
            stmt.execute();
            
            insert = Integer.valueOf(stmt.getString("c_1"));

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Error in inserting Billpayments: " + sw.toString() + " Map: " + xmlMap);
            el.log();
        }

        return insert;
    }

    public String getCorrelationID() {
        String CorrelationID = "";
        String command = "{call SP_GET_ESB_NEXT_SEQ(?)}";
        try (Connection con = createMvisaConnection();
                CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.registerOutParameter("cv_1", java.sql.Types.VARCHAR);
            callableStatement.executeUpdate();
            CorrelationID = callableStatement.getString("cv_1");
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Error in correlationID function: " + sw.toString());
            el.log();
        }
        return CorrelationID;
    }

    public HashMap getSMSTemplate(String smsCode) {
        HashMap detailsMap = new HashMap<>();
        String template = "";
        String command = "{call SP_GET_SMS_TEMPLATE(?,?)}";
        ResultSet rs = null;

        try (Connection con = createMvisaConnection();
                CallableStatement stmt = con.prepareCall(command);) {
            stmt.setString("IV_SMSCODE", smsCode);
            stmt.registerOutParameter("cv_1", OracleTypes.CURSOR);
            stmt.execute();

            rs = (ResultSet) stmt.getObject("cv_1");
            while (rs.next()) {
                template = rs.getString("MESSAGE");
            }

            detailsMap.put("SMSTEMPLATE", template);

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Exception occured while calling"
                    + " SP_GET_SMS_TEMPLATE SP to get sms template for sms code:" + smsCode + "|Error:" + sw.toString() + "\n");
            el.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog("SQLExceptions", "Error occured closing resultset of get sms template function. Error: " + sw.toString());
                el.log();
            }
        }

        return detailsMap;

    }
}
