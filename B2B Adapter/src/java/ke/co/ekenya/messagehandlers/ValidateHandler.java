package ke.co.ekenya.messagehandlers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author kkarue
 */
public class ValidateHandler implements SOAPHandler<SOAPMessageContext> {

    private String syscode;
    public String myXML;
    private Map<String, String> Configs;

    public String getMyXML() {
        return myXML;
    }

    public ValidateHandler() {
    }

    public ValidateHandler(Map<String, String> Configs) {
        this.Configs = Configs;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext smc) {
        syscode = Configs.get("SOA_SYSCODE");
        String msgid=GenerateRandomNumber(10);

        Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (outboundProperty) {
            try {
                SOAPMessage message = smc.getMessage();
                SOAPEnvelope env = smc.getMessage().getSOAPPart().getEnvelope();

                env.addNamespaceDeclaration("soap", "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader");

                QName MsgID = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageID", "soap");
                QName qname = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "HeaderRequest", "soap");
                QName Credentials = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Credentials", "soap");
                QName q_syscode = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "SystemCode", "soap");

                javax.xml.soap.Name kaname = new javax.xml.soap.Name() {
                    @Override
                    public String getLocalName() {
                        return "Header";
                        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public String getQualifiedName() {
                        return "Header";
                        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public String getPrefix() {
                        return "soap";
                        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    @Override
                    public String getURI() {
                        return "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader";
                        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }
                };
                String stan = GenerateRandomNumber(10);
                SOAPElement soapElement2 = env.getHeader().addHeaderElement(qname);

                SOAPElement soapElement3 = soapElement2.addChildElement(MsgID);
                soapElement3.addTextNode(msgid);
                SOAPElement soapElement7 = soapElement2.addChildElement(Credentials);
                SOAPElement soapElement5 = soapElement7.addChildElement(q_syscode);//env.getHeader().addHeaderElement(q_syscode);
                soapElement5.addTextNode(syscode);
                message.saveChanges();

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                message.writeTo(out);
                String strMsg = new String(out.toByteArray());
                //System.out.println(strMsg);

                ESBLog el = new ESBLog("VALIDATE_REQUEST_TO_SOA", msgid+" : "+strMsg);
                el.log(); 
            } catch (SOAPException | IOException e) {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog(sw.toString());
                el.log();
            }
        } else {
            try {
                SOAPMessage message = smc.getMessage();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                message.writeTo(out);
                String strMsg = new String(out.toByteArray());
                myXML = strMsg;
                
                ESBLog el = new ESBLog("VALIDATE_RESPONSE_FROM_SOA", msgid+" : "+strMsg);
                el.log(); 
            } catch (SOAPException | IOException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog(sw.toString());
                el.log();
            }
        }
        return outboundProperty;
    }

    @Override
    public Set getHeaders() {
        //throw new UnsupportedOperationException("Not supported yet.");
        return null;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        //throw new UnsupportedOperationException("Not supported yet.");
        return true;
    }

    @Override
    public void close(MessageContext context) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public static String GenerateRandomNumber(int charLength) {
        return String.valueOf(charLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1)
                + (int) Math.pow(10, charLength - 1));
    }
}
