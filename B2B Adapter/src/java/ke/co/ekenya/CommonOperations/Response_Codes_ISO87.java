/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.CommonOperations;

/**
 * Contains Transaction Response codes for ESB conforming to the ISO8587 standard (3 digit based field 39)
 * @author Hesbon
 */
public class Response_Codes_ISO87 {
    public Response_Codes_ISO87()
    {
        
    }

    public static final  String SUCCESS = "000";
    public static final  String CR_ACCOUNT_MISSING = "200";
    public static final  String INVALID_ACCOUNT = "111";
    public static final  String INSUFFICIENT_FUNDS = "020";
    public static final  String INSUFFICIENT_POINTS = "021";
    public static final  String AGENT_DEPOSIT_REQUIRED = "520";
    public static final  String DR_ACCOUNT_MISSING = "530";
    public static final  String DONOT_HONOR = "570";
    public static final  String NO_MATCHING_RECORD = "580";
    public static final  String EXPIRED_CODE = "590";
    public static final  String WRONG_AMOUNT = "600";
    public static final  String LIMIT_EXCEEDED = "610";
    public static final  String DAILY_LIMIT_EXCEEDED = "611";
    public static final  String WRONG_MESSAGE_FORMAT = "612";
    public static final  String DECLINE = "100";
    public static final  String ACCOUNT_RELATED_ISSUES = "101";
    public static final  String INVALID_MERCHANT = "109";
    public static final  String MISSING_FUCNTIONALITY = "115";
    public static final  String NO_CARD_RECORD = "118";
    public static final  String CLOSED_ACCOUNT = "284";
    public static final  String DORMANT_ACCOUNT = "528";
    public static final  String NODR_ACCOUNT = "323";
    public static final  String STOPPED_ACCOUNT = "172";
    public static final  String NOCR_ACCOUNT = "325";
    public static final  String MISSING_PROCODE = "115";
    public static final  String TRANSACTION_NOT_ALLOWED = "119";
    public static final  String SERVICE_UNAVAILABLE = "120";
    public static final  String AGENT_CODE_ALREADY_USED = "300";
    public static final  String AGENT_WRONG_IDNUMBER = "302";
    public static final  String AGENT_WRONG_AGENTNUMBER = "303";
    public static final  String AGENT_WRONG_REF_NUMBER = "304";
    public static final  String HOST_CONNECTION_DOWN = "912";
    public static final  String HOST_CONNECTION_TIMEOUT = "980";
    public static final  String ACKOWLEDGEMENT = "777";
    public static final  String UNABLE_TOFETCH_LIMITS = "613";
    public static final  String NEGATIVE_B2C_COMMISSION = "711";
    public static final  String UNDEFINED_RESPONSECODE = "713";
    public static final  String ERROR_WHILE_PROCESSING = "713";
    public static final  String UNMAPPED_RTPS_RESPONSE = "714";
    public static final  String UNKNOWN_PROCESSING_CODE = "716";
    public static final  String TRANSACTION_NOT_AUTHORIZED = "713";
    public static final  String ACCOUNT_BRANCH_MISSING = "712";
    public static final  String MISSING_SACCO_GL = "712";
    public static final  String SACCO_BLOCKED_FROM_TRANSACTING = "718";
    public static final  String INVALID_CUSTOMER_ACCOUNT = "719";
    public static final  String REPEAT_TRANSACTION = "720";
    public static final  String MISSING_IMPORTANT_DATA_IN_FIELD = "721";
    public static final  String MKARO_INSTITUTION_ACCOUNT_MISSING = "712";
    public static final  String ACCOUNT_LOCKED_FOR_PROCESSING = "722";
    public static final  String EMPLOYER_CODE_NOT_DEFINED = "723";
    public static final  String PRODUCT_NOT_DEFINED = "724";
    public static final  String FAILED_TO_GENERATE_OTP = "725";
    public static final  String DATABASE_ERROR = "726";
    public static  final String MISSING_CUSTOMER_IDENTIFIER = "727";
    public static  final String MISSING_TRANSACTION_IDENTIFIER = "728";

}
/**
 * Contains response codes for BFUB (Universal Banking) Core Banking System
 * @author Hesbon
 */

class MQResponseCodes {
    public MQResponseCodes()
    {
        
    }

    public static final  String ACCOUNT_CLOSED = "40200284";
    public static final  String REFFEREAL_REQUIRED_FOR_POSTING = "40007327";
    public static final  String REFFEREAL_REQUIRED_FOR_ENQUIRY = "40112173";
    public static final  String UNABLE_TO_PROCESS_TRANSACTION = "40421536";
    public static final  String ACCOUNT_DORMANT = "40409528";
    public static final  String NO_DEBITS = "40007323";
    public static final  String VALUE_DATE_BEFORE_OPEN_DATE = "40243028";
    public static final  String ACCOUNT_CLOSED_1 = "40000132";
    public static final  String FROM_DATE_GREATER_TO_DATE = "40000304";
    public static final  String TRANSACTION_REFERENCE_NOT_FOUND = "40407606";
    public static final  String CANNOT_PROCESS_TRANSACTION = "40108300";
    public static final  String ACCOUNT_IS_STOPPED = "40112172";
    public static final  String NO_CREDITS = "40007325";
    public static final  String SUPERVISOR_AUTHORIZATION_REQUIRED = "40205109";
    public static final  String NO_DEBITS_ALLOWED = "40007322";
    public static final  String TRANSACTION_ALREADY_POSTED = "40422049";
    public static final  String NO_DEBITS_ALLOWED_1 = "40180194";
    public static final  String ACCOUNT_DOES_NOT_EXIST = "20020000";
    public static final  String FUND_SETTLEMENT_ACCOUNT = "20201005";
    public static final  String UNEXPECTED_ERROR = "40000127";
    public static final  String REFFERAL_REQUIRED_FOR_POSTING = "40007319";
    public static final  String REFFERAL_REQUIRED_FOR_POSTING_ENQUIRY = "40112171";
    public static final  String CUSTOMER_IS_NOT_ACTIVE = "40411056";
    public static final  String ACCOUNT_CANNOT_BE_FOUND = "40000126";
    public static final  String ACCOUNT_STOPPED = "40007321";
    public static final  String INSUFFICIENT_AVAILABLE_BALANCE = "40507020";
    public static final  String NO_CREDIT_ALLOWED_ACCOUNT = "40180193";

}

class FlexEbank_Operations {
    public FlexEbank_Operations()
    {
        
    }

    public static final  String FLEX2FLEX = "01";
    public static final  String FLEX2MINI = "02";
    public static final  String FLEXDEPOSIT = "03";
    public static final  String MINI2FLEX = "04";
    public static final  String FLEXCHARGES = "05";
    public static final  String DONOT_HONOR = "57";
    public static final  String NO_MATCHING_RECORD = "58";
    public static final  String EXPIRED_CODE = "59";
    public static final  String WRONG_AMOUNT = "60";
    public static final  String LIMIT_EXCEEDED = "61";

}

/* enum Operation
 {
 CASH_WITHDRAWAL = 01,
 PURCHASE = 04,
 FOREX = 2,
 CHQ_INWARD = 03,
 STOP_PAY = 04,
 PIN = 6,
 PAY = 7,
 TOPUPOTHER = 9,
 STAT = 10,
 STOP_CARD = 11,
 LOYALTY = 12,
 DEPOSIT = 21,
 SALU = 14,
 SALT = 15,
 STANDING_ORDER = 16,
 FOREX_PURCHASE = 17,
 RTGS = 18,
 BULK = 47,
 BULKU = 20,
 MOBILE_MONEY = 43,
 ATM = 22,
 FS = 23,
 FS1 = 24,
 TAX = 25,
 CHQ_OUTWARD = 24,
 SCHEDULED_BILL = 26,
 MT103TISS = 27,
 MT202TISS = 28,
 MT202SWIFT = 29,
 MT103SWIFT = 30,
 //  MT700 = 31,
 BAL = 31,
 LN_BAL = 66,
 BALLPT = 33,
 BALBT = 32,
 BAL_SHARE = 34,
 LN_REQ = 92,
 //AGENT_LIMIT_INQUIRY = 32,
 //BANKERSCHQ = 32,
 COUNTERSCHQ = 33,


 MINI = 38,
 LN_MIN = 67,
 FULL_STMT = 37,
 ACC_INQ = 39,
 FT = 40,
 FT_PTS = 94,
 INWARD_FT = 41,
 OUTWARD_FT = 44,
 TOPUP = 42,
 //KITDELIVERY = 43,

 // MoneyGram Recieve cash by Customer....Addition by Faith 
 MONEY_TRANSFER = 45,

 BILLPAYMENT = 50,
 LN_DS = 64,
 LN_REPAY = 65,
 LN_PRV = 74,
 LN_WRTOFF = 76,
 LN_WRTDWN = 80,
 LN_OTHERCHARGES = 81,
 BILLPRESENTMENT = 53,
 REMITTANCE_ORIGINATION = 62,
 REMITTANCE_FULFILMENT = 63,

 Eswitch_LoadToWallet = 80,
 Eswitch_UnLoadWallet = 81,

 STATEMENT_REQUEST = 90,
 CHQ_REQUEST = 91,
 CHQ_NO_VALIDATION = 92,
 CHQ_INFORMATION = 93,
 POS_TO_MINI_FROM_CBS = 111,
 BAL_CBS = 311,
 MINI_CBS = 381,

 EOD_Credit = 70,
 EOD_Bank_Recovery = 71,
 EOD_Write_Back = 77,
 EOD_Commissions = 78,
 EOD_ClosedAcc = 79,
 //===========================================


 }*/
