package ke.co.ekenya.CommonOperations;

import commonj.work.WorkManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.mdb.B2B_MDB;

/**
 * 
 * @author Ngari Josh
 */
public class WorkManagerUtilities {
    public WorkManagerUtilities()
    {
        
    }

    /**
     * It fetches a named thread pool from weblogic work managers
     *
     * @return workManager - The Weblogic ThreadPool
     */
    public static WorkManager getWorkManager() {
        WorkManager workManager = null;
        //workManager is a static variable  
        if (workManager == null) {
            try {
                InitialContext ctx = new InitialContext();
                //InitialContext ctx = getInitialContext();
                //default work manager
                //String jndiName = "java:comp/env/wm/ESBWorkManager";
                String jndiName = "java:global/wm/default";
                //String jndiName = "java:global/wm/ESBWorkManager";
                workManager = (WorkManager) ctx.lookup(jndiName);
            } catch (Exception e) {
                //ESBLog el = new ESBLog(e, "");
                //el.log();
            }
        }
        return workManager;
    }

    /**
     * Instantiates a weblogic initial JMS context
     *
     * @return ic - The weblogic JMS Context
     */
    private static InitialContext getInitialContext() {
        Hashtable env = new Hashtable();
        InitialContext ic = null;
        try {
            env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
            env.put(Context.PROVIDER_URL, B2B_MDB.configs.get("PROVIDER_URL"));
            ic = new InitialContext(env);
        } catch (NamingException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("JMSExceptions","Exception occured while intializing context factory:" + "\nError:" + sw.toString());
            
            el.log();
        }
        return ic;
    }
}
