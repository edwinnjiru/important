/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.CommonOperations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.mdb.B2B_MDB;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;

/**
 *
 * @author Ngari Josh
 */
public class PostGetESBRequest {

    public PostGetESBRequest() {

    }

    public HashMap<String, String> CURLRequest(HashMap<String, String> requestMap) {
        String result = "";
        String xmlRequest = "";
        HashMap<String, String> resultMap = new HashMap();
        Map<String, String> configs = new HashMap();
        try {
            configs = B2B_MDB.configs;
            if (configs.isEmpty()) {
                B2B_MDB.readPropertyFile(configs);
            }
            xmlRequest = formRequestXML(requestMap);
            ESBLog el = new ESBLog("XMLRequestsToESB", "XML Request sent to ESB :" + xmlRequest + "\n");

            el.log();
            //String urlStr = "http://172.16.4.68:8002/ESBWSConnector";
            String urlStr = configs.get("ESB_SERVLET_URL");
            URL url = new URL(urlStr);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            try (OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream())) {
                writer.write(xmlRequest);
                writer.flush();
                result = getStringFromInputStream(conn.getInputStream());

                ESBLog el2 = new ESBLog("XMLResponsesFromESB", "XML Response from ESB :" + result + "\n");

                el2.log();

                resultMap = readXML(result);

//                ESBLogger el3 = new ESBLogger("MapResponsesFromESB", Utilities.logPreString()
//                        + "Map result formed from ESB XML Response:" + resultMap + "\n");
//
//                el3.log();
            } catch (Exception ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el4 = new ESBLog("CURLExceptions", "Exception occured while posting CURL request to ESB|" + "Error:" + sw.toString());

                el4.log();

                resultMap.put("39", "12");
                resultMap.put("48", "System Malfunction");
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el5 = new ESBLog("CURLExceptions", "Exception occured while posting CURL request to ESB|" + "Error:" + sw.toString());

            el5.log();

            resultMap.put("39", "12");
            resultMap.put("48", "System Malfunction");
        }
        return resultMap;
    }

    public String formRequestXML(HashMap<String, String> fields) {
        String strXML = "";
        StringBuilder sb = new StringBuilder();
        try {
            fields.put("0", fields.get("MTI"));

            String NewLine = "\r\n";
            sb.append("<?xml version= \"1.0\"   encoding= \"utf-8\"?>").append(NewLine);
            sb.append("<message>").append(NewLine);
            sb.append("<isomsg direction=\"request\">").append(NewLine);
            for (Map.Entry<String, String> entry : fields.entrySet()) {
                if (entry.getValue() != null) {
                    sb.append("<field id=\"").append(entry.getKey().replaceAll("\"", "")).append("\" value=\"").append(entry.getValue().replaceAll("'", "")).append("\"/>").append(NewLine);
                }
            }
            sb.append("</isomsg>").append(NewLine);
            sb.append("</message>").append(NewLine);
            strXML = sb.toString();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("XMLExceptions", "Exception occured while forming XML request to ESB|" + "Error:" + sw.toString());

            el.log();
        }

        return strXML;
    }

    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("CURLExceptions", "Exception occured while reading input stream|" + "Error:" + sw.toString());

            el.log();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    StringWriter sw = new StringWriter();
                    e.printStackTrace(new PrintWriter(sw));
                    ESBLog el = new ESBLog("CURLExceptions", "Exception occured while closing input stream|" + "Error:" + sw.toString());

                    el.log();
                }
            }
        }

        return sb.toString();

    }

    /**
     * Function ReadXML - Receives XML message String, breaks down the message
     * into individual elements and store them in a HashMap
     *
     * @param xmlString - XML message string
     * @return FieldsMap- HashMap of fields in the input XML Message
     */
    public HashMap readXML(String xmlString) {
        HashMap<String, String> FieldsMap = new HashMap();
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            //factory.setFeature(xmlString, true);
            DefaultHandler handler = new DefaultHandler() {
                public void startElement(String uri, String localName, String qName,
                        Attributes attributes) throws SAXException {
                    if (qName.equalsIgnoreCase("field")) {
                        FieldsMap.put(attributes.getValue(0), attributes.getValue(1));
                        //System.out.println(attributes.getValue(0) + " : " + attributes.getValue(1));
                    }
                }
            };

            saxParser.parse(new InputSource(new StringReader(xmlString)), handler);

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("XMLExceptions", "Exception occured while breaking down ESB XML response|" + "Error:" + sw.toString());

            el.log();

        }
        return FieldsMap;
    }

}
