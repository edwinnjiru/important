import { Injectable } from '@angular/core';
import { LoadingController} from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


import { GlobalVars } from './global-vars';
import { ApiConnect } from './api-connect';

/*
  Generated class for the LocationService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LocationService {
 
    data: any;
	locations: any=[];
	locationsLoaded:any;
	usersLocation:any;
 
    constructor(public http: Http, public apiConnect: ApiConnect, public loadingController: LoadingController, public globalVars: GlobalVars) {
	    
        
    }
	load(endUrl): Promise<any>{
	    let loader = this.loadingController.create({content:'Fetching Co-ordinates'});
	    let requestType="post";
	    let body="";
	    let authority="coop";
	    loader.present();
        return new Promise(resolve => {	
        	
			this.apiConnect.load(requestType,body,endUrl,authority)
				.then(data => {
					 
					this.globalVars.setLocationLoaded(this.createLocation(data));
					loader.dismiss();
					
					resolve(true);
					 
				}
				
			);
			
		});
		
	}
    
    createLocation(data){
	    data=JSON.parse(data);
		
		this.locations=[];
        for(var i=0;i<data.length;i++){
            this.locations.push({title:data[i][0], latitude: data[i][1], longitude: data[i][2]});
		}
		this.data = this.applyHaversine(this.locations);
 
		this.data.sort((locationA, locationB) => {
			return locationA.distance - locationB.distance;
		});
		
        return this.data;
               
    }
	applyHaversine(locations){
        if(this.globalVars.getCurrentLocation().latitude!=undefined){
			this.usersLocation = {
				lat: this.globalVars.getCurrentLocation().latitude, 
				lng: this.globalVars.getCurrentLocation().longitude
			};
		}
		else{
		    this.usersLocation = {
				lat: -1.2991016, 
				lng: 36.8196439
			};
		}

        locations.map((location) => {
 
            let placeLocation = {
                lat: location.latitude,
                lng: location.longitude
            };
 
            location.distance = this.getDistanceBetweenPoints(
                this.usersLocation,
                placeLocation,
                'km'
            ).toFixed(2);
			
        });
 
        return locations;
    }
 
    getDistanceBetweenPoints(start, end, units){
 
        let earthRadius = {
            miles: 3958.8,
            km: 6371
        };
 
        let R = earthRadius[units || 'km'];
        let lat1 = start.lat;
        let lon1 = start.lng;
        let lat2 = end.lat;
        let lon2 = end.lng;
 
        let dLat = this.toRad((lat2 - lat1));
        let dLon = this.toRad((lon2 - lon1));
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;
 
        return d;
 
    }
 
    toRad(x){
        return x * Math.PI / 180;
    }
}
