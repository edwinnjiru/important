import { Injectable } from '@angular/core';

import CryptoJS from 'crypto-js';

/*
  Generated class for the GlobalVars provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class GlobalVars {

 
  locationsLoaded: any;
  currentLocation: any={};
  partialReg:any;
  accounts:any=[];
  linkedAccounts:any;
  kitsMaximum:any;
  kitsMinimum:any;
  customerNo:any;
  kitsBanks:any;
  loanAccounts:any;
  banks:any;
  uuid:any;
  beneficiaries:any;
  loginTime:any;
  mobileNo:string;
  otp:string;
  lock:boolean;

  constructor() {
    this.locationsLoaded=new Array();
  }
  doAESencrypt(data, uuid){
    let encrypted= '' +CryptoJS.AES.encrypt(data, uuid);
    return encrypted;
  }
  doAESdecrypt(encrypted, uuid){
    let decrypted=CryptoJS.AES.decrypt(encrypted, uuid).toString(CryptoJS.enc.Utf8);
    return decrypted;
  }
  //sets
  setOtp(value){
    this.otp=value;
  }
  setLock(value){
    this.lock=value;
  }
  setUsername(value){
    this.mobileNo=value;
  }
  setPartialReg(value){
    this.partialReg=value;
  }
  setAccounts(value){
    this.accounts=value;
  }
  setLoanAccounts(value){
    this.loanAccounts=value;
  }
  setKitsLimits(value, value2){
    this.kitsMaximum=value;
    this.kitsMinimum=value2;
  }
  setLinkedAccounts(value){
    this.linkedAccounts=value;
  }
  setCustomerNo(value){
    this.customerNo=value;
  }
  setKitsBanks(value){
    this.kitsBanks=value;
  }
  setBanks(value){
    this.banks=value;
  }
  setBeneficiaries(value){
    this.beneficiaries=value;
  }
  setUUID(value){
    this.uuid=value;
  }
  setLoginTime(value){
    this.loginTime=value;
  }
  //gets
  getLock(){
    return this.lock;
  }
  getOtp(){
    return this.otp;
  }
  getUsername(){
    return this.mobileNo;
  }
  getLoginTime(){
    return this.loginTime;
  }
  getUUID(){
    return this.uuid;
  }
  getPartialReg(){
    return this.partialReg;
  }
  getAccounts(){
    return this.accounts;
  }
  getLoanAccounts(){
    return this.loanAccounts;
  }
  getKitsMaximum(){
    return this.kitsMaximum;
  }
  getKitsMinimum(){
    return this.kitsMinimum;
  }
  getLinkedAccounts(){
    return this.linkedAccounts;
  }
  getCustomerNo(){
    return this.customerNo;
  }
  getKitsBanks(){
    return this.kitsBanks;
  }
  getBanks(){
    return this.banks;
  }
  getBeneficiaries(){
    return this.beneficiaries;
  }
  getEncryptedVars(userdata, password){
  
    let key =CryptoJS.enc.Hex.parse("ab765f9876543210abcdef98765abcd0");
  	let iv  = CryptoJS.enc.Hex.parse("cb765f9876543210abcdef9876543def");
    let key2=CryptoJS.enc.Hex.parse("f9876543210abcdef98765abcd0ab765");
    let iv2=CryptoJS.enc.Hex.parse("f9876543210abcdef9876543defcb765");
  	if(!password){
  	
      let encrypted = CryptoJS.AES.encrypt(userdata, key, {iv: iv});

    	encrypted=encrypted.ciphertext.toString(CryptoJS.enc.Base64);
      return encrypted;
    }
    else{
      let encrypted = CryptoJS.AES.encrypt(userdata, key2, {iv: iv2});

      encrypted=encrypted.ciphertext.toString(CryptoJS.enc.Base64);
      return encrypted;
    }
  	
  	
  }
  setLocationLoaded(data){
     this.locationsLoaded=new Array();
     this.locationsLoaded=data;

  }
  getLocationLoaded(){
      return this.locationsLoaded;
  }
  setCurrentLocation(data){
      this.currentLocation=data;
  }
  getCurrentLocation(){
      return this.currentLocation;
  }

}
