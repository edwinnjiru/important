import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
/*
  Generated class for the ApiConnect provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ApiConnect {
  data: any;
  constructor(public http: Http) {
    
  }
  load(requestType,body,endUrl,authority) {
      
	  let url="";
      if(authority=="standard"){
      	  body=JSON.parse(body);
	      
	      url='https://www.standardmedia.co.ke/rss/coop_bank_mobile_app_api.php?cat_id='+body.catid+'&story_id=&last_id=&userpass=obuggfd630986dsaewv735xfsghs838djkr'
	      if(requestType=='post'){
	  
			  let headers = new Headers({
						'Content-Type': 'application/x-www-form-urlencoded'
			  });
					  
			  let options = new RequestOptions({ headers: headers });
			  
			  // don't have the data yet
			  
			  return new Promise(resolve => {
			  
				// We're using Angular HTTP provider to request the data,
				// then on the response, it'll map the JSON data to a parsed JS object.
				// Next, we process the data and resolve the promise with the new data.
				
				this.http.post(url, body, options)
				  
				  .map(res => res.json())
				  .subscribe(data => {
				  
					// we've got back the raw data, now generate the core schedule data
					// and save the data for later reference
					
					this.data = data;
					resolve(this.data);
					
				  });
			  });
		   }
		   else{
		      return new Promise(resolve => {
	 
	            this.http.get(url).map(res => res.json()).subscribe(data => {
	 
	                this.data = data;
					
	                resolve(this.data);
	 
	            },err => console.log(err),
	            () => console.log('added something'));
	  
	         });
		   
		   }
	  }
	  else if(authority=="coop"){
	      //url = 'https://localhost:85/NEWAppUpgradeCak3/'+endUrl;
	      url = 'http://localhost:85/NEWAppUpgradeCak1/'+endUrl;
	      //http://localhost:85/NEWAppUpgradeCak1
	      if(requestType=='post'){
	  
			  let headers = new Headers({
						'Content-Type': 'application/x-www-form-urlencoded'
			  });
					  
			  let options = new RequestOptions({ headers: headers });
			  
			  // don't have the data yet
			  
			  return new Promise(resolve => {
			  
				// We're using Angular HTTP provider to request the data,
				// then on the response, it'll map the JSON data to a parsed JS object.
				// Next, we process the data and resolve the promise with the new data.
				
				this.http.post(url, body, options)
				  .map(res => res.json())
				  .timeout(40000)
				  .subscribe(data => {
				  
					// we've got back the raw data, now generate the core schedule data
					// and save the data for later reference
						this.data = data;
						console.log(this.data);
						resolve({data:this.data,error:""});
					
				    },
				    (err) => { 
				        console.log(err);
				       
						err="Connection error";
						
				        resolve({data:"",error:err}); 
					
					});
			    });
		   }
		   else{
		      return new Promise(resolve => {
	 
	            this.http.get(url).map(res => res.json()).subscribe(data => {
	 
	                this.data = data;
					
	                resolve(this.data);
	 
	            },err => console.log(err),
	            () => console.log('added something'));
	  
	         });
		   
		   }
	  
	    }

    }
}

