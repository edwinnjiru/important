import { Injectable } from '@angular/core';
import { ConnectivityService } from './connectivity-service';
import { Geolocation } from '@ionic-native/geolocation';

import { GlobalVars } from './global-vars';
 
declare var google;
 
@Injectable()
export class GoogleMaps {
 
  mapElement: any;
  pleaseConnect: any;
  map: any;
  mapInitialised: boolean = false;
  mapLoaded: any;
  mapLoadedObserver: any;
  markers: any = [];
  apiKey: string = "AIzaSyAVcO70yJo-4ExurxPnv3Z2OCxFSwsoT6Q";
  line: any=[];
 
  constructor(public connectivityService: ConnectivityService,private geolocation: Geolocation, public globalVars: GlobalVars) {
 
  }
 
  init(mapElement: any, pleaseConnect: any): Promise<any> {
 
    this.mapElement = mapElement;
    this.pleaseConnect = pleaseConnect;
 
    return this.loadGoogleMaps();
 
  }
 
  loadGoogleMaps(): Promise<any> {
 
    return new Promise((resolve) => {
 
      if(typeof google == "undefined" || typeof google.maps == "undefined"){
 
        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap();
 
        if(this.connectivityService.isOnline()){
 
          window['mapInit'] = () => {
 
            this.initMap();
 
            this.enableMap();
          }
 
          let script = document.createElement("script");
          script.id = "googleMaps";
 
          if(this.apiKey){
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
          } else {
            script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';       
          }
 
          document.body.appendChild(script);  
 
        } 
      }
      else {
 
        if(this.connectivityService.isOnline()){
          this.initMap();
          this.enableMap();
        }
        else {
          this.disableMap();
        }
 
      }
 
      this.addConnectivityListeners();
 
    });
 
  }
 
  initMap(): Promise<any> {
 
    this.mapInitialised = true;
 
    return new Promise((resolve) => {
 
      this.geolocation.getCurrentPosition().then((position) => {
 
        // UNCOMMENT FOR NORMAL USE
        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		this.globalVars.setCurrentLocation({latitude:position.coords.latitude, longitude:position.coords.longitude});
 
        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
 
        this.map = new google.maps.Map(this.mapElement, mapOptions);
		this.addMarker("My Spot",position.coords.latitude, position.coords.longitude,"http://maps.google.com/mapfiles/ms/icons/green-dot.png");
		let locations = this.globalVars.getLocationLoaded();
           
		for(let location of locations){
			this.addMarker(location.title,location.latitude, location.longitude,"http://maps.google.com/mapfiles/ms/icons/red-dot.png");
		}
		this.drawRoute({latitude:this.globalVars.getCurrentLocation().latitude,longitude:this.globalVars.getCurrentLocation().longitude},{latitude: this.globalVars.getLocationLoaded()[0].latitude ,longitude: this.globalVars.getLocationLoaded()[0].longitude});
        resolve(true);
 
      });
 
    });
 
  }
 
  disableMap(): void {
 
    if(this.pleaseConnect){
      this.pleaseConnect.style.display = "block";
    }
 
  }
 
  enableMap(): void {
 
    if(this.pleaseConnect){
      this.pleaseConnect.style.display = "none";
    }
 
  }
 
  addConnectivityListeners(): void {
 
    document.addEventListener('online', () => {
 
      console.log("online");
 
      setTimeout(() => {
 
        if(typeof google == "undefined" || typeof google.maps == "undefined"){
          this.loadGoogleMaps();
        } 
        else {
          if(!this.mapInitialised){
            this.initMap();
          }
 
          this.enableMap();
        }
 
      }, 2000);
 
    }, false);
 
    document.addEventListener('offline', () => {
 
      console.log("offline");
 
      this.disableMap();
 
    }, false);
 
  }
 
  addMarker(title,lat: number, lng: number, iconUrl: string): void {
 
    let latLng = new google.maps.LatLng(lat, lng);
 
    let marker = new google.maps.Marker({
	  icon: iconUrl,
      map: this.map,
      position: latLng
    });
	
    let content = "<h4>"+title+"</h4>";          
 
    this.addInfoWindow(marker, content);
	
    this.markers.push(marker);  

 
  }
  removeMarkers(){
      for(var i=0;i<this.markers.length;i++){
	     this.markers[i].setMap(null);
	  }
	  
  }
  removeMarkersComp(){
      this.markers=[];
  }
  addInfoWindow(marker, content){
 
	  let infoWindow = new google.maps.InfoWindow({
		content: content
	  });
	 
	  google.maps.event.addListener(marker, 'click', () => {
		infoWindow.open(this.map, marker);
	  });
	 
  }
  drawRoute(myPos,desPos){
    this.removePolyline();
	let currentPos=new google.maps.LatLng(myPos.latitude, myPos.longitude);
	let closestPos=new google.maps.LatLng(desPos.latitude, desPos.longitude);
  
    let lat_lng = new Array(currentPos,closestPos);
	//Initialize the Path Array
	let path = new google.maps.MVCArray();

	//Initialize the Direction Service
	let service = new google.maps.DirectionsService();

	//Set the Path Stroke Color
	let poly = new google.maps.Polyline({ map: this.map, strokeColor: '#4986E7' });
	this.line.push(poly);
	
	//Loop and Draw Path Route between the Points on MAP
	for (var i = 0; i < lat_lng.length; i++) {
		
		if ((i + 1) < lat_lng.length) {
			
			var src = lat_lng[i];
			var des = lat_lng[i + 1];
			path.push(src);
			poly.setPath(path);
			service.route({
				origin: src,
				destination: des,
				travelMode: google.maps.DirectionsTravelMode.DRIVING
			}, function (result, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
						path.push(result.routes[0].overview_path[i]);
					}
				}
			});
		}
	}
	
	
  }
  removePolyline(){
      for(var i=0;i<this.line.length;i++){
	     this.line[i].setMap(null);
	  }
	  
  }
 
}
