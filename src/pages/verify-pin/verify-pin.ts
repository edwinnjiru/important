import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Platform } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { GlobalVars } from '../../providers/global-vars';
import { ApiConnect } from '../../providers/api-connect';
import { ConnectivityService } from '../../providers/connectivity-service';
import { SecureStorage,  SecureStorageObject } from '@ionic-native/secure-storage';
import * as $ from 'jquery'
/**
 * Generated class for the VerifyPin page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({name:'VerifyPin'})
@Component({
  selector: 'page-verify-pin',
  templateUrl: 'verify-pin.html',
})
export class VerifyPin {
  objectblock: any = {};
  error:number=0;
  data:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: Toast,
   public modalCtrl: ModalController, public globalVars: GlobalVars, public platform: Platform, public apiConnect: ApiConnect,
    public connectivityService: ConnectivityService, private secureStorage: SecureStorage, public loadingController: LoadingController) {
  	this.objectblock={};
  	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyPin');
  }
  submit(objectblock){

  	this.error=0;
    if(objectblock.vcode==""||objectblock.vcode==undefined){
        this.error=1;
    }
    else if(!(/^\d+$/.test(objectblock.vcode))){
        this.error=1;
    }
    else if(objectblock.pin==""||objectblock.pin==undefined){
        this.error=2;
    }
    else if(!(/^\d+$/.test(objectblock.pin))){
        this.error=2;
    }
    else if(objectblock.pin.length<4){
        this.error=2;
    }
    
    else{
    	let otp=this.globalVars.getOtp();
    	if(otp.indexOf(objectblock.vcode)>=0){
	        if(this.connectivityService.isOnline()){
		        let loader = this.loadingController.create({content:'Updating Customer Details'});
                loader.present();
                let pass=this.globalVars.getEncryptedVars(objectblock.pin,true);

		        let uuid=this.globalVars.getUUID();
		        let user=this.globalVars.getUsername();
		        let body=JSON.stringify({
		            username: user,
		            password: pass,
		            uuid:uuid
		        });

		        let endUrl="VerifyCustomerUpdateUUID.php";
		        body=$.param({data: this.globalVars.getEncryptedVars(body,false)});
		        let requestType="post";
		        let authority="coop";
	            this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
	            	loader.dismiss();
	                this.data=data;
	                if(this.data.error==""){
	                  if(this.data.data.success){
	                    let encrypted = this.globalVars.doAESencrypt(user, uuid);
	                    this.platform.ready().then(() => {
		                    this.secureStorage.create('mcoopcash_store')
		                    .then((storage: SecureStorageObject) => {

		                      storage.set('userid',encrypted)
		                      .then(data => {
		                        this.navCtrl.push('Login');
		                      },
		                      error => {
		                          // do nothing - it just means it doesn't exist
		                          console.log("err "+error);
		                      });
		                    });
		                });

	                  } 
	                  else{
	                    let template="<div>"+this.data.data.message+ " </div>";
	                    let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'Login'};
	                    let myModal = this.modalCtrl.create('ConfirmModal',obj);
	                    myModal.present();
	                  }
	                }
	                else{
	                  let template="<div>"+this.data.error+ " </div>";
	                  let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'Login'};
	                  let myModal = this.modalCtrl.create('ConfirmModal',obj);
	                  myModal.present();
	                }
	            });
		    }
		    else{
		        this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
		            toast => {
		        });
		    }
	    }
	    else{
            let template="<div>Invalid Credentials</div>";
            let obj={body:"", template:template, endUrl:"", completed:true, pageTo:'Login'};
            let myModal = this.modalCtrl.create('ConfirmModal',obj);
            myModal.present();
	    }
        
    }

  }
  

}
