import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerifyPin } from './verify-pin';

@NgModule({
  declarations: [
    VerifyPin,
  ],
  imports: [
    IonicPageModule.forChild(VerifyPin),
  ],
  exports: [
    VerifyPin
  ]
})
export class VerifyPinModule {}
