import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Withdraw } from './withdraw';

@NgModule({
  declarations: [
    Withdraw,
  ],
  imports: [
    IonicPageModule.forChild(Withdraw),
  ],
  exports: [
    Withdraw
  ]
})
export class WithdrawModule {}
