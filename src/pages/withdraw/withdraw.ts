import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ApiConnect } from '../../providers/api-connect';
import { ConnectivityService } from '../../providers/connectivity-service';
import { Toast } from '@ionic-native/toast';
import * as $ from 'jquery'
/**
 * Generated class for the WithrawPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-withdraw',
  templateUrl: 'withdraw.html',
})
export class Withdraw {
  objectblock:any={};
  accounts:any=[];
  data:any;
  error:number=0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVars: GlobalVars, 
    public apiConnect: ApiConnect, public loadingController: LoadingController,
    private toast: Toast, public connectivityService: ConnectivityService, public modalCtrl: ModalController) {
  	this.accounts=globalVars.getAccounts();
    this.objectblock.account=this.accounts[0];
    this.objectblock.provider="Agent";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WithrawPage');
  }
  submit(objectblock){
  	this.error=0;
  	if(objectblock.proider=="Agent" && (objectblock.agentno==""||objectblock.agentno==undefined)){
        this.error=1;
    }
  	else if(objectblock.amount==""||objectblock.amount==undefined){
        this.error=2;
    }
    else{
	    if(this.connectivityService.isOnline()){

	      let loader = this.loadingController.create({content:'Withdarwing'});
	      loader.present();
	      let body="";
	      let body2="";
	      if(objectblock.provider=="Agent"){
		      
		  	  body=JSON.stringify({
		        fetchtype: "charge",
	            ttype: "agentcashwithdrawal",
                username: this.globalVars.getUsername(),
                accountfrom: objectblock.account,
                agentno: objectblock.agentno,
                amount: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
	            uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
		        ttype: "agentcashwithdrawal",
                username: this.globalVars.getUsername(),
                accountfrom: objectblock.account,
                agentno: objectblock.agentno,
                amount: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
	            uuid: this.globalVars.getUUID()
		      });
			  
		  }
		  else{
		  	  body=JSON.stringify({
		        fetchtype: "charge",
	            ttype: "atmcashwithdrawal",
                username: this.globalVars.getUsername(),
                accountfrom: objectblock.account,
                amount: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
	            uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
	            ttype: "atmcashwithdrawal",
                username: this.globalVars.getUsername(),
                accountfrom: objectblock.account,
                amount: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
	            uuid: this.globalVars.getUUID()
			   });   
		  }
	    
	      let endUrl="GetData.php";
	      body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

	      let requestType="post";
	      let authority="coop";
	      this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
	        loader.dismiss();
	        this.data=data;
	        if(this.data.error==""){
	          if(this.data.data.success){
	          	let template="";
	          	if(objectblock.provider=='Atm'){
	              template="<div>Withdraw KShs."+objectblock.amount+" from CO-OP Bank ATM from account: "+objectblock.account+"<br>"+this.data.data.message+"</div>";
	            }
	            else{
	              template="<div>Withdraw KShs."+objectblock.amount+" from CO-OP Bank agent: "+objectblock.agentno+" from account: "+objectblock.account+"<br>"+this.data.data.message+"</div>";
	            }
	            let obj={body:body2, template:template, endUrl:endUrl, completed:false, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	            
	          }
	          else{

	            let template="<div>"+this.data.data.message+"</div>";
	            let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	          }
	        }
	        else{
	          console.log(this.data.error);
	          let template="<div>"+this.data.error+"</div>";
	          let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	          let myModal = this.modalCtrl.create('ConfirmModal',obj);
	          myModal.present();
	        }
	        
	      });
	    }
	    else{
	          this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
	            toast => {
	            console.log(toast);
	        });
	    }
    }

  }

}
