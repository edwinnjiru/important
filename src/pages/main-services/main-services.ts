import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MainServices page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({name:'MainServices'})
@Component({
  selector: 'page-main-services',
  templateUrl: 'main-services.html',
})
export class MainServices {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainServices');
  }
  openPage(page){
    if(page=='Balance'){
      this.navCtrl.push('AccountEnquiries',{title:"Check balance"});
    }
    else{
      this.navCtrl.push(page);
    }
  }

}
