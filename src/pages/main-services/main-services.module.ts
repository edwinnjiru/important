import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainServices } from './main-services';

@NgModule({
  declarations: [
    MainServices,
  ],
  imports: [
    IonicPageModule.forChild(MainServices),
  ],
  exports: [
    MainServices
  ]
})
export class MainServicesModule {}
