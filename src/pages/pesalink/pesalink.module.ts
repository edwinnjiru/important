import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PesalinkPage } from './pesalink';

@NgModule({
  declarations: [
    PesalinkPage,
  ],
  imports: [
    IonicPageModule.forChild(PesalinkPage),
  ],
  exports: [
    PesalinkPage
  ]
})
export class PesalinkPageModule {}
