import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubmenusPage } from './submenus';

@NgModule({
  declarations: [
    SubmenusPage,
  ],
  imports: [
    IonicPageModule.forChild(SubmenusPage),
  ],
  exports: [
    SubmenusPage
  ]
})
export class SubmenusPageModule {}
