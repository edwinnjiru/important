import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';

/**
 * Generated class for the SelfRegistration page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-self-registration',
  templateUrl: 'self-registration.html',
})
export class SelfRegistration {

  objectblock: any = {};
  error:number=0;
  data:any;
  firstform:boolean=true;
  selectedPage:string='pagerGrey';
  selectedPage2:string='pagerGreen';

  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: Toast,
   public modalCtrl: ModalController) {
     this.objectblock={};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelfRegistration');
  }
  submit(objectblock){

  	this.error=0;
    if(objectblock.mobile==""||objectblock.mobile==undefined){
        this.error=1;
        this.openPage("1");
    }
    else if(!(/^\d{10}$/.test(objectblock.mobile))){
        this.error=1;
        this.openPage("1");
    }
    else if(objectblock.first==""||objectblock.first==undefined){
        this.error=2;
        this.openPage("1");
    }
    else if(objectblock.second==""||objectblock.second==undefined){
        this.error=3;
        this.openPage("1");
    }
    else if(objectblock.last==""||objectblock.last==undefined){
        this.error=4;
        this.openPage("1");
    }
    else if(objectblock.id==""||objectblock.id==undefined){
        this.error=5;
        this.openPage("2");
    }
    else if(objectblock.dob==""||objectblock.dob==undefined){
        this.error=6;
        this.openPage("2");
    }
    else{
        let coopaccount=objectblock.account;
        if(objectblock.account==""||objectblock.account==undefined){
          coopaccount="none";
        }
        if(objectblock.dob.day.length==1){
          objectblock.dob.day="0"+objectblock.dob.day;
        }
        else if(objectblock.dob.month.length==1){
          objectblock.dob.month="0"+objectblock.dob.month;
        }
    	  let dob=""+objectblock.dob.day+"-"+objectblock.dob.month+"-"+objectblock.dob.year;
        
        let body=JSON.stringify({
        	  action: 'SELFREGISTER',
            mobile: objectblock.mobile,
            fname: objectblock.first,
            sname: objectblock.second,
            lname: objectblock.last,
            idpass: objectblock.id,
            dob: dob,
            coopacc: objectblock.account,
            uuid:"3889398398"
        });
        
        let endUrl="selfregister.php";
        let template="<div>Register "+objectblock.first+" "+objectblock.second+" "+objectblock.last+"<br>ID number "+objectblock.id+"<br>Date of Birth "+dob+"<br>Co-op Account "+coopaccount+"</div>";
		  	let obj={body:body, template:template, endUrl:endUrl, completed:false, pageTo:'Login'};
        let myModal = this.modalCtrl.create('ConfirmModal',obj);
        myModal.present();
        
    }

  }
  

  openPage(page){
  	if(page=="1"){
  		this.firstform=true;
      this.selectedPage='pagerGrey';
      this.selectedPage2='pagerGreen';
  		
  	}
  	else{
  		this.firstform=false;
      this.selectedPage='pagerGreen';
      this.selectedPage2='pagerGrey';
  	}
  }

}
