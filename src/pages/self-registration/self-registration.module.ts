import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelfRegistration } from './self-registration';

@NgModule({
  declarations: [
    SelfRegistration,
  ],
  imports: [
    IonicPageModule.forChild(SelfRegistration),
  ],
  exports: [
    SelfRegistration
  ]
})
export class SelfRegistrationModule {}
