import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Airtime } from './airtime';

@NgModule({
  declarations: [
    Airtime,
  ],
  imports: [
    IonicPageModule.forChild(Airtime),
  ],
  exports: [
    Airtime
  ]
})
export class AirtimeModule {}
