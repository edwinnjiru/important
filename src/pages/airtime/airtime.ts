import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ContactsProvider } from '../../providers/contacts';
import { ApiConnect } from '../../providers/api-connect';
import { ConnectivityService } from '../../providers/connectivity-service';
import { AlertServiceProvider } from '../../providers/alert-service/alert-service';
import { Toast } from '@ionic-native/toast';
import * as $ from 'jquery'

/**
 * Generated class for the Airtime page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-airtime',
  templateUrl: 'airtime.html',
})
export class Airtime {
  objectblock:any={};
  accounts:any=[];
  data:any;
  showmine:boolean=true;
  error:number=0;
  contact:any;
  cname:string="";

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVars: GlobalVars, 
    public apiConnect: ApiConnect, public loadingController: LoadingController,
    private toast: Toast, public connectivityService: ConnectivityService, public modalCtrl: ModalController,
     public contactsProvider: ContactsProvider, public alertService: AlertServiceProvider) {
    this.accounts=globalVars.getAccounts();
    this.objectblock.account=this.accounts[0];
    this.objectblock.provider="Safaricom";
    this.objectblock.receiver="My number";
    if(this.objectblock.receiver=="My number"){
      this.showmine=true;
      this.objectblock.receiverno=this.globalVars.getUsername();
      
  	}
  	else{
  	  this.objectblock.receiverno="";
  	  this.showmine=false;
  	  this.cname="";
  	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Airtime');
  }
  checkRecepeint(receiver){
  	if(receiver=="My number"){
      this.showmine=true;
      this.objectblock.receiverno=this.globalVars.getUsername();
  	}
  	else{
  	  this.objectblock.receiverno="";
  	  this.showmine=false;
  	  this.cname="";
  	}
  }
  pickContact(){
  	this.contactsProvider.pickContact().then(contact => {
  	   console.log(contact);
  	   if(contact.phoneNo.length>1){
           this.alertService.showRadioAlert(contact.phoneNo).then(data => {
           	  this.objectblock.receiverno=data;
           	  if(this.objectblock.receiverno!=""||this.objectblock.receiverno!=undefined){
			    this.cname=contact.name;
			  }
           });
  	   }
  	   else{
	       this.objectblock.receiverno=contact.phoneNo[0];
	       if(this.objectblock.receiverno!=""||this.objectblock.receiverno!=undefined){
		       this.cname=contact.name;
		   }
	   }
       
  	});
  }
  
  submit(objectblock){
  	this.error=0;
  	if(objectblock.amount==""||objectblock.amount==undefined){
        this.error=1;
    }
    else if(objectblock.receiverno==""||objectblock.receiverno==undefined){
        this.error=2;
    }
    else if(!(/^\d{10}$/.test(objectblock.receiverno))){
        this.error=2;
    } 
    else{
	    if(this.connectivityService.isOnline()){
	      let telco="";
	      if(objectblock.provider=="Safaricom"){
	        telco="SAF"
	      }
	      else if(objectblock.provider=="Aitrel"){
	        telco="AIRTEL"
	      }
	      let loader = this.loadingController.create({content:'Buying airtime'});
	      loader.present();
	      let body="";
	      let body2="";
	      if(objectblock.receiver=="My number"){
		      body=JSON.stringify({
		        fetchtype: "charge",
		        ttype: "ownAirtimetopup",
		        username: this.globalVars.getUsername(),
		        telcos: telco,
		        ownacctocharge: objectblock.account,
		        ownAirtimeAmount: objectblock.amount,
		        logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
		        ttype: "ownAirtimetopup",
		        username: this.globalVars.getUsername(),
		        telcos: telco,
		        ownacctocharge: objectblock.account,
		        ownAirtimeAmount: objectblock.amount,
		        logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		  }
		  else{
		  	  body=JSON.stringify({
		        fetchtype: "charge",
		        ttype: "otherAirtimetopup",
	            username: this.globalVars.getUsername(),
	            othertelcos:  telco,
	            otheracctocharge: objectblock.account,
	            otherMobileNumber: objectblock.receiverno,
	            otherAirtimeAmount: objectblock.amount,
	            logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
		        ttype: "otherAirtimetopup",
	            username: this.globalVars.getUsername(),
	            othertelcos:  telco,
	            otheracctocharge: objectblock.account,
	            otherMobileNumber: objectblock.receiverno,
	            otherAirtimeAmount: objectblock.amount,
	            logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		  }
	    
	      let endUrl="GetData.php";
	      body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

	      let requestType="post";
	      let authority="coop";
	      this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
	        loader.dismiss();
	        this.data=data;
	        if(this.data.error==""){
	          if(this.data.data.success){
	            
	            let template="<div>Buy KShs."+objectblock.amount+" "+objectblock.provider+" airtime for mobile no: "+objectblock.receiverno+" from account: "+objectblock.account+"<br>"+this.data.data.message+"</div>";
	            let obj={body:body2, template:template, endUrl:endUrl, completed:false, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	            
	          }
	          else{

	            let template="<div>"+this.data.data.message+"</div>";
	            let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	          }
	        }
	        else{
	          console.log(this.data.error);
	          let template="<div>"+this.data.error+"</div>";
	          let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	          let myModal = this.modalCtrl.create('ConfirmModal',obj);
	          myModal.present();
	        }
	        
	      });
	    }
	    else{
	          this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
	            toast => {
	            console.log(toast);
	        });
	    }
    }

  }
}
