import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ApiConnect } from '../../providers/api-connect';
import { ConnectivityService } from '../../providers/connectivity-service';
import { ContactsProvider } from '../../providers/contacts';
import { AlertServiceProvider } from '../../providers/alert-service/alert-service';
import { Toast } from '@ionic-native/toast';
import * as $ from 'jquery'
/**
 * Generated class for the SendMoney page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({name:'sendmoney'})
@Component({
  selector: 'page-send-money',
  templateUrl: 'send-money.html',
})
export class SendMoney {
  objectblock:any={};
  accounts:any=[];
  data:any;
  showmine:boolean=true;
  error:number=0;
  wallettype:string="";
  contact:any;
  cname:string="";

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVars: GlobalVars, 
    public apiConnect: ApiConnect, public loadingController: LoadingController,
    private toast: Toast, public connectivityService: ConnectivityService, public modalCtrl: ModalController
    , public contactsProvider: ContactsProvider, public alertService: AlertServiceProvider) {
  	this.accounts=globalVars.getAccounts();
    this.objectblock.account=this.accounts[0];
    this.objectblock.provider="MCo-opCash";
    this.objectblock.receiver="My number";
    if(this.objectblock.receiver=="My number"){
      this.showmine=true;
      this.objectblock.receiverno=this.globalVars.getUsername();
  	}
  	else{
  	  this.objectblock.receiverno="";
  	  this.showmine=false;
  	  this.cname="";
  	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SendMoney');
  }
  checkRecepeint(receiver){
  	if(receiver=="My number"){
      this.showmine=true;
      this.objectblock.receiverno=this.globalVars.getUsername();
  	}
  	else{
  	  this.objectblock.receiverno="";
  	  this.showmine=false;
  	  this.cname="";
  	}
  }
  pickContact(){
  	this.contactsProvider.pickContact().then(contact => {
  	   console.log(contact);
  	   if(contact.phoneNo.length>1){
           this.alertService.showRadioAlert(contact.phoneNo).then(data => {
           	  this.objectblock.receiverno=data;
           	  if(this.objectblock.receiverno!=""||this.objectblock.receiverno!=undefined){
			    this.cname=contact.name;
			  }
           });
  	   }
  	   else{
	       this.objectblock.receiverno=contact.phoneNo[0];
	       if(this.objectblock.receiverno!=""||this.objectblock.receiverno!=undefined){
		       this.cname=contact.name;
		   }
	   }
       
  	});
  }
  submit(objectblock){
  	this.error=0;
  	if(objectblock.amount==""||objectblock.amount==undefined){
        this.error=1;
    }
    else if(objectblock.receiverno==""||objectblock.receiverno==undefined){
        this.error=2;
    }
    else if(!(/^\d{10}$/.test(objectblock.receiverno))){
        this.error=2;
    } 
    else{
	    if(this.connectivityService.isOnline()){

	      if(objectblock.provider=="airtelmoney"){
	      	this.wallettype = "AIRTELMONEYOUT";
	      }else{
	      	this.wallettype = "MPESAOUT";
	      }
	      
	      let loader = this.loadingController.create({content:'Sending money'});
	      loader.present();
	      let body="";
	      let body2="";
	      if(objectblock.provider!="MCo-opCash"){
		      if(objectblock.receiver=="My number"){
			      body=JSON.stringify({
			        fetchtype: "charge",
			        ttype: "thiswalletMobileMoney",
			        username: this.globalVars.getUsername(),
			        sourceacc: objectblock.account,
	                wallettype: this.wallettype,
			        amount: objectblock.amount,
			        logintime: this.globalVars.getLoginTime(),
			        uuid: this.globalVars.getUUID()
			      });
			      body2=JSON.stringify({
			        fetchtype: "transact",
			        ttype: "thiswalletMobileMoney",
			        username: this.globalVars.getUsername(),
			        sourceacc: objectblock.account,
	                wallettype: this.wallettype,
			        amount: objectblock.amount,
			        logintime: this.globalVars.getLoginTime(),
			        uuid: this.globalVars.getUUID()
			      });
			  }
			  else{
			  	  body=JSON.stringify({
			        fetchtype: "charge",
			        ttype: "sendtootherwallet",
			        username: this.globalVars.getUsername(),
	                sourceacc: objectblock.account,
	                destwallettotype: this.wallettype,
	                destinattionacc: objectblock.receiverno,
	                amount: objectblock.amount,
	                logintime: this.globalVars.getLoginTime(),
		            uuid: this.globalVars.getUUID()
			      });
			      body2=JSON.stringify({
			        fetchtype: "transact",
			        ttype: "sendtootherwallet",
			        username: this.globalVars.getUsername(),
	                sourceacc: objectblock.account,
	                destwallettotype: this.wallettype,
	                destinattionacc: objectblock.receiverno,
	                amount: objectblock.amount,
	                logintime: this.globalVars.getLoginTime(),
		            uuid: this.globalVars.getUUID()
			      });
			  }
		  }
		  else{
		  	 if(objectblock.receiver=="My number"){
			      body=JSON.stringify({
			        fetchtype: "charge",
			        ttype: "ftown",
					username: this.globalVars.getUsername(),
					accFrom: objectblock.account,
					accTo: this.globalVars.getUsername(),
					txnAmount: objectblock.amount,
					logintime: this.globalVars.getLoginTime(),
			        uuid: this.globalVars.getUUID()
			      });
			      body2=JSON.stringify({
			        fetchtype: "transact",
			        ttype: "ftown",
					username: this.globalVars.getUsername(),
					accFrom: objectblock.account,
					accTo: this.globalVars.getUsername(),
					txnAmount: objectblock.amount,
					logintime: this.globalVars.getLoginTime(),
			        uuid: this.globalVars.getUUID()
			      });
			  }
			  else{
			  	  body=JSON.stringify({
			        fetchtype: "charge",
			        ttype: "ftother",
                    username: this.globalVars.getUsername(),
                    sourceacc: objectblock.account,
                    destacc: objectblock.receiverno,
                    amount: objectblock.amount,
                    saccoid: "1234",
                    destacctype: "mwallet",
                    logintime: this.globalVars.getLoginTime(),
		            uuid: this.globalVars.getUUID()
			      });
			      body2=JSON.stringify({
			        fetchtype: "transact",
		            ttype: "ftother",
                    username: this.globalVars.getUsername(),
                    sourceacc: objectblock.account,
                    destacc: objectblock.receiverno,
                    amount: objectblock.amount,
                    saccoid: "1234",
                    destacctype: "mwallet",
                    logintime: this.globalVars.getLoginTime(),
		            uuid: this.globalVars.getUUID()
			      });
			  }
		  }
	    
	      let endUrl="GetData.php";
	      body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

	      let requestType="post";
	      let authority="coop";
	      this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
	        loader.dismiss();
	        this.data=data;
	        if(this.data.error==""){
	          if(this.data.data.success){
	            let template="<div>Send KShs."+objectblock.amount+" to  "+objectblock.provider+" number: "+objectblock.receiverno+" from account: "+objectblock.account+"<br>"+this.data.data.message+"</div>";
	            let obj={body:body2, template:template, endUrl:endUrl, completed:false, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	            
	          }
	          else{

	            let template="<div>"+this.data.data.message+"</div>";
	            let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	          }
	        }
	        else{
	          console.log(this.data.error);
	          let template="<div>"+this.data.error+"</div>";
	          let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	          let myModal = this.modalCtrl.create('ConfirmModal',obj);
	          myModal.present();
	        }
	        
	      });
	    }
	    else{
	          this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
	            toast => {
	            console.log(toast);
	        });
	    }
    }

  }


}
