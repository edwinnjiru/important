import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendMoney } from './send-money';

@NgModule({
  declarations: [
    SendMoney,
  ],
  imports: [
    IonicPageModule.forChild(SendMoney),
  ],
  exports: [
    SendMoney
  ]
})
export class SendMoneyModule {}
