import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';

import { ConnectivityService } from '../../providers/connectivity-service';

/**
 * Generated class for the News page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class News {

  constructor(public navCtrl: NavController, public navParams: NavParams, public connectivityService: ConnectivityService, private toast: Toast) {
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad News');
  }
  openNews(title,id){
    if(this.connectivityService.isOnline()){
      this.navCtrl.push('NewsList',{
  	    title:title,
    		id:id
    	});
    }
    else{
      this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
        toast => {
        console.log(toast);
        }
      );
    }
  }

}
