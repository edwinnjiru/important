import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ConnectivityService } from '../../providers/connectivity-service';
import { AlertServiceProvider } from '../../providers/alert-service/alert-service';
import { Toast } from '@ionic-native/toast';
import * as $ from 'jquery';
/**
 * Generated class for the ChangePinPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-change-pin',
  templateUrl: 'change-pin.html',
})
export class ChangePinPage {
  objectblock:any={};
  error:number=0;
  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVars: GlobalVars,
    private toast: Toast, public connectivityService: ConnectivityService, public modalCtrl: ModalController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePinPage');
  }
  submit(objectblock){
  	if(objectblock.current==""||objectblock.current==undefined||!(/^\d{4}$/.test(objectblock.current))){
        this.error=1;
    }
	  else if(objectblock.new==""||objectblock.new==undefined||!(/^\d{4}$/.test(objectblock.new))){
        this.error=2;
    }
    else if(objectblock.confirm==""||objectblock.confirm==undefined||!(/^\d{4}$/.test(objectblock.confirm))){
        this.error=3;
    }
    else if(objectblock.confirm!=objectblock.new){
    	this.error=4;
    }
    else{
	    if(this.connectivityService.isOnline()){
	      
	      let current=this.globalVars.getEncryptedVars(objectblock.current,true);
	      let newpin=this.globalVars.getEncryptedVars(objectblock.new,true);
	      let confirm=this.globalVars.getEncryptedVars(objectblock.confirm,true);

	      let body=JSON.stringify({
	        username: this.globalVars.getUsername(),
	        currentpin: current,
            newpin: newpin,
            confirmpin: confirm,
	        logintime: this.globalVars.getLoginTime(),
	        uuid: this.globalVars.getUUID()
	      });
	      console.log(current);
	      
	      let endUrl="pinchange.php";
	      body=$.param({data: this.globalVars.getEncryptedVars(body,false)});
          this.objectblock={};
	      let template="<div>Change PIN</div>";
	      let obj={body:body, template:template, endUrl:endUrl, completed:false, pageTo:'Login'};
	      let myModal = this.modalCtrl.create('ConfirmModal',obj);
	      myModal.present();
	    }
	    else{
	          this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
	            toast => {
	            console.log(toast);
	        });
	    }
	}
  
  }


}
