import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ContactsProvider } from '../../providers/contacts';
import { ApiConnect } from '../../providers/api-connect';
import { ConnectivityService } from '../../providers/connectivity-service';
import { AlertServiceProvider } from '../../providers/alert-service/alert-service';
import { Toast } from '@ionic-native/toast';
import * as $ from 'jquery';

/**
 * Generated class for the BanktransfersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-banktransfers',
  templateUrl: 'banktransfers.html',
})
export class BanktransfersPage {
  objectblock:any={};
  accounts:any=[];
  data:any;
  showmine:boolean=true;
  error:number=0;
  banks:any=[];
  rawBanks:any=[];
  branches:any=[];
  rawBranches:any=[];
  beneficiaries:any=[];
  rawBeneficiaries:any=[];
  selectOptionsBeneficiary:any;
  selectOptionsBranch:any;
  selectOptionsBank:any;
  myaccountTypes:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVars: GlobalVars, 
    public apiConnect: ApiConnect, public loadingController: LoadingController,
    private toast: Toast, public connectivityService: ConnectivityService, public modalCtrl: ModalController,
     public contactsProvider: ContactsProvider, public alertService: AlertServiceProvider) {

  	this.objectblock.provider="My account";

  	this.selectOptionsBeneficiary={
  		title: 'Beneficiaries',
        subTitle: 'Select beneficiary',
  	}
  	this.selectOptionsBank={
  		title: 'Banks',
        subTitle: 'Select bank',
  	}
  	this.selectOptionsBranch={
  		title: 'Branches',
        subTitle: 'Select branch',
  	}

  	this.accounts=globalVars.getAccounts();
    this.objectblock.accountfrom=this.accounts[0];
    this.objectblock.accountto=this.accounts[0];
    this.myaccountTypes = [{
        val: "mwallet",
        name: "MCo-op Cash"
    }, {
        val: "coopbank",
        name: "COOP BANK ACCOUNT"
    }, {
        val: "sacco",
        name: "SACCO ACCOUNT"
    }, {
        val: "creditcard",
        name: "CREDIT CARD"
    }, {
        val: "virtualcard",
        name: "VIRTUAL CARD"
    }];

    this.objectblock.myaccountType = this.myaccountTypes[0];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BanktransfersPage');
  }
  submit(objectblock){
  	console.log(objectblock.saveben);
  	this.error=0;
  	
  	if(objectblock.accountfrom==""||objectblock.accountfrom==undefined){
        this.error=1;
    }
	  else if(objectblock.amount==""||objectblock.amount==undefined){
        this.error=3;
    }
    else if(objectblock.accountto==""||objectblock.accountto==undefined){
        this.error=2;
    }
    else if(objectblock.provider=="Other Co-op account"&&objectblock.myaccountType.name=="SACCO ACCOUNT"){
        if(objectblock.saccoid==""||objectblock.saccoid==undefined){
		    this.error=4;
		}
		else{
			this.doTransfer(objectblock);
		}
    }
    else if(objectblock.saveben){
    	if(objectblock.benename==""||objectblock.benename==undefined){
		    this.error=5;
		}
		else{
			this.saveBeneficiary(objectblock);
		}
    }
    else{
       this.doTransfer(objectblock);
    }
	
  }
  doTransfer(objectblock){
  	if(this.connectivityService.isOnline()){
	      let loader = this.loadingController.create({content:'Transferring Cash'});
	      loader.present();
	      let body="";
	      let body2="";
	      if(objectblock.provider=="My account"){
		      body=JSON.stringify({
		      	fetchtype: "charge",
                ttype: "ftown",
                username: this.globalVars.getUsername(),
                accFrom: objectblock.accountfrom,
                accTo: objectblock.accountto,
                txnAmount: objectblock.amount, 
		        logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
                ttype: "ftown",
                username: this.globalVars.getUsername(),
                accFrom: objectblock.accountfrom,
                accTo: objectblock.accountto,
                txnAmount: objectblock.amount, 
		        logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		  }
		  else if(objectblock.provider=="Other Co-op account"){
		  	  body=JSON.stringify({
		  	  	fetchtype: "charge",
                ttype: "ftother",
                username: this.globalVars.getUsername(),
                sourceacc: objectblock.accountfrom,
                destacc: objectblock.accountto,
                amount: objectblock.amount,
                saccoid: objectblock.saccoid,
                destacctype: objectblock.myaccountType.val,
                logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
		        ttype: "ftother",
                username: this.globalVars.getUsername(),
                sourceacc: objectblock.accountfrom,
                destacc: objectblock.accountto,
                amount: objectblock.amount,
                saccoid: objectblock.saccoid,
                destacctype: objectblock.myaccountType.val,
                logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		  }
		  else if(objectblock.provider=="Other bank"){
		  	  body=JSON.stringify({
		  	  	fetchtype: "charge",
                ttype: "ftotherbank",
                username: this.globalVars.getUsername(),
                bankcode: objectblock.bank.code,
                branchcode: objectblock.branch.code,
                otherbankfromacct: objectblock.accountfrom,
                beneficiaryacct: objectblock.accountto,
                otherbankamount: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		        
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
		        ttype: "ftotherbank",
                username: this.globalVars.getUsername(),
                bankcode: objectblock.bank.code,
                branchcode: objectblock.branch.code,
                otherbankfromacct: objectblock.accountfrom,
                beneficiaryacct: objectblock.accountto,
                otherbankamount: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		  }
		  else if(objectblock.provider=="Saved Beneficiary"){
		  	  objectblock.accountto=objectblock.beneficiary.code;
		  	  body=JSON.stringify({
		  	  	fetchtype: "charge",
                ttype: "fttobeneficiaries",
                username: this.globalVars.getUsername(),
                sourceacc: objectblock.account,
                benaccount: objectblock.beneficiary.code,
                amount: objectblock.amountben,
                bensortcode: objectblock.beneficiary.code,
                benname: objectblock.beneficiary.name,
                customerno: this.globalVars.getCustomerNo(),
                logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
		        ttype: "fttobeneficiaries",
                username: this.globalVars.getUsername(),
                sourceacc: objectblock.account,
                benaccount: objectblock.beneficiary.code,
                amount: objectblock.amountben,
                bensortcode: objectblock.beneficiary.code,
                benname: objectblock.beneficiary.name,
                customerno: this.globalVars.getCustomerNo(),
                logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
		      });
		  }
	    
	      let endUrl="GetData.php";
	      body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

	      let requestType="post";
	      let authority="coop";
	      this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
	        loader.dismiss();
	        this.data=data;
	        if(this.data.error==""){
	          if(this.data.data.success){
	          	let template="";
	            if(objectblock.provider!="Other bank"){
		            template="<div>Transfer KShs."+objectblock.amount+" to <br>"+objectblock.provider+" account number "+objectblock.accountto+"<br> from account: "+objectblock.accountfrom+"<br>"+this.data.data.message+"</div>";
		        }
		        else{
	                template="<div>Transfer KShs."+objectblock.amount+" to <br>"+objectblock.bank.name+" branch: "+objectblock.branch.name+"<br> account number "+objectblock.accountto+"<br> from account: "+objectblock.accountfrom+"<br>"+this.data.data.message+"</div>";
	            }
	            let obj={body:body2, template:template, endUrl:endUrl, completed:false, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	            
	          }
	          else{

	            let template="<div>"+this.data.data.message+"</div>";
	            let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	          }
	        }
	        else{
	          console.log(this.data.error);
	          let template="<div>"+this.data.error+"</div>";
	          let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	          let myModal = this.modalCtrl.create('ConfirmModal',obj);
	          myModal.present();
	        }
	        
	      });
	    }
	    else{
	          this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
	            toast => {
	            console.log(toast);
	        });
	    }

  }
  saveBeneficiary(objectblock){
  	if(this.connectivityService.isOnline()){
  		let loader = this.loadingController.create({content:'Fetching Branches'});
	    loader.present();
  		let body = JSON.stringify({
			fetchtype: "transact",
			ttype: "addbeneficiary",
			username: this.globalVars.getUsername(),
			balias: objectblock.benename,
			bbankcode: objectblock.bank.code,
			bbranchcode: objectblock.branch.code,
			baccount: objectblock.accountto,
			customerno:  this.globalVars.getCustomerNo(),
			logintime: this.globalVars.getLoginTime(),
		    uuid: this.globalVars.getUUID()
		});

  	    let endUrl="GetData.php";
	    body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

        let requestType="post";
        let authority="coop";
        this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
	        loader.dismiss();
	        this.data=data;
	        if(this.data.error==""){
	            if(this.data.data.success){
	            	this.loadBeneficiaries(this.globalVars.getUsername(), this.globalVars.getCustomerNo());
                    this.doTransfer(objectblock);
	            }
	            else{
	            	let template="<div>"+this.data.data.message+"</div>";
		            let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
		            let myModal = this.modalCtrl.create('ConfirmModal',obj);
		            myModal.present();
	            }
	        }
	        else{
	        	let template="<div>"+this.data.data.message+"</div>";
	            let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	        }
	    });
	}
    else{
        this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
            toast => {
            console.log(toast);
        });
    }           

  }
  getBranches(bank){
  	this.branches=[];
  	
  	if(this.connectivityService.isOnline()){
  		let loader = this.loadingController.create({content:'Fetching Branches'});
	    loader.present();
  		let body = JSON.stringify({
            action: 'LOADBRANCH',
            username: this.globalVars.getUsername(),
            bank: bank.name,
            logintime: this.globalVars.getLoginTime(),
		    uuid: this.globalVars.getUUID()
        });
  	    let endUrl="bank.php";
	    body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

        let requestType="post";
        let authority="coop";
        this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
	        loader.dismiss();
	        this.data=data;
	        if(this.data.error==""){
	            this.rawBranches=this.data.data;
	            this.rawBranches.shift();
			    for(var i=0; i<this.rawBranches.length;i++){
			    	this.branches.push({name:this.rawBranches[i].split("|")[0],code:this.rawBranches[i].split("|")[1]});
			    }
			    this.objectblock.branch=this.branches[0];
	        }
	        else{
	        	let template="<div>"+this.data.data.message+"</div>";
	            let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	        }
	    });
	}
    else{
        this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
            toast => {
            console.log(toast);
        });
    }           

  }
  getProvider(provider){
  	if(provider=="Other bank"){
        this.rawBanks=this.globalVars.getBanks();
	    this.rawBanks.shift();
	    for(var i=0; i<this.rawBanks.length;i++){
	    	this.banks.push({name:this.rawBanks[i].split("|")[0],code:this.rawBanks[i].split("|")[1]});
	    }
	    this.objectblock.bank=this.banks[0];
	    this.getBranches(this.objectblock.bank);

  	}
  	else if(provider=="Saved Beneficiary"){
        this.beneficiaries=[];
        this.rawBeneficiaries=this.globalVars.getBeneficiaries();
	    this.rawBeneficiaries.shift();
	    for(var i=0; i<this.rawBeneficiaries.length;i++){
	    	this.beneficiaries.push({name:this.rawBeneficiaries[i].split("|")[0],code:this.rawBeneficiaries[i].split("|")[1]});
	    }

  	}
  	else if(provider=="My account"){
        this.objectblock.accountto=this.accounts[0];
  	}
  }
  loadBeneficiaries(mobileNo, customerno){
    let body=JSON.stringify({
      username: mobileNo,
      customerno: customerno,
      uuid: this.globalVars.getUUID()
    });
  
    let endUrl="beneficiaries.php";
    body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

    let requestType="post";
    let authority="coop";
    this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
        this.data=data;
        if(this.data.error==""){
      	    this.beneficiaries=[];
            this.globalVars. setBeneficiaries(this.data.data);
            this.rawBeneficiaries=this.data.data;
		    this.rawBeneficiaries.shift();
		    for(var i=0; i<this.rawBeneficiaries.length;i++){
		    	this.beneficiaries.push({name:this.rawBeneficiaries[i].split("|")[0],code:this.rawBeneficiaries[i].split("|")[1]});
		    }
        }
        else{
            console.log(this.data.error);
        }
      
    });
  }

}
