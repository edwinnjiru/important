import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BanktransfersPage } from './banktransfers';

@NgModule({
  declarations: [
    BanktransfersPage,
  ],
  imports: [
    IonicPageModule.forChild(BanktransfersPage),
  ],
  exports: [
    BanktransfersPage
  ]
})
export class BanktransfersPageModule {}
