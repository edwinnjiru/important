import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ApiConnect } from '../../providers/api-connect';

/**
 * Generated class for the NewsList page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-news-list',
  templateUrl: 'news-list.html',
})
export class NewsList {
  categoryTitle:any;
  newslist:any=[];
  data:any;
  title:string="";
  id:string="";
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public apiConnect: ApiConnect) {
    this.categoryTitle=this.navParams.get('title');
  	this.title=this.navParams.get('title');
  	this.id=this.navParams.get('id');
  	this.fetchNewsList(this.title,this.id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsList');
  }
  fetchNewsList(title,id){
    let body=JSON.stringify({catid:id});
  	let requestType="post";
  	let endUrl="";
  	let authority="standard";
      this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
        
  		this.data=data;
  		if(this.data.status==1){

  			this.newslist=this.data.data;
        console.log(this.data.status);
  		}
  	});			
  }
  openNewsItem(news){
    this.navCtrl.push('NewsItem',{categoryTitle:this.categoryTitle,news:news});
  }
  doRefresh(refresher){
    this.fetchNewsList(this.title,this.id);
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  };

}
;