import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsList } from './news-list';

@NgModule({
  declarations: [
    NewsList,
  ],
  imports: [
    IonicPageModule.forChild(NewsList),
  ],
  exports: [
    NewsList
  ]
})
export class NewsListModule {}
