import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountEnquiries } from './account-enquiries';

@NgModule({
  declarations: [
    AccountEnquiries,
  ],
  imports: [
    IonicPageModule.forChild(AccountEnquiries),
  ],
  exports: [
    AccountEnquiries
  ]
})
export class AccountEnquiriesModule {}
