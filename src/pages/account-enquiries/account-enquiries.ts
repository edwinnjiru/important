import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ApiConnect } from '../../providers/api-connect';
import { ConnectivityService } from '../../providers/connectivity-service';
import { Toast } from '@ionic-native/toast';
import * as $ from 'jquery';
/**
 * Generated class for the AccountEnquiries page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-account-enquiries',
  templateUrl: 'account-enquiries.html',
})
export class AccountEnquiries {
  title:string="Check balance";
  objectblock: any = {};
  accounts:any=[];
  data:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVars: GlobalVars, 
    public apiConnect: ApiConnect, public loadingController: LoadingController,
    private toast: Toast, public connectivityService: ConnectivityService, public modalCtrl: ModalController) {
     this.title=navParams.get('title');
     this.accounts=globalVars.getAccounts();
     this.objectblock.account=this.accounts[0];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountEnquiries');
  }
  submit(objectblock){
    if(this.connectivityService.isOnline()){
      let loader = this.loadingController.create({content:'Checking balance'});
      loader.present();
      
      let body=JSON.stringify({
        fetchtype: "charge",
        ttype: "bal",
        username: this.globalVars.getUsername(),
        AccNo: objectblock.account,
        logintime: this.globalVars.getLoginTime(),
        uuid: this.globalVars.getUUID()
      });
      let body2=JSON.stringify({
        fetchtype: "transact",
        ttype: "bal",
        username: this.globalVars.getUsername(),
        AccNo: objectblock.account,
        logintime: this.globalVars.getLoginTime(),
        uuid: this.globalVars.getUUID()
      });
    
      let endUrl="GetData.php";
      body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

      let requestType="post";
      let authority="coop";
      this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
        loader.dismiss();
        this.data=data;
        if(this.data.error==""){
          if(this.data.data.success){
            let template="<div>Check balance for account: "+objectblock.account+"<br>"+this.data.data.message+"</div>";
            let obj={body:body2, template:template, endUrl:endUrl, completed:false, pageTo:'MainServices'};
            let myModal = this.modalCtrl.create('ConfirmModal',obj);
            myModal.present();
            
          }
          else{

            let template="<div>"+this.data.data.message+"</div>";
            let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
            let myModal = this.modalCtrl.create('ConfirmModal',obj);
            myModal.present();
          }
        }
        else{
          console.log(this.data.error);
          let template="<div>"+this.data.error+"</div>";
          let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
          let myModal = this.modalCtrl.create('ConfirmModal',obj);
          myModal.present();
        }
        
      });
    }
    else{
          this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
            toast => {
            console.log(toast);
        });
    }

  }
  
}
