import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ApiConnect } from '../../providers/api-connect';
import { ConnectivityService } from '../../providers/connectivity-service';
import { Toast } from '@ionic-native/toast';
import * as $ from 'jquery';
/**
 * Generated class for the StatementPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-statement',
  templateUrl: 'statement.html',
})
export class StatementPage {
  objectblock: any = {};
  accounts:any=[];
  data:any;
  periods:any=[];
  modes:any=[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVars: GlobalVars, 
    public apiConnect: ApiConnect, public loadingController: LoadingController,
    private toast: Toast, public connectivityService: ConnectivityService, public modalCtrl: ModalController) {

    this.accounts=globalVars.getAccounts();
    this.objectblock.account=this.accounts[0];
    this.objectblock.stmttype="Mini";

    this.periods = [{
        val: "1",
        name: "1 Month"
    }, {
        val: "2",
        name: "2 Months"
    }, {
        val: "3",
        name: "3 Months"
    }];
    this.objectblock.period = this.periods[0];

    this.modes = [{
        val: "email",
        name: "Email"
    }, {
        val: "branch",
        name: "Collect at Branch"
    }];
    this.objectblock.mode = this.modes[0];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountEnquiries');
  }
  submit(objectblock){
    if(this.connectivityService.isOnline()){
      let loader = this.loadingController.create({content:'Fetching Statement'});
      loader.present();
      let body="";
      let body2="";
      if(objectblock.stmttype=="Full"){
	      body=JSON.stringify({
	        fetchtype: "charge",
	        ttype: "full",
	        username: this.globalVars.getUsername(),
	        accfullstatement: objectblock.account,
	        period: objectblock.period.val,
	        delivery: objectblock.mode.val,
	        logintime: this.globalVars.getLoginTime(),
	        uuid: this.globalVars.getUUID()
	      });
	      body2=JSON.stringify({
	        fetchtype: "transact",
	        ttype: "full",
	        username: this.globalVars.getUsername(),
	        accfullstatement: objectblock.account,
	        period: objectblock.period.val,
	        delivery: objectblock.mode.val,
	        logintime: this.globalVars.getLoginTime(),
	        uuid: this.globalVars.getUUID()
	      });
	  }
	  else{
	  	body=JSON.stringify({
	        fetchtype: "charge",
	        ttype: "mini",
	        username: this.globalVars.getUsername(),
	        AccNoMini: objectblock.account,
	        logintime: this.globalVars.getLoginTime(),
	        uuid: this.globalVars.getUUID()
	      });
	      body2=JSON.stringify({
	        fetchtype: "transact",
	        ttype: "mini",
	        username: this.globalVars.getUsername(),
	        AccNoMini: objectblock.account,
	        logintime: this.globalVars.getLoginTime(),
	        uuid: this.globalVars.getUUID()
	      });
	  }

      let endUrl="GetData.php";
      body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

      let requestType="post";
      let authority="coop";
      this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
        loader.dismiss();
        this.data=data;
        if(this.data.error==""){
          if(this.data.data.success){
          	let template="";
          	if(objectblock.stmttype=="Full"){
	            template="<div>"+objectblock.mode+" Full Statement for account: "+objectblock.account+" for period of "+objectblock.period+"<br>"+this.data.data.message+"</div>";
	        }
	        else{
	        	template="<div>Get Mini Statement for account: "+objectblock.account+"<br>"+this.data.data.message+"</div>";
	        }
            let obj={body:body2, template:template, endUrl:endUrl, completed:false, pageTo:'MainServices'};
            let myModal = this.modalCtrl.create('ConfirmModal',obj);
            myModal.present();
            
          }
          else{

            let template="<div>"+this.data.data.message+"</div>";
            let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
            let myModal = this.modalCtrl.create('ConfirmModal',obj);
            myModal.present();
          }
        }
        else{
          console.log(this.data.error);
          let template="<div>"+this.data.error+"</div>";
          let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
          let myModal = this.modalCtrl.create('ConfirmModal',obj);
          myModal.present();
        }
        
      });
    }
    else{
          this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
            toast => {
            console.log(toast);
        });
    }

  }
  
}
