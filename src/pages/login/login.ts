import { Component, PipeTransform } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, ModalController,  Platform } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ApiConnect } from '../../providers/api-connect';
import { ConnectivityService } from '../../providers/connectivity-service';
import { Toast } from '@ionic-native/toast';
import { Keyboard } from '@ionic-native/keyboard';
import { Device } from '@ionic-native/device';
import { Firebase } from '@ionic-native/firebase';
import { Storage } from '@ionic/storage';
import * as $ from 'jquery'
/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

declare var cordova:any;
declare var window: any;

@IonicPage(
   {name: 'Login'}
)
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login implements PipeTransform{

  objectblock: any = {};
  valueforngif:boolean=true;
  isPasswordForm:boolean=false;
  isPasswordForm2:boolean=false;
  error:number=0;
  data:any;
  smses:any;
  mobileNo:string="";
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, public keyboard: Keyboard,
   public globalVars: GlobalVars, public apiConnect: ApiConnect, public loadingController: LoadingController,
    private toast: Toast, public connectivityService: ConnectivityService, public modalCtrl: ModalController, private device: Device,
    public platform: Platform, private storage: Storage, private firebase: Firebase) {
    this.menu.swipeEnable(false);
    this.platform.ready().then(() => {
      this.firebase.getToken()
  	  .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
  	  .catch(error => console.error('Error getting token', error));


  	  this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Login"}).then(data => console.log(`The data is ${data}`)) 
  	  .catch(error => console.error('Error getting logData', error));
      storage.get('xyz').then((val) => {
        if(val){
          this.isPasswordForm=true;
          this.isPasswordForm2=true;
          let uuidmobilemix = val;
          let uuid = this.device.uuid;
          this.mobileNo=globalVars.doAESdecrypt(uuidmobilemix, uuid+"blablahblah");
          globalVars.setUsername(this.mobileNo);
        }
      });
    });  
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');

    this.keyboard.onKeyboardShow().subscribe(()=>{this.valueforngif=false});
    this.keyboard.onKeyboardHide().subscribe(()=>{this.valueforngif=true});
  
  }
  openPage(page){
    this.navCtrl.push(page)
  }
  openMain(){
    this.globalVars.setAccounts(["254708730788"]);
    this.navCtrl.setRoot('MainServices');
  }
  submit(objectblock){
    
      this.error=0;
      if(objectblock.user==""||objectblock.user==undefined){
          this.error=1;
      }
      else if(!(/^\d{10}$/.test(objectblock.user))){
          this.error=1;
      } 
      else{
          if(this.connectivityService.isOnline()){
            let loader = this.loadingController.create({content:'Verifying Credentials'});
            loader.present();
            let body=JSON.stringify({
                username: objectblock.user
            });
          
            let endUrl="VerifyCustomer.php";
            body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

            let requestType="post";
            let authority="coop";
            this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
              loader.dismiss();
              this.data=data;
              if(this.data.error==""){
                if(this.data.data.success){
                  this.isPasswordForm=true;
                  this.globalVars.setUsername(objectblock.user);
                }
                else{

                  let template="<div>The number "+objectblock.user+ " is not registered.<br>Kindly register to access this service</div>";
                  let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'LoginPage'};
                  let myModal = this.modalCtrl.create('ConfirmModal',obj);
                  myModal.present();
                }
              }
              else{
                
                let template="<div>"+this.data.error+ "</div>";
                let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'Login'};
                let myModal = this.modalCtrl.create('ConfirmModal',obj);
                myModal.present();
              }
              
            });
          }
          else{
                this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
                  toast => {
                  
              });
          }
          
      }
    
  }
  verifyPin(objectblock){
    
    if(!this.isPasswordForm2){
      this.error=0;
      if(objectblock.password==""||objectblock.password==undefined){
          this.error=2;
      }
      else if(!(/^\d{4}$/.test(objectblock.password))){
          this.error=2;
      } 
      else{
          if(this.connectivityService.isOnline()){
         
            let pass=this.globalVars.getEncryptedVars(objectblock.password,true);

            let loader = this.loadingController.create({content:'Verifying Credentials'});
            loader.present();
            let uuid=this.device.uuid;
            this.globalVars.setUUID(uuid);
            let body=JSON.stringify({
                username: objectblock.user,
                password: pass,
                uuid:uuid
            });
          
            let endUrl="firstloginAppTest.php";
            body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

            let requestType="post";
            let authority="coop";
            this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
              
              this.data=data;
              if(this.data.error==""){
                if(this.data.data.success){
                  this.globalVars.setOtp(this.data.data.otpPassword);
                  this.isPasswordForm=true;
                  if(this.device.platform=="Android"){
                    this.platform.ready().then(() => { 
                      var permissions = cordova.plugins.permissions;

                      permissions.hasPermission(permissions.READ_SMS, checkPermissionCallback, null);

                      this.readSMS(this.data.data.otpPassword).then(data => {
                        loader.dismiss();
                        if(data){
                          let loader = this.loadingController.create({content:'Updating Customer Details'});
                          loader.present();
                          let endUrl="VerifyCustomerUpdateUUID.php";
                          this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
                            loader.dismiss();
                            this.data=data;
                            if(this.data.error==""){
                              if(this.data.data.success){
                                let encrypted = this.globalVars.doAESencrypt(objectblock.user, this.device.uuid+"blablahblah");
                                this.storage.set('xyz', encrypted);
                                this.navCtrl.push('Login'); 
                              } 
                              else{
                                let template="<div>"+this.data.data.message+ " </div>";
                                let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'Login'};
                                let myModal = this.modalCtrl.create('ConfirmModal',obj);
                                myModal.present();
                              }
                            }
                            else{
                              let template="<div>"+this.data.error+ " </div>";
                              let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'Login'};
                              let myModal = this.modalCtrl.create('ConfirmModal',obj);
                              myModal.present();
                            }
                          });
                        }
                        else{
                          console.log("err "+data);
                        }
                      });

                      function checkPermissionCallback(status) {
                        if(!status.hasPermission) {
                          var errorCallback = function() {
                            alert('READ_SMS permission is not turned on');
                          }

                          permissions.requestPermission(
                          permissions.READ_SMS,
                          function(status) {
                            if(!status.hasPermission) {
                              errorCallback();
                            }
                          },errorCallback);
                        }
                      }
                    });
                  }
                  else{
                    loader.dismiss();
                    this.toast.show("Enter verification code that will be sent to you", '8000', 'bottom').subscribe(
                      toast => {
                     
                    });
                    this.navCtrl.push('VerifyPin');
                  }
                  
                }
                else{

                  let template="<div>The number "+objectblock.user+ " is not registered.<br>Kindly register to access this service</div>";
                  let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'Login'};
                  let myModal = this.modalCtrl.create('ConfirmModal',obj);
                  myModal.present();
                }
              }
              else{
                loader.dismiss();
                let template="<div>"+this.data.error+ " </div>";
                let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'Login'};
                let myModal = this.modalCtrl.create('ConfirmModal',obj);
                myModal.present();
              }
              
            });
          }
          else{
                this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
                  toast => {
                  
              });
          }
          
      }
    }
    else{
      this.error=0;
      if(objectblock.password==""||objectblock.password==undefined){
          this.error=2;
      }
      else if(!(/^\d{4}$/.test(objectblock.password))){
          this.error=2;
      } 
      else{
          if(this.connectivityService.isOnline()){
         
            let pass=this.globalVars.getEncryptedVars(objectblock.password,true);

            let loader = this.loadingController.create({content:'Verifying Credentials'});
            loader.present();
            let uuid=this.device.uuid;
            this.globalVars.setUUID(uuid);
            let body=JSON.stringify({
                username: this.mobileNo,
                password: pass,
                uuid:uuid
            });
          
            let endUrl="firstloginAppPROD.php";
            body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

            let requestType="post";
            let authority="coop";
            this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
              
              this.data=data;
              if(this.data.error==""){
                if(this.data.data.success){

                  this.loadLoginUtilites(this.mobileNo,this.data.data);
                  
                  this.globalVars.setPartialReg(this.data.data.partialregistration);
                  this.globalVars.setCustomerNo(this.data.data.customerno);
                  this.globalVars.setLoginTime(this.data.data.logintime);
                  loader.dismiss();
                  this.navCtrl.setRoot('MainServices');
                  
                }
                else{
                  loader.dismiss();
                  let template="<div></div>";
                  let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'Login'};
                  let myModal = this.modalCtrl.create('ConfirmModal',obj);
                  myModal.present();
                }
              }
              else{
                loader.dismiss();
                let template="<div>"+this.data.error+"</div>";
                let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'Login'};
                let myModal = this.modalCtrl.create('ConfirmModal',obj);
                myModal.present();
              }
              
            });
          }
          else{
              this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
                  toast => {
              });
          }
          
      }
    }
  }
  
  readSMS(search): Promise<any> {
 
    return new Promise((resolve) => {
      let startTime = new Date().getTime();
      let filter={box:'inbox', address:"COOPBANK", body:search, maxCount:1000};
      let smsresponse=false;
      var temp = this;
      var interval=setInterval(()=>{
         
        if ((new Date().getTime() - startTime) > 30000) {

          clearInterval(interval);
          smsresponse = false;
          resolve(smsresponse);
        }
        if(window.SMS) window.SMS.listSMS(filter,data=>{
            temp.smses=data;
            for(var i=0;i<temp.smses.length;i++){
                
              if(temp.smses[i].address == "COOPBANK" && temp.smses[i].body.indexOf(search)>=0){
                clearInterval(interval);
                smsresponse = true;
                resolve(smsresponse);
              }
            }

        },error=>{
          console.log(error);
        });
      },2000);
    });

  }
  loadLoginUtilites(mobileNo,cusdata){
    this.loadAccounts(mobileNo, cusdata.logintime);
    this.loadBanks(mobileNo);
    this.loadKitsBanks(mobileNo, cusdata.logintime);
    this.loanAccounts(mobileNo, cusdata.logintime, cusdata.customerno);
    this.loadBeneficiaries(mobileNo, cusdata.customerno);
    this.getKitsLimits(mobileNo, cusdata.logintime);
    this.linkedacc(mobileNo, cusdata.logintime);
  }
  loadAccounts(mobileNo, logintime){
    let body=JSON.stringify({
      username: mobileNo,
      uuid: this.globalVars.getUUID(),
      logintime: logintime
    });
  
    let endUrl="GetAccounts.php";
    body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

    let requestType="post";
    let authority="coop";
    this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
      this.data=data;
      if(this.data.error==""){
           
        this.globalVars.setAccounts(this.transform(this.data.data));
      }
 
      else{
        console.log(this.data.error);
      }
      
    });
  }
  transform(value: any, args?: any[]): any[] {
      // create instance vars to store keys and final output
      let keyArr: any[] = Object.keys(value),
          dataArr = [];

      // loop through the object,
      // pushing values to the return array
      keyArr.forEach((key: any) => {
          dataArr.push(value[key]);
      });

      // return the resulting array
      return dataArr;
  }
  loadBanks(mobileNo){
    let body=JSON.stringify({
      action: "LOADBANK",
      username: mobileNo,
      uuid: this.globalVars.getUUID()
    });
  
    let endUrl="bank.php";
    body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

    let requestType="post";
    let authority="coop";
    this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
      this.data=data;
      if(this.data.error==""){
          this.globalVars.setBanks(this.data.data);
      }
 
      else{
        console.log(this.data.error);
      }
      
    });
  }
  loadKitsBanks(mobileNo, logintime){
    let body=JSON.stringify({
      username: mobileNo,
      uuid: this.globalVars.getUUID(),
      logintime: logintime
    });
  
    let endUrl="kitsbank.php";
    body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

    let requestType="post";
    let authority="coop";
    this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
      this.data=data;
      if(this.data.error==""){
          this.globalVars.setKitsBanks(this.data.data);
      }
 
      else{
        console.log(this.data.error);
      }
      
    });
  }
  loanAccounts(mobileNo, logintime, customerno){
    let body=JSON.stringify({
      ttype: "loanacounts",
      username: mobileNo,
      customerno: customerno,
      logintime: logintime,
      uuid: this.globalVars.getUUID()
    });
  
    let endUrl="GetData.php";
    body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

    let requestType="post";
    let authority="coop";
    this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
      this.data=data;
      if(this.data.error==""){
          this.globalVars.setLoanAccounts(this.data.data);
      }
 
      else{
        console.log(this.data.error);
      }
      
    });
  }
  loadBeneficiaries(mobileNo, customerno){
    let body=JSON.stringify({
      username: mobileNo,
      customerno: customerno,
      uuid: this.globalVars.getUUID()
    });
  
    let endUrl="beneficiaries.php";
    body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

    let requestType="post";
    let authority="coop";
    this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
      this.data=data;
      if(this.data.error==""){
          this.globalVars. setBeneficiaries(this.data.data);
      }
 
      else{
        console.log(this.data.error);
      }
      
    });
  }
  getKitsLimits(mobileNo,logintime){
    let body=JSON.stringify({
      ttype: "getkitslimits",
      username: mobileNo,
      logintime: logintime,
      uuid: this.globalVars.getUUID()
    });
  
    let endUrl="GetData.php";
    body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

    let requestType="post";
    let authority="coop";
    this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
      this.data=data;
      if(this.data.error==""){
          this.globalVars.setKitsLimits(this.data.data.maximumLimit, this.data.data.minimumLimit);
      }
 
      else{
        console.log(this.data.error);
      }
      
    });
  }
  linkedacc(mobileNo, logintime){
    let body=JSON.stringify({
      ttype: "linkedacc",
      username: mobileNo,
      logintime: logintime,
      uuid: this.globalVars.getUUID()
    });
  
    let endUrl="GetData.php";
    body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

    let requestType="post";
    let authority="coop";
    this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
      this.data=data;
      if(this.data.error==""){
          this.globalVars.setLinkedAccounts(this.data.data.kitsnumber);
      }
 
      else{
        console.log(this.data.error);
      }
      
    });
  }

}
