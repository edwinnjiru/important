import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ContactsProvider } from '../../providers/contacts';
import { ApiConnect } from '../../providers/api-connect';
import { ConnectivityService } from '../../providers/connectivity-service';
import { AlertServiceProvider } from '../../providers/alert-service/alert-service';
import { Toast } from '@ionic-native/toast';
import * as $ from 'jquery'
/**
 * Generated class for the BillsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bills',
  templateUrl: 'bills.html',
})
export class BillsPage {
  objectblock:any={};
  accounts:any=[];
  data:any;
  error:number=0;
  ismobile:boolean=false;
  title:string;
  contact:any;
  cname:string="";

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVars: GlobalVars, 
    public apiConnect: ApiConnect, public loadingController: LoadingController,
    private toast: Toast, public connectivityService: ConnectivityService, public modalCtrl: ModalController,
     public contactsProvider: ContactsProvider, public alertService: AlertServiceProvider) {
    this.accounts=globalVars.getAccounts();
    this.objectblock.account=this.accounts[0];
    this.title=navParams.get('title');
    if(this.title=="Safaricom Postpaid"){
        this.ismobile=true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BillsPage');
  }
  pickContact(){
  	this.contactsProvider.pickContact().then(contact => {
  	   console.log(contact);
  	   if(contact.phoneNo.length>1){
           this.alertService.showRadioAlert(contact.phoneNo).then(data => {
           	  this.objectblock.receiverno=data;
           	  if(this.objectblock.receiverno!=""||this.objectblock.receiverno!=undefined){
			    this.cname=contact.name;
			  }
           });
  	   }
  	   else{
	       this.objectblock.receiverno=contact.phoneNo[0];
	       if(this.objectblock.receiverno!=""||this.objectblock.receiverno!=undefined){
		       this.cname=contact.name;
		   }
	   }
       
  	});
  }
  submit(objectblock){
  	this.error=0;
  	if(objectblock.amount==""||objectblock.amount==undefined){
        this.error=3;
    }
    else if(!this.ismobile && (objectblock.accountto==""||objectblock.accountto==undefined)){
        this.error=1;
    }
    else if(this.ismobile && (objectblock.accountto==""||objectblock.accountto==undefined)){
    	this.error=2;
    }
    else if(this.ismobile && !(/^\d{10}$/.test(objectblock.accountto))){
        this.error=2;
    } 
    else{
	    if(this.connectivityService.isOnline()){
	     
	      let loader = this.loadingController.create({content:'Buying airtime'});
	      loader.present();
	      let body="";
	      let body2="";
	      if(this.title=="KPLC Postpaid"|| this.title=="KPLC Prepaid"){
	      	let ttype="power";
	      	if(this.title=="KPLC Postpaid"){
               ttype="power";
	      	}
	      	else{
               ttype="power-prepay";
	      	}
		      body=JSON.stringify({
		        fetchtype: "charge",
				ttype: ttype,
				username: this.globalVars.getUsername(),
				fromPower: objectblock.account,
				meterno: objectblock.accountto,
				AmountPower: objectblock.amount,
				logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()
				
	          });
		      body2=JSON.stringify({
		        fetchtype: "transact",
				ttype: ttype,
				username: this.globalVars.getUsername(),
				fromPower: objectblock.account,
				meterno: objectblock.accountto,
				AmountPower: objectblock.amount,
				logintime: this.globalVars.getLoginTime(),
		        uuid: this.globalVars.getUUID()        
		      });
		  }
		  else if(this.title=="Nairobi Water"){
		  	  body=JSON.stringify({
		        fetchtype: "charge",
                ttype: "water",
                username: this.globalVars.getUsername(),
                fromWater: objectblock.account,
                watermeterno: objectblock.accountto,
                AmountWater: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
	            uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
		        ttype: "water",
                username: this.globalVars.getUsername(),
                fromWater: objectblock.account,
                watermeterno: objectblock.accountto,
                AmountWater: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
	            uuid: this.globalVars.getUUID()
		      });
		  }
		  else if(this.title=="Zuku"){
		  	  body=JSON.stringify({
		        
	            fetchtype: "charge",
                ttype: "zuku",
                username: this.globalVars.getUsername(),
                fromzuku: objectblock.account,
                customernozuku: objectblock.accountto,
                Amountzuku: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
                uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
		        ttype: "zuku",
                username: this.globalVars.getUsername(),
                fromzuku: objectblock.account,
                customernozuku: objectblock.accountto,
                Amountzuku: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
                uuid: this.globalVars.getUUID()
		      });
		  }
		  else if(this.title=="DSTV"){
		  	  body=JSON.stringify({
		        
	            fetchtype: "charge",
                ttype: "dstv",
                username: this.globalVars.getUsername(),
                fromdstv: objectblock.account,
                customerno: objectblock.accountto,
                Amountdstv: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
                uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
		        ttype: "dstv",
                username: this.globalVars.getUsername(),
                fromdstv: objectblock.account,
                customerno: objectblock.accountto,
                Amountdstv: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
                uuid: this.globalVars.getUUID()
		      });
		  }
		  else if(this.title=="Startimes"){
		  	  body=JSON.stringify({
		        
	            fetchtype: "charge",
                ttype: "paystar",
                username: this.globalVars.getUsername(),
                accfromstar: objectblock.account,
                startimescustomerno: objectblock.accountto,
                AmountStar: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
                uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
		        ttype: "paystar",
                username: this.globalVars.getUsername(),
                accfromstar: objectblock.account,
                startimescustomerno: objectblock.accountto,
                AmountStar: objectblock.amount,
                logintime: this.globalVars.getLoginTime(),
                uuid: this.globalVars.getUUID()
		      });
		  }
		  else if(this.title=="Safaricom Postpaid"){
		  	  body=JSON.stringify({
		        
	            fetchtype: "charge",
                ttype: "safpostpaid",
                username: this.globalVars.getUsername(),
                mobino: objectblock.accountto,
                amount: objectblock.amount,
                sourceacc: objectblock.account,
                logintime: this.globalVars.getLoginTime(),
                uuid: this.globalVars.getUUID()
		      });
		      body2=JSON.stringify({
		        fetchtype: "transact",
                ttype: "safpostpaid",
                username: this.globalVars.getUsername(),
                mobino: objectblock.accountto,
                amount: objectblock.amount,
                sourceacc: objectblock.account,
                logintime: this.globalVars.getLoginTime(),
                uuid: this.globalVars.getUUID()
		      });
		  }
	    
	      let endUrl="GetData.php";
	      body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

	      let requestType="post";
	      let authority="coop";
	      this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
	        loader.dismiss();
	        this.data=data;
	        if(this.data.error==""){
	          if(this.data.data.success){
	            
	            let template="<div>Pay KShs."+objectblock.amount+" for account number: "+objectblock.accountto+" from account: "+objectblock.account+"<br>"+this.data.data.message+"</div>";
	            let obj={body:body2, template:template, endUrl:endUrl, completed:false, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	            
	          }
	          else{

	            let template="<div>"+this.data.data.message+"</div>";
	            let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	            let myModal = this.modalCtrl.create('ConfirmModal',obj);
	            myModal.present();
	          }
	        }
	        else{
	          console.log(this.data.error);
	          let template="<div>"+this.data.error+"</div>";
	          let obj={body:body, template:template, endUrl:endUrl, completed:true, pageTo:'MainServices'};
	          let myModal = this.modalCtrl.create('ConfirmModal',obj);
	          myModal.present();
	        }
	        
	      });
	    }
	    else{
	          this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
	            toast => {
	            console.log(toast);
	        });
	    }
    }

  }

}

