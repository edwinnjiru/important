import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NewsItem page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-news-item',
  templateUrl: 'news-item.html',
})
export class NewsItem {
  newsItem:any={};
  categoryTitle:string;
  news: any={};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.news=navParams.get('news');
    console.log(this.news);
    this.news.story=this.news.story.replace(/src="/g,'src="http://www.standardmedia.co.ke');
    this.news.story=this.news.story.replace(/<img /g,'<img width="100%" ');
	this.newsItem=this.news;
    this.categoryTitle=navParams.get('categoryTitle');

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsItem');
  }

}
