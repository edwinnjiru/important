import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsItem } from './news-item';

@NgModule({
  declarations: [
    NewsItem,
  ],
  imports: [
    IonicPageModule.forChild(NewsItem),
  ],
  exports: [
    NewsItem
  ]
})
export class NewsItemModule {}
