import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

import { GoogleMaps } from '../../providers/google-maps';
import { GlobalVars } from '../../providers/global-vars';
import { LocationService } from '../../providers/location-service';
/*
  Generated class for the Location page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})

export class Map {

  mapType:string;
  endUrl:string;

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, public maps: GoogleMaps,public platform: Platform, public globalVars: GlobalVars, public locationService: LocationService) {
     this.mapType="atm";
  }

  ionViewDidLoad(){
    this.platform.ready().then(() => {
 
        this.maps.init(this.mapElement.nativeElement, this.pleaseConnect.nativeElement);
		
		if(this.mapType=="atm"){
		    this.endUrl="atmcoordinates.php";
		}
		else if(this.mapType=="branch"){
		    this.endUrl="branchcoordinates.php";
		}
		else{
		    this.endUrl="atmcoordinates.php";
		}
		this.locationService.load(this.endUrl);
        
    });
 
  }
  loadPoints(){
        if(this.mapType=="atm"){
		    this.endUrl="atmcoordinates.php";
		}
		else if(this.mapType=="branch"){
		    this.endUrl="branchcoordinates.php";
		}
		else{
		    this.endUrl="atmcoordinates.php";
		}
		this.maps.removeMarkers();
		this.maps.removeMarkersComp();
		let locationsLoaded = this.locationService.load(this.endUrl);
		Promise.all([
			locationsLoaded
		]).then((result) => {
			
			let locations = this.globalVars.getLocationLoaded();
           
			for(let location of locations){
				this.maps.addMarker(location.title,location.latitude, location.longitude,"http://maps.google.com/mapfiles/ms/icons/red-dot.png");

			}
			this.maps.addMarker("My Spot",this.globalVars.getCurrentLocation().latitude, this.globalVars.getCurrentLocation().longitude,"http://maps.google.com/mapfiles/ms/icons/green-dot.png");
			
            this.maps.drawRoute({latitude:this.globalVars.getCurrentLocation().latitude,longitude:this.globalVars.getCurrentLocation().longitude},{latitude: this.globalVars.getLocationLoaded()[0].latitude ,longitude: this.globalVars.getLocationLoaded()[0].longitude});
		});
  }

}
