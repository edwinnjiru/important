import { Component } from '@angular/core';
import { IonicPage, App, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ApiConnect } from '../../providers/api-connect';
import { ConnectivityService } from '../../providers/connectivity-service';
import { Toast } from '@ionic-native/toast';
import * as $ from 'jquery'
/**
 * Generated class for the ConfirmModal page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-confirm-modal',
  templateUrl: 'confirm-modal.html',
})
export class ConfirmModal {
  template:string="<div></div>";
  body:any;
  endUrl:any;
  data:any;
  completed:boolean=false;
  pageTo:string="Login";

  constructor(public appCtrl: App, public navParams: NavParams, public viewCtrl: ViewController,
   public globalVars: GlobalVars, public apiConnect: ApiConnect, public loadingController: LoadingController,
    private toast: Toast, public connectivityService: ConnectivityService) {
    this.body = navParams.get('body');
    this.endUrl = navParams.get('endUrl');
    this.template = navParams.get('template');
    this.completed = navParams.get('completed');
    this.pageTo = navParams.get('pageTo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmModal');
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
  closeModal2() {
    this.viewCtrl.dismiss();
    if(this.template=="<div>Ooops! Took too long to respond.<br>Kindly log in again</div>"){
      this.globalVars.setLock(false);
    }
    if(this.pageTo=="MainServices"){
      this.viewCtrl.dismiss();
      this.appCtrl.getRootNav().setRoot('MainServicesPage');
    }
    else if(this.pageTo==""){
      
    }
    else{
      this.viewCtrl.dismiss();
      this.appCtrl.getRootNav().setRoot('MainServicesPage');
    }
  }
  submit(){
  	if(this.connectivityService.isOnline()){
		  let loader = this.loadingController.create({content:'Submitting Request'});
	    loader.present();
        let body=this.body;
        body=$.param({data: this.globalVars.getEncryptedVars(body,false)});

	  	let requestType="post";
	  	let endUrl=this.endUrl;
	  	let authority="coop";
	    this.apiConnect.load(requestType,body,endUrl,authority).then(data => {
	        loader.dismiss();
	  		this.data=data;
	  		if(this.data.error==""){
	  		   this.template=this.data.data.message;
	  		   this.completed=true;
	  		}
	  		else{
	  		   this.template=this.data.error;
	  		   this.completed=true;
	  		}
	  		
	    });		
  	}
  	else{
          this.toast.show("Please connect to the Internet", '8000', 'bottom').subscribe(
  	        toast => {
  	        console.log(toast);
  	    });
  	}
  }

}
