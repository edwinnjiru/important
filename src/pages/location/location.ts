import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

/**
 * Generated class for the Location page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class Location {

  tab1Root: any = 'Map';
  tab2Root: any = 'LocationList';
 
  constructor() {}

  ionViewDidLoad(){
   // console.log("ionViewDidLoad LocationPage");
 
  }

}