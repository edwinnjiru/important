import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { GlobalVars } from '../../providers/global-vars';
import { LocationService } from '../../providers/location-service';
/**
 * Generated class for the LocationList page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-location-list',
  templateUrl: 'location-list.html',
})
export class LocationList {

  locations:any;
  mapType:string;
  endUrl:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalVars: GlobalVars, public locationService: LocationService) {
    this.mapType="atm";
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPage');
	if(this.mapType=="atm"){
		this.endUrl="atmcoordinates.php";
	}
	else if(this.mapType=="branch"){
		this.endUrl="branchcoordinates.php";
	}
	else{
		this.endUrl="atmcoordinates.php";
	}
	let locationsLoaded = this.locationService.load(this.endUrl);
 
	Promise.all([
		locationsLoaded
	]).then(() => {
        this.locations=[];
		this.locations = this.globalVars.getLocationLoaded();

	});
  }

}
