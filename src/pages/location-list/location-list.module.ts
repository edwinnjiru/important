import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocationList } from './location-list';

@NgModule({
  declarations: [
    LocationList,
  ],
  imports: [
    IonicPageModule.forChild(LocationList),
  ],
  exports: [
    LocationList
  ]
})
export class LocationListModule {}
