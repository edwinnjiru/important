<?php

namespace App\Http\Middleware;

use Closure;

class ArrayTransform
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public static function Array2XML($param)
    {
        $strXML = "";

        if (is_array($param)) {
            $strXML="<?xml version= \"1.0\"  encoding= \"utf-8\"?>";
            $strXML.="<message>";
            $strXML.="<authHeader sourceid=\"\" password=\"\"/>";
            $strXML.="<isomsg direction=\"request\">";
            foreach ($param as $key => $value) {
                $key=str_replace("field","",$key);
                $strXML.="<field id=\"$key\" value=\"$value\"/>";
               
            }
            
            $strXML.="</isomsg>";
            $strXML.="</message>";
            
        } else {
            $strXML = "ERROR. Empty Array";
        }
        return $strXML;
    }

    public static function XML2Array(\SimpleXMLElement $parent)
    {
        $array = array();

        foreach ($parent as $name => $element) {
            ($node = &$array[$name])
            && (1 === count($node) ? $node = array($node) : 1)
            && $node = &$node[];

            $node = $element->count() ? XML2Array($element) : trim($element);
        }
        return $array;
    }

    public static function stdObjectToArray($d)
    {
        if (is_object($d)) {
            $d = get_object_vars($d);
        } else {
            if (is_array($d)) {
                return array_map(__FUNCTION__, $d);
            }
        }
        return $d;
    }
}
