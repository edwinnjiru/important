<?php

namespace App\Http\Middleware;
use Closure;
use JWTAuth;
use Exception;
class authJWT
{
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::toUser($request->header('token'));

            \JWTAuth::setToken($request->header('token'));
            $user = \JWTAuth::getToken();
            $explode_token = explode('.', $user);
            $token_body = json_decode(base64_decode($explode_token[1]));

            $sub = base64_decode($token_body->sub);
            $aud = base64_decode($token_body->aud);
            $phone = base64_decode($token_body->id);

            if($sub!="eclectics"){
                throw  new \Tymon\JWTAuth\Exceptions\TokenInvalidException;
            }else if($aud!=env('INSTITUTION_NAME')){
                throw  new \Tymon\JWTAuth\Exceptions\TokenInvalidException;
            }else if($phone!=$request->input('username')){
                throw  new \Tymon\JWTAuth\Exceptions\TokenInvalidException;
            }

        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['error'=>'Token is Invalid']);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['error'=>'Token is Expired']);
            }else{
                return response()->json(['error'=>'Something is wrong']);
            }
        }
        return $next($request);
    }
}