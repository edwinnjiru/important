<?php

namespace App\Http\Middleware;

use Closure;

class DecryptionUtilities
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $encrypted_data = $request->data;
        $decrypted_data = $this->processDecryption($encrypted_data);
        $new_request = $request;
        $new_request->data = $decrypted_data;

        return $next($new_request);
    }
    public function processDecryption($encrypted_data){
        $decrypted_data = base64_decode($encrypted_data);
        return $decrypted_data;
    }
}
