<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;
//use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Illuminate\Support\Facades\File;

class ApiUtilities
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public static function dateSubtraction($date1timestamp, $date2timestamp)
    {
        $all = round(($date1timestamp - $date2timestamp) / 60);
        $d = floor($all / 1440);
        $h = floor(($all - $d * 1440) / 60);
        $m = $all - ($d * 1440) - ($h * 60);
        return array('hours' => $h, 'mins' => $m);
    }

    public static function log($dirname, $file, $info)
    {
        $logDir = env('LOG_DIR');
        $todayslogs = $logDir . strtoupper(date("d-M-Y"));
        if (!file_exists($todayslogs)) {
            mkdir($todayslogs);
        }
        $foldername = $todayslogs . '/' . $dirname;
        if (!file_exists($foldername)) {
            mkdir($foldername);
        }
        $filename = $foldername . '/' . $file . substr(date('YmdHis'), 0, strlen(date('YmdHis')) - 4) . ".log";
        file_put_contents($filename, $info . " " . date('Y-m-d H:i:s') . PHP_EOL, FILE_APPEND | LOCK_EX);
    }

    public static function getTransType($accFrom, $accTo)
    {
        $transType = "";
        if (is_numeric($accFrom) && is_numeric($accTo)) {
            if ((strlen($accFrom) > 12) && (strlen($accTo) > 12)) {
                $transType = "CC";
            } else if ((strlen($accFrom) <= 12) && (strlen($accTo) > 12)) {
                $transType = "MC";
            } else if ((strlen($accFrom) <= 12) && (strlen($accTo) <= 12)) {
                $transType = "MM";
            } else if ((strlen($accFrom) > 12) && (strlen($accTo) <= 12)) {
                $transType = "CM";
            }

            return $transType;
        }
        return "";
    }

    public static function decodeFiftyFour($balance)
    {
        if (strpos($balance, "C") !== FALSE) {
            $balance = str_replace("C", "", $balance);
        } else if (strpos($balance, "D") !== FALSE) {
            $balance = str_replace("D", "-", $balance);
        }

        $balance = floatval($balance) / 100;
        $balance = number_format((float)$balance, 2, '.', '');

        return $balance;
    }

    public static function loadJson($filename)
    {
        $path = storage_path() . "/json/${filename}.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        if (!File::exists($path)) {
            throw new Exception("Invalid File");
        }

        $json = file_get_contents($path);

        return $json;

        // Verify Validate JSON?

        // Your other Stuff

    }

    public static function generateStan()
    {
        $ran = mt_rand(100000, 999999) . mt_rand(100000, 999999);
        $stan = substr($ran, 0, 6);
        $unique = str_shuffle($stan);
        return $stan;
    }

    public static function generateUnique()
    {
        $ran = mt_rand(100000, 999999) . mt_rand(100000, 999999);
        $stan = substr($ran, 0, 6);
        $unique = str_shuffle($stan);
        return $stan . $unique;
    }

    public static function getCodedTelephone($countryCode, $phone)
    {
        try {
            if (strlen($phone) < 10) {
                throw new \Exception("Invalid phone number");
            } else if (strlen($phone) == 10) {
                $phoneNumber = substr($phone, -9);
                $codedTelephone = $countryCode . $phoneNumber;
                $codedTelephone = substr($codedTelephone, 1);
                return $codedTelephone;
            } else {
                return $phone;
            }
        } catch (\Exception $ex) {
            throw new \Exception("Problems coding phone number.");
        }
    }

    public static function cleanJSON($json)
    {
        $json = str_replace("{", "", str_replace("}", "", $json));
        $fieldsArray = explode(',', $json);
        $fieldsArraySize = count($fieldsArray);
        $cleanJSON = array();

        for ($i = 0; $i < $fieldsArraySize; $i++) {
            $field = explode("=", $fieldsArray[$i]);
            $cleanJSON[trim($field[0])] = $field[1];
        }

        return $cleanJSON;
    }

    public static function generateToken($field2, $field68)
    {
        $phone = base64_encode($field2);
        $sub = base64_encode('eclectics');
        $aud = base64_encode(env('INSTITUTION_NAME'));
        $payload = JWTFactory::sub('eclectics')
            ->sub($sub)
            ->aud($aud)
            ->id($phone)
            ->make();

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = \JWTAuth::encode($payload)) {
                return Response::json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return Response::json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token

        return $token;
    }
}
