<?php

namespace App\Http\Middleware;

use Closure;
use \Config;

class TransactionsLimits
{
    public $transaction_name;
    public $amount;

    public static function limit($transaction_name_, $amount_)
    {
        $transaction_name = $transaction_name_;
        $amount = intval($amount_);

        switch ($transaction_name) {
            case Config::get('constants.FUNDS_TRANSFER_CODE'):
                $min = intval(Config::get('constants.FUNDS_TRANSFER_MIN'));
                $max = intval(Config::get('constants.FUNDS_TRANSFER_MAX'));
                if ($amount < $min) {
                    throw new \Exception("The minimum amount for this transaction is " . $min);
                } else if ($amount > $max) {
                    throw new \Exception("The maximum amount for this transaction is " . $max);
                }
                break;
            case Config::get('constants.AIRTIME_ENQUIRY_CODE'):
                $min = intval(Config::get('constants.AIRTIME_ENQUIRY_MIN'));
                $max = intval(Config::get('constants.AIRTIME_ENQUIRY_MAX'));
                if ($amount < $min) {
                    throw new \Exception("The minimum amount for this transaction is " . $min);
                } else if ($amount > $max) {
                    throw new \Exception("The maximum amount for this transaction is " . $max);
                }
                break;
            case Config::get('constants.BILL_PAYMENTS_CODE'):
                $min = intval(Config::get('constants.BILL_PAYMENTS_MIN'));
                $max = intval(Config::get('constants.BILL_PAYMENTS_MAX'));
                if ($amount < $min) {
                    throw new \Exception("The minimum amount for this transaction is " . $min);
                } else if ($amount > $max) {
                    throw new \Exception("The maximum amount for this transaction is " . $max);
                }
                break;
        }
    }

}
