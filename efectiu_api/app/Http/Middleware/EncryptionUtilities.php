<?php

namespace App\Http\Middleware;

use Closure;

class EncryptionUtilities
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
//        $payload = $response->getContent();
//        $payload = $this->processEncryption($payload);
//        $response->setContent($payload);

        return $response;
    }

    public function processEncryption($response_content)
    {
        $encrypted_content = base64_encode($response_content);
        return $encrypted_content;
    }
}
