<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class EconnectUtilities
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    private $ecEconnectPort;
    private $ecEconnectIP;
    private $functions;

    public $isConnected;

    public function __construct()
    {
        try {
            $this->ecEconnectURL = env('ECONNECT_URL');
            $this->ecEconnectIP = env('ECONNECT_IP');
            $this->ecEconnectPort = env('ECONNECT_PORT');
            $this->isConnected = false;
        } catch (Exception $e) {
            $this->isConnected = false;
        }
    }

    public function econnectConnectionStatus()
    {
        $errno = "";
        $timeout = "6";
        try {
            $this->stream = fsockopen($this->ecEconnectIP, $this->ecEconnectPort, $errno, $errstr, $timeout);
            if (!$this->stream) {
                $this->isConnected = false;
            } else {
                $this->isConnected = true;
            }
        } catch (\Exception $e) {
            $this->isConnected = false;
        }

    }

    public function sendToServlet($params)
    {
        Log::info("=============================REQUEST=======================");
        Log::info(base64_decode($params));
        $header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: " . strlen($params),
        );
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_URL, $this->ecEconnectURL);
        curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($channel, CURLOPT_TIMEOUT, 10);
        curl_setopt($channel, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($channel, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($channel, CURLOPT_HTTPHEADER, $header);
        curl_setopt($channel, CURLOPT_POSTFIELDS, $params);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($channel, CURLOPT_VERBOSE, true);

        $response = curl_exec($channel);

        Log::info("=============================RESPONSE=======================");
        Log::info($response);
        Log::info(base64_decode($response));

        if ($response) {
            return $response;
        } else {
            return false;
        }
    }
function getBalXML($xmlFromEC) {
     //   $this->functions->log("INXML", "IN", $xmlFromEC);
        $xmlObject = simplexml_load_string($xmlFromEC);
        foreach ($xmlObject->isomsg->field as $field) {
            $id = $field->attributes()->id;
            $value = $field->attributes()->value;
            switch ($id) {
                case "agentbalance":
                    $agentbalance = $value;
                    $cols['agentbalance'] = $agentbalance;
                    break;

                case "4":
                    $f4 = $value;
                    $cols["f4"] = $f4;
                    break;


                case "39":
                    $f39 = $value;
                    break;
                case "48":
                    $f48 = $value;
                    break;
                case "54":
                    $f54 = $value;
                    break;
                case "68":
                    $f68 = $value;
                    break;
                case "102":
                    $f102 = $value;
                    break;
                case "CREDITBRANCH":
                    $creditbranch = $value;
                    break;
                case "CREDITACCOUNT":
                    $creditaccount = $value;
                    break;
                case "127":
                    $f127 = $value;
                    $cols["f127"] = $f127;
                    break;
                default:
                    break;
            } // end switch
        } // end foreach 

        $cols["CREDITBRANCH"] = $creditbranch;
        $cols["CREDITACCOUNT"] = $creditaccount;
        $cols["f39"] = $f39;
        ///$provider = $this->getResponse(trim($f39));
        $respcode = ($f39 != '777') ? "(" . $f39 . ")" : "";

        $cols["f48"] = "" ;

        $cols["f54"] = $f54;
        $cols["f68"] = $f68;
        $cols["f102"] = $f102;
        if ($f127 != '' && $f127 != null) {
            $cols["f127"] = $f127;
        }

        return $cols;
    }

}
