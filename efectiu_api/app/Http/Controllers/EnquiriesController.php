<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Middleware\ArrayTransform;
use App\Http\Middleware\ApiUtilities;
use App\Http\Middleware\Countries;
use Illuminate\Support\Facades\Validator;
use App\Http\Middleware\EconnectUtilities;
use \Config;

class EnquiriesController extends Controller {

    protected $restful = true;

    public function balance_enquiry(Request $req) {
        try {

            $req = json_decode($req->data);
            $telephone = $req->username;
            $account_number = $req->accountnumber;
            $country = $req->country;


            $country_data = Countries::get_country($country);


            $rules = array(
                'country_name' => 'required',
                'country_code' => 'required',
                'country_prefix' => 'required',
            );
            $messages = array(
                'country_name.required' => 'The country is incorrect',
                'country_code.required' => 'The country code is not provided',
                'country_prefix.required' => 'The country prefix is not provided',
            );
            $validator = Validator::make($data = $country_data, $rules, $messages);

            if ($validator->fails()) {
                $response = array(
                    'error' => true,
                    'message' => array(
                        'error' => $validator->errors()->all()));
                return $response;
            } else {
                $country_name = $country_data['country_name'];
                $country_code = $country_data['country_code'];
                $country_prefix = $country_data['country_prefix'];
                $transType = ApiUtilities::getTransType($telephone, $telephone);

                $transaction_procode = Config::get('constants.BALANCE_ENQUIRY_CODE');
                $transaction_description = Config::get('constants.BALANCE_ENQUIRY_DESC');

                $transaction_channel = "AGENT";
                $amount = "0";


                $request['field0'] = "0200";
                $request['field2'] = ApiUtilities::getCodedTelephone($country_code, $telephone);
                $request['field3'] = $transaction_procode;
                $request['field4'] = $amount;
                $request['field7'] = date('mdHis'); // n10, MMDDhhmmss
                $request['field11'] = ApiUtilities::generateStan();
                $request['field12'] = date('His'); // n6, hhmmss
                $request['field13'] = date('md'); // n4, MMDD
                $request['field24'] = 'CC';
                $request['field32'] = $transaction_channel;
                $request['field37'] = ApiUtilities::generateUnique();
                $request['field41'] = 'FID00001';
                $request['field49'] = $country_prefix; //currency
                $request['field60'] = $country_prefix;
                $request['field65'] =  $account_number;
                $request['field68'] = $transaction_description;
                $request['field98'] = $transaction_description;
                $request['field100'] = 'MVISA';
                $request['field102'] =  $account_number; //41141191939221
                $request['field123'] = $transaction_channel;
                $request['field126'] = $transaction_description;
                $request['TYPE'] = "JSON";
            }

            $requestToEconnect = ArrayTransform::Array2XML($request);
            //return $requestToEconnect;
            ApiUtilities::log('OUTXML', $transaction_description, $requestToEconnect);

            $econnect = new EconnectUtilities();
            $econnect->econnectConnectionStatus();

            if ($econnect->isConnected) {
                try {
                    $encodedResponse = $econnect->sendToServlet($requestToEconnect);
                    return $encodedResponse;
                    $response = json_decode($encodedResponse, true);
                   // ApiUtilities::log('INXML', $transaction_description, $response);



                    if ($response['39'] == "51") {
                        return array(
                            'error' => true,
                            'message' => $response['48']);
                    } else if ($response['39'] != "000" && $response['39'] != "99") {
                        $response = (strlen($response['52']) > 0) ? $response['field52'] : env('INSTITUTION_NAME') . " Mobile Banking Application is Currently Unavailable.Please Try Again Later.";
                        return array(
                            'error' => true,
                            'message' => $response);
                    } else if ($response['39'] == "99") {
                        return array(
                            'error' => true,
                            'message' => $response['48']);
                    } else {

                        $bal = explode("|", $response['54']);

                        return array(
                            'error' => false,
                            'message' => array(
                                'status' => 'Balance enquiry successful',
                                'actual' => $bal[0],
                                'available' => $bal[1]));
                    }
                } catch (\Exception $ex) {
                    throw new \Exception("Problems in processing transaction");
                }
            } else {
                throw new \Exception("System unavailable.");
            }
        } catch (\Exception $ex) {
            return array(
                'error' => true,
                'message' => $ex->getMessage());
        }
    }

}
