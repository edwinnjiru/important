<?php

namespace App\Http\Controllers;

use App\Http\Middleware\TransactionsLimits;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Middleware\ArrayTransform;
use App\Http\Middleware\ApiUtilities;
use App\Http\Middleware\Countries;
use Illuminate\Support\Facades\Validator;
use App\Http\Middleware\EconnectUtilities;
use \Config;

class UtilityPaymentsController extends Controller {

    protected $restful = true;
    protected $txn_min = 100;
    protected $txn_max = 100000;

    public function airtime_purchase(Request $req) {
        try {

            $req = json_decode($req->data);
            $telephone = $req->username;
            $country = $req->country;
            $account_from = $req->account_from;
            $amount = $req->amount;
            $biller_name = $req->biller_name;
            $biller_ref = $req->biller_ref;

            $country_data = Countries::get_country($country);


            $rules = array(
                'country_name' => 'required',
                'country_code' => 'required',
                'country_prefix' => 'required',
            );
            $messages = array(
                'country_name.required' => 'The country is incorrect',
                'country_code.required' => 'The country code is not provided',
                'country_prefix.required' => 'The country prefix is not provided',
            );
            $validator = Validator::make($data = $country_data, $rules, $messages);

            if ($validator->fails()) {
                $response = array(
                    'error' => true,
                    'message' => array(
                        'error' => $validator->errors()->all()));
                return $response;
            } else {
                $country_name = $country_data['country_name'];
                $country_code = $country_data['country_code'];
                $country_prefix = $country_data['country_prefix'];
                $transType = ApiUtilities::getTransType($telephone, $telephone);

                $transaction_procode = Config::get('constants.AIRTIME_ENQUIRY_CODE');
                $transaction_description = Config::get('constants.AIRTIME_ENQUIRY_DESC');
                $transaction_channel = "MBL";
                TransactionsLimits::limit($transaction_procode, $amount);


                $request['field0'] = "0200";
                $request['field2'] = ApiUtilities::getCodedTelephone($country_code, $telephone);
                $request['field3'] = $transaction_procode;
                $request['field4'] = $amount;
                $request['field7'] = date('mdHis'); // n10, MMDDhhmmss
                $request['field11'] = ApiUtilities::generateStan();
                ;
                $request['field12'] = date('His'); // n6, hhmmss
                $request['field13'] = date('md'); // n4, MMDD
                $request['field24'] = $transType;
                $request['field32'] = $transaction_channel;
                $request['field37'] = ApiUtilities::generateUnique();
                ;
                $request['field41'] = 'FID00001';
                $request['field49'] = $country_prefix; //currency
                $request['field60'] = $country_prefix;
                $request['field65'] = $biller_ref;
                $request['field68'] = $transaction_description;
                $request['field100'] = $biller_name;
                $request['field102'] = $account_from;
                $request['field123'] = $transaction_channel;
                $request['field126'] = $transaction_description;
            }

            $requestToEconnect = ArrayTransform::Array2XML($request);
            ApiUtilities::log('OUTXML', $transaction_description, $requestToEconnect);

            $econnect = new EconnectUtilities();
            $econnect->econnectConnectionStatus();

            if ($econnect->isConnected) {
                try {
                    $encodedRequest = base64_encode($requestToEconnect);
                    $encodedResponse = $econnect->sendToServlet($encodedRequest);
                    $responseFromEconnect = base64_decode($encodedResponse);

                    ApiUtilities::log('INXML', $transaction_description, $responseFromEconnect);

                    $xml_string = simplexml_load_string($responseFromEconnect);
                    $response = ArrayTransform::XML2Array($xml_string);


                    if ($response['field39'] == "51") {
                        return array(
                            'error' => true,
                            'message' => $response['field48']);
                    } else if ($response['field39'] != "00" && $response['field39'] != "99") {
                        $response = (strlen($response['field52']) > 0) ? $response['field52'] : env('INSTITUTION_NAME') . " Mobile Banking Application is Currently Unavailable.Please Try Again Later.";

                        return array(
                            'error' => true,
                            'message' => $response);
                    } else if ($response['field39'] == "99") {
                        return array(
                            'error' => true,
                            'message' => $response['field48']);
                    } else {

                        $bal = explode("~", $response['field54']);

                        return array(
                            'error' => false,
                            'message' => array(
                                'status' => 'The airtime purchase was successful.',
                                'balance' => ApiUtilities::decodeFiftyFour($bal[0])));
                    }
                } catch (\Exception $ex) {
                    throw new \Exception("Problems in processing transaction");
                }
            } else {
                throw new \Exception("System unavailable.");
            }
        } catch (\Exception $ex) {
            return array(
                'error' => true,
                'message' => $ex->getMessage());
        }
    }

    public function bill_payments(Request $req) {
        try {
//            dd($req->data);
            $req = json_decode($req->data);
            $telephone = $req->username;
            $country = $req->country;
            $account_from = $req->account_from;
            $amount = $req->amount;
            $biller_name = $req->biller_name;
            $biller_ref = $req->biller_ref;

            $country_data = Countries::get_country($country);


            $rules = array(
                'country_name' => 'required',
                'country_code' => 'required',
                'country_prefix' => 'required',
            );
            $messages = array(
                'country_name.required' => 'The country is incorrect',
                'country_code.required' => 'The country code is not provided',
                'country_prefix.required' => 'The country prefix is not provided',
            );
            $validator = Validator::make($data = $country_data, $rules, $messages);

            if ($validator->fails()) {
                $response = array(
                    'error' => true,
                    'message' => array(
                        'error' => $validator->errors()->all()));
                return $response;
            } else {
                $country_name = $country_data['country_name'];
                $country_code = $country_data['country_code'];
                $country_prefix = $country_data['country_prefix'];
                $transType = ApiUtilities::getTransType($telephone, $telephone);

                $transaction_procode = Config::get('constants.BILL_PAYMENTS_CODE');
                $transaction_description = Config::get('constants.BILL_PAYMENTS_DESC');
                $transaction_channel = "MBL";

                TransactionsLimits::limit($transaction_procode, $amount);


                $request['field0'] = "0200";
                $request['field2'] = ApiUtilities::getCodedTelephone($country_code, $telephone);
                $request['field3'] = $transaction_procode;
                $request['field4'] = $amount;
                $request['field7'] = date('mdHis'); // n10, MMDDhhmmss
                $request['field11'] = ApiUtilities::generateStan();
                ;
                $request['field12'] = date('His'); // n6, hhmmss
                $request['field13'] = date('md'); // n4, MMDD
                $request['field24'] = $transType;
                $request['field32'] = $transaction_channel;
                $request['field37'] = ApiUtilities::generateUnique();
                ;
                $request['field41'] = 'FID00001';
                $request['field49'] = $country_prefix; //currency
                $request['field60'] = $country_prefix;
                $request['field65'] = $biller_ref;
                $request['field68'] = $transaction_description;
                $request['field100'] = $biller_name;
                $request['field102'] = $account_from;
                $request['field123'] = $transaction_channel;
                $request['field126'] = $transaction_description;
            }

            $requestToEconnect = ArrayTransform::Array2XML($request);
            ApiUtilities::log('OUTXML', $transaction_description, $requestToEconnect);

            $econnect = new EconnectUtilities();
            $econnect->econnectConnectionStatus();

            if ($econnect->isConnected) {
                try {
                    $encodedRequest = base64_encode($requestToEconnect);
                    $encodedResponse = $econnect->sendToServlet($encodedRequest);
                    $responseFromEconnect = base64_decode($encodedResponse);

                    ApiUtilities::log('INXML', $transaction_description, $responseFromEconnect);

                    $xml_string = simplexml_load_string($responseFromEconnect);
                    $response = ArrayTransform::XML2Array($xml_string);


                    if ($response['field39'] == "51") {
                        return array(
                            'error' => true,
                            'message' => $response['field48']);
                    } else if ($response['field39'] != "00" && $response['field39'] != "99") {
                        $response = (strlen($response['field52']) > 0) ? $response['field52'] : env('INSTITUTION_NAME') . " Mobile Banking Application is Currently Unavailable.Please Try Again Later.";

                        return array(
                            'error' => true,
                            'message' => $response);
                    } else if ($response['field39'] == "99") {
                        return array(
                            'error' => true,
                            'message' => $response['field48']);
                    } else {

                        $bal = explode("~", $response['field54']);

                        return array(
                            'error' => false,
                            'message' => array(
                                'status' => 'The bill payment was successful.',
                                'balance' => ApiUtilities::decodeFiftyFour($bal[0])));
                    }
                } catch (\Exception $ex) {
                    throw new \Exception("Problems in processing transaction");
                }
            } else {
                throw new \Exception("System unavailable.");
            }
        } catch (\Exception $ex) {
            return array(
                'error' => true,
                'message' => $ex->getMessage());
        }
    }

}
