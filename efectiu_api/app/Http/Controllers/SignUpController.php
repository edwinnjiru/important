<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class SignUpController extends Controller {

    protected $restful = true;

    public function signup(Request $req) {
        
        if (count($req->json()->all())) {
            $postbody = $req->json()->all();
            
            $phonenumber = $req->phonenumber;
            $fname = $req->fname ;
            $lname = $req->lname;
            $surname= $req->surname;
            $email= $req->email;
            $referred_by= $req->referred_by;
            $id_number= '';
            $image= '';
            $gender= '';
            $pin= '';
            
            //$country_data = Countries::get_country($country);

            $rules = array(
                'phonenumber' => 'required',
                'fname' => 'required',
                'lname' => 'required',
                'referred_by' => 'required',
            );

            $messages = array(
                'phonenumber.required' => 'The phonenumber is not provided',
                'fname.required' => 'The first name is not provided',
                'lname.required' => 'The last name is not provided',
                'referred_by.required' => 'The referrer is not provided',
            );
            $validator = Validator::make($data = $postbody, $rules, $messages);

            if ($validator->fails()) {
                $response = array(
                    'error' => true,
                    'message' => array(
                        'error' => $validator->errors()->all()));
                return $response;
            } else {
                //Call customer registration sp
                
                $db_response=DB::select('call sp_create_customer(?,?,?,?,?,?,?,?,?,?)',array($fname,$lname,$surname
                        ,$email,$phonenumber,$pin,$id_number,$image,$gender,$referred_by));                
                
                $response = $db_response;
                
                return $response;

            }
            
        }else{
            
            
        }

    }

}
