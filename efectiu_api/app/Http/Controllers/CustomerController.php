<?php

namespace App\Http\Controllers;

use App\Http\Middleware\ArrayTransform;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\URL;
use App\Http\Middleware\ApiUtilities;
use App\Http\Middleware\Countries;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use App\Http\Middleware\EconnectUtilities;
use \Config;

class CustomerController extends Controller {

    protected $restful = true;

    public function signin(Request $req) {
        $req = json_decode($req->data);
        $req = json_decode(base64_decode($req->get('data')));

        $telephone = $req->username;
        $password = $req->password;
        $country = $req->country;

        return ApiUtilities::generateToken($telephone, "LOGIN");

        $country_data = Countries::get_country($country);


        $rules = array(
            'country_name' => 'required',
            'country_code' => 'required',
            'country_prefix' => 'required',
        );
        $messages = array(
            'country_name.required' => 'The country is incorrect',
            'country_code.required' => 'The country code is not provided',
            'country_prefix.required' => 'The country prefix is not provided',
        );
        $validator = Validator::make($data = $country_data, $rules, $messages);

        if ($validator->fails()) {
            $response = array(
                'error' => true,
                'message' => array(
                    'error' => $validator->errors()->all()));
            return Response::json($response);
        } else {
            $country_name = $country_data['country_name'];
            $country_code = $country_data['country_code'];
            $country_prefix = $country_data['country_prefix'];
            $transType = ApiUtilities::getTransType($telephone, $telephone);

            $transaction_procode = Config::get('constants.LOGIN_CODE');
            $transaction_description = Config::get('constants.LOGIN_DESC');
            $transaction_channel = "MBL";
            $amount = "";

            $request['field0'] = "0200";
            $request['field2'] = ApiUtilities::getCodedTelephone($country_code, $telephone);
            $request['field3'] = $transaction_procode;
            $request['field4'] = $amount;
            $request['field7'] = date('mdHis'); // n10, MMDDhhmmss
            $request['field10'] = '';
            $request['field11'] = ApiUtilities::generateStan();
            ;
            $request['field12'] = date('His'); // n6, hhmmss
            $request['field13'] = date('md'); // n4, MMDD
            $request['field22'] = '';
            $request['field24'] = $transType;
            $request['field32'] = $transaction_channel;
            $request['field37'] = ApiUtilities::generateUnique();
            ;
            $request['field41'] = 'FID00001';
            $request['field49'] = $country_prefix; //currency
            $request['field64'] = $password;
            $request['field60'] = $country_prefix;
            $request['field67'] = '';
            $request['field68'] = $transaction_description;
            $request['field123'] = $transaction_channel;
            $request['field126'] = $transaction_description;
        }
        return "iyaa";

        $requestToEconnect = ArrayTransform::Array2XML($request);
        ApiUtilities::log('OUTXML', $transaction_description, $requestToEconnect);

        $econnect = new EconnectUtilities();
        $econnect->econnectConnectionStatus();

        if ($econnect->isConnected) {
            $encodedRequest = base64_encode($requestToEconnect);
            $encodedResponse = $econnect->sendToServlet($encodedRequest);
            $responseFromEconnect = base64_decode($encodedResponse);
            ApiUtilities::log('INXML', $transaction_description, $responseFromEconnect);
            $xml_string = simplexml_load_string($responseFromEconnect);
            $response = ArrayTransform::XML2Array($xml_string);


            if ($response['field39'] == "99") {
                return Response::json(array(
                                    'error' => true,
                                    'message' => 'Login not successful. ' . $response['field48']), 200)
                                ->header('Content-Type', 'll')
                                ->header('X-Header-One', 'Header Value')
                                ->header('X-Header-Two', 'Header Value');
            } else {
                $field48 = $response['field48'];
                $field48 = ApiUtilities::cleanJSON($field48);


                $firstname = $field48['FIRSTNAME'];
                $secondname = $field48['SECONDNAME'];
                $firstlogin = $field48['FIRSTLOGIN'];
                $mwalletAccount = $field48['MWALLETACCOUNT'];
                $linkedAccounts = $field48['ACCOUNTS'];
                $loanAccounts = $field48['LOANACCOUNTS'];


                $accounts = $linkedAccounts;

                $telco = "";
                $water = "";
                $enter = "";
                $power = "";
                $school = "";
                $otherbills = "";
                $allbillers = "";

                $billers = explode("~", $field48['BILLERS']);
                $countBiller = count($billers);
                for ($b = 0; $b < $countBiller - 1; $b++) {
                    $eachBiller = explode("|", $billers[$b]);
                    if (trim($eachBiller[3]) != "AIRTIME") {
                        $allbillers .= $eachBiller[2] . "~";
                    }
                    if (trim($eachBiller[3]) == "AIRTIME") {
                        $telco .= $eachBiller[2] . "~";
                    } else if (trim($eachBiller[3]) == "ENTERTAINMENT") {
                        $enter .= $eachBiller[2] . "~";
                    } else if (trim($eachBiller[3]) == "WATER") {
                        $water .= $eachBiller[2] . "~";
                    } else if (trim($eachBiller[3]) == "ELECTRICITY") {
                        $power .= $eachBiller[2] . "~";
                    } else if (trim($eachBiller[3]) == "SCHOOL") {
                        $school .= $eachBiller[2] . "~";
                    } else if (trim($eachBiller[3]) == "OTHER UTILITIES") {
                        $otherbills .= $eachBiller[2] . "~";
                    }
                    // return $telco;
                }


                $telcoEx = explode("~", $telco);
                $telcoEx = array_filter($telcoEx);

                $enterEx = explode("~", $enter);
                $enterEx = array_filter($enterEx);

                $waterEx = explode("~", $water);
                $waterEx = array_filter($waterEx);

                $powerEx = explode("~", $power);
                $powerEx = array_filter($powerEx);

                $schoolEx = explode("~", $school);
                $schoolEx = array_filter($schoolEx);

                $otherbillsEx = explode("~", $otherbills);
                $otherbillsExFiltered = array_filter($otherbillsEx);

                $accountsEx = explode("|", $accounts);
                $accountsExFiltered = array_filter($accountsEx);

                $loan_accountsEx = explode("|", $loanAccounts);
                $loan_accountsExFiltered = array_filter($loan_accountsEx);

                $allbillersEx = explode("~", $allbillers);
                $allbillersExFiltered = array_filter($allbillersEx);

                $bank_products_path = storage_path() . "/json/products.json";
                $bank_products = file_get_contents($bank_products_path);
                $products = json_decode(utf8_encode($bank_products), true);

                // return $products['products'];

                $response_to_app = array(
                    'status' => 'Login successful',
                    // 'trials' => trim($econnectResponse['TRIALS']),
                    'customername' => trim($firstname . " " . $secondname),
                    // 'closed' => trim($econnectResponse['CLOSED']),
                    'firstlogin' => trim($firstlogin),
                    // 'timeout' => trim($timeout),
                    'water' => $waterEx,
                    'telcos' => $telcoEx,
                    'entertainment' => $enterEx,
                    'power' => $powerEx,
                    'school' => $schoolEx,
                    'otherbills' => $otherbillsExFiltered,
                    // 'loanbalances' => $response["loanaccounts"],
                    // 'country' => $country,
                    'accounts' => $accountsExFiltered,
                    'loanaccounts' => $loan_accountsExFiltered,
                    'billers' => $allbillersExFiltered,
                    'mwalletAccount' => $mwalletAccount,
                    'version' => 1,
                    'active' => 1,
                    'timeout' => 60000,
                    'bank_products' => $products['products_types']
                        // 'prepaid' => $prepaid,
                        // 'maskedAccounts' => $maskedAccs,
                        // 'allben' => $response["beneficiary"],
                        // 'eachcountry' => $eachCountry
                );


                return Response::json(array(
                                    'error' => false,
                                    'message' => $response_to_app
                                        ), 200)
                                ->header('Content-Type', 'kkk')
                                ->header('X-Header-One', 'Header Value')
                                ->header('X-Header-Two', 'Header Value');
                ;
            }
        } else {

            return Response::json(array(
                        'error' => true,
                        'message' => "System Unavailable"), 200);
        }
    }

    public function pinchange(Request $req) {

        $req = json_decode($req->data);
//        dd($req);
        $telephone = $req->username;
        $old_pin = $req->oldpin;
        $new_pin = $req->newpin;
        $country = $req->country;

        $country_data = Countries::get_country($country);


        $rules = array(
            'country_name' => 'required',
            'country_code' => 'required',
            'country_prefix' => 'required',
        );
        $messages = array(
            'country_name.required' => 'The country is incorrect',
            'country_code.required' => 'The country code is not provided',
            'country_prefix.required' => 'The country prefix is not provided',
        );
        $validator = Validator::make($data = $country_data, $rules, $messages);

        if ($validator->fails()) {
            $response = array(
                'error' => true,
                'message' => array(
                    'error' => $validator->errors()->all()));
            return $response;
        } else {
            $country_name = $country_data['country_name'];
            $country_code = $country_data['country_code'];
            $country_prefix = $country_data['country_prefix'];
            $transType = ApiUtilities::getTransType($telephone, $telephone);

            $transaction_procode = Config::get('constants.PIN_CHANGE_CODE');
            $transaction_description = Config::get('constants.PIN_CHANGE_DESC');
            $transaction_channel = "MBL";
            $amount = "0";

            $request['field0'] = "0200";
            $request['field2'] = ApiUtilities::getCodedTelephone($country_code, $telephone);
            $request['field3'] = $transaction_procode;
            $request['field4'] = $amount;
            $request['field7'] = date('mdHis'); // n10, MMDDhhmmss
            $request['field11'] = ApiUtilities::generateStan();
            ;
            $request['field12'] = date('His'); // n6, hhmmss
            $request['field13'] = date('md'); // n4, MMDD
            $request['field24'] = $transType;
            $request['field32'] = $transaction_channel;
            $request['field37'] = ApiUtilities::generateUnique();
            ;
            $request['field41'] = 'FID00001';
            $request['field49'] = $country_prefix; //currency
            $request['field60'] = $country_prefix;
            $request['field64'] = $old_pin;
            $request['field65'] = $new_pin;
            $request['field68'] = $transaction_description;
            $request['field123'] = $transaction_channel;
            $request['field126'] = $transaction_description;
        }

        $requestToEconnect = ArrayTransform::Array2XML($request);
        ApiUtilities::log('OUTXML', $transaction_description, $requestToEconnect);

        $econnect = new EconnectUtilities();
        $econnect->econnectConnectionStatus();

        if ($econnect->isConnected) {
            try {
                $encodedRequest = base64_encode($requestToEconnect);
                $encodedResponse = $econnect->sendToServlet($encodedRequest);
                $responseFromEconnect = base64_decode($encodedResponse);

                ApiUtilities::log('INXML', $transaction_description, $responseFromEconnect);

                $xml_string = simplexml_load_string($responseFromEconnect);
                $response = ArrayTransform::XML2Array($xml_string);

                if ($response['field39'] == "00") {
                    return Response::json(array(
                                'error' => false,
                                'message' => array(
                                    'status' => $response['field48'])), 200);
                } else if ($response['field39'] != "00" && $response['field39'] != "99") {
                    $response = (strlen($response['field52']) > 0) ? $response['field52'] : env('INSTITUTION_NAME') . " Mobile Banking Application is Currently Unavailable.Please Try Again Later.";

                    return array(
                        'error' => true,
                        'message' => $response);
                } else if ($response['field39'] == "99") {
                    return array(
                        'error' => true,
                        'message' => $response['field48']);
                }
            } catch (\Exception $ex) {
                return array(
                    'error' => true,
                    'message' => "Problems in processing");
            }
        } else {
            return array(
                'error' => true,
                'message' => "System Unavailable");
        }
    }

}
