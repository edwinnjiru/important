<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Middleware\ArrayTransform;
use App\Http\Middleware\ApiUtilities;
use App\Http\Middleware\Countries;
use Illuminate\Support\Facades\Validator;
use App\Http\Middleware\EconnectUtilities;
use App\Http\Middleware\TransactionsLimits;
use \Config;

class CashInController extends Controller {

    protected $restful = true;

    public function cash_in(Request $req) {

        $req = json_decode($req->data);
        $amount = $req->amount;
        $account_number = $req->accountnumber;
        $account_number_agent = $req->account_number_agent;
        $country = $req->country;

        $country_data = Countries::get_country($country);


        $rules = array(
            'country_name' => 'required',
            'country_code' => 'required',
            'country_prefix' => 'required',
        );
        $messages = array(
            'country_name.required' => 'The country is incorrect',
            'country_code.required' => 'The country code is not provided',
            'country_prefix.required' => 'The country prefix is not provided',
        );
        $validator = Validator::make($data = $country_data, $rules, $messages);

        if ($validator->fails()) {
            $response = array(
                'error' => true,
                'message' => array(
                    'error' => $validator->errors()->all()));
            return $response;
        } else {
            $country_name = $country_data['country_name'];
            $country_code = $country_data['country_code'];
            $country_prefix = $country_data['country_prefix'];
//            $transType = ApiUtilities::getTransType($telephone, $telephone);

            $transaction_procode = Config::get('constants.CASH_DEPOSIT_CODE');
//            $transaction_description = Config::get('constants.CASH_DEPOSIT_DESC');
            $transaction_description = $account_number_agent . " " . $account_number . " " . $transaction_procode . " " . ApiUtilities::generateUnique() . " " . 'MVISA_CASH_IN';
            $transaction_channel = "AGENT";

            $request['field0'] = "0200";
            $request['field2'] = '41141191939221';
            $request['field3'] = $transaction_procode;
            $request['field4'] = $amount;
            $request['field7'] = date('mdHis'); // n10, MMDDhhmmss
            $request['field11'] = ApiUtilities::generateStan();
            $request['field12'] = date('His'); // n6, hhmmss
            $request['field13'] = date('md'); // n4, MMDD
            $request['field24'] = 'CC';
            $request['field32'] = $transaction_channel;
            $request['field37'] = ApiUtilities::generateUnique();
            $request['field41'] = 'FID00001';
            $request['field49'] = "KES"; //currency
            $request['field56'] = 'bcac5bdc-32fb-42df-8ddb-ce8f705ba8c5';
            $request['field60'] = $country_prefix;
            $request['field65'] = $account_number; //customer PAN
            $request['field68'] = $transaction_description; //note this is the narration and we need to append more details on it
            $request['field98'] = 'MVISA_CASH_IN';
            $request['field100'] = 'MVISA';
            $request['field102'] = '41141191939221'; //the agent PAN
            $request['field103'] = $account_number; //the customer PAN
            $request['field123'] = $transaction_channel;
            $request['field126'] = 'MVISA';
            $request['DEBITBRANCH'] = '001';
            $request['BRANCH'] = "";
            $request['TYPE'] = "JSON";
        }

        $requestToEconnect = ArrayTransform::Array2XML($request);


        //return $requestToEconnect;
        ApiUtilities::log('OUTXML', 'MVISA_CASH_IN', $requestToEconnect);

        $econnect = new EconnectUtilities();
        $econnect->econnectConnectionStatus();

        if ($econnect->isConnected) {
            try {
                $encodedResponse = $econnect->sendToServlet($requestToEconnect);

                ApiUtilities::log('INXML', 'MVISA_CASH_IN', $encodedResponse);

                $json = json_decode($encodedResponse, true);
                $responsedata = $json['39'];
                $response['success'] = array();
                if ($responsedata == 000) {
                    return array(
                        'error' => false,
                        'message' => array(
                            'status' => 'The cash was successfully deposited.'
                    ));
                } else {
                    return array(
                        'error' => false,
                        'message' => array(
                            'status' => 'Problem in processing the request'
                    ));
                }
            } catch (\Exception $ex) {
                return array(
                    'error' => true,
                    'message' => "Problems in processing");
            }
        } else {
            return array(
                'error' => true,
                'message' => "System Unavailable");
        }
    }

}
