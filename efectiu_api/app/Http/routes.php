<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::group(['middleware' => ['api'], 'prefix' => 'api'], function () {
    Route::post('register', 'APIController@register');
    Route::post('login', 'APIController@login');
    Route::group(['middleware' => 'jwt-auth'], function () {
        Route::post('get_user_details', 'APIController@get_user_details');
    });
});

Route::group(array('prefix' => 'api/v1'), function () {
    Route::post('signin', 'CustomerController@signin');

    Route::group(['middleware' => ['decrypt-utilities', 'encrypt-utilities']], function () {
        Route::post('signup', 'SignUpController@signup');
        
        Route::post('pinchange', 'CustomerController@pinchange');
        Route::post('balance', 'EnquiriesController@balance_enquiry');
        Route::post('mini', 'StatementsController@ministatement');
        Route::post('airtime', 'UtilityPaymentsController@airtime_purchase');
        Route::post('bill', 'UtilityPaymentsController@bill_payments');
        Route::post('ftown', 'FundTransfersController@fund_transfer_own');
        Route::post('ftother', 'FundTransfersController@fund_transfer_other');
        Route::post('ftmobile', 'FundTransfersController@fund_transfer_b2c');
        Route::post('cashin', 'CashInController@cash_in');
        Route::post('login', 'CustomerLoginController@loginagent');
        Route::post('connection', 'CustomerLoginController@connection');
    });
});
