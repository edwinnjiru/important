
package ke.co.coopbank.CardEnquiries;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Card/DataModel/Card/GetCardsByAccount/1.0/Card.getCardsByAccount}getAccountOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAccountOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Card/Service/Card/GetCardsByAccount/1.0/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Card/DataModel/Card/GetCardsByAccount/1.0/Card.getCardsByAccount")
    protected GetAccountOutputType getAccountOutput;

    /**
     * Gets the value of the getAccountOutput property.
     * 
     * @return
     *     possible object is
     *     {@link GetAccountOutputType }
     *     
     */
    public GetAccountOutputType getGetAccountOutput() {
        return getAccountOutput;
    }

    /**
     * Sets the value of the getAccountOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAccountOutputType }
     *     
     */
    public void setGetAccountOutput(GetAccountOutputType value) {
        this.getAccountOutput = value;
    }

}
