/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Validators;

import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.LogEngine.ESBLog;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author njiru
 */
public class ProcessValidateRequests {

    Map<String, String> configs = new HashMap();
    Map<String, String> requestMap = new HashMap();


    public ProcessValidateRequests(Map<String, String> configs, Map<String, String> requestMap) {
        this.configs = configs;
        this.requestMap = requestMap;
    }

    public Map doProcess() {
        Map<String, String> responseMap = new HashMap();
        try {
            switch (requestMap.get("businessshortcode").trim().toUpperCase()) {
                case "400200":
                    if (("01".equalsIgnoreCase(requestMap.get("destinationacc").substring(0, 2))) || ("02".equalsIgnoreCase(requestMap.get("destinationacc").substring(0, 2))) || ("03".equalsIgnoreCase(requestMap.get("destinationacc").substring(0, 2)))) {
                        responseMap = (new ValidateBFUB(configs, requestMap)).validateAcc();
                        sendToHoldingQueue(responseMap);
                        responseMap.remove("receiver");
                    } else if (("254".equalsIgnoreCase(requestMap.get("destinationacc").trim().substring(0, 3))) || ("07".equalsIgnoreCase(requestMap.get("destinationacc").trim().substring(0, 2)))) {
                        responseMap = (new ValidateWallet(configs, requestMap)).validatephonenumber();
                        sendToHoldingQueue(responseMap);
                        responseMap.remove("receiver");
                    } else if (requestMap.get("destinationacc").contains("#")) {
                        responseMap = (new ValidateSacco(configs, requestMap)).validatesacco();
                    } else if (Character.isLetter(requestMap.get("destinationacc").trim().charAt(0))) {//requestMap.get("destinationacc").contains("CR")
                        requestMap.put("destinationacc",requestMap.get("destinationacc").replaceAll("\\s+",""));
                        responseMap = (new ValidateCard(configs, requestMap)).validateCardNo();
                        sendToHoldingQueue(responseMap);
                        responseMap.remove("receiver");
                        responseMap.remove("cardno");
                    } else {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Invalid account");
                        responseMap.put("valid", "false");
                        responseMap.put("ResultCode", "211");
                    }
                    break;
                case "400222":
                    responseMap = (new ValidateInstitution(configs, requestMap)).validateinstitution();
                    break;
                default:
                    responseMap.put("Error", "true");
                    responseMap.put("Message", "Invalid bussiness code");
                    responseMap.put("valid", "false");
                    responseMap.put("ResultCode", "220");
                    break;
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(ValidateAccounts)", "Error:" + sw.toString());
            el.log();
        }

        return responseMap;
    }

    public void sendToHoldingQueue(Map<String, String> responseMap){
        try {
            QueueWriter qr = new QueueWriter("jms/C2B_HOLDINGQUEUE");
            boolean sent = qr.sendObject(responseMap, requestMap.get("destinationacc"));

            (new ESBLog("ToHoldingQueue", "Sent: " + sent + ", Map: " + responseMap)).log();

        }catch (Exception ex){
            (new ESBLog("Exception(STHQ)",ex)).log();
        }

    }

}
