/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Validators;

import ke.co.coopbank.CardEnquiries.*;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.MessageHandlers.CardEnquiryHandler;
import ke.co.ekenya.MessageHandlers.CardEnquiryResolver;

import javax.xml.ws.BindingProvider;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * @author njiru
 */
public class ValidateCard {

    Map<String, String> configs = new HashMap();
    Map<String, String> requestMap = new HashMap();

    public ValidateCard(Map<String, String> configs, Map<String, String> requestMap) {
        this.configs = configs;
        this.requestMap = requestMap;
    }

    public Map validateCardNo() {
        Map<String, String> responseMap = new HashMap();
        try {
            //Fetch card details
            (new DatabaseFunctions()).fetchMappings(requestMap);


            DataInput dataInput = new DataInput();
            GetAccountInputType inputType = new GetAccountInputType();
            ObjectFactory obj = new ObjectFactory();
            GetAccountInputType.OperationParameters operationParameters = new GetAccountInputType.OperationParameters();

            String prefix=requestMap.get("destinationacc").trim().substring(0, 2).toUpperCase();
            operationParameters.setAccountNumber(requestMap.get("destinationacc").replace(prefix, ""));
            operationParameters.setBankCode(obj.createGetAccountInputTypeOperationParametersBankCode(requestMap.get("bankc")));
            operationParameters.setGroupCode(obj.createGetAccountInputTypeOperationParametersGroupCode(requestMap.get("groupc")));

            inputType.setOperationParameters(operationParameters);
            dataInput.setGetAccountInput(inputType);

            String proxypass = (new EncryptionUtils()).decrypt(configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + configs.get("C2B_WSDLS_PATH") + "Card_Card_getCardsByAccount_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            CardEnquiryHandler handler = new CardEnquiryHandler(configs);
            CardEnquiryResolver resolver = new CardEnquiryResolver(handler);
            service.setHandlerResolver(resolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request with proxy username and password
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configs.get("SOA_USERNAME"));
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            //time-outs
            int connecttimeout = Integer.parseInt(configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.getCardsByAccount(dataInput);
            String response = handler.getMyXML();

            if (response == null || response.isEmpty() || response.trim().equalsIgnoreCase("")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "SOA Generic Error");
                responseMap.put("ResultCode", "212");
                responseMap.put("valid", "false");
            } else {
                breakDownSOAResponse(response, responseMap);
            }


        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(validateCardNo)", "Error:" + sw.toString());
            el.log();

            responseMap.put("Error", "true");
            responseMap.put("Message", "Exception occured");
            responseMap.put("ResultCode", "200");
            responseMap.put("valid", "false");
        }
        return responseMap;
    }

    private static void breakDownSOAResponse(String SOAresp, Map<String, String> responseMap) {
        HashMap result = new HashMap();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAresp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("SOAP-ENV:Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("ns:HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("ns:StatusMessages");
            result.put("statusDescription", (String) headerReply.get("ns:StatusDescription"));
            result.put("statusCode", (String) headerReply.get("ns:StatusCode"));
            result.put("messageID", (String) headerReply.get("ns:MessageID"));

            //status messages breakdown for header
            HashMap statusMessage = (HashMap<String, Object>) statusMessages.get("ns:StatusMessage");
            result.put("messageCode", (String) statusMessage.get("ns:MessageCode"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                //BreakDown Of Body
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("SOAP-ENV:Body");
                HashMap<String, Object> dataOutput = (HashMap<String, Object>) body.get("ns0:DataOutput");
                HashMap<String, Object> getAccountOutput = (HashMap<String, Object>) dataOutput.get("ns1:getAccountOutput");


                HashMap<String, Object> Card = (HashMap<String, Object>) getAccountOutput.get("ns2:Card");

                String cardExpiry = (String) Card.get("ns2:ExpiryDate");
                String cardNo = (String) Card.get("ns2:Number");
                String cardHolder = (String) Card.get("ns2:HolderNameInCard");

                responseMap.put("Error", "false");
                responseMap.put("Message", "Card is valid");
                responseMap.put("valid", "true");
                responseMap.put("ResultCode", "000");
                responseMap.put("cardno", cardNo);
                responseMap.put("receiver", cardHolder);


            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "SOA Generic error");
                responseMap.put("ResultCode", "212");
                responseMap.put("valid", "false");

            }

        } catch (Exception ex) {
            (new ESBLog("Exceptions(querycard)", ex)).log();

            responseMap.put("Error", "true");
            responseMap.put("Message", "Exception occured");
            responseMap.put("ResultCode", "200");
            responseMap.put("valid", "false");
        }

    }


}
