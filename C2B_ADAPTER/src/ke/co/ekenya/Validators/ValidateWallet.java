/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Validators;

import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author njiru
 */
public class ValidateWallet {

    Map<String, String> configs = new HashMap();
    Map<String, String> requestMap = new HashMap();


    public ValidateWallet(Map<String, String> configs, Map<String, String> requestMap) {
        this.configs = configs;
        this.requestMap = requestMap;
    }

    public Map validatephonenumber() {
        Map<String, String> responseMap = new HashMap();
        String phonenumber = requestMap.get("destinationacc");
        try {
            if (phonenumber.length() == 10) {
                phonenumber = (new StringBuilder("254")).append(phonenumber.substring(1)).toString();
                requestMap.put("destinationacc",phonenumber);
            }

            (new DatabaseFunctions()).validatecustomer(requestMap);
            if ("TRUE".equalsIgnoreCase(requestMap.get("valid"))) {
                responseMap.put("Error", "false");
                responseMap.put("Message", "Phonenumber is valid");
                responseMap.put("valid", "true");
                responseMap.put("ResultCode", "000");
                responseMap.put("receiver",requestMap.get("name"));

            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Customer not valid");
                responseMap.put("valid", "false");
                responseMap.put("ResultCode", "001");
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(validatephonenumber)", "Error:" + sw.toString());
            el.log();
            responseMap.put("Error", "true");
            responseMap.put("Message", "Exception occured");
            responseMap.put("ResultCode", "200");
            responseMap.put("valid", "false");
        }
        return responseMap;
    }

}
