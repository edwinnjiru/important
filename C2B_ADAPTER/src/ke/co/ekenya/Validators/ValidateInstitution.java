package ke.co.ekenya.Validators;

import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;

import java.util.HashMap;
import java.util.Map;

public class ValidateInstitution {
    Map<String, String> configs = new HashMap();
    Map<String, String> requestMap = new HashMap();

    public ValidateInstitution(Map<String, String> configs, Map<String, String> requestMap) {
        this.configs = configs;
        this.requestMap = requestMap;
    }

    public Map validateinstitution() {
        Map<String, String> responseMap = new HashMap();
        try {
            String institutionID=requestMap.get("destinationacc").trim().split("#")[0];
            boolean valid=(new DatabaseFunctions()).validateinstitution(institutionID);
            if (valid) {
                responseMap.put("Error", "false");
                responseMap.put("Message", "Institution is valid");
                responseMap.put("valid", "true");
                responseMap.put("ResultCode", "000");

            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Institution not valid");
                responseMap.put("valid", "false");
                responseMap.put("ResultCode", "001");
            }
        }catch (Exception ex){
            (new ESBLog("Exception(InstVal)",ex)).log();
        }
        return responseMap;
    }
}
