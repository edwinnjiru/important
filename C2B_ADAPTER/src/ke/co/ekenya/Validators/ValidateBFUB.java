/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Validators;

import ke.co.coopbank.BfubEnquiry.*;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.MessageHandlers.BFUBEnquiryHandler;
import ke.co.ekenya.MessageHandlers.BFUBEnquiryResolver;

import javax.xml.ws.BindingProvider;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


/**
 * @author njiru
 */
public class ValidateBFUB {

    Map<String, String> configs = new HashMap();
    Map<String, String> requestMap = new HashMap();
    Map<String, String> responseMap = new HashMap();

    public ValidateBFUB(Map<String, String> configs, Map<String, String> requestMap) {
        this.configs = configs;
        this.requestMap = requestMap;
    }

    public Map validateAcc() {
        responseMap.clear();
        String response = "";
        try {

            DataInput dataInput = new DataInput();
            AccountType accountType = new AccountType();
            GetDetailsInputType getDetails = new GetDetailsInputType();

            accountType.setAccountNumber(requestMap.get("destinationacc"));
            getDetails.setAccount(accountType);
            dataInput.setGetDetailsInput(getDetails);

            String proxypass = (new EncryptionUtils()).decrypt(configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + configs.get("C2B_WSDLS_PATH") + "Account_Account_getDetails_1.1.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            BFUBEnquiryHandler handler = new BFUBEnquiryHandler(configs);
            BFUBEnquiryResolver resolver = new BFUBEnquiryResolver(handler);
            service.setHandlerResolver(resolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request with proxy username and password
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configs.get("SOA_USERNAME"));
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);
            /*provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "pck");
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "pck123");*/

            //time-outs
            int connecttimeout = Integer.parseInt(configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.getDetails(dataInput);
            response = handler.getMyXML();

            if (response == null || response.isEmpty() || response.trim().equalsIgnoreCase("")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "SOA Generic Error");
                responseMap.put("ResultCode", "212");
                responseMap.put("valid", "false");
            } else {
                breakDownSOAResponse(response);
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(validateBFUB)", "Error:" + sw.toString());
            el.log();

            responseMap.put("Error", "true");
            responseMap.put("Message", "Exception occured");
            responseMap.put("ResultCode", "200");
            responseMap.put("valid", "false");
        }
        return responseMap;
    }

    private void breakDownSOAResponse(String SOAresp) {
        HashMap result = new HashMap();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAresp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("SOAP-ENV:Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("ns:HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("ns:StatusMessages");
            result.put("statusDescription", (String) headerReply.get("ns:StatusDescription"));
            result.put("statusCode", (String) headerReply.get("ns:StatusCode"));
            result.put("messageID", (String) headerReply.get("ns:MessageID"));

            //status messages breakdown for header
            HashMap statusMessage = (HashMap<String, Object>) statusMessages.get("ns:StatusMessage");
            result.put("messageCode", (String) statusMessage.get("ns:MessageCode"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                //BreakDown Of Body
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("SOAP-ENV:Body");
                HashMap<String, Object> dataOutput = (HashMap<String, Object>) body.get("ns0:DataOutput");
                HashMap<String, Object> getDetailsOutput = (HashMap<String, Object>) dataOutput.get("ns1:getDetailsOutput");
                HashMap<String, Object> getAccount = (HashMap<String, Object>) getDetailsOutput.get("ns2:Account");

                responseMap.put("Error", "false");
                responseMap.put("Message", "Account is valid");
                responseMap.put("valid", "true");
                responseMap.put("ResultCode", "000");
                responseMap.put("receiver",getAccount.get("ns2:AccountName").toString().trim().replaceAll("[^a-zA-Z0-9]",""));

            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "SOA Generic Error");
                responseMap.put("valid", "false");
                responseMap.put("ResultCode", "212");
            }

        } catch (Exception ex) {
            (new ESBLog("Exceptions(queryBFUB)", ex)).log();
            responseMap.put("Error", "true");
            responseMap.put("Message", "Exception occured");
            responseMap.put("ResultCode", "200");
            responseMap.put("valid", "false");
        }
    }

}
