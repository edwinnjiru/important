package ke.co.ekenya.Validators;

import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;

import java.util.HashMap;
import java.util.Map;

public class ValidateSacco {

    Map<String, String> configs = new HashMap();
    Map<String, String> requestMap = new HashMap();

    public ValidateSacco(Map<String, String> configs, Map<String, String> requestMap) {
        this.configs = configs;
        this.requestMap = requestMap;
    }

    public Map validatesacco() {
        Map<String, String> responseMap = new HashMap();
        try {
            String saccobin=requestMap.get("destinationacc").trim().split("#")[0];
            boolean valid=(new DatabaseFunctions()).validatesacco(saccobin);
            if (valid) {
                responseMap.put("Error", "false");
                responseMap.put("Message", "Sacco is valid");
                responseMap.put("valid", "true");
                responseMap.put("ResultCode", "000");

            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Sacco not valid");
                responseMap.put("valid", "false");
                responseMap.put("ResultCode", "001");
            }

        }catch (Exception ex){
            (new ESBLog("Exception(SaccoVal)",ex)).log();
            responseMap.put("Error", "true");
            responseMap.put("Message", "Exception occured");
            responseMap.put("ResultCode", "200");
            responseMap.put("valid", "false");
        }
       return responseMap;
    }
}
