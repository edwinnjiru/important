package ke.co.ekenya.LogEngine;

import ke.co.ekenya.CommonOperations.QueueWriter;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

/**
 * ESBLog - Handles logging errors and misc messages
 *
 * @author Edwin Njiru
 */
public class ESBLog {

    private String filename = null;
    private String msg = null;


    /*
     * Initializes filename and msg#ESBLog(filename,msg)
     *
     */
    public ESBLog(String filename, String msg) {
        this.filename = filename;
        this.msg = msg;
    }

    public ESBLog(String filename, HashMap<String, String> map) {
        this.filename = filename;
        HashMap<String, String> logmap = new HashMap<String, String>(map);
        if (logmap.containsKey("RawMessage")) {
            logmap.put("RawMessage", "");
        }
        this.msg = logmap.toString();
    }

    public ESBLog(String msg) {
        this.filename = "exceptions";
        this.msg = msg;
    }

    public ESBLog(String filename, Exception ex) {
        this.filename = filename;
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString();
    }

    public void log() {
        try {
            String directory = "C2B_ADAPTER";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss.SSS");
            LocalDateTime date = LocalDateTime.now();
            String timestamp = date.format(formatter);
            String finalmessage = "###" + directory + "$$$" + filename + "###" + timestamp + "::" + msg;
            QueueWriter qw = new QueueWriter("jms/ESB_LOG_QUEUE");
            qw.send(finalmessage, "log");
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

}
