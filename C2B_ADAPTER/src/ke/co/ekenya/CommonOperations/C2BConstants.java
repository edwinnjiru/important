package ke.co.ekenya.CommonOperations;

/**
 * Created by Felix on 4/29/16.
 */
public final class C2BConstants {

    public static final String SAFARICOM_BILLER_CODE = "SAFCOM";

    public static final String COUNTRY_CODE = "254";
    public static final String CURRENCY_CODE = "KSHS";

    public static final String SAFARICOM_SPID = "151515";
    public static final String SAFARICOM_SPPASSWORD = "ODExNERCMDlCNjVDRkYxQUFCNzE5MkE1OEQyMjJDMzk1RUFCNzgwMjk2ODE4Rjk1OTE2MEFGNDU1QkRCMDkyMg==";
    public static final String SAFARICOM_SERVICE_URL = "http://api-v1.gen.mm.vodafone.com/mminterface/request";
    public static final String VALIDATION_URL = "http://10.66.49.201:8099/mock";
    public static final String CONFIRMATION_URL = "http://10.66.49.201:8099/mock";

    public static final String SUCCESS_RESULT_CODE = "0";
    public static final String INVALID_MSISDN_CODE = "C2B1011";
    public static final String INVALID_ACCOUNT_NUMBER_CODE = "C2B1012";
    public static final String INVALID_AMOUNT_CODE = "C2B1013";
    public static final String INVALID_KYC_DETAILS = "C2B1014";
    public static final String INVALID_SHORT_CODE = "C2B1015";
    public static final String OTHER_ERROR_CODE = "C2B1016";


    public static final String SUCCESS_RESULT_STRING = "Success";
    public static final String INVALID_MSISDN_STRING = "Invalid MSISDN";
    public static final String INVALID_ACCOUNT_NUMBER_STRING = "Invalid Account number";
    public static final String INVALID_AMOUNT_STRING = "Invalid Amount";
    public static final String INVALID_KYC_DETAILS_STRING = "Invalid KYC details";
    public static final String INVALID_SHORT_CODE_STRING = "Invalid Shortcode";
    public static final String OTHER_ERROR_CODE_STRING = "Other Error";

    // 254726403959
    public static final String PAYBILL_PHONE_NUMBER = "254726404412";
    public static final String MKARO_PHONE_NUMBER = "254726404412";
    public static final String INSTITUTIONS_PHONE_NUMBER = "254726403959";
    //MPESA254726403959 institutions
    // 254726403959

    public static final String PAYBILL_TILL_NUMBER = "400200";
    public static final String MKARO_TILL_NUMBER = "400222";
    public static final String RIGHTS_TILL_NUMBER = "400205";

}
