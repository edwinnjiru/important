package ke.co.ekenya.CommonOperations;


import ke.co.ekenya.Api.C2BServlet;
import ke.co.ekenya.LogEngine.ESBLog;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Enumeration;
import java.util.Hashtable;

public class DistributedQueueBrowser {

    private QueueConnection qcon;
    private QueueSession qsession;
    private QueueReceiver receiver;
    private Queue queue;
    private QueueConnectionFactory qconFactory;

    public Message browseWebLogicQueue(String JMSCorrelationID, String url, String QUEUE) {
        InitialContext ic = getInitialContext(url);
        Message msg = null;
        try {
            if (ic != null) {
                if (init(ic, QUEUE)) {
                    QueueBrowser browser = qsession.createBrowser(queue, "JMSCorrelationID = '" + JMSCorrelationID + "'");
                    Enumeration en = browser.getEnumeration();
                    while (en.hasMoreElements()) {
                        Message message = (Message) en.nextElement();
                        msg = message;
                        receiver = qsession.createReceiver(queue, "JMSCorrelationID = '" + JMSCorrelationID + "'");
                        receiver.receive(30 * 1000);
                    }
                    browser.close();
                }
            }
        } catch (Exception ex) {
            ESBLog el = new ESBLog("Exception(Browser)", ex);
            el.log();
        }
        close();
        return msg;
    }

    public boolean init(Context ctx, String queueName) {
        try {
            qconFactory = (QueueConnectionFactory) ctx.lookup(C2BServlet.CONFIGS.get("JMS_FACTORY"));
            qcon = qconFactory.createQueueConnection();
            qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            queue = (Queue) ctx.lookup(queueName);
            qcon.start();
            return true;

        } catch (NamingException | JMSException ex) {
            ESBLog el = new ESBLog("Exception(Init)", ex);
            el.log();
            return false;
        }
    }

    private InitialContext getInitialContext(String url) {
        Hashtable env = new Hashtable();
        InitialContext ic = null;
        String jndi = "weblogic.jndi.WLInitialContextFactory";
        try {
            env.put(Context.INITIAL_CONTEXT_FACTORY, jndi);
            env.put(Context.PROVIDER_URL, url);
            ic = new InitialContext(env);
        } catch (Exception ex) {
            ESBLog el = new ESBLog("", ex);
            el.log();
        }
        return ic;
    }

    public void close() {
        try {
            if (qsession != null) {
                qsession.close();
            }
            if (qcon != null) {
                qcon.close();
            }
            if (receiver != null) {
                receiver.close();
            }
        } catch (JMSException ex) {
            ESBLog el = new ESBLog("Exception(close)", ex);
            el.log();
        }
    }
}
