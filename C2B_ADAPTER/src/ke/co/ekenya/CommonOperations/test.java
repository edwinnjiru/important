package ke.co.ekenya.CommonOperations;

import java.util.Collections;
import java.util.Stack;

public class test {

    public static void main(String[] args) {
        String padNumerals = "%03d";
        try {
            Stack <String> stack = new Stack();

            for (int i = 1; i <= 26; i++) {
                //counter2
                for (int j = 1; j <= 26; j++) {
                    for (int k = 1; k <= 999; k++) {
                        //System.out.println("outer: "+getCharForNumber(i) +" inner: "+getCharForNumber(j)+" numerals: "+String.format(padNumerals,k));
                        stack.push((new StringBuilder(getCharForNumber(i)).append(getCharForNumber(j)).append(String.format(padNumerals, k))).toString());
                    }
                }
            }
            reverse(stack);
            Stack newStack=(Stack)stack.clone();
            System.out.println("Peeked "+newStack.size());



        } catch (Exception ex) {
            System.out.println("error: " + ex.getMessage());
        }
    }

    private static String getCharForNumber(int i) {
        return i > 0 && i < 27 ? String.valueOf((char) (i + 'A' - 1)) : null;
    }

    public static void reverse(Stack<String> arr){
        arr.sort(Collections.reverseOrder());
    }
}
