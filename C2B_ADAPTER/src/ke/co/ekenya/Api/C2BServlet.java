package ke.co.ekenya.Api;

import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "C2BServlet", urlPatterns = {"/coop/c2bservices"})
public class C2BServlet extends HttpServlet {
    public static final Map<String, String> CONFIGS = new HashMap();

    @Override
    public void init() {
        (new Utilities()).readPropertyFile(CONFIGS);
        (new ESBLog("readconfigs", CONFIGS.toString())).log();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            String content = getServletInputStream(request);
            String res = (new RequestProcessor(content.replaceAll("&", ""), CONFIGS)).process();
            out.println(res);
            out.flush();
            (new ESBLog("C2B_Rqst&Resp", "Request: " + content + " Response: " + res)).log();
        } catch (Exception ex) {
            (new ESBLog("Exception(doPost)", ex)).log();
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Available", true);
            out.println(jsonObject.toString());
            out.flush();
        } catch (Exception ex) {
            (new ESBLog("Exception(doGet)", ex)).log();
        }

    }

    public String getServletInputStream(HttpServletRequest request) {
        String response = "";
        StringBuilder jb = new StringBuilder();
        String line = "";
        try (BufferedReader reader = request.getReader()) {
            while ((line = reader.readLine()) != null) {
                jb.append(line);
            }
            response = jb.toString();
        } catch (IOException ex) {
            (new ESBLog("Exceptions(InputStream)", ex)).log();
        }
        return response;
    }
}
