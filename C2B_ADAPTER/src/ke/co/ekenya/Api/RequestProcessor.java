package ke.co.ekenya.Api;

import commonj.work.WorkException;
import commonj.work.WorkManager;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.CommonOperations.WorkManagerUtilities;
import ke.co.ekenya.Confirmation.ProcessConfirmation;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.Validators.ProcessValidateRequests;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RequestProcessor {

    String request;
    Map<String, String> configs;

    public RequestProcessor(String request, Map<String, String> configs) {
        this.request = request;
        this.configs = configs;
    }

    private static boolean validatefields(Map<String, String> requestMap, Map<String, String> responseMap) {
        boolean validity = true;
        try {

            if (!requestMap.containsKey("type")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Missing request type");
                responseMap.put("ResultCode", "201");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

            if (("".equalsIgnoreCase(requestMap.get("type"))) || (!("validate".equalsIgnoreCase(requestMap.get("type"))) && !("confirm".equalsIgnoreCase(requestMap.get("type"))))) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Invalid request type");
                responseMap.put("ResultCode", "202");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

            switch (requestMap.get("type").trim()) {
                case "validate":
                    if (!requestMap.containsKey("destinationacc")) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Missing destinationacc tag");
                        responseMap.put("ResultCode", "203");
                        responseMap.put("valid", "false");
                        validity = false;
                        return validity;
                    }
                    if ("".equalsIgnoreCase(requestMap.get("destinationacc"))) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Empty destinationacc tag");
                        responseMap.put("ResultCode", "204");
                        responseMap.put("valid", "false");
                        validity = false;
                        return validity;
                    }

                    if (!requestMap.containsKey("businessshortcode")) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Missing businessshortcode tag");
                        responseMap.put("ResultCode", "217");
                        responseMap.put("valid", "false");
                        validity = false;
                        return validity;
                    }

                    if ("".equalsIgnoreCase(requestMap.get("businessshortcode").trim())) {
                        responseMap.put("Error", "true");
                        responseMap.put("Message", "Empty businessshortcode tag");
                        responseMap.put("ResultCode", "223");
                        responseMap.put("valid", "false");
                        validity = false;
                        return validity;
                    }

                    break;
                case "confirm":
                    break;
                default:
                    responseMap.put("Error", "true");
                    responseMap.put("Message", "Invalid request type");
                    responseMap.put("ResultCode", "206");
                    responseMap.put("valid", "false");
                    validity = false;
                    return validity;
            }

        } catch (Exception ex) {
            (new ESBLog("Exceptions(ChannelRequest)", ex)).log();
            responseMap.put("Error", "true");
            responseMap.put("Message", "Exception occured");
            responseMap.put("ResultCode", "200");
            responseMap.put("valid", "false");
            validity = false;
        }
        return validity;
    }

    public String process() {
        String response = "";
        Map<String, String> requestMap = new HashMap();
        Map<String, String> responseMap = new HashMap();
        try {
            JSONObject requestObject = new JSONObject(request);
            requestMap = (new Utilities()).parseJSON(requestObject, requestMap);
            if (validatefields(requestMap, responseMap)) {
                switch (requestMap.get("type").trim()) {
                    case "validate":
                        responseMap = (new ProcessValidateRequests(configs, requestMap)).doProcess();
                        break;
                    case "confirm":
                        responseMap.clear();
                        responseMap.put("resultcode", "000");
                        responseMap.put("message", "Received successfully");
                        responseMap.put("ref", requestMap.get("thirdpartytransid"));

                        WorkManager wm= WorkManagerUtilities.getWorkManager();
                        wm.schedule((new ProcessConfirmation(configs,requestMap)));
                        break;
                }
            }
            JSONObject responseObject = new JSONObject(responseMap);
            response = responseObject.toString();
        } catch (JSONException | WorkException ex) {
            (new ESBLog("Exceptions(RequestProcessor)", ex)).log();

        }

        return response;
    }
}
