/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Database;

import ke.co.ekenya.LogEngine.ESBLog;
import oracle.jdbc.OracleTypes;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author njiru
 */
public class DatabaseFunctions extends DatabaseModel {

    public DatabaseFunctions() {
    }

    public void validatecustomer(Map<String, String> requestMap) {
        String command = "{call SP_C2BCUSTOMERVALIDATION(?,?)}";
        ResultSet rs = null;
        try (Connection con = createConnection();
             CallableStatement stm = con.prepareCall(command)) {
            stm.setString("IV_PHONENUMBER", requestMap.get("destinationacc").trim());
            stm.registerOutParameter("CV1", OracleTypes.CURSOR);
            stm.execute();
            rs = (ResultSet) stm.getObject("CV1");
            while (rs.next()) {
                requestMap.put("valid", (rs.getString("VALID") != null) ? rs.getString("VALID") : "");
                String lastName = (rs.getString("LASTNAME") != null) ? rs.getString("LASTNAME") : "";
                String firstName = (rs.getString("FIRSTNAME") != null) ? rs.getString("FIRSTNAME") : "";
                requestMap.put("name", (new StringBuilder(firstName)).append(" ").append(lastName).toString());
            }

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(Validate)", "Error in function: " + sw.toString());
            el.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                (new ESBLog("Exception(rsmwal)", ex)).log();
            }
        }

    }

    public boolean validatesacco(String saccobin) {
        String command = "{call SP_SACCOVALIDATION(?,?)}";
        boolean valid = false;
        try (Connection con = createConnection();
             CallableStatement stm = con.prepareCall(command)) {
            stm.setString("IV_SACCOBIN", saccobin.trim());
            stm.registerOutParameter("CV1", java.sql.Types.VARCHAR);
            stm.execute();
            valid = Boolean.valueOf(stm.getString("CV1"));

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(Validatesacco)", "Error in function: " + sw.toString());
            el.log();
        }
        return valid;
    }

    public boolean validateinstitution(String institutionID) {
        String command = "{call SP_INSTVALIDATION(?,?)}";
        boolean valid = false;
        try (Connection con = createConnection();
             CallableStatement stm = con.prepareCall(command)) {
            stm.setString("IV_INSTID", institutionID.trim());
            stm.registerOutParameter("CV1", java.sql.Types.VARCHAR);
            stm.execute();
            valid = Boolean.valueOf(stm.getString("CV1"));

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(ValidateInst)", "Error in function: " + sw.toString());
            el.log();
        }
        return valid;
    }

    public void getSaccoDetails(Map<String, String> requestMap) {
        String command = "{call SP_GETSACCODETAILS(?,?)}";
        ResultSet rs = null;
        String saccoID = "";
        String saccoName = "";
        String saccoSuspenseAc = "";
        String saccoIP = "";
        String saccoBranchCode = "";
        try (Connection con = createConnection();
             CallableStatement stm = con.prepareCall(command)) {
            stm.setString("IV_SACCOID", requestMap.get("destinationacc").trim().split("#")[0]);
            stm.registerOutParameter("CV", OracleTypes.CURSOR);
            stm.execute();

            rs = (ResultSet) stm.getObject("CV");
            while (rs.next()) {
                saccoID = (rs.getString("SACCOID") != null) ? rs.getString("SACCOID") : "";
                saccoName = (rs.getString("SACCONAMES") != null) ? rs.getString("SACCONAMES") : "";
                saccoSuspenseAc = (rs.getString("SUSPENSEAC") != null) ? rs.getString("SUSPENSEAC") : "";
                saccoIP = (rs.getString("IPADDRESS") != null) ? rs.getString("IPADDRESS") : "";
                saccoBranchCode = (rs.getString("BRANCHCODE") != null) ? rs.getString("BRANCHCODE") : "";
            }
            requestMap.put("saccoid", saccoID);
            requestMap.put("sacconame", saccoName);
            requestMap.put("suspense", saccoSuspenseAc);
            requestMap.put("ip", saccoIP);
            requestMap.put("branchcode", saccoBranchCode);
            (new ESBLog("SaccoDetails", requestMap.toString())).log();
        } catch (Exception ex) {
            (new ESBLog("Exception(saccodetails)", ex)).log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                (new ESBLog("Exception(rssaccodetails)", ex)).log();
            }
        }
    }

    public void getInstitutionDetails(Map<String, String> requestMap) {
        String command = "{call SP_GETINSTITUTIONDETAILS(?,?,?)}";
        ResultSet rs = null;
        String[] details = requestMap.get("destinationacc").trim().split("#");

        try (Connection con = createConnection();
             CallableStatement stm = con.prepareCall(command)) {
            stm.setString("INSTITUTION_ID", details[0]);
            if (details[1].trim().matches("\\d+")) {
                stm.setString("MEMBER_ID", details[1]);
            } else {
                stm.setString("MEMBER_ID", "");
            }
            stm.registerOutParameter("CV", OracleTypes.CURSOR);
            stm.execute();
            rs = (ResultSet) stm.getObject("CV");
            while (rs.next()) {
                requestMap.put("Institutionname", (rs.getString(1) != null) ? rs.getString(1) : "");
                requestMap.put("branchcode", (rs.getString(2) != null) ? rs.getString(2) : "");
                requestMap.put("accountno", (rs.getString(3) != null) ? rs.getString(3) : "");
                requestMap.put("contactperson", (rs.getString(4) != null) ? rs.getString(4) : "");
                requestMap.put("contactmobile", (rs.getString(5) != null) ? rs.getString(5) : "");
                requestMap.put("alerttype", (rs.getString(6) != null) ? rs.getString(6) : "");
                requestMap.put("email", (rs.getString(7) != null) ? rs.getString(7) : "");
                requestMap.put("membername", (rs.getString(8) != null) ? rs.getString(8) : "");
                requestMap.put("memberid", (rs.getString(9) != null) ? rs.getString(9) : "");
            }
            (new ESBLog("InstDetails",requestMap.toString())).log();
        } catch (Exception ex) {
            (new ESBLog("Exception(Instdetails)", ex)).log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                (new ESBLog("Exception(rsInstdetails)", ex)).log();
            }
        }
    }

    public HashMap getSMSTemplate(String smsCode) {
        HashMap detailsMap = new HashMap<>();
        String template = "";
        String command = "{call SP_GET_SMS_TEMPLATE(?,?)}";
        ResultSet rs = null;

        try (Connection con = createMvisaConnection();
             CallableStatement stmt = con.prepareCall(command);) {
            stmt.setString("IV_SMSCODE", smsCode);
            stmt.registerOutParameter("cv_1", OracleTypes.CURSOR);
            stmt.execute();

            rs = (ResultSet) stmt.getObject("cv_1");
            while (rs.next()) {
                template = rs.getString("MESSAGE");
            }

            detailsMap.put("SMSTEMPLATE", template);

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Exception occured while calling"
                    + " SP_GET_SMS_TEMPLATE SP to get sms template for sms code:" + smsCode + "|Error:" + sw.toString() + "\n");
            el.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog("SQLExceptions", "Error occured closing resultset of get sms template function. Error: " + sw.toString());
                el.log();
            }
        }

        return detailsMap;

    }

    public String getCorrelationID() {
        String CorrelationID = "";
        String command = "{call SP_GET_ESB_NEXT_SEQ(?)}";
        try (Connection con = createMvisaConnection();
             CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.registerOutParameter("cv_1", java.sql.Types.VARCHAR);
            callableStatement.executeUpdate();
            CorrelationID = callableStatement.getString("cv_1");
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Error in correlationID function: " + sw.toString());
            el.log();
        }
        return CorrelationID;
    }

    public String getAccountNumber(String bizCode, String AccPrefix) {
        String Accountno = "";
        String command = "{call SP_GETC2BACCOUNTS(?,?,?)}";
        try (Connection con = createConnection();
             CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.setString("IV_BUSINESSCODE", bizCode);
            callableStatement.setString("IV_ACCOUNTPREFIX", AccPrefix);
            callableStatement.registerOutParameter("IV_ACCOUNT", java.sql.Types.VARCHAR);
            callableStatement.execute();
            Accountno = callableStatement.getString("IV_ACCOUNT");
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(GetAccNo)", "Error in getAccountNumber function: " + sw.toString());
            el.log();
        }
        return Accountno;
    }


    public String getAnyGLAccount(String glName) {
        String glAccount = "";
        ResultSet rs = null;
        String command = "{call SP_GETANYGLACCOUNT(?,?)}";
        try (Connection con = createMvisaConnection();
             CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.setString("iv_glName", glName);
            callableStatement.registerOutParameter("cv_1", OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            rs = (ResultSet) callableStatement.getObject("cv_1");
            while (rs.next()) {
                glAccount = rs.getString(1);
            }
        } catch (Exception e) {
            (new ESBLog("Exception(fetchGl)", e)).log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog("SQLExceptions", "Error occured closing resultset of get gl function. Error: " + sw.toString());
                el.log();
            }
        }
        return glAccount;
    }

    public String insertTransaction(Map<String, String> requestMap) {
        String insert = "";
        String command = "{call SP_INSERTC2B(?,?,?,?,?,?,?,?,?,?,?,?)}";
        try (Connection con = createMvisaConnection();
             CallableStatement statement = con.prepareCall(command)) {

            statement.setString("SENDERNAME", requestMap.get("sendername"));
            statement.setString("SENDERMSISDN", requestMap.get("msisdn"));
            statement.setString("DESTINATIONACC", requestMap.get("destinationacc"));
            if (requestMap.get("102").trim().length() == 16) {
                statement.setString("DEBITACC", (new StringBuilder(requestMap.get("102"))).replace(5, 11, "******").toString());
            } else {
                statement.setString("DEBITACC", requestMap.get("102"));
            }

            if (requestMap.get("103").trim().length() == 16) {
                statement.setString("CREDITACC", (new StringBuilder(requestMap.get("103"))).replace(5, 11, "******").toString());
            } else {
                statement.setString("CREDITACC", requestMap.get("103"));
            }

            //statement.setString("CREDITACC",requestMap.get("103"));
            statement.setString("MPESACODE", requestMap.get("transid"));
            statement.setString("AMOUNT", requestMap.get("amount"));
            statement.setString("PAYBILL", requestMap.get("businessshortcode"));
            statement.setString("TRANSTATUS", requestMap.get("39"));
            statement.setString("NARRATION", requestMap.get("48"));
            statement.setString("REFNO", requestMap.get("37"));
            statement.registerOutParameter("STATUS", java.sql.Types.VARCHAR);
            statement.execute();

            insert = statement.getString("STATUS");
            (new ESBLog("DB_INSERTS", "Insert: " + insert + " Map: " + requestMap)).log();
        } catch (Exception ex) {
            (new ESBLog("Exception(Insert)", ex)).log();
        }
        return insert;
    }

    public void fetchMappings(Map<String, String> requestMap) {
        String command = "{call SP_FETCHCARD_MAPPINGS(?,?)}";
        ResultSet rs = null;
        try (Connection con = createMvisaConnection();
             CallableStatement stm = con.prepareCall(command)) {
            stm.setString("IV_PREFIX", requestMap.get("destinationacc").trim().substring(0, 2).toUpperCase());
            stm.registerOutParameter("cv_1", OracleTypes.CURSOR);
            stm.execute();
            rs = (ResultSet) stm.getObject("cv_1");
            while (rs.next()) {
                requestMap.put("prefix", (rs.getString(1) != null) ? rs.getString(1) : "");
                requestMap.put("bankc", (rs.getString(2) != null) ? rs.getString(2) : "");
                requestMap.put("groupc", (rs.getString(3) != null) ? rs.getString(3) : "");
                requestMap.put("cardAcc", (rs.getString(4) != null) ? rs.getString(4) : "");
            }
            (new ESBLog("CardDetails",requestMap.toString())).log();
        } catch (Exception ex) {
            (new ESBLog("Exception(Mappings)", ex)).log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                (new ESBLog("Exception(rsInstdetails)", ex)).log();
            }
        }

    }

}
