/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Confirmation;

import commonj.work.Work;
import ke.co.ekenya.CommonOperations.JMSUtilities;
import ke.co.ekenya.CommonOperations.PostGetESBRequest;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.Validators.ValidateCard;

import java.util.HashMap;
import java.util.Map;

/**
 * @author njiru
 */
public class ProcessConfirmation implements Work {

    Map<String, String> configs = new HashMap();
    Map<String, String> requestMap = new HashMap();

    public ProcessConfirmation(Map<String, String> configs, Map<String, String> requestMap) {
        this.configs = configs;
        this.requestMap = requestMap;
    }

    @Override
    public void run() {
        doProcess();
    }

    @Override
    public void release() {
    }

    @Override
    public boolean isDaemon() {
        return false;
    }

    public void doProcess() {
        try {
            switch (requestMap.get("businessshortcode").trim()) {
                case "400200":
                    process400200();
                    break;
                case "400222":
                    process400222();
                    break;
            }
        } catch (Exception ex) {
            (new ESBLog("Exception(confirmswitch)", ex)).log();
        }
    }

    private void process400200() {
        try {
            if ("07".equalsIgnoreCase(requestMap.get("destinationacc").trim().substring(0, 2))) {
                requestMap.put("destinationacc", (new StringBuilder("254")).append(requestMap.get("destinationacc").trim().substring(1)).toString());
            }

            Map<String, String> customerDetails = (new JMSUtilities()).getWeblogicMessageFromUDQueue(requestMap.get("destinationacc"));
            HashMap<String, String> esbRequest = new HashMap();
            String stan = (new Utilities()).generateStan();
            esbRequest.put("0", "0200");
            esbRequest.put("2", "254722000000");
            esbRequest.put("3", "400000");
            esbRequest.put("4", requestMap.get("amount"));
            esbRequest.put("MTI", "0200");
            esbRequest.put("7", requestMap.get("transtime"));
            esbRequest.put("11", stan);
            esbRequest.put("12", stan);
            esbRequest.put("49", "KES");
            esbRequest.put("32", "C2B");
            esbRequest.put("37", (new DatabaseFunctions()).getCorrelationID());
            esbRequest.put("52", "bcac5bdc-32fb-42df-8ddb-ce8f705ba8c5");
            esbRequest.put("100", "MWALLET");
            esbRequest.put("41", "FID00001");
            esbRequest.put("68", requestMap.get("businessshortcode").trim());
            esbRequest.put("102", (new DatabaseFunctions()).getAnyGLAccount("C2B_400200_GL"));
            esbRequest.put("98", "C2B_400200");
            esbRequest.put("64", requestMap.get("msisdn"));
            esbRequest.put("70", requestMap.get("transid"));
            esbRequest.put("67", requestMap.get("destinationacc"));
            esbRequest.put("77", (new StringBuilder(requestMap.get("firstname"))).append(" ").append(requestMap.get("middlename")).append(" ").append(requestMap.get("lastname")).toString());
            if ("".equalsIgnoreCase(customerDetails.get("receiver"))) {
                esbRequest.put("78", "");
            } else {
                esbRequest.put("78", customerDetails.get("receiver"));
            }
            if (("01".equalsIgnoreCase(requestMap.get("destinationacc").substring(0, 2))) || ("02".equalsIgnoreCase(requestMap.get("destinationacc").substring(0, 2))) || ("03".equalsIgnoreCase(requestMap.get("destinationacc").substring(0, 2)))) {
                esbRequest.put("24", "CC");
                esbRequest.put("103", requestMap.get("destinationacc").trim());
            } else if ("254".equalsIgnoreCase(requestMap.get("destinationacc").substring(0, 3))) {
                esbRequest.put("24", "CM");
                esbRequest.put("103", requestMap.get("destinationacc").trim());
            } else if (requestMap.get("destinationacc").contains("#")) {
                (new DatabaseFunctions()).getSaccoDetails(requestMap);
                //(new ESBLog("SaccoDetails",requestMap.toString())).log();
                if ("".equalsIgnoreCase(requestMap.get("ip"))) {
                    esbRequest.put("24", "CVC");
                    esbRequest.put("40", "4299" + requestMap.get("destinationacc").trim().split("#")[0] + "00000001");
                    esbRequest.put("SACCOLINK", "1");
                } else {
                    esbRequest.put("24", "CS");
                }
                esbRequest.put("35", requestMap.get("saccoid"));
                esbRequest.put("103", requestMap.get("destinationacc").trim());
            } else {
                Map<String, String> responseMap = (new ValidateCard(configs, requestMap)).validateCardNo();

                String prefix = requestMap.get("destinationacc").trim().substring(0, 2).toUpperCase();
                esbRequest.put("24", "CVC");
                esbRequest.put("103", responseMap.get("cardno"));
                esbRequest.put("42", requestMap.get("destinationacc").replace(prefix, ""));
                esbRequest.put("POST_TO_CREDITCARD", "1");
            }
            HashMap<String, String> esbResponse = (new PostGetESBRequest()).CURLRequest(esbRequest);
            requestMap.put("37", esbResponse.get("37"));
            requestMap.put("39", esbResponse.get("39"));
            requestMap.put("48", esbResponse.get("48"));
            requestMap.put("102", esbRequest.get("102"));
            requestMap.put("103", esbRequest.get("103"));
            String senderName = (new StringBuilder(requestMap.get("firstname"))).append(" ").append(requestMap.get("middlename")).append(" ").append(requestMap.get("lastname")).toString();
            requestMap.put("sendername", senderName);

            (new DatabaseFunctions()).insertTransaction(requestMap);
        } catch (Exception ex) {
            (new ESBLog("Exception(400200)", ex)).log();
        }

    }

    private void process400222() {
        try {
            if ("#".equalsIgnoreCase(requestMap.get("destinationacc").trim().substring(requestMap.get("destinationacc").trim().length() - 1))) {
                requestMap.put("destinationacc", (new StringBuilder(requestMap.get("destinationacc").trim())).append("1").toString());
            }

            requestMap.put("destinationacc", requestMap.get("destinationacc").trim().replaceAll("[^a-zA-Z0-9#/]", "")); //remove special characters excluding the hash(#) and forward slash(/)

            (new DatabaseFunctions()).getInstitutionDetails(requestMap);
            HashMap<String, String> esbRequest = new HashMap();
            String stan = (new Utilities()).generateStan();
            esbRequest.put("0", "0200");
            esbRequest.put("2", "254722000000");
            esbRequest.put("3", "400000");
            esbRequest.put("4", requestMap.get("amount"));
            esbRequest.put("MTI", "0200");
            esbRequest.put("7", requestMap.get("transtime"));
            esbRequest.put("11", stan);
            esbRequest.put("12", stan);
            esbRequest.put("49", "KES");
            esbRequest.put("32", "C2B");
            esbRequest.put("37", (new DatabaseFunctions()).getCorrelationID());
            esbRequest.put("52", "bcac5bdc-32fb-42df-8ddb-ce8f705ba8c5");
            esbRequest.put("100", "MWALLET");
            esbRequest.put("41", "FID00001");
            esbRequest.put("67", requestMap.get("destinationacc"));
            esbRequest.put("68", requestMap.get("businessshortcode").trim());
            esbRequest.put("102", (new DatabaseFunctions()).getAnyGLAccount("C2B_400222_GL"));
            esbRequest.put("98", "C2B_400222");
            esbRequest.put("24", "CC");
            esbRequest.put("64", requestMap.get("msisdn"));
            esbRequest.put("103", requestMap.get("accountno"));
            esbRequest.put("70", requestMap.get("transid"));
            esbRequest.put("71", requestMap.get("Institutionname").replaceAll("[^a-zA-Z0-9]", ""));
            esbRequest.put("72", requestMap.get("contactperson").replaceAll("[^a-zA-Z0-9]", ""));
            esbRequest.put("73", requestMap.get("contactmobile"));

            if ("2".equalsIgnoreCase(requestMap.get("alerttype"))) {
                esbRequest.put("74", "2");
            } else if ("3".equalsIgnoreCase(requestMap.get("alerttype"))) {
                esbRequest.put("74", "3");
            } else {
                esbRequest.put("74", "1");
            }

            esbRequest.put("75", requestMap.get("email"));
            if ("".equalsIgnoreCase(requestMap.get("membername"))) {
                esbRequest.put("76", requestMap.get("destinationacc").trim().split("#")[1]);
            } else {
                esbRequest.put("76", requestMap.get("destinationacc").trim().split("#")[1] + " " + requestMap.get("membername"));
            }
            esbRequest.put("77", (new StringBuilder(requestMap.get("firstname"))).append(" ").append(requestMap.get("middlename")).append(" ").append(requestMap.get("lastname")).toString());


            HashMap<String, String> esbResponse = (new PostGetESBRequest()).CURLRequest(esbRequest);
            requestMap.put("37", esbResponse.get("37"));
            requestMap.put("39", esbResponse.get("39"));
            requestMap.put("48", esbResponse.get("48"));
            requestMap.put("102", esbRequest.get("102"));
            requestMap.put("103", esbRequest.get("103"));
            String senderName = (new StringBuilder(requestMap.get("firstname"))).append(" ").append(requestMap.get("middlename")).append(" ").append(requestMap.get("lastname")).toString();
            requestMap.put("sendername", senderName);

            (new DatabaseFunctions()).insertTransaction(requestMap);
        } catch (Exception ex) {
            (new ESBLog("Exception(400222)", ex)).log();
        }
    }

}
