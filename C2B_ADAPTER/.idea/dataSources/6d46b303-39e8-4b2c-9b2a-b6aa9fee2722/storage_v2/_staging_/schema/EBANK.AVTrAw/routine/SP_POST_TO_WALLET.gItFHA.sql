create or replace PROCEDURE "SP_POST_TO_WALLET"(v_TRXNO IN NVARCHAR2 DEFAULT NULL,
                                     iv_MsgType IN NVARCHAR2 DEFAULT NULL,
                                     iv_Field3 IN NVARCHAR2 DEFAULT NULL,
                                     iv_Field4 IN NUMBER DEFAULT 0,
                                     iv_Field7 IN NVARCHAR2 DEFAULT '',
                                     iv_Field11 IN NVARCHAR2 DEFAULT '',
                                     iv_Field24 IN NVARCHAR2 DEFAULT '',
                                     iv_Field32 IN NVARCHAR2 DEFAULT '',
                                     iv_Field35 IN NVARCHAR2 DEFAULT '',
                                     iv_Field90 IN NVARCHAR2 DEFAULT '',
                                     iv_Field100 IN NVARCHAR2 DEFAULT '',
                                     iv_Field101 IN NVARCHAR2 DEFAULT '',
                                     iv_Field102 IN NVARCHAR2 DEFAULT '',
                                     iv_Field103 IN NVARCHAR2 DEFAULT '',
                                     iv_Commission IN NUMBER DEFAULT 0,
                                     iv_VAT IN NUMBER DEFAULT 0,
                                     iv_ExtraCharge IN NUMBER DEFAULT 0,
                                     iv_Field68 IN NVARCHAR2 DEFAULT NULL,
                                     iv_CustCurrency IN NVARCHAR2 DEFAULT 'KES',
                                     iv_Channel IN NVARCHAR2 DEFAULT NULL,
                                     iv_TerminalID IN NVARCHAR2 DEFAULT '',
                                     iv_UserID IN NVARCHAR2 DEFAULT NULL,
                                     iv_TrnCode IN NVARCHAR2 DEFAULT NULL,
                                     iv_TrnType IN NVARCHAR2 DEFAULT NULL,
                                     iv_IsBulk IN NVARCHAR2 DEFAULT NULL,
                                     c_1 IN OUT SYS_REFCURSOR)
AS
  v_Availbal NUMBER(18, 2) := 0;
  v_actualBalance NUMBER(18, 2) := 0;
  v_STATUS NVARCHAR2(5);
  v_Narration NVARCHAR2(30);
  v_SERIALNO NVARCHAR2(20);
  v_MinBalance NUMBER(10, 2);
  v_UBBranch NVARCHAR2(20);
  v_TrnCode NVARCHAR2(20) := iv_TrnCode;
  v_Commission NUMBER(10, 2) := iv_Commission;
  v_VAT NUMBER(10, 2) := iv_VAT;
  v_ExtraCharge NUMBER(10, 2) := iv_ExtraCharge;
  v_Field90 NVARCHAR2(50) := iv_Field90;
  v_MsgType NVARCHAR2(50) := iv_MsgType;
  v_Field3 NVARCHAR2(50) := iv_Field3;
  v_Currency NVARCHAR2(50) := 'KES';
  v_Field4 NVARCHAR2(50) := iv_Field4;
  v_Field7 NVARCHAR2(50) := iv_Field7;
  v_Field11 NVARCHAR2(50) := iv_Field11;
  v_Field24 NVARCHAR2(20) := iv_Field24;
  v_Field35 NVARCHAR2(20) := iv_Field35;
  v_Field68 NVARCHAR2(500) := iv_Field68;
  v_Field100 NVARCHAR2(500) := iv_Field100;
  v_Field102 NVARCHAR2(20) := iv_Field102;
  v_Field103 NVARCHAR2(20) := iv_Field103;
  v_Channel NVARCHAR2(20) := iv_Channel;
  v_TrnType NVARCHAR2(20) := iv_TrnType;
  v_IsBulk NVARCHAR2(20) := iv_IsBulk;
  v_WORKINGDATE DATE;
  v_FINANCIALYR NVARCHAR2(6);
  v_FINANCIALPRD NVARCHAR2(3);
  v_IsCustomerGL NVARCHAR2(3);
  v_DRCR CHAR(1);
  v_GLHoldingAccount NVARCHAR2(20);
  v_Availablebal NUMERIC(18, 5) DEFAULT 0;
  v_GLDRBal NUMERIC(18, 5) DEFAULT 0;
  v_Glcrbal NUMERIC(18, 5) DEFAULT 0;
  v_Todaycr NUMERIC(18, 5) DEFAULT 0;
  v_Todaydr NUMERIC(18, 5) DEFAULT 0;
  v_GlBalance NUMERIC(18, 5) DEFAULT 0;
  v_CLEARBAL NUMERIC(18, 5);
  v_BalanceCheck NVARCHAR2(20);
  v_Account102Exists NUMBER(3) DEFAULT 0;
  v_Account103Exists NUMBER(3) DEFAULT 0;
  v_TrxRefExists NUMBER(3) DEFAULT 0;
  v_ResultCode NVARCHAR2(3);
  v_ResultDescription NVARCHAR2(100);
  v_RunningBalance NUMERIC(18, 5) DEFAULT 0;
  v_Gcaccountno NVARCHAR2(20);
  v_Field68Updated NVARCHAR2(500);

BEGIN

  SAVEPOINT initialState;


  BEGIN
    SELECT WORKINGDATE, FinancialYear, FinancialPeriod INTO v_WORKINGDATE, v_FINANCIALYR,v_FINANCIALPRD
    FROM tbDateSettings;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK TO initialState;
      v_ResultCode := '999';
      v_ResultDescription := 'Generic Error on fetching working date';
      OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
      RETURN;
  END;

  IF (v_TRXNO IS NULL OR v_TRXNO = '') THEN
    v_ResultCode := '001';
    v_ResultDescription := 'Missing Transaction Reference';
    OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
    RETURN;
  END IF;


  IF (v_MsgType IS NULL OR v_MsgType = '' OR v_MsgType <> '0200') THEN
    v_ResultCode := '002';
    v_ResultDescription := 'Missing/Invalid Message Type';
    OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
    RETURN;
  END IF;


  SELECT count(TRXREFNO) INTO v_TrxRefExists FROM TBTRANSACTIONS WHERE TRXREFNO = v_TRXNO;
  IF (v_TrxRefExists > 0 AND v_IsBulk = 0) THEN
    v_ResultCode := '003';
    v_ResultDescription := 'Duplicate transaction reference!';
    OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
    RETURN;
  END IF;

  IF (v_Field102 = v_Field103) THEN
    v_ResultCode := '004';
    v_ResultDescription := 'Debit and Credit Accounts cannot be the same!';
    OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
    RETURN;
  END IF;

  IF (v_TrnType = 'DEBIT_WALLET') THEN
    BEGIN

      IF (v_Field102 IS NULL OR v_Field102 = '') THEN
        v_ResultCode := '005';
        v_ResultDescription := 'Missing Debit account';
        OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
        RETURN;
      END IF;

      SELECT count(Accountno) into v_Account102Exists FROM Tbaccount WHERE Accountno = v_Field102;
      IF (v_Account102Exists < 1) THEN
        v_ResultCode := '006';
        v_ResultDescription := 'Debit account(Field102) not found!';
        OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
        RETURN;
      END IF;

      v_BalanceCheck := FN_VALIDATEWALLETBALANCE(v_Field102, v_Field4, v_Commission, v_VAT, v_ExtraCharge);
      IF (v_BalanceCheck = 'FAIL') THEN
        v_ResultCode := '020';
        v_ResultDescription := 'Insufficient Funds';
        OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
        RETURN;
      END IF;


      IF (NVL(v_Field4, 0) > 0) THEN
        BEGIN


          v_GCAccountNo := v_Field102;
          v_DRCR := 'D';
          v_IsCustomerGL := 'C';
          IF (v_IsBulk = 1) THEN
            BEGIN
              SELECT count(TRXREFNO) INTO v_TrxRefExists
              FROM TBTRANSACTIONS
              WHERE TRXREFNO = v_TRXNO
                AND ACCOUNTNO = v_Field102
                AND DRCR = v_DRCR
                AND AMOUNT = v_Field4;
              IF (v_TrxRefExists > 0) THEN
                ROLLBACK TO initialState;
                v_ResultCode := '003';
                v_ResultDescription := 'Duplicate transaction reference!';
                OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;


                RETURN;
              END IF;
            End;
          END IF;


          BEGIN
            v_CLEARBAL := FN_UPDATEWALLETBALANCE(v_Field4, v_GCAccountNo, v_DRCR);
            v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_Field4, v_GCAccountNo, v_DRCR);
          EXCEPTION
            WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on principal leg 1';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
          BEGIN
            INSERT INTO TBTRANSACTIONS (ID, MSGTYPE, CHANNEL, TRXREFNO, TRXSERIALNO, ACCOUNTCENTRE, ACCOUNTNO, CURRENCY,
                                        DRCR, AMOUNT,
                                        TRXDATE, VALUEDATE, FINANCIALCYCLE, FINANCIALPERIOD, CREATEDBY, CREATEDON,
                                        AUTHORIZEDBY, AUTHORIZEDON, ISCUSTOMERGL,
                                        NARRATION, CLEAREDBALANCE, AVAILABLEBALANCE, PROCODE, TRXCODE, STAN, FIELD90,
                                        RUNNINGBALANCE)
            VALUES (v_SERIALNO, v_MsgType, v_Channel, v_TRXNO, v_SERIALNO, '000', v_GCAccountNo, 'KES', v_DRCR,
                    v_Field4,
                    v_WORKINGDATE, v_WORKINGDATE, v_FINANCIALYR, v_FINANCIALPRD, iv_UserID,
                    SYSDATE, iv_UserID, SYSDATE, v_IsCustomerGL, v_Field68, v_CLEARBAL, v_CLEARBAL,
                    v_Field3, v_Field24, v_Field11, v_Field90, v_RunningBalance);

          EXCEPTION
            WHEN OTHERS THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on principal leg 1 - insert';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;


          END;


          v_DRCR := 'C';
          v_GCAccountNo := fn_GetGlAccount('MWALLETSUSPENSE');


          IF (v_IsBulk = 1) THEN
            BEGIN
              SELECT count(TRXREFNO) INTO v_TrxRefExists
              FROM TBTRANSACTIONS
              WHERE TRXREFNO = v_TRXNO
                AND ACCOUNTNO = v_GCAccountNo
                AND DRCR = v_DRCR
                AND AMOUNT = v_Field4;
              IF (v_TrxRefExists > 0) THEN
                ROLLBACK TO initialState;
                v_ResultCode := '003';
                v_ResultDescription := 'Duplicate transaction reference!';
                OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
                RETURN;
              END IF;
            End;
          END IF;


          IF (SUBSTR(v_Field103, 1, 3) = '254' AND LENGTH(v_Field103) = 12) THEN
            v_IsCustomerGL := 'C';
            v_GCAccountNo := v_Field103;
            BEGIN
              v_CLEARBAL := FN_UPDATEWALLETBALANCE(v_Field4, v_GCAccountNo, v_DRCR);
              v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_Field4, v_GCAccountNo, v_DRCR);
            EXCEPTION
              WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
                ROLLBACK TO initialState;
                v_ResultCode := '999';
                v_ResultDescription := 'Generic Error on principal leg 2';
                OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
                RETURN;
            END;
          ELSE
            BEGIN
              v_IsCustomerGL := 'G';
              v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_Field4, v_GCAccountNo, v_DRCR);
              v_CLEARBAL := v_RunningBalance;
            EXCEPTION
              WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
                ROLLBACK TO initialState;
                v_ResultCode := '999';
                v_ResultDescription := 'Generic Error on principal leg 2';
                OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
                RETURN;
            END;
          END IF;

          BEGIN
            INSERT INTO TBTRANSACTIONS (ID, MSGTYPE, CHANNEL, TRXREFNO, TRXSERIALNO, ACCOUNTCENTRE, ACCOUNTNO, CURRENCY,
                                        DRCR, AMOUNT,
                                        TRXDATE, VALUEDATE, FINANCIALCYCLE, FINANCIALPERIOD, CREATEDBY, CREATEDON,
                                        AUTHORIZEDBY, AUTHORIZEDON, ISCUSTOMERGL,
                                        NARRATION, CLEAREDBALANCE, AVAILABLEBALANCE, PROCODE, TRXCODE, STAN, FIELD90,
                                        RUNNINGBALANCE)
            VALUES (v_SERIALNO, v_MsgType, v_Channel, v_TRXNO, v_SERIALNO, '000', v_GCAccountNo, 'KES', v_DRCR,
                    v_Field4,
                    v_WORKINGDATE, v_WORKINGDATE, v_FINANCIALYR, v_FINANCIALPRD, iv_UserID,
                    SYSDATE, iv_UserID, SYSDATE, v_IsCustomerGL, v_Field68, v_CLEARBAL, v_CLEARBAL,
                    v_Field3, v_Field24, v_Field11, v_Field90, v_RunningBalance);

          EXCEPTION
            WHEN OTHERS THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on principal leg 2 - insert';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
        END;
      END IF;


      IF (NVL(v_Commission, 0) > 0) THEN
        BEGIN

          v_GCAccountNo := v_Field102;
          v_DRCR := 'D';
          v_IsCustomerGL := 'C';

          IF (v_IsBulk = 1) THEN
            BEGIN
              SELECT count(TRXREFNO) INTO v_TrxRefExists
              FROM TBTRANSACTIONS
              WHERE TRXREFNO = v_TRXNO
                AND ACCOUNTNO = v_Field102
                AND DRCR = v_DRCR
                AND AMOUNT = v_Commission;
              IF (v_TrxRefExists > 0) THEN
                ROLLBACK TO initialState;
                v_ResultCode := '003';
                v_ResultDescription := 'Duplicate transaction reference!';
                OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
                RETURN;
              END IF;
            End;
          END IF;
          v_Field68Updated := v_Field68 || ' CHARGES';
          BEGIN
            v_CLEARBAL := FN_UPDATEWALLETBALANCE(v_Commission, v_GCAccountNo, v_DRCR);
            v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_Commission, v_GCAccountNo, v_DRCR);
          EXCEPTION
            WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on commission leg 1';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
          BEGIN
            INSERT INTO TBTRANSACTIONS (ID, MSGTYPE, CHANNEL, TRXREFNO, TRXSERIALNO, ACCOUNTCENTRE, ACCOUNTNO, CURRENCY,
                                        DRCR, AMOUNT,
                                        TRXDATE, VALUEDATE, FINANCIALCYCLE, FINANCIALPERIOD, CREATEDBY, CREATEDON,
                                        AUTHORIZEDBY, AUTHORIZEDON, ISCUSTOMERGL,
                                        NARRATION, CLEAREDBALANCE, AVAILABLEBALANCE, PROCODE, TRXCODE, STAN, FIELD90,
                                        RUNNINGBALANCE)
            VALUES (v_SERIALNO, v_MsgType, v_Channel, v_TRXNO, v_SERIALNO, '000', v_GCAccountNo, 'KES', v_DRCR,
                    v_Commission,
                    v_WORKINGDATE, v_WORKINGDATE, v_FINANCIALYR, v_FINANCIALPRD, iv_UserID,
                    SYSDATE, iv_UserID, SYSDATE, v_IsCustomerGL, v_Field68Updated, v_CLEARBAL, v_CLEARBAL,
                    v_Field3, v_Field24, v_Field11, v_Field90, v_RunningBalance);

          EXCEPTION
            WHEN OTHERS THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on commission leg 1 - insert';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;


          v_DRCR := 'C';
          v_IsCustomerGL := 'G';
          BEGIN
            v_GCAccountNo := fn_GetGlAccount('MWALLETSUSPENSE');


            IF (v_IsBulk = 1) THEN
              BEGIN
                SELECT count(TRXREFNO) INTO v_TrxRefExists
                FROM TBTRANSACTIONS
                WHERE TRXREFNO = v_TRXNO
                  AND ACCOUNTNO = v_GCAccountNo
                  AND DRCR = v_DRCR
                  AND AMOUNT = v_Commission;
                IF (v_TrxRefExists > 0) THEN
                  ROLLBACK TO initialState;
                  v_ResultCode := '003';
                  v_ResultDescription := 'Duplicate transaction reference!';
                  OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
                  RETURN;
                END IF;
              End;
            END IF;


            v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_Commission, v_GCAccountNo, v_DRCR);
          EXCEPTION
            WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on commission leg 2';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
          BEGIN
            INSERT INTO TBTRANSACTIONS (ID, MSGTYPE, CHANNEL, TRXREFNO, TRXSERIALNO, ACCOUNTCENTRE, ACCOUNTNO, CURRENCY,
                                        DRCR, AMOUNT,
                                        TRXDATE, VALUEDATE, FINANCIALCYCLE, FINANCIALPERIOD, CREATEDBY, CREATEDON,
                                        AUTHORIZEDBY, AUTHORIZEDON, ISCUSTOMERGL,
                                        NARRATION, CLEAREDBALANCE, AVAILABLEBALANCE, PROCODE, TRXCODE, STAN, FIELD90,
                                        RUNNINGBALANCE)
            VALUES (v_SERIALNO, v_MsgType, v_Channel, v_TRXNO, v_SERIALNO, '000', v_GCAccountNo, 'KES', v_DRCR,
                    v_Commission,
                    v_WORKINGDATE, v_WORKINGDATE, v_FINANCIALYR, v_FINANCIALPRD, iv_UserID,
                    SYSDATE, iv_UserID, SYSDATE, v_IsCustomerGL, v_Field68Updated, v_RunningBalance, v_RunningBalance,
                    v_Field3, v_Field24, v_Field11, v_Field90, v_RunningBalance);

          EXCEPTION
            WHEN OTHERS THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on commission leg 2 - insert';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
        END;
      END IF;


      IF (NVL(v_VAT, 0) > 0) THEN

        BEGIN

          v_GCAccountNo := v_Field102;
          v_DRCR := 'D';
          v_IsCustomerGL := 'C';


          IF (v_IsBulk = 1) THEN
            BEGIN
              SELECT count(TRXREFNO) INTO v_TrxRefExists
              FROM TBTRANSACTIONS
              WHERE TRXREFNO = v_TRXNO
                AND ACCOUNTNO = v_Field102
                AND DRCR = v_DRCR
                AND AMOUNT = v_VAT;
              IF (v_TrxRefExists > 0) THEN
                ROLLBACK TO initialState;
                v_ResultCode := '003';
                v_ResultDescription := 'Duplicate transaction reference!';
                OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
                RETURN;
              END IF;
            End;
          END IF;

          v_Field68Updated := v_Field68 || ' VAT';
          BEGIN
            v_CLEARBAL := FN_UPDATEWALLETBALANCE(v_VAT, v_GCAccountNo, v_DRCR);
            v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_VAT, v_GCAccountNo, v_DRCR);
          EXCEPTION
            WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on VAT leg 1';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
          BEGIN
            INSERT INTO TBTRANSACTIONS (ID, MSGTYPE, CHANNEL, TRXREFNO, TRXSERIALNO, ACCOUNTCENTRE, ACCOUNTNO, CURRENCY,
                                        DRCR, AMOUNT,
                                        TRXDATE, VALUEDATE, FINANCIALCYCLE, FINANCIALPERIOD, CREATEDBY, CREATEDON,
                                        AUTHORIZEDBY, AUTHORIZEDON, ISCUSTOMERGL,
                                        NARRATION, CLEAREDBALANCE, AVAILABLEBALANCE, PROCODE, TRXCODE, STAN, FIELD90,
                                        RUNNINGBALANCE)
            VALUES (v_SERIALNO, v_MsgType, v_Channel, v_TRXNO, v_SERIALNO, '000', v_GCAccountNo, 'KES', v_DRCR, v_VAT,
                    v_WORKINGDATE, v_WORKINGDATE, v_FINANCIALYR, v_FINANCIALPRD, iv_UserID,
                    SYSDATE, iv_UserID, SYSDATE, v_IsCustomerGL, v_Field68Updated, v_CLEARBAL, v_CLEARBAL,
                    v_Field3, v_Field24, v_Field11, v_Field90, v_RunningBalance);

          EXCEPTION
            WHEN OTHERS THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on VAT leg 1 - insert';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;


          v_DRCR := 'C';
          v_IsCustomerGL := 'G';
          BEGIN
            v_GCAccountNo := fn_GetGlAccount('MWALLETSUSPENSE');

            IF (v_IsBulk = 1) THEN
              BEGIN
                SELECT count(TRXREFNO) INTO v_TrxRefExists
                FROM TBTRANSACTIONS
                WHERE TRXREFNO = v_TRXNO
                  AND ACCOUNTNO = v_GCAccountNo
                  AND DRCR = v_DRCR
                  AND AMOUNT = v_VAT;
                IF (v_TrxRefExists > 0) THEN
                  ROLLBACK TO initialState;
                  v_ResultCode := '003';
                  v_ResultDescription := 'Duplicate transaction reference!';
                  OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
                  RETURN;
                END IF;
              End;
            END IF;

            v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_VAT, v_GCAccountNo, v_DRCR);
          EXCEPTION
            WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on VAT leg 2';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
          BEGIN
            INSERT INTO TBTRANSACTIONS (ID, MSGTYPE, CHANNEL, TRXREFNO, TRXSERIALNO, ACCOUNTCENTRE, ACCOUNTNO, CURRENCY,
                                        DRCR, AMOUNT,
                                        TRXDATE, VALUEDATE, FINANCIALCYCLE, FINANCIALPERIOD, CREATEDBY, CREATEDON,
                                        AUTHORIZEDBY, AUTHORIZEDON, ISCUSTOMERGL,
                                        NARRATION, CLEAREDBALANCE, AVAILABLEBALANCE, PROCODE, TRXCODE, STAN, FIELD90,
                                        RUNNINGBALANCE)
            VALUES (v_SERIALNO, v_MsgType, v_Channel, v_TRXNO, v_SERIALNO, '000', v_GCAccountNo, 'KES', v_DRCR, v_VAT,
                    v_WORKINGDATE, v_WORKINGDATE, v_FINANCIALYR, v_FINANCIALPRD, iv_UserID,
                    SYSDATE, iv_UserID, SYSDATE, v_IsCustomerGL, v_Field68Updated, v_RunningBalance, v_RunningBalance,
                    v_Field3, v_Field24, v_Field11, v_Field90, v_RunningBalance);

          EXCEPTION
            WHEN OTHERS THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on VAT leg 2 - insert';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
        END;
      END IF;


      IF (NVL(v_ExtraCharge, 0) > 0) THEN
        BEGIN

          v_GCAccountNo := v_Field102;
          v_DRCR := 'D';
          v_IsCustomerGL := 'C';

          IF (v_IsBulk = 1) THEN
            BEGIN
              SELECT count(TRXREFNO) INTO v_TrxRefExists
              FROM TBTRANSACTIONS
              WHERE TRXREFNO = v_TRXNO
                AND ACCOUNTNO = v_Field102
                AND DRCR = v_DRCR
                AND AMOUNT = v_ExtraCharge;
              IF (v_TrxRefExists > 0) THEN
                ROLLBACK TO initialState;
                v_ResultCode := '003';
                v_ResultDescription := 'Duplicate transaction reference!';
                OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
                RETURN;
              END IF;
            End;
          END IF;
          v_Field68Updated := v_Field68 || ' EXTRACHARGE';
          BEGIN
            v_CLEARBAL := FN_UPDATEWALLETBALANCE(v_ExtraCharge, v_GCAccountNo, v_DRCR);
            v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_ExtraCharge, v_GCAccountNo, v_DRCR);
          EXCEPTION
            WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on ExtraCharge leg 1';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
          BEGIN
            INSERT INTO TBTRANSACTIONS (ID, MSGTYPE, CHANNEL, TRXREFNO, TRXSERIALNO, ACCOUNTCENTRE, ACCOUNTNO, CURRENCY,
                                        DRCR, AMOUNT,
                                        TRXDATE, VALUEDATE, FINANCIALCYCLE, FINANCIALPERIOD, CREATEDBY, CREATEDON,
                                        AUTHORIZEDBY, AUTHORIZEDON, ISCUSTOMERGL,
                                        NARRATION, CLEAREDBALANCE, AVAILABLEBALANCE, PROCODE, TRXCODE, STAN, FIELD90,
                                        RUNNINGBALANCE)
            VALUES (v_SERIALNO, v_MsgType, v_Channel, v_TRXNO, v_SERIALNO, '000', v_GCAccountNo, 'KES', v_DRCR,
                    v_ExtraCharge,
                    v_WORKINGDATE, v_WORKINGDATE, v_FINANCIALYR, v_FINANCIALPRD, iv_UserID,
                    SYSDATE, iv_UserID, SYSDATE, v_IsCustomerGL, v_Field68Updated, v_CLEARBAL, v_CLEARBAL,
                    v_Field3, v_Field24, v_Field11, v_Field90, v_RunningBalance);


          EXCEPTION
            WHEN OTHERS THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on ExtraCharge leg 1 - insert';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;


          v_DRCR := 'C';
          v_IsCustomerGL := 'G';
          BEGIN
            v_GCAccountNo := fn_GetGlAccount('MWALLETSUSPENSE');

            IF (v_IsBulk = 1) THEN
              BEGIN
                SELECT count(TRXREFNO) INTO v_TrxRefExists
                FROM TBTRANSACTIONS
                WHERE TRXREFNO = v_TRXNO
                  AND ACCOUNTNO = v_GCAccountNo
                  AND DRCR = v_DRCR
                  AND AMOUNT = v_ExtraCharge;
                IF (v_TrxRefExists > 0) THEN
                  ROLLBACK TO initialState;
                  v_ResultCode := '003';
                  v_ResultDescription := 'Duplicate transaction reference!';
                  OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
                  RETURN;
                END IF;
              End;
            END IF;
            v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_ExtraCharge, v_GCAccountNo, v_DRCR);
          EXCEPTION
            WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on ExtraCharge leg 2';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
          BEGIN
            INSERT INTO TBTRANSACTIONS (ID, MSGTYPE, CHANNEL, TRXREFNO, TRXSERIALNO, ACCOUNTCENTRE, ACCOUNTNO, CURRENCY,
                                        DRCR, AMOUNT,
                                        TRXDATE, VALUEDATE, FINANCIALCYCLE, FINANCIALPERIOD, CREATEDBY, CREATEDON,
                                        AUTHORIZEDBY, AUTHORIZEDON, ISCUSTOMERGL,
                                        NARRATION, CLEAREDBALANCE, AVAILABLEBALANCE, PROCODE, TRXCODE, STAN, FIELD90,
                                        RUNNINGBALANCE)
            VALUES (v_SERIALNO, v_MsgType, v_Channel, v_TRXNO, v_SERIALNO, '000', v_GCAccountNo, 'KES', v_DRCR,
                    v_ExtraCharge,
                    v_WORKINGDATE, v_WORKINGDATE, v_FINANCIALYR, v_FINANCIALPRD, iv_UserID,
                    SYSDATE, iv_UserID, SYSDATE, v_IsCustomerGL, v_Field68Updated, v_RunningBalance, v_RunningBalance,
                    v_Field3, v_Field24, v_Field11, v_Field90, v_RunningBalance);

          EXCEPTION
            WHEN OTHERS THEN
              ROLLBACK TO initialState;
              v_ResultCode := '999';
              v_ResultDescription := 'Generic Error on ExtraCharge leg 2 - insert';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
          END;
        END;
      END IF;
    END;
  ELSIF (v_TrnType = 'CREDIT_WALLET') THEN

    IF (SUBSTR(v_Field102, 1, 3) = '254' AND LENGTH(v_Field102) = 12) THEN
      v_ResultCode := '007';
      v_ResultDescription := 'Invalid Debit Account.';

      OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
      RETURN;
    END IF;

    IF (v_Field103 IS NULL OR v_Field103 = '') THEN
      v_ResultCode := '008';
      v_ResultDescription := 'Missing Credit account';

      OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
      RETURN;
    END IF;


    SELECT count(Accountno) into v_Account103Exists FROM Tbaccount WHERE Accountno = v_Field103;
    IF (v_Account103Exists < 1) THEN
      v_ResultCode := '009';
      v_ResultDescription := 'Credit account(Field103) not found!';
      OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
      RETURN;
    END IF;


    v_DRCR := 'D';
    v_IsCustomerGL := 'G';
    IF (NVL(v_Field4, 0) > 0) THEN
      BEGIN
        v_GCAccountNo := fn_GetGlAccount('MWALLETSUSPENSE');
        IF (v_IsBulk = 1) THEN
          BEGIN
            SELECT count(TRXREFNO) INTO v_TrxRefExists
            FROM TBTRANSACTIONS
            WHERE TRXREFNO = v_TRXNO
              AND ACCOUNTNO = v_GCAccountNo
              AND DRCR = v_DRCR
              AND AMOUNT = v_Field4;
            IF (v_TrxRefExists > 0) THEN
              ROLLBACK TO initialState;
              v_ResultCode := '003';
              v_ResultDescription := 'Duplicate transaction reference!';
              OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
              RETURN;
            END IF;
          End;
        END IF;

        v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_Field4, v_GCAccountNo, v_DRCR);
      EXCEPTION
        WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
          ROLLBACK TO initialState;
          v_ResultCode := '999';
          v_ResultDescription := 'Generic Error on Credit to Wallet leg 1(GL)';
          OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
          RETURN;
      END;
      BEGIN
        INSERT INTO TBTRANSACTIONS (ID, MSGTYPE, CHANNEL, TRXREFNO, TRXSERIALNO, ACCOUNTCENTRE, ACCOUNTNO, CURRENCY,
                                    DRCR, AMOUNT,
                                    TRXDATE, VALUEDATE, FINANCIALCYCLE, FINANCIALPERIOD, CREATEDBY, CREATEDON,
                                    AUTHORIZEDBY, AUTHORIZEDON, ISCUSTOMERGL,
                                    NARRATION, CLEAREDBALANCE, AVAILABLEBALANCE, PROCODE, TRXCODE, STAN, FIELD90,
                                    RUNNINGBALANCE)
        VALUES (v_SERIALNO, v_MsgType, v_Channel, v_TRXNO, v_SERIALNO, '000', v_GCAccountNo, 'KES', v_DRCR, v_Field4,
                v_WORKINGDATE, v_WORKINGDATE, v_FINANCIALYR, v_FINANCIALPRD, iv_UserID,
                SYSDATE, iv_UserID, SYSDATE, v_IsCustomerGL, v_Field68, v_RunningBalance, v_RunningBalance,
                v_Field3, v_Field24, v_Field11, v_Field90, v_RunningBalance);

      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK TO initialState;
          v_ResultCode := '999';
          v_ResultDescription := 'Generic Error on Credit to Wallet leg 1(GL) - insert';
          OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
          RETURN;
      END;

      v_GCAccountNo := v_Field103;
      v_DRCR := 'C';
      v_IsCustomerGL := 'C';

      IF (v_IsBulk = 1) THEN
        BEGIN
          SELECT count(TRXREFNO) INTO v_TrxRefExists
          FROM TBTRANSACTIONS
          WHERE TRXREFNO = v_TRXNO
            AND ACCOUNTNO = v_Field103
            AND DRCR = v_DRCR
            AND AMOUNT = v_Field4;
          IF (v_TrxRefExists > 0) THEN
            ROLLBACK TO initialState;
            v_ResultCode := '003';
            v_ResultDescription := 'Duplicate transaction reference!';
            OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
            RETURN;
          END IF;
        End;
      END IF;

      BEGIN
        v_CLEARBAL := FN_UPDATEWALLETBALANCE(v_Field4, v_GCAccountNo, v_DRCR);
        v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(v_Field4, v_GCAccountNo, v_DRCR);
      EXCEPTION
        WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
          ROLLBACK TO initialState;
          v_ResultCode := '999';
          v_ResultDescription := 'Generic Error on Credit to Wallet leg 2(GL)';
          OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
          RETURN;
      END;
      BEGIN
        INSERT INTO TBTRANSACTIONS (ID, MSGTYPE, CHANNEL, TRXREFNO, TRXSERIALNO, ACCOUNTCENTRE, ACCOUNTNO, CURRENCY,
                                    DRCR, AMOUNT,
                                    TRXDATE, VALUEDATE, FINANCIALCYCLE, FINANCIALPERIOD, CREATEDBY, CREATEDON,
                                    AUTHORIZEDBY, AUTHORIZEDON, ISCUSTOMERGL,
                                    NARRATION, CLEAREDBALANCE, AVAILABLEBALANCE, PROCODE, TRXCODE, STAN, FIELD90,
                                    RUNNINGBALANCE)
        VALUES (v_SERIALNO, v_MsgType, v_Channel, v_TRXNO, v_SERIALNO, '000', v_GCAccountNo, 'KES', v_DRCR, v_Field4,
                v_WORKINGDATE, v_WORKINGDATE, v_FINANCIALYR, v_FINANCIALPRD, iv_UserID,
                SYSDATE, iv_UserID, SYSDATE, v_IsCustomerGL, v_Field68, v_CLEARBAL, v_CLEARBAL,
                v_Field3, v_Field24, v_Field11, v_Field90, v_RunningBalance);

      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK TO initialState;
          v_ResultCode := '999';
          v_ResultDescription := 'Generic Error on Credit to Wallet leg 2(GL) - insert';
          OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
          RETURN;
      END;
    END IF;
  ELSE
    BEGIN
      v_ResultCode := '010';
      v_ResultDescription := 'Invalid transaction type';
      OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
      RETURN;
    END;
  END IF;

  v_ResultCode := '000';
  v_ResultDescription := 'SUCCESS';


  IF (v_Account102Exists > 0 AND v_TrnType = 'DEBIT_WALLET') THEN
    BEGIN
      SELECT ACTUALBAL, AVAILABLEBAL INTO v_actualBalance,v_Availbal FROM TBACCOUNT WHERE ACCOUNTNO = v_Field102;
    EXCEPTION
      WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
        ROLLBACK TO initialState;
        v_ResultCode := '999';
        v_ResultDescription := 'Generic Error on Getting final balance for f102';
        OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
        RETURN;
    END;
  ELSIF (v_Account103Exists > 0 AND v_TrnType = 'CREDIT_WALLET') THEN
    BEGIN
      SELECT ACTUALBAL, AVAILABLEBAL INTO v_actualBalance,v_Availbal FROM TBACCOUNT WHERE ACCOUNTNO = v_Field103;
    EXCEPTION
      WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
        ROLLBACK TO initialState;
        v_ResultCode := '999';
        v_ResultDescription := 'Generic Error on Getting final balance for f103';
        OPEN c_1 FOR SELECT v_ResultCode RESULTCODE, v_ResultDescription RESULTDESC FROM DUAL;
        RETURN;
    END;
  ELSE
    v_actualBalance := '';
    v_Availbal := '';
  END IF;

  OPEN c_1 FOR SELECT v_ResultCode        RESULTCODE,
                      v_ResultDescription RESULTDESC,
                      v_Availbal          AVAILABLEBAL,
                      v_actualBalance     ACTUALBAL
               FROM DUAL;
  RETURN;
END "SP_POST_TO_WALLET";

