/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.web;

import commonj.work.Work;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.commonoperations.QueueWriter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author njiru
 */
public class ProcessIncoming implements Work {

    Map<String, String> details = new HashMap();

    public ProcessIncoming(Map<String, String> details) {
        this.details = details;
    }

    @Override
    public void run() {
        try {
            doProcessing();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("RunException", "Exception occured while attempting to run.\nError:" + sw.toString());
            el.log();
        }
    }

    public void doProcessing() {

        try {
            switch (details.get("channel")) {
                case "app":
                    sendtofirebase();
                    break;
                case "email":
                    sendemail();
                    break;
                case "sms":
                    sendsms();
                    break;
                default:
                    break;

            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("switchingException", "Exception occured while switching.\nError:" + sw.toString());
            el.log();
        }

    }

    void sendtofirebase() {
        String tokenId = "cbwYSfsDtys:APA91bFUagp9Ck4aMrcQfmPNnFHJDWwsQGg2oIT05rT9hnawrOWE5P_JKYit4VpyRv_GRUNllgjyQxNemwYWoARVzqwNkhFlsr1HVo_BpsmgOdkwVkLJCCCg-ypJJUQT2GCre4K5rJeO";
        String server_key = "AAAATXNYxbY:APA91bETs1t4aEmqLNWzXGHVs1HIcX9VeuCtiUXP5dtUVAQYGH1s2OlQS65gU99-6fd6qQvJSYetCG7PAZ_JBO33IcR7BPKRfatc5zYIoePfgcHl1IkbY46xD4bJ8stzTwMYDsPwpQZD";
        try {
            //send to firebase
            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");

            //pass FCM server key
            conn.setRequestProperty("Authorization", "key=" + server_key);
            conn.setRequestProperty("Content-Type", "application/json");

            JSONObject notification = new JSONObject();
            notification.put("title", details.get("title"));
            notification.put("body", details.get("message"));
            JSONObject json = new JSONObject();
            json.put("to", "/topics/all");
            json.put("notification", notification);

            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(json.toString());
            wr.flush();
            int status = conn.getResponseCode();

            switch (status) {
                case 200:
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    System.out.println("Android Notification Response : " + reader.readLine());
                    break;
                case 401:
                    System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
                    break;
                case 501:
                    System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
                    break;
                case 503:
                    System.out.println("Notification Response : FCM Service is Unavailable TokenId: " + tokenId);
                    break;
                default:
                    break;
            }
        } catch (IOException | JSONException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("FirebaseException", "Exception occured while sending request to google firebase.\nError:" + sw.toString());
            el.log();
        }

    }

    void sendemail() {
        HashMap<String, String> mailMap = new HashMap();
        try {
            if ("".equals(details.get("recepient")) || null == details.get("recepient")) {
                // send bulk
                (new DatabaseFunctions()).sendbulkemails(details);

            } else {
                mailMap.put("NotificationType", "EMAIL");
                mailMap.put("EMAILSUBJECT", details.get("subject"));
                mailMap.put("EMAILMESSAGE", details.get("message"));
                mailMap.put("EMAILADDRESS", details.get("recepient"));
                QueueWriter qW = new QueueWriter("jms/NOTIFICATIONS_QUEUE");
                qW.sendObject(mailMap, (new DatabaseFunctions()).getCorrelationID());
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("mailException", "Exception occured while sending email to notification queue.\nError:" + sw.toString());
            el.log();
        }
    }

    void sendsms() {
        HashMap<String, String> smsMap = new HashMap();
        try {
            if ("".equals(details.get("phonenumber")) || null == details.get("phonenumber")) {
                //send bulk
            } else {
                smsMap.put("NotificationType", "SMS");
                smsMap.put("2", details.get("phonenumber"));
                smsMap.put("98", "SMS");
                smsMap.put("32", "Portal");
                smsMap.put("SMSMESSAGE", details.get("message"));
                smsMap.put("102", details.get("phonenumber"));
                QueueWriter qW = new QueueWriter("jms/NOTIFICATIONS_QUEUE");
                qW.sendObject(smsMap, (new DatabaseFunctions()).getCorrelationID());
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("smsException", "Exception occured while sending sms to notification queue.\nError:" + sw.toString());
            el.log();
        }
    }

    @Override
    public void release() {
    }

    @Override
    public boolean isDaemon() {
        return false;
    }
}
