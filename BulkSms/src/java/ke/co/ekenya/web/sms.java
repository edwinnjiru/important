/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.web;

import commonj.work.WorkException;
import commonj.work.WorkManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import ke.co.ekenya.commonoperations.Utilities;

/**
 *
 * @author njiru
 */
@WebService(serviceName = "sms")
@HandlerChain(file = "sms_handler.xml")
public class sms {

    @WebMethod(operationName = "appnotification")
    public String appnotification(@WebParam(name = "Title") String Title, @WebParam(name = "message") String message) {
        String response = "<?xml version=\"1.0\"?>"
                + "<BulkSmsResponse>"
                + "<statusCode>1</statusCode>"
                + "<resultmessage>Success</resultmessage>"
                + "</BulkSmsResponse>";

        Map<String, String> details = new HashMap();
        details.put("channel", "app");
        details.put("title", Title);
        details.put("message", message);

        WorkManager wm = Utilities.getWorkManager();
        ProcessIncoming processIncoming = new ProcessIncoming(details);
        try {
            wm.schedule(processIncoming);
        } catch (WorkException | IllegalArgumentException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
        }

        return response;
    }

    @WebMethod(operationName = "sendemail")
    public String sendemail(@WebParam(name = "subject") String subject, @WebParam(name = "message") String message, @WebParam(name = "recepient") String recepient) {
        String response = "<?xml version=\"1.0\"?>"
                + "<BulkSmsResponse>"
                + "<statusCode>1</statusCode>"
                + "<resultmessage>Success</resultmessage>"
                + "</BulkSmsResponse>";
        if (recepient.equals("?")) {
            recepient = "";
        }
        Map<String, String> details = new HashMap();
        details.put("channel", "email");
        details.put("subject", subject);
        details.put("message", message);
        details.put("recepient", recepient);
        System.out.println(details.toString());

        WorkManager wm = Utilities.getWorkManager();
        ProcessIncoming processIncoming = new ProcessIncoming(details);
        try {
            wm.schedule(processIncoming);
        } catch (WorkException | IllegalArgumentException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
        }
        return response;
    }

    @WebMethod(operationName = "sendsms")
    public String sendsms(@WebParam(name = "recepient") String recepient, @WebParam(name = "message") String message, @WebParam(name = "phonenumber") String phonenumber) {
        String response = "<?xml version=\"1.0\"?>"
                + "<BulkSmsResponse>"
                + "<statusCode>1</statusCode>"
                + "<resultmessage>Success</resultmessage>"
                + "</BulkSmsResponse>";

        Map<String, String> details = new HashMap();
        details.put("channel", "sms");
        details.put("recepient", recepient);
        details.put("message", message);
        details.put("phonenumber", phonenumber);

        WorkManager wm = Utilities.getWorkManager();
        ProcessIncoming processIncoming = new ProcessIncoming(details);
        try {
            wm.schedule(processIncoming);
        } catch (WorkException | IllegalArgumentException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
        }
        return response;
    }

}
