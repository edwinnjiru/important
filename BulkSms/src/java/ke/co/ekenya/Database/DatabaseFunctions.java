/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.commonoperations.QueueWriter;

/**
 *
 * @author njiru
 */
public class DatabaseFunctions extends DatabaseModel {

    public DatabaseFunctions() {
    }

    public String getCorrelationID() {
        String CorrelationID = "";
        String command = "{call SP_GET_ESB_NEXT_SEQ(?)}";
        try (Connection con = createMvisaConnection();
                CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.registerOutParameter("cv_1", java.sql.Types.VARCHAR);
            callableStatement.executeUpdate();
            CorrelationID = callableStatement.getString("cv_1");
        } catch (Exception ex) {
            ESBLog el = new ESBLog(ex, "");
            el.log();
        }
        return CorrelationID;
    }

    public void sendbulkemails(Map<String, String> details) {
        Map<Integer, Map<String, String>> allids = getrange();
        System.out.println("amepata ranges");

        int i = 0;
        for (int k = 0; k < allids.size(); k++) {
            Map<String, String> singlerow = allids.get(k);

            String sql = "select EMAILADDRESS,MWALLETACCOUNT from tbcustomers where id>='" + singlerow.get("min") + "' and id<'" + singlerow.get("max") + "' and EMAILADDRESS is not null";
            try (Connection con = createConnection();
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(sql)) {

                while (rs.next()) {
                    HashMap<String, String> mailMap = new HashMap();
                    mailMap.put("NotificationType", "EMAIL");
                    mailMap.put("EMAILSUBJECT", details.get("subject"));
                    mailMap.put("EMAILMESSAGE", details.get("message"));
                    mailMap.put("EMAILADDRESS", rs.getString("EMAILADDRESS").toLowerCase());
                    QueueWriter qW = new QueueWriter("jms/NOTIFICATIONS_QUEUE");
                    qW.sendObject(mailMap, (new DatabaseFunctions()).getCorrelationID());

                    System.out.println("cumulative records sent: " + i);
                    i++;
                }

            } catch (Exception ex) {
                System.out.println("kuna exception: " + ex.getMessage());
                ESBLog el = new ESBLog(ex, "");
                el.log();
            }

        }
    }

    public int getmaxid() {
        String sql = "select max(ID) as ID from tbcustomers";
        int maxid = 0;
        try (Connection con = createConnection();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql);) {
            while (rs.next()) {
                maxid = rs.getInt("ID");
            }
            System.out.println("max id: " + maxid);

        } catch (Exception ex) {
            ESBLog el = new ESBLog(ex, "");
            el.log();
        }
        return maxid;
    }

    public Map getrange() {
        //code by Edwin Njiru(El capitan)
        int id = getmaxid();
        Map<Integer, Map<String, String>> allids = new HashMap();
        int rangewidth=100;
        try {
            int j = 0;
            while (id > 0) {
                Map<String, String> ids = new HashMap();
                ids.put("max", String.valueOf(id));
                if (id >= rangewidth) {
                    ids.put("min", String.valueOf((id - rangewidth)));
                    id = id - rangewidth; //new max value
                    allids.put(j, ids);
                } else {
                    ids.put("min", "0");
                    allids.put(j, ids);
                    break;
                }
                j++;
            }

        } catch (Exception ex) {
            ESBLog el = new ESBLog(ex, "");
            el.log();
        }
        return allids;

    }

}
