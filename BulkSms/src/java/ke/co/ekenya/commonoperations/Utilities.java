/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.commonoperations;

import commonj.work.WorkManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author njiru
 */
public class Utilities {
    
      public static WorkManager getWorkManager() {
        WorkManager workManager = null;
        if (workManager == null) {
            try {
                InitialContext ctx = new InitialContext();
                String jndiName = "java:global/wm/default";
                workManager = (WorkManager) ctx.lookup(jndiName);
            } catch (NamingException e) {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
            }
        }
        return workManager;
    }
    
}
