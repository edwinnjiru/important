/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ngarijosh.process;

import com.ngarijosh.hellowsclient.TransactionResult;

/**
 *
 * @author jngari
 */
public class ProcessWS {
    
    public static void main(String args[]){
        
        //Request parameter - Wakanda
        String response = sayHello("Wakanda");
        //Response - Black Panther, Wakanda Forever!!!
        
        //Request parameter - Arsenal
        String response2 = sayHello("Arsenal");
        //Response - Wenger left ....
        
        //Request parameter - SGR
        String response3 = sayHello("SGR");
        //Response - Twende tujivinjari.
        
        //Request parameter - Sudan
        String response4 = sayHello("Sudan");
        //Response - World's last male northern white rhino, RIP.
        
        //Request parameter - Safaricom
        String response5 = sayHello("Safaricom");
        //Response - Pamoja twaweza
        
        System.out.println(response);
        System.out.println(response2);
        System.out.println(response3);
        System.out.println(response4);
        System.out.println(response5);
    }
    
    public static String sayHello(String name){  
        
        TransactionResult transactionResult = hello(name);                
        return transactionResult.getResponseDesc();
    }

    private static TransactionResult hello(java.lang.String name) {
        com.ngarijosh.hellowsclient.TestWS_Service service = new com.ngarijosh.hellowsclient.TestWS_Service();
        com.ngarijosh.hellowsclient.TestWS port = service.getTestWSPort();
        return port.hello(name);
    }
    
}
