/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ngarijosh_company.exam;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.Map;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author jngari
 */
public class Utilities {
    
    /**
     * Method to perform basic authorization
     * @param authString
     * @return 
     */
    public boolean basicAuthorization(String authString) {
        boolean authenticated = false;
        String authInfo = "";
        String decodedAuth = "";
        String apiUsername = "";
        String apiPassword = "";
        String validationString = "";

        try {

//         Header is in the format "Basic 5tyc0uiDat4"
//         We need to extract data before decoding it back to original string
            if (!"".equalsIgnoreCase(authString)) {
                String[] authParts = authString.split("\\s+");
                authInfo = authParts[1];
            }
            // Decode the data back to original string
            byte[] bytes = null;
            bytes = java.util.Base64.getDecoder().decode(authInfo);

            decodedAuth = new String(bytes);

            apiUsername = "ServiceExam@Username";
            apiPassword = "ServiceExam@Password";

            validationString = apiUsername + ":" + apiPassword;

            if (decodedAuth.equalsIgnoreCase(validationString)) {
                authenticated = true;
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

        }

        return authenticated;
    }
    
    /**
     * Converts JSON string into a hashmap of transaction fields into a from
     * storage
     *
     * @param json - JSON string message
     * @param dataFields - hashmap of transaction fields
     * @return dataFields - hashmap of transaction fields
     */
    public Map<String, String> parseJSON(JSONObject json, Map<String, String> dataFields) throws JSONException {
        Iterator<String> keys = json.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            String val = null;
            try {
                JSONObject value = json.getJSONObject(key);
                parseJSON(value, dataFields);
            } catch (Exception e) {
                if (json.isNull(key)) {
                    val = "";
                } else {
                    try {
                        val = json.getString(key);
                    } catch (Exception ex) {
                        System.out.println("Error:" + ex.getMessage());
                    }
                }
            }

            if (val != null && key != null) {
                dataFields.put(key, val);
            }
        }
        return dataFields;
    }

}
