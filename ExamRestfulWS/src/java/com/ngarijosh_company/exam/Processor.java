/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ngarijosh_company.exam;

import Logger.ESBLog;
import com.ngarijosh_company.database.DBFunctions;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author jngari
 */
public class Processor {

    public String processQuestions(String questionNo) {

        boolean error = false;
        String resultJson = "";
        JSONObject resultsObj = new JSONObject();

        if ("".equalsIgnoreCase(questionNo)) {
            try {
                error = true;
                resultsObj.put("error", error);
                resultsObj.put("errorMessage", "Kindly provide a question number");
                resultJson = resultsObj.toString();
            } catch (JSONException ex) {
                Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            DBFunctions dBFunctions = new DBFunctions();

            resultJson = dBFunctions.getQuestions("SELECT question_id,question_description,choice_id,question,choice,"
                    + "choice_description FROM TB_QUESTIONS JOIN TB_CHOICES ON TB_QUESTIONS.question_id=TB_CHOICES.question"
                    + " WHERE question_id='" + questionNo + "'");

            ESBLog logger = new ESBLog("E:\\logs\\ResftfulExam_Logs\\", "resultJson");
            logger.logfile();

        }

        return resultJson;
    }

    

}
