/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ngarijosh_company.exam;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * REST Web Service
 *
 * @author jngari
 */
@Path("questions")
public class Questions {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Questions
     */
    public Questions() {
    }

    /**
     * Retrieves representation of an instance of
     * com.ngarijosh_company.exam.Questions
     *
     * @param headers
     * @param questionNo
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getXml(@Context HttpHeaders headers, @QueryParam("questionNo") String questionNo) {

        boolean error = false;
        String resultJson = "";
        JSONObject resultsObj = new JSONObject();
        String authString = "";
        try {

            if ("".equalsIgnoreCase(headers.toString())) {
                error = true;
                resultsObj = new JSONObject();
                try {
                    resultsObj.put("error", error);
                    resultsObj.put("errorMessage", "User Authentication Failed!");
                    resultJson = resultsObj.toString();
                } catch (JSONException ex1) {
                    Logger.getLogger(Questions.class.getName()).log(Level.SEVERE, null, ex1);
                }

                return resultJson;

            } else {
                authString = "";
                authString = headers.getRequestHeader("Authorization").get(0).trim();
            }

            boolean isUserAuthenticated = false;

            if (!"".equalsIgnoreCase(authString)) {
                isUserAuthenticated = new Utilities().basicAuthorization(authString);
            }

            if (isUserAuthenticated) {

                resultJson=new Processor().processQuestions(questionNo);

            } else {
                error = true;
                resultsObj.put("error", error);
                resultsObj.put("errorMessage", "Kindly provide a question number");
                resultJson = resultsObj.toString();

                return resultJson;
            }

        } catch (JSONException ex) {
            error = true;
            resultsObj = new JSONObject();
            try {
                resultsObj.put("error", error);
                resultsObj.put("errorMessage", "An error occured while processing your request");
                resultJson = resultsObj.toString();
            } catch (JSONException ex1) {
                Logger.getLogger(Questions.class.getName()).log(Level.SEVERE, null, ex1);
            }

        }

        return resultJson;
    }

    /**
     * Retrieves representation of an instance of
     * com.ngarijosh_company.exam.Questions
     *
     * @param headers
     * @param request
     * @return an instance of java.lang.String
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String processRequest(@Context HttpHeaders headers, String request) {
        boolean isUserAuthenticated = false;
        boolean error = false;
        String resultJson = "";
        JSONObject resultsObj = new JSONObject();
        String authString = "";

        try {
            JSONObject requestObj = new JSONObject(request);
            Map<String, String> requestMap = new HashMap<>();
            requestMap = (new Utilities()).parseJSON(requestObj, requestMap);

            String questionNo = requestMap.get("questionNo");

            if ("".equalsIgnoreCase(headers.toString())) {
                error = true;
                resultsObj = new JSONObject();
                try {
                    resultsObj.put("error", error);
                    resultsObj.put("errorMessage", "User Authentication Failed!");
                    resultJson = resultsObj.toString();
                } catch (JSONException ex1) {
                    Logger.getLogger(Questions.class.getName()).log(Level.SEVERE, null, ex1);
                }

//                return resultJson;
            } else {
                authString = "";
                authString = headers.getRequestHeader("Authorization").get(0).trim();
                if (!"".equalsIgnoreCase(authString)) {
                    isUserAuthenticated = new Utilities().basicAuthorization(authString);

                    if (isUserAuthenticated) {

                         resultJson=new Processor().processQuestions(questionNo);

                    } else {
                        error = true;
                        resultsObj.put("error", error);
                        resultsObj.put("errorMessage", "User Authentication Failed!");
                        resultJson = resultsObj.toString();
                    }
                } else {
                    resultsObj.put("error", error);
                    resultsObj.put("errorMessage", "User Authentication Failed!");
                    resultJson = resultsObj.toString();
                }

            }

        } catch (JSONException ex) {
            Logger.getLogger(Questions.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultJson;
    }

    /**
     * PUT method for updating or creating an instance of Questions
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }

}
