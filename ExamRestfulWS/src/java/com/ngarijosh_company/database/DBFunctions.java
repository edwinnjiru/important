package com.ngarijosh_company.database;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Responsible for all database specific methods for Select, Update, Delete
 * queries and executing stored procedures (callable statements) as well as
 * prepared statements.
 *
 * @author Hesbon
 */
public class DBFunctions {

    private DBconn DBconn;

    private CallableStatement callableStatement = null;

    Map<String, String> Configs;

    public DBFunctions() {

    }

    /**
     * Initializes the eConnect Database connections
     */
    private void getDBconn() {
        this.DBconn = new DBconn();
    }

    /**
     * Fetches result set from database of a give query and returns a JSON
     * string of data
     *
     * @param query- The SQL query to execute
     * @return string - A json string containing the questions
     */
    public String getQuestions(String query) {
        getDBconn();
        String questionsJson = "";
        HashMap<String, String> hm = new HashMap();

        try {
            DBconn.rs = DBconn.stmt.executeQuery(query);
            while (DBconn.rs.next()) {

                hm.put("QUESTION_NO", DBconn.rs.getString("question_id"));
                hm.put("QUESTION", DBconn.rs.getString("question_description"));
            }

            String questionNo = hm.get("QUESTION_NO");
            String queryQuestionChoices = "SELECT choice_id,question,choice,"
                    + "choice_description FROM TB_CHOICES WHERE QUESTION='" + questionNo + "'";

            JSONObject choicesObj = new JSONObject();

            DBconn.rs = DBconn.stmt.executeQuery(queryQuestionChoices);
            while (DBconn.rs.next()) {
                choicesObj.put(DBconn.rs.getString("choice"), DBconn.rs.getString("choice_description"));
            }

            JSONObject questionsObj = new JSONObject();

            questionsObj.put("QUESTION_NO", hm.get("QUESTION_NO"));
            questionsObj.put("QUESTION", hm.get("QUESTION"));
            questionsObj.put("CHOICES", choicesObj);

            questionsJson = questionsObj.toString();

        } catch (SQLException e) {

//            ESBLog el = new ESBLog(e, "");
//            el.log();
        } catch (JSONException ex) {
            Logger.getLogger(DBFunctions.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBconn.closeESBConn();
        }
        return questionsJson;
    }

    public boolean processAnswers(String questionNo, String answer) {
        boolean correct = false;
        getDBconn();

        try {
            
            String answersQuery = "SELECT question_id,question_description,answer_id,choice_answer" +
"                    FROM TB_QUESTIONS JOIN TB_ANSWERS ON TB_QUESTIONS.question_id=TB_ANSWERS.question" +
"                    WHERE question_id='" + questionNo + "' AND TB_ANSWERS.choice_answer='" + answer + "'";
            
            System.out.println(answersQuery);
            
            DBconn.rs = DBconn.stmt.executeQuery(answersQuery);
            HashMap<String, String> answerMap = new HashMap();

            while (DBconn.rs.next()) {
                answerMap.put("QUESTION_NO", DBconn.rs.getString("question_id"));
                answerMap.put("CORRECT_ANSWER", DBconn.rs.getString("choice_answer"));
            }

            if (answerMap.containsKey("CORRECT_ANSWER")) {
                if (answer.equalsIgnoreCase(answerMap.get("CORRECT_ANSWER"))) {
                    correct = true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBFunctions.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBconn.closeESBConn();
        }

        return correct;
    }
}
