package com.ngarijosh_company.database;

import java.sql.*;
import java.util.*;
import javax.naming.*;
import javax.sql.DataSource;
/**
 * This DBConn class implements an application that initializes
 * database connection and other database factories
 * 
 * @version 1.0
 * @since   2018-05-14
 * @author jngari
 */


public final class DBconn {

      /**
     Datasource Connection
     **/
    public Connection conn = null;
    /**
     Datasource Connection Statment
     **/
    public Statement stmt = null;
    /**
     Datasource Connection Result Set
     **/
    public ResultSet rs = null;
    /**
     Weblogic Innitial Context
     **/
    public Context ctx = null;
    
    private DataSource ds;

    /**
     * Injects the Econnect ESB Database JNDI and cast it to a datasource
     * Returning an SQL Connection
     */
    public DBconn() {

        try {
            this.conn = esbDbConnection();
            this.stmt = this.conn.createStatement();
        } catch (Exception e) {
//           ESBLog el = new ESBLog(e,"");
//            el.log();
        }
    }

    public Connection esbDbConnection() {
        Connection dbConnection = null;
        Hashtable ht = new Hashtable();
        ht.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        ht.put(Context.PROVIDER_URL, "t3://localhost:7003");
        try {
            ctx = new InitialContext(ht);
            ds = (javax.sql.DataSource) ctx.lookup("jdbc/Exam_DB");
//            ds.setLoginTimeout(1);
            dbConnection = ds.getConnection();
//            ESBLog el = new ESBLog("esbDbConnection", "esbDbConnection initialized");
//            el.log();
        } catch (Exception e) {
//            ESBLog el = new ESBLog(e,"");
//            el.log();
        }

        return dbConnection;
    }

    /**
     * Releases JMS and Database resources by closing them.
     */
    public void closeESBConn() {
        try {
            //Close JDBC objects as soon as possible
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (conn != null) {
                conn.close();
                conn = null;
            }
            //Close context
            if (ctx != null) {
                ctx.close();
            }
        } catch (Exception e) {
//            ESBLog el = new ESBLog(e,"");
//            el.log();
        }
    }

}
