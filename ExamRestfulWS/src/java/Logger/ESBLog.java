package Logger;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SocketHandler;

/**
 * 
 * @author jngari
 */
public class ESBLog {

    private FileHandler fh = null;
    private String pathtologs = "E:\\logs\\ResftfulExam_Logs";
    private SocketHandler sh = null;
    //for socket handler
    private String directory = "Logs";

    private String filename = null;
    private String msg = null;
    private Logger logger = null;
    /**
     * Hahmap to be logged to db *
     */
    public HashMap<String, String> logmap;

    /**
     * Constructor for Text messages
     *
     * @param filename
     * @param msg - Text message to be logged
     */
    public ESBLog(String filename, String msg) {
        this.filename = filename;
        this.msg = msg;
    }

    public ESBLog(String filename, Exception ex, String msg) {
        this.filename = filename;
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString() + "\n" + msg;
    }

    public ESBLog(String filename, HashMap<String, String> map) {
        this.filename = filename;
        logmap = new HashMap(map);
        this.msg = logmap.toString();
    }

    public ESBLog(String filename, Map<String, String> map) {
        this.filename = filename;
        logmap = new HashMap(map);
        this.msg = logmap.toString();
    }

    /**
     * Constructor for Error messages (Exception Stack traces)
     *
     * @param ex
     * @param msg - Text message to be logged
     */
    public ESBLog(Exception ex, String msg) {
        this.filename = "exceptions";
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString() + "\n" + msg;
    }

    public ESBLog(Exception ex, HashMap<String, String> map) {
        this.filename = "exceptions";
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString() + "\n" + msg;
    }

    public ESBLog(Exception ex, Map<String, String> map) {
        this.filename = "exceptions";
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString() + "\n" + msg;
    }

    /**
     * Writes text files to file directly
     */
    public void logfile() {
        logger = Logger.getLogger("");
        String daylog = anyDate("dd-MMM-yyyy");
        try {
            fh = new FileHandler(createDailyDirectory() + filename + "%g.txt", 26000000, 20, true);
            fh.setFormatter(new EsbFormatter());
            logger.addHandler(fh);
            logger.setLevel(Level.FINE);
            logger.fine(this.msg);
            fh.close();
        } catch (Exception e) {
            //System.out.println("AT ESB Class ESBLog{ log() }: " + e.getMessage());
            e.printStackTrace();
        }

    }


    /**
     * Writes text files to file directly
     */
    public void logToFile() {
        logger = Logger.getLogger("");

        try {
            fh = new FileHandler(createDailyDirectory() + filename + "%g.txt", 26000000, 20, true);
            fh.setFormatter(new EsbFormatter());
            logger.addHandler(fh);
            logger.setLevel(Level.FINE);
            logger.fine("");
            fh.close();
        } catch (Exception e) {
            //System.out.println("AT ESB Class ESBLog{ log() }: " + e.getMessage());
            e.printStackTrace();
        }

    }

    /**
     * Creates a directory based on daily path
     *
     * @return Dailydirectory - Created directory
     */
    public String createDailyDirectory() {
        String Dailydirectory = "";
        String daylog = anyDate("dd-MMM-yyyy");
        Dailydirectory = pathtologs + daylog;
        try {
            new File(Dailydirectory).mkdirs();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Dailydirectory + "/";
    }

    public String anyDate(String format) {
        String myDate = "";
        try {
            if ("".equals(format)) {
                format = "yyyy-MM-dd HH:mm:ss"; // default
            }
            LocalDateTime date = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            myDate = date.format(formatter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return myDate;
    }

}
