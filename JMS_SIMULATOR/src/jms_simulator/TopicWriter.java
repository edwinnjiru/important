package jms_simulator;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;

public class TopicWriter {

    public final static String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";
    private String TOPIC;
    private TopicConnectionFactory tconFactory;
    private TopicConnection tcon;
    private TopicSession tsession;
    private TopicPublisher tpublisher;
    private Topic topic;
    private TextMessage msg;
    private ObjectMessage objmsg;

    public TopicWriter(String TOPIC) {
        this.TOPIC = TOPIC;
        InitialContext ic = getInitialContext();
        init(ic, TOPIC);
    }

    public void init(Context ctx, String topicName) {
        try {
            tconFactory = (TopicConnectionFactory) PortableRemoteObject.narrow(ctx.lookup(Config.CONNECTION_FACTORY), TopicConnectionFactory.class);
            tcon = tconFactory.createTopicConnection();
            tsession = tcon.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            topic = (Topic) PortableRemoteObject.narrow(ctx.lookup(topicName), Topic.class);
            tpublisher = tsession.createPublisher(topic);
            msg = tsession.createTextMessage();
            objmsg = tsession.createObjectMessage();
            tcon.start();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
        }
    }

    public void close() {
        try {
            tpublisher.close();
            tsession.close();
            tcon.close();
        } catch (JMSException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
        }
    }

    private InitialContext getInitialContext() {
        InitialContext ic = null;
        try {
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
            //live
            //env.put(Context.PROVIDER_URL, "t3://192.168.114.211:8010,192.168.114.212:8010");
            //test
            env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);
            env.put("weblogic.jndi.createIntermediateContexts", "true");
            ic = new InitialContext(env);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
        }
        return ic;
    }

    public boolean send(String message) {
        boolean sent = false;
        try {
            msg.setText(message);
            //tpublisher.publish(msg);
            tpublisher.publish(msg, DeliveryMode.PERSISTENT, 1, 0);
            sent = true;
        } catch (JMSException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
        }
        close();
        return sent;
    }

    public boolean sendObject(Map message, String biller, String CorrelationID) {
        boolean sent = false;
        try {
            objmsg.setJMSCorrelationID(CorrelationID);
            objmsg.setObject((HashMap)message);
            objmsg.setStringProperty("biller", biller);
            tpublisher.publish(objmsg, DeliveryMode.PERSISTENT, 1, 0);
            sent = true;
        } catch (JMSException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
        }
        close();
        return sent;
    }

}
