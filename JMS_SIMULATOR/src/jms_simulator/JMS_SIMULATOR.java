/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jms_simulator;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import javax.jms.JMSException;
import javax.jms.MapMessage;

/**
 *
 * @author Hesbon
 */
public class JMS_SIMULATOR {

    /**
     * @param args the command line arguments
     */
    static MapMessage mapmsg;
    static Map<String, String> kitsend = new HashMap<>();

    public static void main(String[] args) throws JMSException, InterruptedException {
        try (Scanner reader = new Scanner(System.in)) {
            System.out.println("Input either \n 1. Sendtophone \n 2. Pop \n 3. del \n 4. reg \n 5. update \n 6. set \n 7. Sendtoaccount \n 8. s2psidian \n 9. s2asidian \n");
            String type = reader.next();

            if (type.contains("sendtophone") || type.contains("1")) {
                for(int k=0;k<=100;k++){
                String pref = GenerateRandomNumber(2);
                String ref = pref + GenerateRandomNumber(10);
                String stan = GenerateRandomNumber(6);
                String amt= GenerateRandomNumber(4);
                kitsend.clear();
                kitsend.put("0", "1200");
                kitsend.put("2", "254723713069");
                kitsend.put("3", "400000");
                kitsend.put("4", amt);
                kitsend.put("5", amt);
                kitsend.put("6", amt);
                kitsend.put("24", "MM");
                kitsend.put("32", "USSD");
                kitsend.put("11", stan);
                kitsend.put("35", "SENDTOACCOUNT");
                kitsend.put("37", ref);
                kitsend.put("38", "");
                kitsend.put("39", "000");
                kitsend.put("56", "40466000");
                kitsend.put("126", "254723713069 01001030023296 023310205711 400000 MM PESALINK UPGRADE");
                kitsend.put("102", "254723713069");
                kitsend.put("103", "01001030023296");
                kitsend.put("93", "UPGRADE");
                kitsend.put("98", "PESALINK_FT");
                kitsend.put("CHANNEL", "KITSEND");
                kitsend.put("EXTRACHARGE", "11.6");
                System.out.println(ref + "\n" + stan + "\n");

                QueueWriter qr = new QueueWriter("jms/CBS_REQUEST_QUEUE");
                boolean sent=qr.sendObject(kitsend, ref, "PESALINK_FT");
                    System.out.println("sent: "+sent);
                }
            } else if (type.contains("pop") || type.contains("2")) {
                String ref = GenerateRandomNumber(10);
                kitsend.clear();
                System.out.println("Enter destination number beginning with 254 \n");
                String dest = reader.next();
                kitsend.put("103", dest);
                kitsend.put("2", "254726649409");
                kitsend.put("3", "320000");
                kitsend.put("0", "0200");
                kitsend.put("MTI", "0200");
                kitsend.put("98", "pesalink_inq");

                QueueWriter qr = new QueueWriter("jms/ADAPTOR_REQUEST_QUEUE");
                qr.sendObject(kitsend, ref, "PESALINK_INQ");
            } else if (type.contains("del") || type.contains("3")) {
                kitsend.clear();
                System.out.println("Enter cutomer number beginning with 254 \n");
                String dest = reader.next();
                kitsend.put("2", dest);
                kitsend.put("sortcode", "40411000");
                kitsend.put("98", "PESALINK_DEL");
                kitsend.put("3", "320000");
                kitsend.put("0", "0200");
                kitsend.put("MTI", "0200");
                QueueWriter qr = new QueueWriter("jms/ADAPTOR_REQUEST_QUEUE");
                qr.sendObject(kitsend, GenerateRandomNumber(10), "PESALINK_DEL");

            } else if (type.contains("reg") || type.contains("4")) {
                kitsend.clear();
                kitsend.put("2", "254724696680");
                kitsend.put("sortcode", "40411000");
                kitsend.put("71", "29100581");
                kitsend.put("72", "NATIONAL_ID");
                kitsend.put("73", "Collins Sikolia");
                kitsend.put("65", "01125516691000");
                kitsend.put("74", "");
                kitsend.put("CHANNEL", "USSD");
                kitsend.put("cardNumber", "");
                kitsend.put("98", "pesalink_reg");
                kitsend.put("3", "320000");
                kitsend.put("0", "0200");
                kitsend.put("MTI", "0200");
                QueueWriter qr = new QueueWriter("jms/ADAPTOR_REQUEST_QUEUE");
                qr.sendObject(kitsend, GenerateRandomNumber(10), "PESALINK_REG");

            } else if (type.contains("update") || type.contains("5")) {
                System.out.println("Enter new account or new mobile no beginning 254 \n");
                String dest = reader.next();
                kitsend.put("2", "254724696680");
                kitsend.put("sortcode", "40411000");
                kitsend.put("71", "29100581");
                kitsend.put("72", "NATIONAL_ID");
                kitsend.put("73", "Edwin Kivuti Njiru");
                kitsend.put("65", dest);//account number to be set as default
                kitsend.put("74", "edwinnjiru@hotmail.com");
                kitsend.put("cardNumber", "");
                kitsend.put("98", "pesalink_upd");
                kitsend.put("3", "320000");
                kitsend.put("0", "0200");
                kitsend.put("MTI", "0200");
                QueueWriter qr = new QueueWriter("jms/ADAPTOR_REQUEST_QUEUE");
                qr.sendObject(kitsend, GenerateRandomNumber(10), "PESALINK_UPDATE");

            } else if (type.contains("set") || type.contains("6")) {
                System.out.println("Enter new account or new mobile no beginning 254 \n");
                String dest = reader.next();
                kitsend.put("2", "254724696680");
                kitsend.put("sortcode", "40411000");
                kitsend.put("71", "29100581");
                kitsend.put("72", "NATIONAL_ID");
                kitsend.put("73", "Edwin Kivuti Njiru");
                kitsend.put("65", dest);//account to be set
                kitsend.put("74", "edwinnjiru@hotmail.com");
                kitsend.put("cardNumber", "");
                kitsend.put("98", "pesalink_setacc");
                kitsend.put("3", "320000");
                kitsend.put("0", "0200");
                kitsend.put("MTI", "0200");
                QueueWriter qr = new QueueWriter("jms/ADAPTOR_REQUEST_QUEUE");
                qr.sendObject(kitsend, GenerateRandomNumber(10), "PESALINK_SETACC");

            } else if (type.contains("sendtoaccount") || type.contains("7")) {
                String pref = GenerateRandomNumber(2);
                String ref = pref + GenerateRandomNumber(10);
                String stan = GenerateRandomNumber(6);
                kitsend.clear();
                kitsend.put("0", "1200");
                kitsend.put("2", "254723226137");
                kitsend.put("3", "500000");
                kitsend.put("4", "782.6");
                kitsend.put("5", "771");
                kitsend.put("6", "771");
                kitsend.put("24", "MM");
                kitsend.put("32", "USSD");
                kitsend.put("11", stan);
                kitsend.put("35", "SENDTOACCOUNT");
                kitsend.put("37", "EC" + ref);
                kitsend.put("38", "");
                kitsend.put("39", "000");
                kitsend.put("56", "40411000");
                kitsend.put("126", "254723226137 01108407807800 056571205863 500000 MM PESALINK test");
                kitsend.put("102", "254723226137");
                kitsend.put("103", "01108407807800");
                kitsend.put("93", "UPGRADE");
                kitsend.put("98", "PESALINK_FT");
                kitsend.put("CHANNEL", "KITSEND");

                System.out.println(ref + "\n" + stan + "\n");

                QueueWriter qr = new QueueWriter("jms/ADAPTOR_REQUEST_QUEUE");
                qr.sendObject(kitsend, "EC" + ref, "PESALINK_FT");
            } else if (type.contains("s2psidian") || type.contains("8")) {
                String pref = GenerateRandomNumber(2);
                String ref = pref + GenerateRandomNumber(10);
                String stan = GenerateRandomNumber(6);
                kitsend.clear();
                kitsend.put("0", "1200");
                kitsend.put("2", "254725306007");
                kitsend.put("3", "500000");
                kitsend.put("4", "782.6");
                kitsend.put("5", "771");
                kitsend.put("6", "771");
                kitsend.put("24", "MM");
                kitsend.put("32", "USSD");
                kitsend.put("11", stan);
                kitsend.put("35", "SENDTOPHONE");
                kitsend.put("37", "EC" + ref);
                kitsend.put("38", "");
                kitsend.put("39", "000");
                kitsend.put("56", "40466000");
                kitsend.put("126", "254723713069 01108407807800 023310205711 500000 MM PESALINK UPGRADE");
                kitsend.put("102", "254723713069");
                kitsend.put("103", "254722777151");
                kitsend.put("93", "UPGRADE");
                kitsend.put("98", "PESALINK_FT");
                kitsend.put("CHANNEL", "KITSEND");

                System.out.println(ref + "\n" + stan + "\n");

                QueueWriter qr = new QueueWriter("jms/ADAPTOR_REQUEST_QUEUE");
                qr.sendObject(kitsend, "EC" + ref, "PESALINK_FT");
            } else if (type.contains("s2asidian") || type.contains("9")) {
                for(int j=0;j<=50;j++){
                String pref = GenerateRandomNumber(2);
                String ref = pref + GenerateRandomNumber(10);
                String stan = GenerateRandomNumber(6);
                String amt= GenerateRandomNumber(4);
                kitsend.clear();
                kitsend.put("0", "1200");
                kitsend.put("2", "254723713069");
                kitsend.put("3", "400000");
                kitsend.put("4", amt);
                kitsend.put("5", amt);
                kitsend.put("6", amt);
                kitsend.put("24", "MM");
                kitsend.put("32", "USSD");
                kitsend.put("11", stan);
                kitsend.put("35", "SENDTOACCOUNT");
                kitsend.put("37", "EC" + ref);
                kitsend.put("38", "");
                kitsend.put("39", "000");
                kitsend.put("56", "40466000");
                kitsend.put("126", "254723713069 01108407807800 056571205863 400000 MM PESALINK test");
                kitsend.put("102", "254723713069");
                kitsend.put("103", "01001030023296");
                kitsend.put("93", "UPGRADE");
                kitsend.put("98", "PESALINK_FT");
                kitsend.put("CHANNEL", "KITSEND");
                kitsend.put("EXTRACHARGE", "11.6");

                System.out.println(ref + "\n" + stan + "\n");

                QueueWriter qr = new QueueWriter("jms/ADAPTOR_REQUEST_QUEUE");
                qr.sendObject(kitsend, "EC" + ref, "PESALINK_FT");
                }
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    public static String GenerateRandomNumber(int charLength) {
        return String.valueOf(charLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1)
                + (int) Math.pow(10, charLength - 1));
    }

}
