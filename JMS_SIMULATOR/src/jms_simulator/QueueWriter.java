package jms_simulator;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class QueueWriter {

    // Defines the JNDI context factory.
    private final String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";
    // Defines the JMS context factory.
    // Defines the queue.
    private String QUEUE;
    private QueueConnectionFactory qconFactory;
    private QueueConnection qcon;
    private QueueSession qsession;
    private QueueSender qsender;
    private Queue queue;
    private TextMessage msg;
    private ObjectMessage objmsg;
    private MapMessage mapmsg;
    
    public QueueWriter() {
    }

    public QueueWriter(String QUEUE) {
        this.QUEUE = QUEUE;
        InitialContext ic = getInitialContext();
        init(ic, QUEUE);
    }

    public void init(Context ctx, String queueName) {
        try {
            qconFactory = (QueueConnectionFactory) ctx.lookup(Config.CONNECTION_FACTORY);
            qcon = qconFactory.createQueueConnection();
            qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            queue = (Queue) ctx.lookup(queueName);
            qsender = qsession.createSender(queue);
            msg = qsession.createTextMessage();
            objmsg = qsession.createObjectMessage();
            mapmsg = qsession.createMapMessage();
            qcon.start();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            e.printStackTrace();
        }
    }

    private InitialContext getInitialContext() {
        Hashtable env = new Hashtable();
        InitialContext ic = null;
        try {
            env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
            env.put(Context.PROVIDER_URL, Config.PROVIDER_URL.trim());
            ic = new InitialContext(env);
        } catch (NamingException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            e.printStackTrace();
        }
        return ic;
    }

    public void close() {
        try {
            qsender.close();
            qsession.close();
            qcon.close();
        } catch (JMSException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            e.printStackTrace();
        }
    }

    /*public boolean send(String message, String CorrelationID) {
        boolean sent = false;
        try {
            mapmsg.setString("biller", "PESALINK");
            mapmsg.setString("Field37", "758846756");
            mapmsg.setString("Field2", "1200");
            mapmsg.setString("Field4", "5000");            
            qsender.send(mapmsg);
            sent = true;
        } catch (JMSException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            e.printStackTrace();
        }
        close();
        return sent;
    }*/

    public boolean sendObject(Map message, String CorrelationID, String biller) {
        boolean sent = false;
        try { //biller = 'KITS_SEND'
            objmsg.setStringProperty("host", "BFUB");
            objmsg.setJMSCorrelationID(CorrelationID);
            objmsg.setObject((HashMap)message);
            qsender.send(objmsg);
            sent = true;
            System.out.println("Sent Successfully");
        } catch (JMSException e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            
        }
        close();
        return sent;
    }

}
