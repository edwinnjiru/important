package ke.co.ekenya.ToSOA;

import ke.co.coopbank.banking.external.iprs._1.IPRSRequestType;
import ke.co.coopbank.commonservices.data.message.messageheader.CredentialsType;
import ke.co.coopbank.commonservices.data.message.messageheader.ObjectFactory;
import ke.co.coopbank.commonservices.data.message.messageheader.RequestHeaderType;
import ke.co.coopbank.commonservices.data.message.messageheader.ResponseHeaderType;
import ke.co.coopbank.commonservices.internal.ts.iprs.IPRS;
import ke.co.coopbank.commonservices.internal.ts.iprs.IPRS2;
import ke.co.coopbank.iprsmanager.commondata.GetIPRSDataByGenericID;
import ke.co.coopbank.iprsmanager.commondata.HumanInfoCommon;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.Messagehandlers.IPRSResolver;
import ke.co.ekenya.Messagehandlers.IprsHandler;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SendRequest {

    Map<String, String> configs;
    Map<String, String> requestMap;
    Map<String, String> responseMap;

    public SendRequest(Map<String, String> configs, Map<String, String> requestMap, Map<String, String> responseMap) {
        this.configs = configs;
        this.requestMap = requestMap;
        this.responseMap = responseMap;
    }

    public void sendToSoa() {
        try {
            ObjectFactory objectFactory = new ObjectFactory();
            ke.co.coopbank.iprsmanager.commondata.ObjectFactory obj = new ke.co.coopbank.iprsmanager.commondata.ObjectFactory();

            CredentialsType credentialsType = new CredentialsType();
            credentialsType.setSystemCode(objectFactory.createCredentialsTypeSystemCode(configs.get("SOA_SYSCODE")));

            RequestHeaderType requestHeaderType = new RequestHeaderType();
            requestHeaderType.setCorrelationID("4565656565");
            requestHeaderType.setMessageID("65657676");
            requestHeaderType.setCredentials(credentialsType);

            GetIPRSDataByGenericID getIPRSDataByGenericID = new GetIPRSDataByGenericID();
            getIPRSDataByGenericID.setLog(obj.createGetIPRSDataByGenericIDLog(configs.get("IPRS_USERNAME").trim()));
            getIPRSDataByGenericID.setPass(obj.createGetIPRSDataByGenericIDPass((new EncryptionUtils()).decrypt(configs.get("IPRS_PASSWORD").trim())));
            getIPRSDataByGenericID.setDocNumber(obj.createGetIPRSDataByGenericIDDocNumber(requestMap.get("idno")));

            if ("nationalid".equalsIgnoreCase(requestMap.get("idtype"))) {
                getIPRSDataByGenericID.setDocType(obj.createGetIPRSDataByGenericIDDocType("1"));
            } else {
                getIPRSDataByGenericID.setDocType(obj.createGetIPRSDataByGenericIDDocType("2"));
            }

            IPRSRequestType iprsRequestType = new IPRSRequestType();
            iprsRequestType.setGetCitizensDetailsReqData(getIPRSDataByGenericID);

            String wsdlLoc = "file:" + configs.get("IPRS_WSDLS_PATH") + "APP.BS.Customer.IPRSDetails.Get-concrete.wsdl";
            URL url = new URL(wsdlLoc);
            IPRS2 service = new IPRS2(url);
            IprsHandler handler = new IprsHandler();
            IPRSResolver resolver = new IPRSResolver(handler);
            service.setHandlerResolver(resolver);
            IPRS port = service.getIPRS2SOAP();

            String proxypass = (new EncryptionUtils()).decrypt(configs.get("SOA_PASSWORD"));
            BindingProvider provider = (BindingProvider) port;
            /*provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configs.get("SOA_USERNAME"));
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);*/

            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "omni");
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "omni123");

            //time-outs
            int connecttimeout = Integer.parseInt(configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            ResponseHeaderType responseHeaderType = new ResponseHeaderType();
            Holder responseHeaderTypeHolder = new Holder(responseHeaderType);

            HumanInfoCommon humanInfoCommon = new HumanInfoCommon();
            Holder humanInfoCommonHolder = new Holder(humanInfoCommon);

            port.getDetails(requestHeaderType, getIPRSDataByGenericID, responseHeaderTypeHolder, humanInfoCommonHolder);
            String response = handler.getMyXML();
            breakDownSOAResponse(response);
        } catch (Exception ex) {
            (new ESBLog("Exception(SOA)", ex)).log();
        }

    }

    private void breakDownSOAResponse(String SOAresp) {
        HashMap result = new HashMap();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAresp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("soapenv:Header");
            HashMap<String, Object> headerResponse = (HashMap<String, Object>) header.get("head:ResponseHeader");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerResponse.get("head:StatusMessages");
            String MSGSTAT = statusMessages.get("head:MessageCode").equals("ISB-100") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("soapenv:Body");
                HashMap<String, Object> citizenDetails = (HashMap<String, Object>) body.get("ns012:GetCitizensDetailsRespData");

                responseMap.put("error", "false");
                responseMap.put("message", MSGSTAT);
                responseMap.put("firstname", citizenDetails.get("ns6:FirstName").toString());
                responseMap.put("middlename", citizenDetails.get("ns6:MiddleName").toString());
                responseMap.put("lastname", citizenDetails.get("ns6:LastName").toString());
                responseMap.put("doctype", citizenDetails.get("ns6:DocumentType").toString());
                responseMap.put("docno", citizenDetails.get("ns6:DocumentNumber").toString());
                responseMap.put("docserial", citizenDetails.get("ns6:DocumentSerial").toString());
                responseMap.put("gender", citizenDetails.get("ns6:Gender").toString());
                responseMap.put("dob", citizenDetails.get("ns6:DateOfBirth").toString());
                responseMap.put("photo", citizenDetails.get("ns6:Photo").toString());
            } else {
                responseMap.put("error", "true");
                responseMap.put("message", "Details not found");
            }


        } catch (Exception ex) {
            (new ESBLog("Exception(BrkDwn)",ex)).log();
            responseMap.put("error", "true");
            responseMap.put("message", "Error breaking down response");
        }
    }
}
