package ke.co.ekenya.api;

import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.ToSOA.SendRequest;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RequestProcessor {

    Map<String, String> configs;
    String request;

    public RequestProcessor(Map<String, String> configs, String request) {
        this.configs = configs;
        this.request = request;
    }

    public Map doProcessing() {
        Map<String, String> responseMap = new HashMap();
        Map<String, String> requestMap = new HashMap();
        try {
            JSONObject requestObject = new JSONObject(request);
            requestMap = (new Utilities()).parseJSON(requestObject, requestMap);
            validateRequest(requestMap,responseMap);
            if("true".equalsIgnoreCase(responseMap.get("valid"))){
                responseMap.clear();
             //send to soa else return response map
                (new SendRequest(configs,requestMap,responseMap)).sendToSoa();
            }


        } catch (Exception ex) {
            (new ESBLog("Exception(proc)",ex)).log();
        }
        return responseMap;
    }

    private void validateRequest(Map<String, String> requestMap, Map<String, String> responseMap) {
        try {
            if(!requestMap.containsKey("idtype")){
             responseMap.put("valid","false");
                responseMap.put("error","true");
                responseMap.put("message","Request missing id type(idtype)");
                return;
            }

            if(("".equalsIgnoreCase(requestMap.get("idtype"))) || (!"nationalid".equalsIgnoreCase(requestMap.get("idtype")) && !"passport".equalsIgnoreCase(requestMap.get("idtype")))){
                responseMap.put("valid","false");
                responseMap.put("error","true");
                responseMap.put("message","Wrong id type. Accepts nationalid and passport");
                return;
            }

            if(!requestMap.containsKey("idno")){
                responseMap.put("valid","false");
                responseMap.put("error","true");
                responseMap.put("message","Request missing id number(idno)");
                return;
            }

            if("".equalsIgnoreCase(requestMap.get("idno").trim())){
                responseMap.put("valid","false");
                responseMap.put("error","true");
                responseMap.put("message","Empty id number(idno)");
                return;
            }

            switch (requestMap.get("idtype").trim()){
                case "nationalid":
                    if (!requestMap.get("idno").trim().matches("\\d+")) {
                        responseMap.put("valid","false");
                        responseMap.put("error","true");
                        responseMap.put("message","Nationl Id accepts numerals only");
                        return;
                    }

                    if (requestMap.get("idno").trim().length()<7) {
                        responseMap.put("valid","false");
                        responseMap.put("error","true");
                        responseMap.put("message","Invalid national id, must have at least 7 characters");
                        return;
                    }
                    break;
                case "passport":
                    break;
            }

            responseMap.put("valid","true");
            responseMap.put("error","false");
            responseMap.put("message","Request is valid");

        } catch (Exception ex) {
            (new ESBLog("Exception(val)",ex)).log();
        }
    }
}
