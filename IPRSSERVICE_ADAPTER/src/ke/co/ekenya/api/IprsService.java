package ke.co.ekenya.api;


import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

// The Java class will be hosted at the URI path "/helloworld"
@Path("/iprsservice")
public class IprsService {

    public static final Map<String, String> CONFIGS = new HashMap();

    static {
        (new Utilities()).readPropertyFile(CONFIGS);
       (new ESBLog("readconfigs", CONFIGS.toString())).log();
    }

    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media type "text/plain"
    @Produces("text/plain")
    public String getClichedMessage() {
        // Return some cliched textual content
        return "System is up and running";
    }

    //This java method will process HTTP POST requests

    /**
     * @param content
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response ProcessPost(String content) {
        Response resp = null;
        try {
            Map<String, String> responseMap = (new RequestProcessor(CONFIGS, content)).doProcessing();

            if ("true".equalsIgnoreCase(responseMap.get("valid"))) {
                responseMap.remove("valid");
                switch (responseMap.get("error").trim()) {
                    case "true":
                        resp = Response.status(503).type(MediaType.APPLICATION_JSON).entity((new JSONObject(responseMap)).toString()).build();
                        break;
                    case "false":
                        resp = Response.status(200).type(MediaType.APPLICATION_JSON).entity((new JSONObject(responseMap)).toString()).build();
                        break;
                }
            } else {
                responseMap.remove("valid");
                resp = Response.status(400).type(MediaType.APPLICATION_JSON).entity((new JSONObject(responseMap)).toString()).build();
            }

        } catch (Exception ex) {
            (new ESBLog("Exception(Init)",ex)).log();
        }
        return resp;
    }

}
