/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.CommonOperations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author njiru
 */
public class SaccoValidations {

    public SaccoValidations() {

    }

    public static String getBillTypePhone(String businessNumber, int institutionType) {
        //payment to bank account
        if (businessNumber.equals(C2BConstants.PAYBILL_TILL_NUMBER)) {
            return C2BConstants.PAYBILL_PHONE_NUMBER;
        } //payment to institutions and saccos
        else if (businessNumber.equals(C2BConstants.MKARO_TILL_NUMBER) && institutionType == 3) {
            return C2BConstants.INSTITUTIONS_PHONE_NUMBER;
        } //payments to Saccos
        else if (businessNumber.equals(C2BConstants.MKARO_TILL_NUMBER)) {
            return C2BConstants.MKARO_PHONE_NUMBER;
        } //payment to rights Issues
        else if (businessNumber.equals(C2BConstants.RIGHTS_TILL_NUMBER)) {
            return C2BConstants.RIGHTS_TILL_NUMBER;
        }
        return null;
    }

    //method to get whether this is an Institution/Sacco link /Directly Integrated Sacco
    public int getInstitutionOrSacco(String destinationacc) {
        int institutionType = 0;//0 means unknown institution type, 1 Means Directly Integrated Sacco, 2 Means Sacco Link, 3 Means Institution
        //split into member code and instition/sacco code, and store both in the array
        String[] accountDetail = destinationacc.split("#");
        String numericRegex = "[0-9]+";

        String accountNumber = accountDetail[0]; //get account number for mapping
        //determine if directly intergrated sacco
        if (isAlphaNumeric(accountNumber)) {
            institutionType = 1;
        } else if (accountNumber.length() == 4 && accountNumber.matches(numericRegex) && (accountNumber.startsWith("33") || accountNumber.startsWith("34"))) {
            institutionType = 2;
        } else if (accountNumber.matches(numericRegex)) {
            institutionType = 3;
        }
        return institutionType;

    }

    //method to check if alphanumberic
    public boolean isAlphaNumeric(String theString) {
        Pattern pattern = Pattern.compile("^(?=.*[A-Z])(?=.*[0-9])[A-Z0-9]+$");
        Matcher matcher = pattern.matcher(theString);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    //method to check if credit card is valid
    public boolean isCreditCard(String card) {
        String newCard = card.toUpperCase();

        if (newCard.startsWith("CR")) {
            String cardInfo[] = newCard.split("CR");

            String cardNumber = cardInfo[1].trim();
            String finalCard = cardNumber;
            if (cardNumber.startsWith("-")) {
                finalCard = cardNumber.replaceFirst("-", "");
            } else {
                finalCard = cardNumber;
            }

            String numericRegex = "[0-9]+";

            if (finalCard.trim().matches(numericRegex)) {
                //valid credit card
                return true;
            } else {
                //invalid credit card
                return false;
            }
        }

        return false;
    }

    public String getCardClientId(String card) {
        String newCard = card.toUpperCase();

        if (newCard.startsWith("CR")) {
            String cardInfo[] = newCard.split("CR");

            String cardNumber = cardInfo[1].trim();
            String finalCard = "";
            if (cardNumber.startsWith("-")) {
                finalCard = cardNumber.replaceFirst("-", "");
            } else {
                finalCard = cardNumber;
            }

            String numericRegex = "[0-9]+";

            if (finalCard.trim().matches(numericRegex)) {
                //valid credit card
                return finalCard.trim();
            } else {
                //invalid credit card
                return "";
            }
        }

        return "";
    }
}
