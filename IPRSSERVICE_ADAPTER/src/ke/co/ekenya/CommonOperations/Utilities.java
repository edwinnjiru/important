package ke.co.ekenya.CommonOperations;

import ke.co.ekenya.LogEngine.ESBLog;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.*;

public class Utilities {
    public Utilities() {

    }

    public static HashMap convertNodesFromXml(String xml) throws Exception {
        xml = trimmessage(xml);//remove white spaces and new lines otherwhise it fails if xml is formatted ---Edwin Njiru 22/01/2018
        InputStream is = new ByteArrayInputStream(xml.getBytes());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setIgnoringElementContentWhitespace(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(is);
        return (HashMap) createMap(document.getDocumentElement());
    }

    public static Object createMap(Node node) {
        Map<String, Object> map = new HashMap<>();
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            String name = currentNode.getNodeName().trim();
            Object value = null;
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                value = createMap(currentNode);
            } else if (currentNode.getNodeType() == Node.TEXT_NODE) {
                return currentNode.getTextContent();
            }
            if (map.containsKey(name)) {
                Object object = map.get(name);
                if (object instanceof List) {
                    ((List<Object>) object).add(value);
                } else {
                    List<Object> objectList = new LinkedList<Object>();
                    objectList.add(object);
                    objectList.add(value);
                    map.put(name, objectList);
                }
            } else {
                map.put(name, value);
            }
        }
        return map;
    }

    public static String trimmessage(String input) {
        BufferedReader reader = new BufferedReader(new StringReader(input));
        StringBuilder result = new StringBuilder();
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.replaceAll("> <", "><");
                result.append(line.trim());
            }
            return result.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String GenerateRandomNumber(int charLength) {
        return String.valueOf(charLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1)
                + (int) Math.pow(10, charLength - 1));
    }

    public String generateStan() {
        String stan = "";
        try {
            Random rnd = new Random();
            int n = 100000 + rnd.nextInt(900000);
            stan = String.valueOf(n);
        } catch (Exception e) {
            System.out.println("Exc: " + e.getMessage());
        }

        return stan;
    }

    public Map<String, String> parseJSON(JSONObject json, Map<String, String> dataFields) throws JSONException {
        dataFields.clear();
        Iterator<String> keys = json.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            String val = null;
            try {
                JSONObject value = json.getJSONObject(key);
                parseJSON(value, dataFields);
            } catch (Exception e) {
                if (json.isNull(key)) {
                    val = "";
                } else {
                    try {
                        val = json.getString(key);
                    } catch (Exception ex) {
                        System.out.println("Error:" + ex.getMessage());
                    }
                }
            }

            if (val != null && key != null) {
                dataFields.put(key, val);
            }
        }
        return dataFields;
    }

    public void readPropertyFile(Map configs) {
        InitialContext initialContext = null;
        try {
            initialContext = new InitialContext();
            NamingEnumeration<NameClassPair> list = initialContext.list("configs");
            while (list.hasMoreElements()) {
                String key = list.next().getName();
                String value = "";
                try {
                    value = (String) initialContext.lookup("configs/" + key);
                    configs.put(key.trim(), value.trim());
                } catch (Exception ex) {
                }
            }
            initialContext.close();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog(sw.toString());
            el.log();
        } finally {
            try {
                if (initialContext != null) {
                    initialContext.close();
                }
            } catch (Exception ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog(sw.toString());
                el.log();
            }
        }
    }
}
