
package ke.co.coopbank.iprsmanager.commondata;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.iprsmanager.commondata package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetIPRSDataByGenericIDSerialNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "serial_number");
    private final static QName _GetIPRSDataByGenericIDDocType_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "doc_type");
    private final static QName _GetIPRSDataByGenericIDDocNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "doc_number");
    private final static QName _GetIPRSDataByGenericIDLog_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "log");
    private final static QName _GetIPRSDataByGenericIDPass_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "pass");
    private final static QName _HumanInfoCommonPhoto_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "Photo");
    private final static QName _HumanInfoCommonLastName_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "LastName");
    private final static QName _HumanInfoCommonGender_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "Gender");
    private final static QName _HumanInfoCommonDocumentType_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "DocumentType");
    private final static QName _HumanInfoCommonMiddleName_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "MiddleName");
    private final static QName _HumanInfoCommonDocumentNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "DocumentNumber");
    private final static QName _HumanInfoCommonDocumentSerial_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "DocumentSerial");
    private final static QName _HumanInfoCommonDateOfBirth_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "DateOfBirth");
    private final static QName _HumanInfoCommonFirstName_QNAME = new QName("http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", "FirstName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.iprsmanager.commondata
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link HumanInfoCommon }
     * 
     */
    public HumanInfoCommon createHumanInfoCommon() {
        return new HumanInfoCommon();
    }

    /**
     * Create an instance of {@link GetIPRSDataByGenericID }
     * 
     */
    public GetIPRSDataByGenericID createGetIPRSDataByGenericID() {
        return new GetIPRSDataByGenericID();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "serial_number", scope = GetIPRSDataByGenericID.class)
    public JAXBElement<String> createGetIPRSDataByGenericIDSerialNumber(String value) {
        return new JAXBElement<String>(_GetIPRSDataByGenericIDSerialNumber_QNAME, String.class, GetIPRSDataByGenericID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "doc_type", scope = GetIPRSDataByGenericID.class)
    public JAXBElement<String> createGetIPRSDataByGenericIDDocType(String value) {
        return new JAXBElement<String>(_GetIPRSDataByGenericIDDocType_QNAME, String.class, GetIPRSDataByGenericID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "doc_number", scope = GetIPRSDataByGenericID.class)
    public JAXBElement<String> createGetIPRSDataByGenericIDDocNumber(String value) {
        return new JAXBElement<String>(_GetIPRSDataByGenericIDDocNumber_QNAME, String.class, GetIPRSDataByGenericID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "log", scope = GetIPRSDataByGenericID.class)
    public JAXBElement<String> createGetIPRSDataByGenericIDLog(String value) {
        return new JAXBElement<String>(_GetIPRSDataByGenericIDLog_QNAME, String.class, GetIPRSDataByGenericID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "pass", scope = GetIPRSDataByGenericID.class)
    public JAXBElement<String> createGetIPRSDataByGenericIDPass(String value) {
        return new JAXBElement<String>(_GetIPRSDataByGenericIDPass_QNAME, String.class, GetIPRSDataByGenericID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "Photo", scope = HumanInfoCommon.class)
    public JAXBElement<byte[]> createHumanInfoCommonPhoto(byte[] value) {
        return new JAXBElement<byte[]>(_HumanInfoCommonPhoto_QNAME, byte[].class, HumanInfoCommon.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "LastName", scope = HumanInfoCommon.class)
    public JAXBElement<String> createHumanInfoCommonLastName(String value) {
        return new JAXBElement<String>(_HumanInfoCommonLastName_QNAME, String.class, HumanInfoCommon.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "Gender", scope = HumanInfoCommon.class)
    public JAXBElement<String> createHumanInfoCommonGender(String value) {
        return new JAXBElement<String>(_HumanInfoCommonGender_QNAME, String.class, HumanInfoCommon.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "DocumentType", scope = HumanInfoCommon.class)
    public JAXBElement<String> createHumanInfoCommonDocumentType(String value) {
        return new JAXBElement<String>(_HumanInfoCommonDocumentType_QNAME, String.class, HumanInfoCommon.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "MiddleName", scope = HumanInfoCommon.class)
    public JAXBElement<String> createHumanInfoCommonMiddleName(String value) {
        return new JAXBElement<String>(_HumanInfoCommonMiddleName_QNAME, String.class, HumanInfoCommon.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "DocumentNumber", scope = HumanInfoCommon.class)
    public JAXBElement<String> createHumanInfoCommonDocumentNumber(String value) {
        return new JAXBElement<String>(_HumanInfoCommonDocumentNumber_QNAME, String.class, HumanInfoCommon.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "DocumentSerial", scope = HumanInfoCommon.class)
    public JAXBElement<String> createHumanInfoCommonDocumentSerial(String value) {
        return new JAXBElement<String>(_HumanInfoCommonDocumentSerial_QNAME, String.class, HumanInfoCommon.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "DateOfBirth", scope = HumanInfoCommon.class)
    public JAXBElement<String> createHumanInfoCommonDateOfBirth(String value) {
        return new JAXBElement<String>(_HumanInfoCommonDateOfBirth_QNAME, String.class, HumanInfoCommon.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/IPRSManager/CommonData", name = "FirstName", scope = HumanInfoCommon.class)
    public JAXBElement<String> createHumanInfoCommonFirstName(String value) {
        return new JAXBElement<String>(_HumanInfoCommonFirstName_QNAME, String.class, HumanInfoCommon.class, value);
    }

}
