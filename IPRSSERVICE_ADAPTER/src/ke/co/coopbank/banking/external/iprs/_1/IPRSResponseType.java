
package ke.co.coopbank.banking.external.iprs._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ke.co.coopbank.commonservices.data.message.messageheader.ResponseHeaderType;
import ke.co.coopbank.iprsmanager.commondata.HumanInfoCommon;


/**
 * <p>Java class for IPRSResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPRSResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/CommonServices/Data/Message/MessageHeader}ResponseHeader"/>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/External/IPRS/1.0}GetCitizensDetailsRespData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPRSResponseType", propOrder = {
    "responseHeader",
    "getCitizensDetailsRespData"
})
public class IPRSResponseType {

    @XmlElement(name = "ResponseHeader", namespace = "urn://co-opbank.co.ke/CommonServices/Data/Message/MessageHeader", required = true)
    protected ResponseHeaderType responseHeader;
    @XmlElement(name = "GetCitizensDetailsRespData", required = true)
    protected HumanInfoCommon getCitizensDetailsRespData;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeaderType }
     *     
     */
    public ResponseHeaderType getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeaderType }
     *     
     */
    public void setResponseHeader(ResponseHeaderType value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the getCitizensDetailsRespData property.
     * 
     * @return
     *     possible object is
     *     {@link HumanInfoCommon }
     *     
     */
    public HumanInfoCommon getGetCitizensDetailsRespData() {
        return getCitizensDetailsRespData;
    }

    /**
     * Sets the value of the getCitizensDetailsRespData property.
     * 
     * @param value
     *     allowed object is
     *     {@link HumanInfoCommon }
     *     
     */
    public void setGetCitizensDetailsRespData(HumanInfoCommon value) {
        this.getCitizensDetailsRespData = value;
    }

}
