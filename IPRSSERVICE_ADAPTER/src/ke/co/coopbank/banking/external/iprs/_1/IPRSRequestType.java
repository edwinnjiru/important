
package ke.co.coopbank.banking.external.iprs._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ke.co.coopbank.commonservices.data.message.messageheader.RequestHeaderType;
import ke.co.coopbank.iprsmanager.commondata.GetIPRSDataByGenericID;


/**
 * <p>Java class for IPRSRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPRSRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/CommonServices/Data/Message/MessageHeader}RequestHeader"/>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/External/IPRS/1.0}GetCitizensDetailsReqData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPRSRequestType", propOrder = {
    "requestHeader",
    "getCitizensDetailsReqData"
})
public class IPRSRequestType {

    @XmlElement(name = "RequestHeader", namespace = "urn://co-opbank.co.ke/CommonServices/Data/Message/MessageHeader", required = true)
    protected RequestHeaderType requestHeader;
    @XmlElement(name = "GetCitizensDetailsReqData", required = true)
    protected GetIPRSDataByGenericID getCitizensDetailsReqData;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeaderType }
     *     
     */
    public RequestHeaderType getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeaderType }
     *     
     */
    public void setRequestHeader(RequestHeaderType value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the getCitizensDetailsReqData property.
     * 
     * @return
     *     possible object is
     *     {@link GetIPRSDataByGenericID }
     *     
     */
    public GetIPRSDataByGenericID getGetCitizensDetailsReqData() {
        return getCitizensDetailsReqData;
    }

    /**
     * Sets the value of the getCitizensDetailsReqData property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetIPRSDataByGenericID }
     *     
     */
    public void setGetCitizensDetailsReqData(GetIPRSDataByGenericID value) {
        this.getCitizensDetailsReqData = value;
    }

}
