
package ke.co.coopbank.banking.external.iprs._1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import ke.co.coopbank.iprsmanager.commondata.GetIPRSDataByGenericID;
import ke.co.coopbank.iprsmanager.commondata.HumanInfoCommon;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ke.co.opbank.co.banking.external.iprs._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetCitizensDetailsRespData_QNAME = new QName("urn://co-opbank.co.ke/Banking/External/IPRS/1.0", "GetCitizensDetailsRespData");
    private final static QName _GetCitizensDetailsReqData_QNAME = new QName("urn://co-opbank.co.ke/Banking/External/IPRS/1.0", "GetCitizensDetailsReqData");
    private final static QName _GetCitizensDetailsRequest_QNAME = new QName("urn://co-opbank.co.ke/Banking/External/IPRS/1.0", "GetCitizensDetailsRequest");
    private final static QName _GetCitizensDetailsResponse_QNAME = new QName("urn://co-opbank.co.ke/Banking/External/IPRS/1.0", "GetCitizensDetailsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ke.co.opbank.co.banking.external.iprs._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IPRSResponseType }
     * 
     */
    public IPRSResponseType createIPRSResponseType() {
        return new IPRSResponseType();
    }

    /**
     * Create an instance of {@link IPRSRequestType }
     * 
     */
    public IPRSRequestType createIPRSRequestType() {
        return new IPRSRequestType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HumanInfoCommon }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/External/IPRS/1.0", name = "GetCitizensDetailsRespData")
    public JAXBElement<HumanInfoCommon> createGetCitizensDetailsRespData(HumanInfoCommon value) {
        return new JAXBElement<HumanInfoCommon>(_GetCitizensDetailsRespData_QNAME, HumanInfoCommon.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIPRSDataByGenericID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/External/IPRS/1.0", name = "GetCitizensDetailsReqData")
    public JAXBElement<GetIPRSDataByGenericID> createGetCitizensDetailsReqData(GetIPRSDataByGenericID value) {
        return new JAXBElement<GetIPRSDataByGenericID>(_GetCitizensDetailsReqData_QNAME, GetIPRSDataByGenericID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IPRSRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/External/IPRS/1.0", name = "GetCitizensDetailsRequest")
    public JAXBElement<IPRSRequestType> createGetCitizensDetailsRequest(IPRSRequestType value) {
        return new JAXBElement<IPRSRequestType>(_GetCitizensDetailsRequest_QNAME, IPRSRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IPRSResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/External/IPRS/1.0", name = "GetCitizensDetailsResponse")
    public JAXBElement<IPRSResponseType> createGetCitizensDetailsResponse(IPRSResponseType value) {
        return new JAXBElement<IPRSResponseType>(_GetCitizensDetailsResponse_QNAME, IPRSResponseType.class, null, value);
    }

}
