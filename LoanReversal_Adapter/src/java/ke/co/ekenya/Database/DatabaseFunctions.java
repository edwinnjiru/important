/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Database;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Map;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 * @author njiru
 */
public class DatabaseFunctions extends DatabaseModel {

    public DatabaseFunctions() {
    }

    public int updateTb(Map<String, String> xmlMap) {
        int update = '0';
        String query = "update TBMESSAGES_EXTERNAL set FIELD39='000', DEBITACCOUNT='" + xmlMap.get("DEBITACCOUNT") + "', CREDITACCOUNT='" + xmlMap.get("CREDITACCOUNT") + "', FIELD12='"+xmlMap.get("12")+"' where ID='" + xmlMap.get("tbmessagesID") + "'";
        try (Connection con = createMvisaConnection();
                Statement stm = con.createStatement();) {
            update = stm.executeUpdate(query);
        } catch (Exception ex) {
            (new ESBLog("Exc(update)", ex)).log();
        }

        return update;
    }

    public int insertTimeout(Map<String, String> xmlMap) {
        int insert = '0';
        String query = "INSERT INTO TBLOANS_TIMEOUT_TRACKER(Loanaccount,Account,Amount,Flexiref,Field37,Field38,Field68 ,Status,STAN)VALUES('" + xmlMap.get("65") + "','" + xmlMap.get("102") + "','" + xmlMap.get("4") + "','" + xmlMap.get("97") + "','" + xmlMap.get("org_refno") + "','" + xmlMap.get("38") + "','" + xmlMap.get("68") + "','0','"+xmlMap.get("11")+"')";
        try (Connection con = createConnection();
                Statement stm = con.createStatement();) {
            insert = stm.executeUpdate(query);
        } catch (Exception ex) {
            (new ESBLog("Exc(update)", ex)).log();
        } finally {
            (new ESBLog("Insert_Query", query)).log();
        }

        return insert;
    }

}
