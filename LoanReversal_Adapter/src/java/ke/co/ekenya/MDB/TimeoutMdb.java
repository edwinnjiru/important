/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.MDB;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.CommonOperations.WorkManagerUtilities;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author njiru
 */
@MessageDriven(mappedName = "jms/LOANS_TIMEOUT_QUEUE", activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
    ,
     @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "durable")
    ,
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
    ,
    @ActivationConfigProperty(propertyName = "connectionFactoryLookup", propertyValue = "jms/ECONNECT_JMSFACTORY")
})
public class TimeoutMdb implements MessageListener {

    public static Map<String, String> Configs = new HashMap();

    @Override
    public void onMessage(Message message) {
        try {
            WorkManagerUtilities.getWorkManager().schedule((new TimeoutProcessor(message, Configs)));
        } catch (Exception ex) {
            (new ESBLog("Exceptions(mdb)", ex)).log();
        }
    }

    @PostConstruct
    public void initialize() {
        (new Utilities()).readPropertyFile(Configs);
        (new ESBLog("ReadConfig", Configs.toString())).log();

    }

    @PreDestroy
    public void release() {
        // Destruction logic
    }

}
