/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.MDB;

import commonj.work.Work;
import java.util.HashMap;
import java.util.Map;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import ke.co.ekenya.CommonOperations.PostGetESBRequest;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author njiru
 */
public class TimeoutProcessor implements Work {

    private Message message;
    Map<String, String> Configs;

    public TimeoutProcessor(Message message, Map<String, String> Configs) {
        this.message = message;
        this.Configs = Configs;
    }

    @Override
    public void run() {
        Map<String, String> requestMap = new HashMap();
        try {
            if (message instanceof ObjectMessage) {
                requestMap = (HashMap) (((ObjectMessage) message).getObject());
                requestMap.put("CorrelationID", message.getJMSCorrelationID());
                (new ESBLog("RawRequests", requestMap.toString())).log();

                doProcess(requestMap);
            }

        } catch (JMSException ex) {
            (new ESBLog("Exceptions(RequestMapper)", ex)).log();
        }
    }

    @Override
    public void release() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isDaemon() {
        return false;
    }

    void doProcess(Map<String, String> requestMap) {
        try {
            if (!"MM".equalsIgnoreCase(requestMap.get("24"))) {
                (new DatabaseFunctions()).updateTb(requestMap);
                /*requestMap.put("0", "0420");
                requestMap.put("MTI", "0420");
                requestMap.remove("39");
                requestMap.put("XREF", requestMap.get("37"));
                Map<String, String> response = (new PostGetESBRequest()).CURLRequest((new HashMap<>(requestMap)));
                (new ESBLog("ResponseMap", response.toString())).log();*/
                int insert=(new DatabaseFunctions()).insertTimeout(requestMap);
                (new ESBLog("DB_Inserts", "Insert: "+insert+" map: "+requestMap.toString())).log();
            } else {
                (new ESBLog("Wallets", requestMap.toString())).log();
            }

        } catch (Exception ex) {
            (new ESBLog("Exceptions(ProcRev)", ex)).log();
        }

    }
}
