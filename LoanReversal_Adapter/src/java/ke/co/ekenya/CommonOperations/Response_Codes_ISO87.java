package ke.co.ekenya.CommonOperations;

public class Response_Codes_ISO87 {

    public static String SUCCESS = "000";
    public static String CR_ACCOUNT_MISSING = "200";
    public static String INVALID_ACCOUNT = "111";
    public static String INSUFFICIENT_FUNDS = "020";
    public static String INSUFFICIENT_POINTS = "021";
    public static String AGENT_DEPOSIT_REQUIRED = "520";
    public static String DR_ACCOUNT_MISSING = "530";
    // public static String DR_ACCOUNT_MISSING = "530";
    public static String DONOT_HONOR = "570";
    public static String NO_MATCHING_RECORD = "580";
    public static String EXPIRED_CODE = "590";
    public static String WRONG_AMOUNT = "600";
    public static String LIMIT_EXCEEDED = "610";
    public static String DAILY_LIMIT_EXCEEDED = "611";
    public static String WRONG_MESSAGE_FORMAT = "612";
    //public static String DECLINE = "100";
    public static String ACCOUNT_RELATED_ISSUES = "101";
    public static String INVALID_MERCHANT = "109";
    public static String MISSING_FUCNTIONALITY = "115";
    public static String NO_CARD_RECORD = "118";
    public static String CLOSED_ACCOUNT = "284";
    public static String DORMANT_ACCOUNT = "528";
    public static String NODR_ACCOUNT = "323";
    public static String STOPPED_ACCOUNT = "172";
    public static String NOCR_ACCOUNT = "325";
    //public static String INVALID_MERCHANT = "109";
    //public static String MISSING_PROCODE = "115";
    public static String TRANSACTION_NOT_ALLOWED = "119";
    public static String SERVICE_UNAVAILABLE = "120";

    public static String AGENT_CODE_ALREADY_USED = "300";
    //public static String AGENT_WRONG_AMOUNT = "301";
    public static String AGENT_WRONG_IDNUMBER = "302";
    public static String AGENT_WRONG_AGENTNUMBER = "303";
    public static String AGENT_WRONG_REF_NUMBER = "304";
    public static String HOST_CONNECTION_DOWN = "912";
    public static String HOST_CONNECTION_TIMEOUT = "980";
    public static String ACKOWLEDGEMENT = "777";
    public static String UNABLE_TOFETCH_LIMITS = "613";
    public static String NEGATIVE_B2C_COMMISSION = "711";
    public static String UNDEFINED_RESPONSECODE = "713";
    public static String ERROR_WHILE_PROCESSING = "713";
    public static String UNMAPPED_RTPS_RESPONSE = "714";
    public static String UNKNOWN_PROCESSING_CODE = "716";
    public static String TRANSACTION_NOT_AUTHORIZED = "713";
    public static String ACCOUNT_BRANCH_MISSING = "712";
    public static String MISSING_SACCO_GL = "712";
    public static String SACCO_BLOCKED_FROM_TRANSACTING = "718";
    public static String INVALID_CUSTOMER_ACCOUNT = "719";
    public static String REPEAT_TRANSACTION = "720";
    public static String MISSING_IMPORTANT_DATA_IN_FIELD = "721";
    public static String MKARO_INSTITUTION_ACCOUNT_MISSING = "712";
    public static String ACCOUNT_LOCKED_FOR_PROCESSING = "722";
    public static String EMPLOYER_CODE_NOT_DEFINED = "723";
    public static String PRODUCT_NOT_DEFINED = "724";
    public static String FAILED_TO_GENERATE_OTP = "725";
    public static String DATABASE_ERROR = "726";
    public static final String MISSING_CUSTOMER_IDENTIFIER = "727";
    public static final String MISSING_TRANSACTION_IDENTIFIER = "728";

}

class MQResponseCodes2 {

    public static String ACCOUNT_CLOSED = "40200284";
    public static String REFFEREAL_REQUIRED_FOR_POSTING = "40007327";
    public static String REFFEREAL_REQUIRED_FOR_ENQUIRY = "40112173";
    public static String UNABLE_TO_PROCESS_TRANSACTION = "40421536";
    public static String ACCOUNT_DORMANT = "40409528";
    public static String NO_DEBITS = "40007323";
    public static String VALUE_DATE_BEFORE_OPEN_DATE = "40243028";
    public static String ACCOUNT_CLOSED_1 = "40000132";
    public static String FROM_DATE_GREATER_TO_DATE = "40000304";
    public static String TRANSACTION_REFERENCE_NOT_FOUND = "40407606";

    public static String CANN0T_PROCESS_TRANSACTION = "40108300";
    public static String ACCOUNT_IS_STOPPED = "40112172";
    public static String NO_CREDITS = "40007325";
    public static String SUPERVISOR_AUTHORIZATION_REQUIRED = "40205109";
    public static String NO_DEBITS_ALLOWED = "40007322";

    public static String TRANSACTION_ALREADY_POSTED = "40422049";
    public static String NO_DEBITS_ALLOWED_1 = "40180194";
    public static String ACCOUNT_DOES_NOT_EXIST = "20020000";
    public static String FUND_SETTLEMENT_ACCOUNT = "20201005";
    public static String UNEXPECTED_ERROR = "40000127";

    public static String REFFERAL_REQUIRED_FOR_POSTING = "40007319";
    public static String REFFERAL_REQUIRED_FOR_POSTING_ENQUIRY = "40112171";
    public static String CUSTOMER_IS_NOT_ACTIVE = "40411056";
    public static String ACCOUNT_CANNOT_BE_FOUND = "40000126";
    public static String ACCOUNT_STOPPED = "40007321";

    public static String INSUFFICIENT_AVAILABLE_BALANCE = "40507020";
    public static String NO_CREDIT_ALLOWED_ACCOUNT = "40180193";

}

class FlexEbank_Operations {

    public static String FLEX2FLEX = "01";
    public static String FLEX2MINI = "02";
    public static String FLEXDEPOSIT = "03";
    public static String MINI2FLEX = "04";
    public static String FLEXCHARGES = "05";
    //public static String DONOT_HONOR = "57";
    //public static String NO_MATCHING_RECORD = "58";
    //public static String EXPIRED_CODE = "59";
    //public static String WRONG_AMOUNT = "60";
    //public static String LIMIT_EXCEEDED = "61";

}

