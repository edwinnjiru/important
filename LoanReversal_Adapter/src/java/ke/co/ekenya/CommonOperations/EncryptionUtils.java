package ke.co.ekenya.CommonOperations;

import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author hmungai
 */
public class EncryptionUtils {

    private String key = "@123COOP321#";
    private final String HASHING_ALG = "MD5";
    private final String ENCODING = "utf-8";
    private final String ENCRYPTION_ALG = "DESede";

    public EncryptionUtils() {
    }

    public String encrypt(String plainPass) throws Exception {
        MessageDigest md = MessageDigest.getInstance(HASHING_ALG);
        byte[] digestOfPassword = md.digest(key.getBytes(ENCODING));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        SecretKey key = new SecretKeySpec(keyBytes, ENCRYPTION_ALG);
        Cipher cipher = Cipher.getInstance(ENCRYPTION_ALG);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] plainTextBytes = plainPass.getBytes(ENCODING);
        byte[] buf = cipher.doFinal(plainTextBytes);
        byte[] base64Bytes = java.util.Base64.getEncoder().encode(buf);
        String base64EncryptedString = new String(base64Bytes);
        return base64EncryptedString;
    }

    public String decrypt(String encryptedPass) throws Exception {
        byte[] message = java.util.Base64.getDecoder().decode(encryptedPass.getBytes(ENCODING));
        MessageDigest md = MessageDigest.getInstance(HASHING_ALG);
        byte[] digestOfPassword = md.digest(key.getBytes(ENCODING));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        SecretKey key = new SecretKeySpec(keyBytes, ENCRYPTION_ALG);
        Cipher decipher = Cipher.getInstance(ENCRYPTION_ALG);
        decipher.init(Cipher.DECRYPT_MODE, key);
        byte[] plainText = decipher.doFinal(message);
        return new String(plainText, ENCODING);
    }
}
