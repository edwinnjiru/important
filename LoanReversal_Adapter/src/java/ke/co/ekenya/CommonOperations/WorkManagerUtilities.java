package ke.co.ekenya.CommonOperations;

import javax.naming.InitialContext;

import commonj.work.WorkManager;
import ke.co.ekenya.LogEngine.ESBLog;


/**
 * 
 * @author Ngari Josh
 */
public class WorkManagerUtilities {

    /**
     * It fetches a named thread pool from weblogic work managers
     *
     * @return workManager - The Weblogic ThreadPool
     */
    public static WorkManager getWorkManager() {
        WorkManager workManager = null;
        if (workManager == null) {
            try {
                InitialContext ctx = new InitialContext();
                String jndiName = "java:global/wm/default";
                workManager = (WorkManager) ctx.lookup(jndiName);
            } catch (Exception ex) {
            	(new ESBLog("Exceptions(WorkManager)", ex)).log();
            }
        }
        return workManager;
    }
}
