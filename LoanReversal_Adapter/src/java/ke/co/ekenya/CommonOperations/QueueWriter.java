package ke.co.ekenya.CommonOperations;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.MDB.TimeoutMdb;

/**
 * QueueWriter - writes messages to weblogic Queues
 *
 * @author Edwin Njiru
 * @version 1.0
 * @since 1.0
 */
public class QueueWriter {

    private QueueConnection qcon;
    private QueueSession qsession;
    private QueueSender qsender;
    // private Queue queue;
    private TextMessage msg;
    private ObjectMessage objmsg;
    @SuppressWarnings({"unchecked", "rawtypes"})
    Map<String, String> configs = new HashMap();

    /**
     * Default constructor.
     */
    public QueueWriter() {
    }

    public QueueWriter(String QUEUE) {
        configs = TimeoutMdb.Configs;
        /*if (configs.isEmpty()) {
            KplcMdb.readPropertyFile(configs);
        }*/
        InitialContext ic = getInitialContext();
        init(ic, QUEUE);
    }

    private void init(Context ctx, String queueName) {
        //QueueConnectionFactory qconFactory;
        //Queue queue;
        // MapMessage mapmsg;
        try {
            QueueConnectionFactory qconFactory = (QueueConnectionFactory) ctx.lookup("jms/ECONNECT_JMSFACTORY");
            qcon = qconFactory.createQueueConnection();
            qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = (Queue) ctx.lookup(queueName);
            qsender = qsession.createSender(queue);
            msg = qsession.createTextMessage();
            objmsg = qsession.createObjectMessage();
            // mapmsg = qsession.createMapMessage();
            qcon.start();
        } catch (Exception e) {
            ESBLog el = new ESBLog("InitException", e.getMessage());
            el.log();
        }
    }

    private InitialContext getInitialContext() {
        Hashtable env = new Hashtable();
        InitialContext ic = null;
        String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";
        try {
            env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
            //env.put(Context.PROVIDER_URL, configs.get("PROVIDER_URL").trim());
            env.put(Context.PROVIDER_URL, "t3://172.16.4.43:8002");
            //env.put(Context.PROVIDER_URL, "t3://localhost:7001");
            ic = new InitialContext(env);
        } catch (NamingException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
        }
        return ic;
    }

    public void close() {
        try {
            qsender.close();
            qsession.close();
            qcon.close();
        } catch (JMSException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
        }
    }

    public boolean send(String message, String CorrelationID) {
        boolean sent = false;
        try {
            msg.setText(message);
            qsender.send(msg);
            sent = true;
        } catch (JMSException e) {
            ESBLog el = new ESBLog("SendtoQueueException", e.getMessage());
            el.log();
        }
        close();
        return sent;
    }

    public boolean sendObject(Map message, String CorrelationID, String biller) {
        boolean sent = false;
        try {

            objmsg.setStringProperty("biller", biller);
            objmsg.setJMSCorrelationID(CorrelationID);
            objmsg.setObject((HashMap) message);
            qsender.send(objmsg);
            sent = true;
            // System.out.println("Sent Successfully");
        } catch (JMSException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));

        }
        close();
        return sent;
    }

    public boolean sendObject(Map message, String CorrelationID) {
        boolean sent = false;
        try {
            if (message.containsKey("NotificationType")) {
                objmsg.setStringProperty("NotificationType", message.get("NotificationType").toString());
            }
            objmsg.setStringProperty("biller", "KPLCTOKEN");
            objmsg.setJMSCorrelationID(CorrelationID);
            objmsg.setObject((HashMap) message);
            qsender.send(objmsg);
            sent = true;
        } catch (JMSException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));

        }
        close();
        return sent;
    }

}
