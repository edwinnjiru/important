package ke.co.coopbank.co.bs.safaricom.stkpushquery;

import ke.co.coopbank.co.commonservices.data.message.messageheader.RequestHeaderType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuerySTKRequestPayload complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="QuerySTKRequestPayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Header" type="{urn://co-opbank.co.ke/CommonServices/Data/Message/MessageHeader}RequestHeaderType"/&gt;
 *         &lt;element name="Body" type="{urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery}QuerySTKRequestBody"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySTKRequestPayload", propOrder = {
        "header",
        "body"
})
public class QuerySTKRequestPayload {

    @XmlElement(name = "Header", required = true)
    protected RequestHeaderType header;
    @XmlElement(name = "Body", required = true)
    protected QuerySTKRequestBody body;

    /**
     * Gets the value of the header property.
     *
     * @return possible object is
     * {@link RequestHeaderType }
     */
    public RequestHeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     *
     * @param value allowed object is
     *              {@link RequestHeaderType }
     */
    public void setHeader(RequestHeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the body property.
     *
     * @return possible object is
     * {@link QuerySTKRequestBody }
     */
    public QuerySTKRequestBody getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     *
     * @param value allowed object is
     *              {@link QuerySTKRequestBody }
     */
    public void setBody(QuerySTKRequestBody value) {
        this.body = value;
    }

}
