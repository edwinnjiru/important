package ke.co.coopbank.co.bs.safaricom.stkpushquery;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuerySTKResponseBody complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="QuerySTKResponseBody"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CheckoutRequestID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ResultDesc" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ResponseDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ResultCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySTKResponseBody", propOrder = {
        "checkoutRequestID",
        "responseCode",
        "resultDesc",
        "responseDescription",
        "resultCode"
})
public class QuerySTKResponseBody {

    @XmlElement(name = "CheckoutRequestID", required = true)
    protected String checkoutRequestID;
    @XmlElement(name = "ResponseCode", required = true)
    protected String responseCode;
    @XmlElement(name = "ResultDesc", required = true)
    protected String resultDesc;
    @XmlElement(name = "ResponseDescription", required = true)
    protected String responseDescription;
    @XmlElement(name = "ResultCode", required = true)
    protected String resultCode;

    /**
     * Gets the value of the checkoutRequestID property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCheckoutRequestID() {
        return checkoutRequestID;
    }

    /**
     * Sets the value of the checkoutRequestID property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCheckoutRequestID(String value) {
        this.checkoutRequestID = value;
    }

    /**
     * Gets the value of the responseCode property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the resultDesc property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getResultDesc() {
        return resultDesc;
    }

    /**
     * Sets the value of the resultDesc property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setResultDesc(String value) {
        this.resultDesc = value;
    }

    /**
     * Gets the value of the responseDescription property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getResponseDescription() {
        return responseDescription;
    }

    /**
     * Sets the value of the responseDescription property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setResponseDescription(String value) {
        this.responseDescription = value;
    }

    /**
     * Gets the value of the resultCode property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getResultCode() {
        return resultCode;
    }

    /**
     * Sets the value of the resultCode property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setResultCode(String value) {
        this.resultCode = value;
    }

}
