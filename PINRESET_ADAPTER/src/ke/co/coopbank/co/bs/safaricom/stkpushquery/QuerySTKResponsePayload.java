package ke.co.coopbank.co.bs.safaricom.stkpushquery;

import ke.co.coopbank.co.commonservices.data.message.messageheader.ResponseHeaderType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuerySTKResponsePayload complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="QuerySTKResponsePayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Header" type="{urn://co-opbank.co.ke/CommonServices/Data/Message/MessageHeader}ResponseHeaderType"/&gt;
 *         &lt;element name="Body" type="{urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery}QuerySTKResponseBody"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySTKResponsePayload", propOrder = {
        "header",
        "body"
})
public class QuerySTKResponsePayload {

    @XmlElement(name = "Header", required = true)
    protected ResponseHeaderType header;
    @XmlElement(name = "Body", required = true)
    protected QuerySTKResponseBody body;

    /**
     * Gets the value of the header property.
     *
     * @return possible object is
     * {@link ResponseHeaderType }
     */
    public ResponseHeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     *
     * @param value allowed object is
     *              {@link ResponseHeaderType }
     */
    public void setHeader(ResponseHeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the body property.
     *
     * @return possible object is
     * {@link QuerySTKResponseBody }
     */
    public QuerySTKResponseBody getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     *
     * @param value allowed object is
     *              {@link QuerySTKResponseBody }
     */
    public void setBody(QuerySTKResponseBody value) {
        this.body = value;
    }

}
