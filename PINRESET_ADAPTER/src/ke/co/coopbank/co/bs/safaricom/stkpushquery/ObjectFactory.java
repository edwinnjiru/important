package ke.co.coopbank.co.bs.safaricom.stkpushquery;

import ke.co.coopbank.co.commonservices.data.message.messageheader.RequestHeaderType;
import ke.co.coopbank.co.commonservices.data.message.messageheader.ResponseHeaderType;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ke.co.opbank.co.bs.safaricom.stkpushquery package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RequestHeader_QNAME = new QName("urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", "RequestHeader");
    private final static QName _ResponseHeader_QNAME = new QName("urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", "ResponseHeader");
    private final static QName _QuerySTKRequestBody_QNAME = new QName("urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", "QuerySTKRequestBody");
    private final static QName _QuerySTKResponseBody_QNAME = new QName("urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", "QuerySTKResponseBody");
    private final static QName _QuerySTKRequestPayload_QNAME = new QName("urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", "QuerySTKRequestPayload");
    private final static QName _QuerySTKResponsePayload_QNAME = new QName("urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", "QuerySTKResponsePayload");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ke.co.opbank.co.bs.safaricom.stkpushquery
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QuerySTKRequestBody }
     */
    public QuerySTKRequestBody createQuerySTKRequestBody() {
        return new QuerySTKRequestBody();
    }

    /**
     * Create an instance of {@link QuerySTKResponseBody }
     */
    public QuerySTKResponseBody createQuerySTKResponseBody() {
        return new QuerySTKResponseBody();
    }

    /**
     * Create an instance of {@link QuerySTKRequestPayload }
     */
    public QuerySTKRequestPayload createQuerySTKRequestPayload() {
        return new QuerySTKRequestPayload();
    }

    /**
     * Create an instance of {@link QuerySTKResponsePayload }
     */
    public QuerySTKResponsePayload createQuerySTKResponsePayload() {
        return new QuerySTKResponsePayload();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestHeaderType }{@code >}}
     */
    @XmlElementDecl(namespace = "urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", name = "RequestHeader")
    public JAXBElement<RequestHeaderType> createRequestHeader(RequestHeaderType value) {
        return new JAXBElement<RequestHeaderType>(_RequestHeader_QNAME, RequestHeaderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseHeaderType }{@code >}}
     */
    @XmlElementDecl(namespace = "urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", name = "ResponseHeader")
    public JAXBElement<ResponseHeaderType> createResponseHeader(ResponseHeaderType value) {
        return new JAXBElement<ResponseHeaderType>(_ResponseHeader_QNAME, ResponseHeaderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuerySTKRequestBody }{@code >}}
     */
    @XmlElementDecl(namespace = "urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", name = "QuerySTKRequestBody")
    public JAXBElement<QuerySTKRequestBody> createQuerySTKRequestBody(QuerySTKRequestBody value) {
        return new JAXBElement<QuerySTKRequestBody>(_QuerySTKRequestBody_QNAME, QuerySTKRequestBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuerySTKResponseBody }{@code >}}
     */
    @XmlElementDecl(namespace = "urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", name = "QuerySTKResponseBody")
    public JAXBElement<QuerySTKResponseBody> createQuerySTKResponseBody(QuerySTKResponseBody value) {
        return new JAXBElement<QuerySTKResponseBody>(_QuerySTKResponseBody_QNAME, QuerySTKResponseBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuerySTKRequestPayload }{@code >}}
     */
    @XmlElementDecl(namespace = "urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", name = "QuerySTKRequestPayload")
    public JAXBElement<QuerySTKRequestPayload> createQuerySTKRequestPayload(QuerySTKRequestPayload value) {
        return new JAXBElement<QuerySTKRequestPayload>(_QuerySTKRequestPayload_QNAME, QuerySTKRequestPayload.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuerySTKResponsePayload }{@code >}}
     */
    @XmlElementDecl(namespace = "urn://www.co-opbank.co.ke/BS/Safaricom/STKPushQuery", name = "QuerySTKResponsePayload")
    public JAXBElement<QuerySTKResponsePayload> createQuerySTKResponsePayload(QuerySTKResponsePayload value) {
        return new JAXBElement<QuerySTKResponsePayload>(_QuerySTKResponsePayload_QNAME, QuerySTKResponsePayload.class, null, value);
    }

}
