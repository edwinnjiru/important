package ke.co.coopbank.stkpayments;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InputPayload complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InputPayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="PhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MerchantRequestID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CallBackURL" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TransactionDesc" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Parameter"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ReferenceItem"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InputPayload", propOrder = {
        "amount",
        "phoneNumber",
        "merchantRequestID",
        "callBackURL",
        "transactionDesc",
        "parameter"
})
public class InputPayload {

    @XmlElement(name = "Amount")
    protected double amount;
    @XmlElement(name = "PhoneNumber", required = true)
    protected String phoneNumber;
    @XmlElement(name = "MerchantRequestID", required = true)
    protected String merchantRequestID;
    @XmlElement(name = "CallBackURL", required = true)
    protected String callBackURL;
    @XmlElement(name = "TransactionDesc", required = true)
    protected String transactionDesc;
    @XmlElement(name = "Parameter", required = true)
    protected InputPayload.Parameter parameter;

    /**
     * Gets the value of the amount property.
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     */
    public void setAmount(double value) {
        this.amount = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the merchantRequestID property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMerchantRequestID() {
        return merchantRequestID;
    }

    /**
     * Sets the value of the merchantRequestID property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMerchantRequestID(String value) {
        this.merchantRequestID = value;
    }

    /**
     * Gets the value of the callBackURL property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCallBackURL() {
        return callBackURL;
    }

    /**
     * Sets the value of the callBackURL property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCallBackURL(String value) {
        this.callBackURL = value;
    }

    /**
     * Gets the value of the transactionDesc property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTransactionDesc() {
        return transactionDesc;
    }

    /**
     * Sets the value of the transactionDesc property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTransactionDesc(String value) {
        this.transactionDesc = value;
    }

    /**
     * Gets the value of the parameter property.
     *
     * @return possible object is
     * {@link InputPayload.Parameter }
     */
    public InputPayload.Parameter getParameter() {
        return parameter;
    }

    /**
     * Sets the value of the parameter property.
     *
     * @param value allowed object is
     *              {@link InputPayload.Parameter }
     */
    public void setParameter(InputPayload.Parameter value) {
        this.parameter = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ReferenceItem"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "referenceItem"
    })
    public static class Parameter {

        @XmlElement(name = "ReferenceItem", required = true)
        protected InputPayload.Parameter.ReferenceItem referenceItem;

        /**
         * Gets the value of the referenceItem property.
         *
         * @return possible object is
         * {@link InputPayload.Parameter.ReferenceItem }
         */
        public InputPayload.Parameter.ReferenceItem getReferenceItem() {
            return referenceItem;
        }

        /**
         * Sets the value of the referenceItem property.
         *
         * @param value allowed object is
         *              {@link InputPayload.Parameter.ReferenceItem }
         */
        public void setReferenceItem(InputPayload.Parameter.ReferenceItem value) {
            this.referenceItem = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "key",
                "value"
        })
        public static class ReferenceItem {

            @XmlElement(name = "Key", required = true)
            protected String key;
            @XmlElement(name = "Value", required = true)
            protected String value;

            /**
             * Gets the value of the key property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getKey() {
                return key;
            }

            /**
             * Sets the value of the key property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setKey(String value) {
                this.key = value;
            }

            /**
             * Gets the value of the value property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setValue(String value) {
                this.value = value;
            }

        }

    }

}
