package ke.co.coopbank.stkpayments;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutputPayload complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OutputPayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ResponseCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ResponseDesc" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CheckoutRequestID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MerchantRequestID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustomerMessage" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutputPayload", propOrder = {
        "responseCode",
        "responseDesc",
        "checkoutRequestID",
        "merchantRequestID",
        "customerMessage"
})
public class OutputPayload {

    @XmlElement(name = "ResponseCode", required = true)
    protected String responseCode;
    @XmlElement(name = "ResponseDesc", required = true)
    protected String responseDesc;
    @XmlElement(name = "CheckoutRequestID", required = true)
    protected String checkoutRequestID;
    @XmlElement(name = "MerchantRequestID", required = true)
    protected String merchantRequestID;
    @XmlElement(name = "CustomerMessage", required = true)
    protected String customerMessage;

    /**
     * Gets the value of the responseCode property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the responseDesc property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getResponseDesc() {
        return responseDesc;
    }

    /**
     * Sets the value of the responseDesc property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setResponseDesc(String value) {
        this.responseDesc = value;
    }

    /**
     * Gets the value of the checkoutRequestID property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCheckoutRequestID() {
        return checkoutRequestID;
    }

    /**
     * Sets the value of the checkoutRequestID property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCheckoutRequestID(String value) {
        this.checkoutRequestID = value;
    }

    /**
     * Gets the value of the merchantRequestID property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMerchantRequestID() {
        return merchantRequestID;
    }

    /**
     * Sets the value of the merchantRequestID property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMerchantRequestID(String value) {
        this.merchantRequestID = value;
    }

    /**
     * Gets the value of the customerMessage property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCustomerMessage() {
        return customerMessage;
    }

    /**
     * Sets the value of the customerMessage property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCustomerMessage(String value) {
        this.customerMessage = value;
    }

}
