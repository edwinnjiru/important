package ke.co.coopbank.stkpayments;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.example.stkpayments package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OutputPayload_QNAME = new QName("http://www.example.org/STKPayments", "OutputPayload");
    private final static QName _InputPayload_QNAME = new QName("http://www.example.org/STKPayments", "InputPayload");
    private final static QName _StepBit_QNAME = new QName("http://www.example.org/STKPayments", "StepBit");
    private final static QName _BackendResponse_QNAME = new QName("http://www.example.org/STKPayments", "BackendResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.example.stkpayments
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InputPayload }
     */
    public InputPayload createInputPayload() {
        return new InputPayload();
    }

    /**
     * Create an instance of {@link InputPayload.Parameter }
     */
    public InputPayload.Parameter createInputPayloadParameter() {
        return new InputPayload.Parameter();
    }

    /**
     * Create an instance of {@link OutputPayload }
     */
    public OutputPayload createOutputPayload() {
        return new OutputPayload();
    }

    /**
     * Create an instance of {@link STKPaymentsBackendResponse }
     */
    public STKPaymentsBackendResponse createSTKPaymentsBackendResponse() {
        return new STKPaymentsBackendResponse();
    }

    /**
     * Create an instance of {@link InputPayload.Parameter.ReferenceItem }
     */
    public InputPayload.Parameter.ReferenceItem createInputPayloadParameterReferenceItem() {
        return new InputPayload.Parameter.ReferenceItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutputPayload }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.example.org/STKPayments", name = "OutputPayload")
    public JAXBElement<OutputPayload> createOutputPayload(OutputPayload value) {
        return new JAXBElement<OutputPayload>(_OutputPayload_QNAME, OutputPayload.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InputPayload }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.example.org/STKPayments", name = "InputPayload")
    public JAXBElement<InputPayload> createInputPayload(InputPayload value) {
        return new JAXBElement<InputPayload>(_InputPayload_QNAME, InputPayload.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.example.org/STKPayments", name = "StepBit")
    public JAXBElement<String> createStepBit(String value) {
        return new JAXBElement<String>(_StepBit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link STKPaymentsBackendResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.example.org/STKPayments", name = "BackendResponse")
    public JAXBElement<STKPaymentsBackendResponse> createBackendResponse(STKPaymentsBackendResponse value) {
        return new JAXBElement<STKPaymentsBackendResponse>(_BackendResponse_QNAME, STKPaymentsBackendResponse.class, null, value);
    }

}
