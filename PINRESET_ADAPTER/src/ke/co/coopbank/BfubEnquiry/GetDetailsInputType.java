package ke.co.coopbank.BfubEnquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDetailsInputType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getDetailsInputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0}Account" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDetailsInputType", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/Account/GetDetails/1.1/Account.getDetails", propOrder = {
        "account"
})
public class GetDetailsInputType {

    @XmlElement(name = "Account", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0")
    protected AccountType account;

    /**
     * Gets the value of the account property.
     *
     * @return possible object is
     * {@link AccountType }
     */
    public AccountType getAccount() {
        return account;
    }

    /**
     * Sets the value of the account property.
     *
     * @param value allowed object is
     *              {@link AccountType }
     */
    public void setAccount(AccountType value) {
        this.account = value;
    }

}
