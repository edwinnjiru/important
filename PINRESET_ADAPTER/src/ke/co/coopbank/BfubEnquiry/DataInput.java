package ke.co.coopbank.BfubEnquiry;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Account/DataModel/Account/GetDetails/1.1/Account.getDetails}getDetailsInput"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "getDetailsInput"
})
@XmlRootElement(name = "DataInput", namespace = "urn://co-opbank.co.ke/Banking/Account/Service/Account/GetDetails/1.1/DataIO")
public class DataInput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/Account/GetDetails/1.1/Account.getDetails", required = true)
    protected GetDetailsInputType getDetailsInput;

    /**
     * Gets the value of the getDetailsInput property.
     *
     * @return possible object is
     * {@link GetDetailsInputType }
     */
    public GetDetailsInputType getGetDetailsInput() {
        return getDetailsInput;
    }

    /**
     * Sets the value of the getDetailsInput property.
     *
     * @param value allowed object is
     *              {@link GetDetailsInputType }
     */
    public void setGetDetailsInput(GetDetailsInputType value) {
        this.getDetailsInput = value;
    }

}
