package ke.co.coopbank.BfubEnquiry;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Account/DataModel/Account/GetDetails/1.1/Account.getDetails}getDetailsOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "getDetailsOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Account/Service/Account/GetDetails/1.1/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/Account/GetDetails/1.1/Account.getDetails")
    protected GetDetailsOutputType getDetailsOutput;

    /**
     * Gets the value of the getDetailsOutput property.
     *
     * @return possible object is
     * {@link GetDetailsOutputType }
     */
    public GetDetailsOutputType getGetDetailsOutput() {
        return getDetailsOutput;
    }

    /**
     * Sets the value of the getDetailsOutput property.
     *
     * @param value allowed object is
     *              {@link GetDetailsOutputType }
     */
    public void setGetDetailsOutput(GetDetailsOutputType value) {
        this.getDetailsOutput = value;
    }

}
