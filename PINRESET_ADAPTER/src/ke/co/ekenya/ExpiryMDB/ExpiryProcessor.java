package ke.co.ekenya.ExpiryMDB;

import commonj.work.Work;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.ToSOA.QuerySOA;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ExpiryProcessor implements Work {

    private Message message;

    public ExpiryProcessor(Message message) {
        this.message = message;
    }

    @Override
    public void run() {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        try {
            if (message instanceof ObjectMessage) {
                //Log ESB request

                Map<String, String> requestMap = (HashMap) (((ObjectMessage) message).getObject());
                requestMap.put("CorrelationID", message.getJMSCorrelationID());
                if(!requestMap.containsKey("OTP")){
                    requestMap.put("Queryat", LocalDateTime.now().format(formatter));
                    Map<String, String> queryMap=new HashMap(requestMap);
                    (new QuerySOA(queryMap)).process();
                    (new ESBLog("CallBackMissing", "Map :" + requestMap.toString())).log();
                }else {
                    requestMap.put("Expiredat", LocalDateTime.now().format(formatter));
                    (new DatabaseFunctions()).pinreset(requestMap.get("OTP"), requestMap.get("phonenumber"), "EXPIRED");
                    (new ESBLog("ExpiredOTP", "RequestMap to process: " + requestMap.toString())).log();
                }

            }

        } catch (JMSException e) {
            (new ESBLog("Exceptions(ExProc)", e)).log();
        }
    }

    @Override
    public void release() {
    }

    @Override
    public boolean isDaemon() {
        return false;
    }
}
