package ke.co.ekenya.ExpiryMDB;

import commonj.work.WorkException;
import commonj.work.WorkManager;
import ke.co.ekenya.CommonOperations.WorkManagerUtilities;
import ke.co.ekenya.LogEngine.ESBLog;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(name = "STKEXPIRYEJB", mappedName = "jms/PINRESET_EXPIRYQUEUE", activationConfig = {
        @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "durable")
        ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
        ,
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
        ,
        @ActivationConfigProperty(propertyName = "connectionFactoryLookup", propertyValue = "jms/ECONNECT_JMSFACTORY")
})
public class ExpiryBean implements MessageListener {
    public ExpiryBean() {
    }

    @Override
    public void onMessage(Message message) {
        WorkManager wm = WorkManagerUtilities.getWorkManager();
        ExpiryProcessor processor = new ExpiryProcessor(message);
        try {
            wm.schedule(processor);
        } catch (WorkException | IllegalArgumentException ex) {
            (new ESBLog("Exception(MDB)", ex)).log();
        }
    }//STKExpiryBeanEJB
}
