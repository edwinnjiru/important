package ke.co.ekenya.Api;


import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

// The Java class will be hosted at the URI path "/stkpush"
@Path("/stkpush")
public class StkpushApi {

    public static final Map<String, String> CONFIGS = new HashMap();

    static {
        (new Utilities()).readPropertyFile(CONFIGS);
        (new ESBLog("readconfigs", CONFIGS.toString())).log();
    }

    // The Java method will process HTTP GET requests
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getClichedMessage() {
        return "Service is currently available";
    }


    /**
     * Post method for receiving json from channels and acknowledging the same
     *
     * @param content
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String process(String content) {
        String response = "";
        try {
            response = (new RequestProcessor(content, CONFIGS)).process();
            ESBLog es = new ESBLog("STK_Rqst&Resp", (new StringBuilder("Request: ")).append(content).append(" Response: ").append(response).toString());
            es.log();

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es2 = new ESBLog("InitException", "Error: " + sw.toString());
            es2.log();
        }
        return response;
    }

}
