package ke.co.ekenya.Api;

import javax.ws.rs.core.Application;
import java.util.Set;

/**
 *
 */
@javax.ws.rs.ApplicationPath("coop")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(ke.co.ekenya.Api.StkpushApi.class);
        resources.add(ke.co.ekenya.Stkcallback.StkCallBack.class);
        resources.add(ke.co.ekenya.AccountEnquiries.AccountApi.class);
    }


}
