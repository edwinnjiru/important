package ke.co.ekenya.Api;

import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.ToSOA.InitStkPush;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RequestProcessor {

    String request;
    Map<String, String> configs;


    public RequestProcessor(String request, Map<String, String> configs) {
        this.request = request;
        this.configs = configs;
    }

    public String process() {
        String response = "";
        Map<String, String> requestMap = new HashMap();
        Map<String, String> responseMap = new HashMap();
        try {
            JSONObject requestObject = new JSONObject(request);
            requestMap = (new Utilities()).parseJSON(requestObject, requestMap);
            if (validatefields(requestMap, responseMap)) {
                responseMap = (new InitStkPush(configs, requestMap)).doProcess();
            }
            JSONObject responseObject = new JSONObject(responseMap);
            response = responseObject.toString();

        } catch (Exception ex) {
            (new ESBLog("Exception(Processor)", ex)).log();
        }
        return response;
    }

    public boolean validatefields(Map<String, String> requestMap, Map<String, String> responseMap) {
        boolean validity = true;
        try {
            if (!requestMap.containsKey("phonenumber")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Missing phonenumber tag");
                responseMap.put("ResultCode", "203");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

            if ("".equalsIgnoreCase(requestMap.get("phonenumber"))) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Empty phonenumber tag");
                responseMap.put("ResultCode", "204");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

            if ((requestMap.get("phonenumber").trim().length() != 12) && (requestMap.get("phonenumber").trim().length() != 10)) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Phonenumber length validation failed");
                responseMap.put("ResultCode", "205");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

            if (!requestMap.containsKey("pinreset")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Missing pinreset tag");
                responseMap.put("ResultCode", "207");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

            if (!(requestMap.get("pinreset").trim().equalsIgnoreCase("0")) && !(requestMap.get("pinreset").trim().equalsIgnoreCase("1"))) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Invalid value of pinreset tag. Put 1 for pin reset, 0 for normal stk push");
                responseMap.put("ResultCode", "208");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

            if (!requestMap.containsKey("amount")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Missing amount tag");
                responseMap.put("ResultCode", "209");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

            if ("".equalsIgnoreCase(requestMap.get("amount")) && "0".equalsIgnoreCase(requestMap.get("amount"))) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Invalid amount");
                responseMap.put("ResultCode", "210");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

            if (!requestMap.containsKey("ref")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Missing ref tag");
                responseMap.put("ResultCode", "235");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

            if ("".equalsIgnoreCase(requestMap.get("ref"))) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Empty ref tag");
                responseMap.put("ResultCode", "236");
                responseMap.put("valid", "false");
                validity = false;
                return validity;
            }

        } catch (Exception ex) {
            (new ESBLog("Exception(Validate)", ex)).log();
            responseMap.put("Error", "true");
            responseMap.put("Message", "Exception occured");
            responseMap.put("ResultCode", "200");
            responseMap.put("valid", "false");
            validity = false;
        }
        return validity;
    }
}
