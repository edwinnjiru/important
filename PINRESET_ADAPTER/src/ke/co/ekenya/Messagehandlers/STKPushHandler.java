package ke.co.ekenya.Messagehandlers;

import ke.co.ekenya.LogEngine.ESBLog;

import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * @author kkarue
 */
public class STKPushHandler implements SOAPHandler<SOAPMessageContext> {

    public String myXML;
    private String syscode;
    private Map<String, String> Configs;

    public STKPushHandler() {
    }

    public STKPushHandler(Map<String, String> Configs) {
        this.Configs = Configs;
    }

    public static String GenerateRandomNumber(int charLength) {
        return String.valueOf(charLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1)
                + (int) Math.pow(10, charLength - 1));
    }

    public String getMyXML() {
        return myXML;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext smc) {
        String msgid = GenerateRandomNumber(10);

        Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (outboundProperty) {
            try {
                SOAPMessage message = smc.getMessage();
                SOAPEnvelope env = smc.getMessage().getSOAPPart().getEnvelope();

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                message.writeTo(out);
                String strMsg = new String(out.toByteArray());
                //System.out.println(strMsg);

                ESBLog el = new ESBLog("STKPUSH_REQUEST_TO_SOA", msgid + " : " + strMsg);
                el.log();
            } catch (SOAPException | IOException e) {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog(sw.toString());
                el.log();
            }
        } else {
            try {
                SOAPMessage message = smc.getMessage();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                message.writeTo(out);
                String strMsg = new String(out.toByteArray());
                myXML = strMsg;

                ESBLog el = new ESBLog("STKPUSH_RESPONSE_FROM_SOA", msgid + " : " + strMsg);
                el.log();
            } catch (SOAPException | IOException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog(sw.toString());
                el.log();
            }
        }
        return outboundProperty;
    }

    @Override
    public Set getHeaders() {
        //throw new UnsupportedOperationException("Not supported yet.");
        return null;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        //throw new UnsupportedOperationException("Not supported yet.");
        return true;
    }

    @Override
    public void close(MessageContext context) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }
}
