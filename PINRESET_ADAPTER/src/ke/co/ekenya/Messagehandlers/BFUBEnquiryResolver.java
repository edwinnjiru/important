/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Messagehandlers;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kkarue
 */
public class BFUBEnquiryResolver implements HandlerResolver {

    BFUBEnquiryHandler ch = new BFUBEnquiryHandler();

    public BFUBEnquiryResolver(BFUBEnquiryHandler ch) {
        this.ch = ch;
    }


    public List<Handler> getHandlerChain(PortInfo portInfo) {
        List<Handler> handlerChain = new ArrayList<Handler>();
        handlerChain.add(ch);

        return handlerChain;
    }
}
