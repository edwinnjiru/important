package ke.co.ekenya.CommonOperations;

import ke.co.ekenya.Api.StkpushApi;
import ke.co.ekenya.LogEngine.ESBLog;

import javax.jms.Message;
import javax.jms.ObjectMessage;
import java.util.HashMap;


/**
 * @author kkarue
 */
public class JMSUtilities {

    public HashMap getWeblogicMessageFromUDQueue(String JMSCorrelationID) {
        Message msg = null;
        Object ResponseMessage = null;
        HashMap fields = new HashMap();
        int CLUSTER_SERVERS = Integer.parseInt(StkpushApi.CONFIGS.get("CLUSTER_SERVERS"));
        try {
            DistributedQueueBrowser udqb = new DistributedQueueBrowser();

            String url = "";
            String server = "";

            //edu interchange
            String QuenName = "@jms.PINRESET_HOLDING_QUEUE";
            String QUEUE = "";
            for (int i = 1; i <= CLUSTER_SERVERS; i++) {
                url = StkpushApi.CONFIGS.get("MACHINE" + i);
                server = StkpushApi.CONFIGS.get("SERVER" + i);
                QUEUE = server + QuenName;
                if (url != null || server != null) {
                    msg = udqb.browseWebLogicQueue(JMSCorrelationID, url, QUEUE);
                    if (msg instanceof ObjectMessage) {
                        ResponseMessage = ((ObjectMessage) msg).getObject();
                        fields = (HashMap) ResponseMessage;
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            ESBLog el = new ESBLog("Exception(QueueBrowser", ex);
            el.log();
        }
        return fields;
    }
}
