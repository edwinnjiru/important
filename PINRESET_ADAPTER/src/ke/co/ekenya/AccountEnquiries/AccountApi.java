package ke.co.ekenya.AccountEnquiries;

import ke.co.ekenya.Api.StkpushApi;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

// The Java class will be hosted at the URI path "/helloworld"
@Path("/bfubenquiry")
public class AccountApi {
    // The Java method will process HTTP GET requests
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getClichedMessage() {
        return "Service is currently available";
    }

    /**
     * Post method for receiving json from portal and return account details
     *
     * @param content
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String process(String content) {
        String response = "";
        Map<String, String> configs = new HashMap();
        try {
            configs = StkpushApi.CONFIGS;
            if (configs.isEmpty()) {
                (new Utilities()).readPropertyFile(configs);
            }
            JSONObject responseObject = new JSONObject((new FetchAccountDetails(content, configs)).fetchDetails());
            response = responseObject.toString();
            ESBLog es = new ESBLog("Acc_Rqst&Resp", (new StringBuilder("Request: ")).append(content).append(" Response: ").append(response).toString());
            es.log();
        } catch (Exception ex) {
            (new ESBLog("Exception(InitAcc)", ex)).log();
        }
        return response;
    }
}
