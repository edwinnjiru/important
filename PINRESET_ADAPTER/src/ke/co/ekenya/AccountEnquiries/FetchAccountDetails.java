package ke.co.ekenya.AccountEnquiries;

import ke.co.coopbank.BfubEnquiry.*;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.Messagehandlers.BFUBEnquiryHandler;
import ke.co.ekenya.Messagehandlers.BFUBEnquiryResolver;
import org.json.JSONObject;

import javax.xml.ws.BindingProvider;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class FetchAccountDetails {

    String request;
    Map<String, String> configs;

    public FetchAccountDetails(String request, Map<String, String> configs) {
        this.request = request;
        this.configs = configs;
    }

    public Map fetchDetails() {
        Map<String, String> responseMap = new HashMap();
        String response = "";
        try {
            JSONObject requestObject = new JSONObject(request);
            if (validateFields(requestObject, responseMap)) {
                DataInput dataInput = new DataInput();
                AccountType accountType = new AccountType();
                GetDetailsInputType getDetails = new GetDetailsInputType();

                accountType.setAccountNumber(requestObject.getString("account"));
                getDetails.setAccount(accountType);
                dataInput.setGetDetailsInput(getDetails);

                String proxypass = (new EncryptionUtils()).decrypt(configs.get("SOA_PASSWORD"));

                String wsdlLoc = "file:" + configs.get("C2B_WSDLS_PATH") + "Account_Account_getDetails_1.1.wsdl";
                URL url = new URL(wsdlLoc);
                ServiceStarter service = new ServiceStarter(url);
                BFUBEnquiryHandler handler = new BFUBEnquiryHandler(configs);
                BFUBEnquiryResolver resolver = new BFUBEnquiryResolver(handler);
                service.setHandlerResolver(resolver);
                PortType port = service.getSOAPSpEventSpSource();

                //Binding request with proxy username and password
                BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configs.get("SOA_USERNAME"));
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);
                /*provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "pck");
                provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "pck123");*/

                //time-outs
                int connecttimeout = Integer.parseInt(configs.get("CONNECTTIMEOUT"));
                int readtimeout = Integer.parseInt(configs.get("READTIMEOUT"));
                provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
                provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

                port.getDetails(dataInput);
                response = handler.getMyXML();

                if (response == null || response.isEmpty() || response.trim().equalsIgnoreCase("")) {
                    responseMap.put("Error", "true");
                    responseMap.put("Message", "SOA Generic Error");
                } else {
                    breakDownSOAResponse(response, responseMap);
                }
            }

        } catch (Exception ex) {
            (new ESBLog("Exception(FetchAccountDetails)", ex)).log();
        }
        return responseMap;
    }

    private void breakDownSOAResponse(String SOAresp, Map<String, String> responseMap) {
        HashMap result = new HashMap();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAresp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("StatusMessages");
            result.put("statusDescription", (String) headerReply.get("StatusDescription"));
            result.put("statusCode", (String) headerReply.get("StatusCode"));
            result.put("messageID", (String) headerReply.get("MessageID"));

            //status messages breakdown for header
            HashMap statusMessage = (HashMap<String, Object>) statusMessages.get("StatusMessage");
            result.put("messageCode", (String) statusMessage.get("MessageCode"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                //BreakDown Of Body
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("Body");
                HashMap<String, Object> dataOutput = (HashMap<String, Object>) body.get("DataOutput");
                HashMap<String, Object> getDetailsOutput = (HashMap<String, Object>) dataOutput.get("getDetailsOutput");
                HashMap<String, Object> getAccount = (HashMap<String, Object>) getDetailsOutput.get("Account");

                responseMap.put("Error", "false");
                responseMap.put("SortCode", getAccount.get("BranchSortCode").toString());
                responseMap.put("BranchName", getAccount.get("BranchName").toString());
                responseMap.put("AccountName", getAccount.get("AccountName").toString());
                responseMap.put("AccountNumber", getAccount.get("AccountNumber").toString());
                responseMap.put("Message", "Success");

            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "SOA Generic Error");
            }

        } catch (Exception ex) {
            ;
            (new ESBLog("Exception(QueryAccountBrkDwn)", ex)).log();

            responseMap.put("Error", "true");
            responseMap.put("Message", "Breakdown Exception Occured");
        }
    }

    private boolean validateFields(JSONObject requestObject, Map<String, String> responseMap) {
        boolean validity = true;
        try {
            if (!requestObject.has("account")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Missing account tag and value");
                validity = false;
                return validity;
            }

            if ("".equalsIgnoreCase(requestObject.getString("account").trim())) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Empty account passed");
                validity = false;
                return validity;
            }

            if (requestObject.getString("account").trim().length() != 14) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Invalid account passed. Must be 14 digits");
                validity = false;
                return validity;
            }

            /*if (!requestObject.getString("account").trim().matches("\\d+")) {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Invalid account. Enter only numerals");
                validity = false;
                return validity;
            }*/


        } catch (Exception ex) {
            (new ESBLog("Exception(BFUBValidate)", ex)).log();
            responseMap.put("Error", "true");
            responseMap.put("Message", "Validate Exception Occured");
            validity = false;
        }

        return validity;
    }
}
