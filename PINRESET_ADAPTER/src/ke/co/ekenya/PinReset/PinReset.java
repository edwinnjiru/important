package ke.co.ekenya.PinReset;


import commonj.work.Work;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class PinReset implements Work {

    Map<String, String> configs;
    Map<String, String> requestMap;

    public PinReset(Map<String, String> configs, Map<String, String> requestMap) {
        this.configs = configs;
        this.requestMap = requestMap;
    }

    @Override
    public void run() {
        try {
            doprocess();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            (new ESBLog("RunException", "Intitialization error: " + sw.toString())).log();
        }
    }

    @Override
    public void release() {
    }

    @Override
    public boolean isDaemon() {
        return false;
    }

    public void doprocess() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        try {

            String OTP=(new Utilities()).GenerateRandomNumber(6);
            requestMap.put("OTP", OTP);
            sendsms();
            requestMap.put("OTP",(new EncryptionUtils()).hashing(OTP));
            //update db and set OTP value and expiry
            (new DatabaseFunctions()).pinreset(requestMap.get("OTP"), requestMap.get("phonenumber"), "PINRESET");
            requestMap.put("OTPTime", LocalDateTime.now().format(formatter));
            boolean sent = (new QueueWriter("jms/OTP_HOLDINGQUEUE")).sendObject(requestMap, requestMap.get("phonenumber"));
            (new ESBLog("To_OTP_HOLDINGQUEUE", "Sent: " + sent + " Map: " + requestMap)).log();

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            ESBLog es = new ESBLog("Exception(PinReset)", "do process: " + sw.toString());
            es.log();
        }

    }

    public void sendsms() {
        String msg = "";
        Map<String, String> smsMap = new HashMap();
        boolean sent = false;
        try {
            Map<String, String> smsTemplate = (new DatabaseFunctions()).getSMSTemplate("STK_001");
            msg = smsTemplate.get("SMSTEMPLATE");
            if (!"".equals(msg)) {
                msg = msg.replace("OTP", requestMap.get("OTP"));

                smsMap.put("PHONENUMBER", requestMap.get("phonenumber"));
                smsMap.put("SMSMESSAGE", msg);
                smsMap.put("NotificationType", "SMS");
                smsMap.put("98", "SMS");
                smsMap.put("37", requestMap.get("ref"));
                smsMap.put("32", "STK");
                smsMap.put("102", requestMap.get("phonenumber"));
                smsMap.put("2", requestMap.get("phonenumber"));

                QueueWriter queueWriter = new QueueWriter("jms/NOTIFICATIONS_QUEUE");
                sent = queueWriter.sendObject(smsMap, requestMap.get("ref"));
            } else {
                ESBLog es2 = new ESBLog("missingSmsTemplate", "Template code: STK_001");
                es2.log();
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog lg = new ESBLog("SendsmsException", "Tran ref: " + requestMap.get("ref") + ", error: " + sw.toString());
            lg.log();
        } finally {

            ESBLog es2 = new ESBLog("SendSms", "sent: " + sent + ", messageMap: " + smsMap);
            es2.log();
        }

    }
}
