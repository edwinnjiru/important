package ke.co.ekenya.ToSOA;


import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import ke.co.ekenya.Messagehandlers.STKPushHandler;
import ke.co.ekenya.Messagehandlers.STKPushResolver;
import ke.co.coopbank.commonservices.data.message.messageheader.CredentialsType;
import ke.co.coopbank.commonservices.data.message.messageheader.ObjectFactory;
import ke.co.coopbank.commonservices.data.message.messageheader.RequestHeaderType;
import ke.co.coopbank.commonservices.data.message.messageheader.ResponseHeaderType;
import ke.co.coopbank.stkpayments.InputPayload;
import ke.co.coopbank.stkpayments.OutputPayload;
import ke.co.coopbank.tibco.stkpayments.STKPayments;
import ke.co.coopbank.tibco.stkpayments.STKPayments_Service;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

public class InitStkPush {
    Map<String, String> configs;
    Map<String, String> requestMap;


    public InitStkPush(Map<String, String> configs, Map<String, String> requestMap) {
        this.configs = configs;
        this.requestMap = requestMap;
    }

    public Map doProcess() {
        String ref = "";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        Map<String, String> responseMap = new HashMap();
        try {
            if (requestMap.get("phonenumber").trim().length() == 10) {
                requestMap.put("phonenumber", (new StringBuilder("254").append(requestMap.get("phonenumber").trim().substring(1))).toString());
            }
            ref = "EC" + (new DatabaseFunctions()).getCorrelationID();
            InputPayload paytype = new InputPayload();
            InputPayload.Parameter param = new InputPayload.Parameter();

            //Response header holder
            ResponseHeaderType respHeader = new ResponseHeaderType();
            Holder headerHolder = new Holder(respHeader);

            //Response body holder
            OutputPayload resType = new OutputPayload();
            Holder paramHolder = new Holder(resType);
            ObjectFactory obj = new ObjectFactory();

            CredentialsType creds = new CredentialsType();
            creds.setSystemCode(obj.createCredentialsTypeSystemCode(configs.get("SOA_SYSCODE")));

            RequestHeaderType header = new RequestHeaderType();
            header.setCorrelationID(ref);
            header.setCredentials(creds);
            header.setMessageID(ref);

            paytype.setAmount(Integer.valueOf(requestMap.get("amount")));
            paytype.setPhoneNumber(requestMap.get("phonenumber"));
            paytype.setMerchantRequestID(ref);
            paytype.setTransactionDesc("Pin reset");
            paytype.setCallBackURL("http://172.16.4.68:7010/PINRESET_ADAPTER/coop/stkcallback");
            //paytype.setCallBackURL("http://172.16.4.43:8002/PINRESET_ADAPTER/coop/stkcallback");


            InputPayload.Parameter.ReferenceItem refitem = new InputPayload.Parameter.ReferenceItem();
            refitem.setKey("AccountReference");
            refitem.setValue(requestMap.get("phonenumber"));

            param.setReferenceItem(refitem);

            paytype.setParameter(param);

            String wsdlLoc = "file:" + configs.get("C2B_WSDLS_PATH") + "STKPayments_gen_http_final.wsdl";
            URL url = new URL(wsdlLoc);
            STKPayments_Service service = new STKPayments_Service(url);
            STKPushHandler handler = new STKPushHandler();
            STKPushResolver resolver = new STKPushResolver(handler);
            service.setHandlerResolver(resolver);
            STKPayments port = service.getSTKPaymentsSOAP();

            String proxypass = (new EncryptionUtils()).decrypt(configs.get("SOA_PASSWORD"));
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configs.get("SOA_USERNAME"));
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            /*provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "omni");
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "omni123");*/

            //time-outs
            int connecttimeout = Integer.parseInt(configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.postPayment(header, paytype, headerHolder, paramHolder);

            String response = handler.getMyXML();
            breakDownSOAResponse(response,responseMap);


            if ("000".equalsIgnoreCase(responseMap.get("ResultCode"))) {
                /*
                 *send to holding queue for pin reset
                 *on callback browse queue
                 *if found generate OTP
                 *Send customer OTP
                 *
                 */
                Map<String, String> pinMap = new HashMap(requestMap);
                pinMap.put("time", LocalDateTime.now().format(formatter));
                pinMap.put("requestid",responseMap.get("requestid"));
                QueueWriter queueWriter = new QueueWriter("jms/PINRESET_HOLDING_QUEUE");
                boolean sent = queueWriter.sendObject(pinMap, pinMap.get("phonenumber"));
                (new ESBLog("To_PinHoldingQueue", "Sent: " + sent + " Map: " + pinMap)).log();
            }

        }catch (Exception ex){
            (new ESBLog("Exception(InitStk)", ex," map: "+requestMap.toString())).log();
            responseMap.put("Error", "true");
            responseMap.put("Message", "SOA Exception occured");
            responseMap.put("ResultCode", "109");
        }
        return responseMap;
    }

    private void breakDownSOAResponse(String SOAresp, Map<String, String> responseMap) {
        HashMap result = new HashMap();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAresp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("Header");
            HashMap<String, Object> responseBody=(HashMap<String, Object>) response.get("Body");
            HashMap<String, Object> responseHeader = (HashMap<String, Object>) header.get("ResponseHeader");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) responseHeader.get("StatusMessages");
            String MSGSTAT = statusMessages.get("MessageCode").equals("0") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                HashMap<String, Object> outputPayload = (HashMap<String, Object>) responseBody.get("OutputPayload");
                System.out.println(outputPayload);
                responseMap.put("Error", "false");
                responseMap.put("Message", "Stk push initiated");
                responseMap.put("ResultCode", "000");
                responseMap.put("requestid",outputPayload.get("CheckoutRequestID").toString());

            } else {
                responseMap.put("Error", "true");
                responseMap.put("Message", "Stk push service error");
                responseMap.put("ResultCode", "101");
            }

        } catch (Exception ex) {
            (new ESBLog("Exception(InitStk-breakdown)", ex)).log();

            responseMap.put("Error", "true");
            responseMap.put("Message", "Exception occured");
            responseMap.put("ResultCode", "102");
        }
    }
}
