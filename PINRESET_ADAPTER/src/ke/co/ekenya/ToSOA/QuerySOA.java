package ke.co.ekenya.ToSOA;

import ke.co.coopbank.co.bs.safaricom.stkpushquery.QuerySTKRequestBody;
import ke.co.coopbank.co.bs.safaricom.stkpushquery.QuerySTKRequestPayload;
import ke.co.coopbank.co.bs.safaricom.stkpushquery.send._1_0.contract.SafaricomSTKCheckoutQuery;
import ke.co.coopbank.co.bs.safaricom.stkpushquery.send._1_0.contract.SafaricomSTKCheckoutQuery_Service;
import ke.co.coopbank.co.commonservices.data.message.messageheader.CredentialsType;
import ke.co.coopbank.co.commonservices.data.message.messageheader.ObjectFactory;
import ke.co.coopbank.co.commonservices.data.message.messageheader.RequestHeaderType;
import ke.co.coopbank.co.commonservices.data.message.messageheader.ResponseHeaderType;
import ke.co.ekenya.Api.StkpushApi;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.Messagehandlers.STKQueryHandler;
import ke.co.ekenya.Messagehandlers.STKQueryResolver;
import ke.co.ekenya.Stkcallback.SendToESB;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class QuerySOA {

    Map<String, String> configs;
    Map<String, String> requestMap;

    public QuerySOA(Map<String, String> requestMap) {
        this.configs = StkpushApi.CONFIGS;
        this.requestMap = requestMap;
    }

    public void process() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        try {
            if (configs.isEmpty()) {
                (new Utilities()).readPropertyFile(configs);
            }
            QuerySTKRequestBody querySTKRequestBody = new QuerySTKRequestBody();
            querySTKRequestBody.setCheckoutRequestID(requestMap.get("requestid"));

            QuerySTKRequestPayload querySTKRequestPayload = new QuerySTKRequestPayload();
            querySTKRequestPayload.setBody(querySTKRequestBody);

            ObjectFactory objectFactory = new ObjectFactory();
            CredentialsType credentials = new CredentialsType();
            credentials.setSystemCode(objectFactory.createCredentialsTypeSystemCode(configs.get("SOA_SYSCODE")));

            String ref = (new DatabaseFunctions().getCorrelationID());
            RequestHeaderType header = new RequestHeaderType();
            header.setCorrelationID(ref);
            header.setMessageID(ref);
            header.setCredentials(credentials);
            String wsdlLoc = "file:" + configs.get("C2B_WSDLS_PATH") + "SafaricomSTKCheckoutQuery_gen_http.wsdl";
            URL url = new URL(wsdlLoc);
            SafaricomSTKCheckoutQuery_Service service = new SafaricomSTKCheckoutQuery_Service(url);
            STKQueryHandler handler = new STKQueryHandler();
            STKQueryResolver resolver = new STKQueryResolver(handler);
            service.setHandlerResolver(resolver);
            SafaricomSTKCheckoutQuery port = service.getSafaricomSTKCheckoutQuerySOAP();

            String proxypass = (new EncryptionUtils()).decrypt(configs.get("SOA_PASSWORD"));
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, configs.get("SOA_USERNAME"));
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

           /*provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "omni");
           provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "omni123");*/

            //time-outs
            int connecttimeout = Integer.parseInt(configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            Holder responseHeaderHolder = new Holder((new ResponseHeaderType()));
            Holder responseBodyHolder = new Holder(new ResponseHeaderType());
            port.queryTransaction(header, querySTKRequestBody, responseHeaderHolder, responseBodyHolder);

            String response = handler.getMyXML();
            breakDownSOAResponse(response);
            if ("0".equalsIgnoreCase(requestMap.get("resultcode"))) {
                String date = LocalDateTime.now().format(formatter);
                requestMap.put("transactiondate", date);
                requestMap.put("mpesareceiptnumber", "");
                (new SendToESB(requestMap)).processRequest();
            } else {
                (new ESBLog("FailedTransactions", requestMap.toString())).log();
            }

        } catch (Exception ex) {
            (new ESBLog("Exception(query)", ex," map: "+requestMap.toString())).log();
        }
    }

    private void breakDownSOAResponse(String SOAresp) {
        HashMap result = new HashMap();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAresp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("Header");
            HashMap<String, Object> responseHeader = (HashMap<String, Object>) header.get("ResponseHeader");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) responseHeader.get("StatusMessages");
            System.out.println(statusMessages);
            String MSGSTAT = statusMessages.get("MessageCode").equals("200") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("Body");
                HashMap<String, Object> responseBody = (HashMap<String, Object>) body.get("QuerySTKResponseBody");
                requestMap.put("checkoutrqstid", responseBody.get("CheckoutRequestID").toString());
                requestMap.put("resultcode", responseBody.get("ResultCode").toString());
            } else {
                requestMap.put("resultcode", "");
            }

        } catch (Exception ex) {
            (new ESBLog("Exception(Query-breakdown)", ex)).log();
        }
    }
}
