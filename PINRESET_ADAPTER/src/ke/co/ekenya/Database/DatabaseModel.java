package ke.co.ekenya.Database;

import ke.co.ekenya.Api.StkpushApi;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class DatabaseModel {

    static Map<String, String> configs = new HashMap();

    /**
     * Initiate configs
     */
    public DatabaseModel() {
        configs = StkpushApi.CONFIGS;
        if (configs.isEmpty()) {
            (new Utilities()).readPropertyFile(configs);
        }
    }

    /**
     * create connection to EBANK SCHEMA
     *
     * @return
     */
    public static Connection createConnection() {
        Connection con = null;
        try {

            //EBANK Schema
            Hashtable ht = new Hashtable();
            ht.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
            ht.put(Context.PROVIDER_URL, configs.get("PROVIDER_URL"));
            Context ctx = new InitialContext(ht);
            javax.sql.DataSource ds = (javax.sql.DataSource) ctx.lookup("jdbc/EBANK");
            con = ds.getConnection();

        } catch (NamingException | SQLException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es = new ESBLog("DatabaseException", "Error creating connection error: " + sw.toString());
            es.log();
        }
        return con;
    }

    /**
     * create connection to MVISA SCHEMA
     *
     * @return
     */
    public static Connection createMvisaConnection() {
        Connection con = null;
        try {

            //MVISA Schema
            Hashtable ht = new Hashtable();
            ht.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
            ht.put(Context.PROVIDER_URL, configs.get("PROVIDER_URL"));
            Context ctx = new InitialContext(ht);
            javax.sql.DataSource ds = (javax.sql.DataSource) ctx.lookup("jdbc/ECONNECTESB");
            con = ds.getConnection();

        } catch (NamingException | SQLException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es = new ESBLog("DatabaseException", "Error creating 4.0 connection error: " + sw.toString());
            es.log();
        }
        return con;
    }
}
