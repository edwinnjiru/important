package ke.co.ekenya.Database;

import ke.co.ekenya.LogEngine.ESBLog;
import oracle.jdbc.OracleTypes;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class DatabaseFunctions extends DatabaseModel {

    public DatabaseFunctions() {

    }


    /**
     * Function used to fetch correlation/transaction ref
     *
     * @return
     */
    public String getCorrelationID() {
        String CorrelationID = "";
        String command = "{call SP_GET_ESB_NEXT_SEQ(?)}";
        try (Connection con = createMvisaConnection();
             CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.registerOutParameter("cv_1", java.sql.Types.VARCHAR);
            callableStatement.executeUpdate();
            CorrelationID = callableStatement.getString("cv_1");
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Error in correlationID function: " + sw.toString());
            el.log();
        }
        return CorrelationID;
    }


    /**
     * Fuction to fetch sms templates stored in the database by
     * passing in the sms code
     *
     * @param smsCode
     * @return
     */
    public HashMap getSMSTemplate(String smsCode) {
        HashMap detailsMap = new HashMap<>();
        String template = "";
        String command = "{call SP_GET_SMS_TEMPLATE(?,?)}";
        ResultSet rs = null;

        try (Connection con = createMvisaConnection();
             CallableStatement stmt = con.prepareCall(command);) {
            stmt.setString("IV_SMSCODE", smsCode);
            stmt.registerOutParameter("cv_1", OracleTypes.CURSOR);
            stmt.execute();

            rs = (ResultSet) stmt.getObject("cv_1");
            while (rs.next()) {
                template = rs.getString("MESSAGE");
            }

            detailsMap.put("SMSTEMPLATE", template);

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Exception occured while calling"
                    + " SP_GET_SMS_TEMPLATE SP to get sms template for sms code:" + smsCode + "|Error:" + sw.toString() + "\n");
            el.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog("SQLExceptions", "Error occured closing resultset of get sms template function. Error: " + sw.toString());
                el.log();
            }
        }

        return detailsMap;

    }

    public int pinreset(String token, String phoneno, String action) {
        int update = 0;
        String command = "{call SP_PINRESET(?,?,?,?)}";
        try (Connection con = createConnection();
             CallableStatement callableStatement = con.prepareCall(command)) {
            callableStatement.setString("IV_TOKEN", token);
            callableStatement.setString("IV_PHONENO", phoneno);
            callableStatement.setString("IV_ACTION", action);
            callableStatement.registerOutParameter("c_1", java.sql.Types.VARCHAR);
            callableStatement.execute();
            update = Integer.parseInt(callableStatement.getString("c_1"));
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("Exceptions(pinreset)", "Error in pinreset function: " + sw.toString());
            el.log();
        }
        return update;
    }

    public String getAnyGLAccount(String glName) {
        String glAccount = "";
        ResultSet rs = null;
        String command = "{call SP_GETANYGLACCOUNT(?,?)}";
        try (Connection con = createMvisaConnection();
             CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.setString("iv_glName", glName);
            callableStatement.registerOutParameter("cv_1", OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            rs = (ResultSet) callableStatement.getObject("cv_1");
            while (rs.next()) {
                glAccount = rs.getString(1);
            }
        } catch (Exception e) {
            (new ESBLog("Exception(GETGL)", e)).log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog("SQLExceptions", "Error occured closing resultset of get gl function. Error: " + sw.toString());
                el.log();
            }
        }
        return glAccount;
    }
}
