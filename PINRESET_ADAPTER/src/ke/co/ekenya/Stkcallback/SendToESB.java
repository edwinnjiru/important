package ke.co.ekenya.Stkcallback;

import commonj.work.WorkManager;
import ke.co.ekenya.Api.StkpushApi;
import ke.co.ekenya.CommonOperations.JMSUtilities;
import ke.co.ekenya.CommonOperations.PostGetESBRequest;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.CommonOperations.WorkManagerUtilities;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.PinReset.PinReset;

import java.util.HashMap;
import java.util.Map;

public class SendToESB {

    Map<String, String> configs;
    Map<String, String> requestMap;

    public SendToESB(Map<String, String> requestMap) {
        configs = StkpushApi.CONFIGS;
        this.requestMap = requestMap;
    }

    public void processRequest() {
        try {
            (new ESBLog("CallBack_requestmap", requestMap.toString())).log();

            HashMap<String, String> resetMap = (new JMSUtilities()).getWeblogicMessageFromUDQueue(requestMap.get("phonenumber"));
            if ("0".equalsIgnoreCase(requestMap.get("resultcode"))) {
                /*
                 *  Generate OTP, and send to customer
                 *  flag table
                 *
                 */
                if ("1".equalsIgnoreCase(resetMap.get("pinreset"))) {
                    WorkManager wm = WorkManagerUtilities.getWorkManager();
                    PinReset pinReset = new PinReset(configs, resetMap);
                    wm.schedule(pinReset);
                    (new ESBLog("ResetMap", resetMap)).log();
                }

                if ("1".equalsIgnoreCase(requestMap.get("pinreset"))) {
                    WorkManager wm = WorkManagerUtilities.getWorkManager();
                    PinReset pinReset = new PinReset(configs, requestMap);
                    wm.schedule(pinReset);
                    (new ESBLog("ResetMap", requestMap.toString())).log();
                }
            }

            HashMap<String, String> esbRequest = new HashMap();
            String stan = (new Utilities()).generateStan();
            esbRequest.put("0", "0200");
            esbRequest.put("2", "254722000000");
            esbRequest.put("3", "400000");
            esbRequest.put("4", requestMap.get("amount"));
            esbRequest.put("MTI", "0200");
            esbRequest.put("7", requestMap.get("transactiondate"));
            esbRequest.put("11", stan);
            esbRequest.put("12", stan);
            esbRequest.put("24", "MM");
            esbRequest.put("49", "KES");
            esbRequest.put("37", (new DatabaseFunctions()).getCorrelationID());
            esbRequest.put("32", "C2B");
            esbRequest.put("52", "bcac5bdc-32fb-42df-8ddb-ce8f705ba8c5");
            esbRequest.put("98", "PINRESET");
            esbRequest.put("100", "MWALLET");
            esbRequest.put("102", (new DatabaseFunctions()).getAnyGLAccount("STKPUSHGL"));
            esbRequest.put("103", requestMap.get("phonenumber"));
            esbRequest.put("41", "FID00001");
            esbRequest.put("68", requestMap.get("checkoutrqstid").trim());
            esbRequest.put("70", requestMap.get("mpesareceiptnumber"));
            esbRequest.put("CREDIT_WALLET", "1");
            (new PostGetESBRequest()).CURLRequest(esbRequest);

        } catch (Exception ex) {
            (new ESBLog("Exception(send)", ex)).log();
        }
    }

}
