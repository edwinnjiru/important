package ke.co.ekenya.Stkcallback;


import commonj.work.WorkException;
import commonj.work.WorkManager;
import ke.co.ekenya.Api.StkpushApi;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.CommonOperations.WorkManagerUtilities;
import ke.co.ekenya.LogEngine.ESBLog;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

// The Java class will be hosted at the URI path "/stkcallback"
@Path("/stkcallback")
public class StkCallBack {

    WorkManager wm = null;


    // The Java method will process HTTP GET requests
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getClichedMessage() {
        // Return some cliched textual content
        return "Service is currently available";
    }

    /**
     * Post method for receiving json from SOA and acknowledging the same
     *
     * @param content representation for the resource
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String process(String content) {
        String response = "";
        Map<String, String> configs = new HashMap();
        try {
            configs = StkpushApi.CONFIGS;
            if (configs.isEmpty()) {
                (new Utilities()).readPropertyFile(configs);
            }
            (new ESBLog("Callback", content)).log();
            JSONObject recJson = new JSONObject(content);

            wm = WorkManagerUtilities.getWorkManager();
            ProcessCallBack processor = new ProcessCallBack(recJson, configs);
            wm.schedule(processor);

            JSONObject respJSON = new JSONObject();
            respJSON.put("message", "ok");
            respJSON.put("MerchantRequestID", recJson.get("MerchantRequestID"));
            response = respJSON.toString();
        } catch (WorkException | IllegalArgumentException | JSONException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es2 = new ESBLog("Exception(CallBacl)", "Error: " + sw.toString());
            es2.log();
        }
        return response;
    }

}
