package ke.co.ekenya.Stkcallback;

import commonj.work.Work;
import ke.co.ekenya.LogEngine.ESBLog;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProcessCallBack implements Work {

    JSONObject requestObject;
    Map<String, String> configs;

    public ProcessCallBack(JSONObject requestObject, Map<String, String> configs) {
        this.requestObject = requestObject;
        this.configs = configs;
    }

    @Override
    public void run() {
        doProcess();
    }

    @Override
    public void release() {
    }

    @Override
    public boolean isDaemon() {
        return false;
    }

    void doProcess() {
        try {
            Map<String, String> requestMap = convertToMap();

            //send request to esb if successful;
            if ("0".equalsIgnoreCase(requestMap.get("resultcode"))) {
                (new SendToESB(requestMap)).processRequest();
            }

        } catch (Exception ex) {
            (new ESBLog("Exception(ProcCallBck)", ex)).log();
        }

    }

    private Map convertToMap() {
        Map<String, String> callBackParams = new HashMap();
        try {
            JSONObject callBackMetadata = requestObject.getJSONObject("CallbackMetadata");
            JSONArray metaArray = callBackMetadata.getJSONArray("Item");

            callBackParams.put("resultcode", requestObject.getString("ResultCode"));
            callBackParams.put("merchrqstid", requestObject.getString("MerchantRequestID"));
            callBackParams.put("checkoutrqstid", requestObject.getString("CheckoutRequestID"));
            callBackParams.put("resultdesc", requestObject.getString("ResultDesc"));

            for (int i = 0; i < metaArray.length(); i++) {
                JSONObject jsObect = metaArray.getJSONObject(i);
                if (!jsObect.has("Value")) {
                    jsObect.put("Value", "");
                }
                callBackParams.put(jsObect.getString("Name").toLowerCase(), jsObect.getString("Value"));
            }

        } catch (Exception ex) {
            (new ESBLog("Exception(convertToMap)", ex)).log();
        }
        return callBackParams;
    }

}
