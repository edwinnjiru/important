create or replace PROCEDURE SP_POST_REVERSAL_TO_WALLET 
(
  IV_MSGTYPE IN VARCHAR2 
, IV_TRXREFNO IN VARCHAR2
, c_1 IN OUT SYS_REFCURSOR
) AS
V_MSGTYPE VARCHAR2(20) := IV_MSGTYPE;
V_TRXREFNO VARCHAR2(20) := IV_TRXREFNO;
v_ResultCode NVARCHAR2(3);  
v_ResultDescription NVARCHAR2(100);
V_DRCR CHAR(1);
v_RunningBalance    NUMERIC(18,5) DEFAULT 0;
V_Gcaccountno NVARCHAR2(20);
v_SERIALNO NVARCHAR2(20);
v_WORKINGDATE DATE;
v_CLEARBAL     NUMERIC(18,5);
v_Narration NVARCHAR2(500);
v_OriginalTxnExists NUMBER(3) DEFAULT 0;

BEGIN
  
  SAVEPOINT initialState; --Roll back position if anything fails
  /*Check if field 102 exists*/
   SELECT count(TRXREFNO) into v_OriginalTxnExists FROM TBTRANSACTIONS WHERE TRXREFNO = V_TRXREFNO AND REVERSED='0' AND MSGTYPE='0200';
   IF(v_OriginalTxnExists<1) THEN 
      v_ResultCode:='001';
      v_ResultDescription:='Original Transaction missing or Reversal already done!';              
      OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;
      RETURN;
   END IF;
  /*Fetch working date for inserting to tbtransactions. In place to take care of financial year control */
  BEGIN
    SELECT WORKINGDATE INTO v_WORKINGDATE FROM tbDateSettings;
    EXCEPTION
        WHEN OTHERS THEN
         ROLLBACK TO initialState;                        
              v_ResultCode:='999';
              v_ResultDescription:='Generic Error on Wallet Reversal - Fetching Working Date';                          
              OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;                          
              RETURN;
  END;            
  IF(V_TRXREFNO IS NULL OR V_TRXREFNO='') THEN
    v_ResultCode:='002';
    v_ResultDescription:='Missing Transaction Reference';            
    OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;
    RETURN;
  END IF;
  IF(V_MSGTYPE IS NULL OR V_MSGTYPE='' OR V_MSGTYPE<>'0420') THEN
    v_ResultCode:='003';
    v_ResultDescription:='Missing or incorrect Message Type';
    OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;
    RETURN;
  END IF;
  
  BEGIN
    FOR rec in (SELECT ID,MSGTYPE,CHANNEL,TRXREFNO,TRXSERIALNO,ACCOUNTCENTRE,ACCOUNTNO,CURRENCY,DRCR,AMOUNT,TRXDATE,VALUEDATE,FINANCIALCYCLE,FINANCIALPERIOD,CREATEDBY,CREATEDON,
    AUTHORIZEDBY,AUTHORIZEDON,ISCUSTOMERGL,NARRATION,CLEAREDBALANCE,AVAILABLEBALANCE,PROCODE,TRXCODE,STAN,FIELD90,RUNNINGBALANCE FROM Tbtransactions where TRXREFNO=V_TRXREFNO and REVERSED='0' and MSGTYPE='0200')
    LOOP
      IF(rec.DRCR='C' AND rec.ISCUSTOMERGL='G') THEN --Reverse money credited to GL Account
       V_DRCR:='D';
       v_GCAccountNo:=rec.ACCOUNTNO;
       BEGIN
        v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(rec.AMOUNT,v_GCAccountNo,v_DRCR);
        EXCEPTION
          WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
            ROLLBACK TO initialState;
            v_ResultCode:='003';
            v_ResultDescription:='Generic Error on updating GL balance';              
            OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;
            RETURN;          
       END;
       BEGIN
         INSERT INTO TBTRANSACTIONS (ID,MSGTYPE,CHANNEL,TRXREFNO,TRXSERIALNO,ACCOUNTCENTRE,ACCOUNTNO,CURRENCY,DRCR,AMOUNT, TRXDATE,VALUEDATE,FINANCIALCYCLE,FINANCIALPERIOD,
         CREATEDBY,CREATEDON,AUTHORIZEDBY,AUTHORIZEDON,ISCUSTOMERGL,NARRATION,CLEAREDBALANCE,AVAILABLEBALANCE,PROCODE,TRXCODE,STAN,FIELD90,RUNNINGBALANCE)VALUES 
         (v_SERIALNO,V_MSGTYPE,rec.CHANNEL,rec.TRXREFNO,rec.TRXSERIALNO,rec.ACCOUNTCENTRE,rec.ACCOUNTNO,rec.CURRENCY,V_DRCR,rec.AMOUNT,v_WORKINGDATE,v_WORKINGDATE,rec.FINANCIALCYCLE,
         rec.FINANCIALPERIOD,rec.CREATEDBY,SYSDATE,rec.AUTHORIZEDBY,SYSDATE,rec.ISCUSTOMERGL,'REV-'||rec.NARRATION,v_RunningBalance,v_RunningBalance,rec.PROCODE,rec.TRXCODE,
         rec.STAN,rec.FIELD90,v_RunningBalance);      
            EXCEPTION
              WHEN OTHERS THEN
                ROLLBACK TO initialState;                        
                  v_ResultCode:='005';
                  v_ResultDescription:='Generic Error on GL Debit Reversal - insert';                          
                  OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;                          
                  RETURN;
       END;
       BEGIN
        UPDATE TBTRANSACTIONS SET REVERSED='1' WHERE ID=rec.ID;
        EXCEPTION
          WHEN OTHERS THEN
           ROLLBACK TO initialState;                        
                v_ResultCode:='006';
                v_ResultDescription:='Generic Error on GL Debit Reversal - update';                          
                OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;                          
                RETURN; 
       END;
      END IF;
      
      
      IF(rec.DRCR='D' AND rec.ISCUSTOMERGL='G') THEN --Reverse money Debited to GL Account
       V_DRCR:='C';
       v_GCAccountNo:=rec.ACCOUNTNO;
       BEGIN
        v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(rec.AMOUNT,v_GCAccountNo,v_DRCR);
        EXCEPTION
          WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
            ROLLBACK TO initialState;
            v_ResultCode:='007';
            v_ResultDescription:='Generic Error on updating GL balance';              
            OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;
            RETURN;          
       END;
       BEGIN
         INSERT INTO TBTRANSACTIONS (ID,MSGTYPE,CHANNEL,TRXREFNO,TRXSERIALNO,ACCOUNTCENTRE,ACCOUNTNO,CURRENCY,DRCR,AMOUNT, TRXDATE,VALUEDATE,FINANCIALCYCLE,FINANCIALPERIOD,
         CREATEDBY,CREATEDON,AUTHORIZEDBY,AUTHORIZEDON,ISCUSTOMERGL,NARRATION,CLEAREDBALANCE,AVAILABLEBALANCE,PROCODE,TRXCODE,STAN,FIELD90,RUNNINGBALANCE)VALUES 
         (v_SERIALNO,V_MSGTYPE,rec.CHANNEL,rec.TRXREFNO,rec.TRXSERIALNO,rec.ACCOUNTCENTRE,rec.ACCOUNTNO,rec.CURRENCY,V_DRCR,rec.AMOUNT,v_WORKINGDATE,v_WORKINGDATE,rec.FINANCIALCYCLE,
         rec.FINANCIALPERIOD,rec.CREATEDBY,SYSDATE,rec.AUTHORIZEDBY,SYSDATE,rec.ISCUSTOMERGL,'REV-'||rec.NARRATION,v_RunningBalance,v_RunningBalance,rec.PROCODE,rec.TRXCODE,
         rec.STAN,rec.FIELD90,v_RunningBalance);      
            EXCEPTION
              WHEN OTHERS THEN
                ROLLBACK TO initialState;                        
                v_ResultCode:='008';
                v_ResultDescription:='Generic Error on GL Credit Reversal - insert';                          
                OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;                          
                RETURN;
       END;
       BEGIN
        UPDATE TBTRANSACTIONS SET REVERSED='1' WHERE ID=rec.ID;
        EXCEPTION
          WHEN OTHERS THEN
           ROLLBACK TO initialState;                        
                v_ResultCode:='009';
                v_ResultDescription:='Generic Error on GL Debit Reversal - update';                          
                OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;                          
                RETURN; 
       END;       
      END IF;
      
      IF(rec.DRCR='C' AND rec.ISCUSTOMERGL='C') THEN --Reverse money credited to Wallet Account
       V_DRCR:='D';
       v_GCAccountNo:=rec.ACCOUNTNO;
       BEGIN
        v_CLEARBAL:=FN_UPDATEWALLETBALANCE(rec.AMOUNT,v_GCAccountNo,v_DRCR);
        v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(rec.AMOUNT,v_GCAccountNo,v_DRCR); --Calculate running balance
        EXCEPTION
          WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
            ROLLBACK TO initialState;
            v_ResultCode:='010';
            v_ResultDescription:='Generic Error on updating Wallet balance';              
            OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;
            RETURN;          
       END;
       BEGIN
         INSERT INTO TBTRANSACTIONS (ID,MSGTYPE,CHANNEL,TRXREFNO,TRXSERIALNO,ACCOUNTCENTRE,ACCOUNTNO,CURRENCY,DRCR,AMOUNT, TRXDATE,VALUEDATE,FINANCIALCYCLE,FINANCIALPERIOD,
         CREATEDBY,CREATEDON,AUTHORIZEDBY,AUTHORIZEDON,ISCUSTOMERGL,NARRATION,CLEAREDBALANCE,AVAILABLEBALANCE,PROCODE,TRXCODE,STAN,FIELD90,RUNNINGBALANCE)VALUES 
         (v_SERIALNO,V_MSGTYPE,rec.CHANNEL,rec.TRXREFNO,rec.TRXSERIALNO,rec.ACCOUNTCENTRE,rec.ACCOUNTNO,rec.CURRENCY,V_DRCR,rec.AMOUNT,v_WORKINGDATE,v_WORKINGDATE,rec.FINANCIALCYCLE,
         rec.FINANCIALPERIOD,rec.CREATEDBY,SYSDATE,rec.AUTHORIZEDBY,SYSDATE,rec.ISCUSTOMERGL,'REV-'||rec.NARRATION,v_CLEARBAL,v_CLEARBAL,rec.PROCODE,rec.TRXCODE,
         rec.STAN,rec.FIELD90,v_RunningBalance);      
            EXCEPTION
              WHEN OTHERS THEN
                ROLLBACK TO initialState;                        
                  v_ResultCode:='011';
                  v_ResultDescription:='Generic Error on Wallet Debit Reversal - insert';                          
                  OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;                          
                  RETURN;
       END;
       BEGIN
        UPDATE TBTRANSACTIONS SET REVERSED='1' WHERE ID=rec.ID;
        EXCEPTION
          WHEN OTHERS THEN
           ROLLBACK TO initialState;                        
                v_ResultCode:='012';
                v_ResultDescription:='Generic Error on Wallet Debit Reversal - update';                          
                OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;                          
                RETURN; 
       END;
      END IF;
      
      IF(rec.DRCR='D' AND rec.ISCUSTOMERGL='C') THEN --Reverse money Debited to Wallet Account
       V_DRCR:='C';
       v_GCAccountNo:=rec.ACCOUNTNO;
       BEGIN
        v_CLEARBAL:=FN_UPDATEWALLETBALANCE(rec.AMOUNT,v_GCAccountNo,v_DRCR);
        v_RunningBalance := FN_UPDATEWALLETGLRUNNINGBAL(rec.AMOUNT,v_GCAccountNo,v_DRCR); --Calculate running balance
        EXCEPTION
          WHEN POST_TO_WALLET_EXCEPTIONS.GENERIC_EXCEPTION THEN
            ROLLBACK TO initialState;
            v_ResultCode:='013';
            v_ResultDescription:='Generic Error on updating Wallet balance';              
            OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;
            RETURN;          
       END;
       BEGIN
         INSERT INTO TBTRANSACTIONS (ID,MSGTYPE,CHANNEL,TRXREFNO,TRXSERIALNO,ACCOUNTCENTRE,ACCOUNTNO,CURRENCY,DRCR,AMOUNT, TRXDATE,VALUEDATE,FINANCIALCYCLE,FINANCIALPERIOD,
         CREATEDBY,CREATEDON,AUTHORIZEDBY,AUTHORIZEDON,ISCUSTOMERGL,NARRATION,CLEAREDBALANCE,AVAILABLEBALANCE,PROCODE,TRXCODE,STAN,FIELD90,RUNNINGBALANCE)VALUES 
         (v_SERIALNO,V_MSGTYPE,rec.CHANNEL,rec.TRXREFNO,rec.TRXSERIALNO,rec.ACCOUNTCENTRE,rec.ACCOUNTNO,rec.CURRENCY,V_DRCR,rec.AMOUNT,v_WORKINGDATE,v_WORKINGDATE,rec.FINANCIALCYCLE,
         rec.FINANCIALPERIOD,rec.CREATEDBY,SYSDATE,rec.AUTHORIZEDBY,SYSDATE,rec.ISCUSTOMERGL,'REV-'||rec.NARRATION,v_CLEARBAL,v_CLEARBAL,rec.PROCODE,rec.TRXCODE,
         rec.STAN,rec.FIELD90,v_RunningBalance);      
            EXCEPTION
              WHEN OTHERS THEN
                ROLLBACK TO initialState;                        
                  v_ResultCode:='014';
                  v_ResultDescription:='Generic Error on Wallet Credit Reversal - insert';                          
                  OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;                          
                  RETURN;
       END;
       BEGIN
        UPDATE TBTRANSACTIONS SET REVERSED='1' WHERE ID=rec.ID;
        EXCEPTION
          WHEN OTHERS THEN
           ROLLBACK TO initialState;                        
                v_ResultCode:='015';
                v_ResultDescription:='Generic Error on Wallet Credit Reversal - update';                          
                OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;                          
                RETURN; 
       END;
      END IF;      
    END LOOP;
  END;
  v_ResultCode:='000';
  v_ResultDescription:='SUCCESS';                          
  OPEN c_1 FOR SELECT v_ResultCode,v_ResultDescription FROM DUAL;                          
END SP_POST_REVERSAL_TO_WALLET;