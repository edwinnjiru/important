/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

/**
 *
 * @author njiru
 */
public class Utilities {
    
    public Utilities(){
    
    }
    
    public boolean validateAuth(String authString){
    boolean isAuthorized=false;
    String[] authParts;
    String authInfo="";
        try {
            authParts=authString.split("\\s+");
            authInfo = authParts[1];
            
            byte[] bytes = null;
            bytes = java.util.Base64.getDecoder().decode(authInfo);
            String decodedString=new String(bytes);
            String validationString=(new StringBuilder("njiru")).append(":").append("1234").toString();
            if(decodedString.equals(validationString)){
            isAuthorized=true;
            }
            
        } catch (Exception ex) {
            System.out.println("Validation exc: "+ex.getMessage());
        }
    
    return isAuthorized;
    }
}
