/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import common.Utilities;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONObject;

/**
 * REST Web Service
 *
 * @author njiru
 */
@Path("njiru")
public class NjiruResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of NjiruResource
     */
    public NjiruResource() {
    }

    /**
     * Retrieves representation of an instance of Resources.NjiruResource
     *
     * @param headers
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@Context HttpHeaders headers) {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of NjiruResource
     *
     * @param headers
     * @param request
     * @return 
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response processRequest(@Context HttpHeaders headers, String request) {
        boolean isUserAuthenticated = false;
        boolean error = false;
        JSONObject resultsObj = new JSONObject();
        Response resp=null;
        try {
           // JSONObject requestObj = new JSONObject(request);
            System.out.println("headers: "+headers);
            System.out.println("Auth header: "+headers.getRequestHeader("Authorization"));
            if (headers.getRequestHeader("Authorization")==null) {
                error = true;
                resultsObj.put("error", true);
                resultsObj.put("Message", "Missing credentials");
                resp=Response.status(401).type(MediaType.APPLICATION_JSON).entity(resultsObj.toString()).build();
            }else{
            String authString = headers.getRequestHeader("Authorization").get(0).trim();
            
                isUserAuthenticated=(new Utilities()).validateAuth(authString);
                if(isUserAuthenticated){
                resultsObj.put("error", false);
                resultsObj.put("Message", "User authenticated successfully");
                resp=Response.status(200).type(MediaType.APPLICATION_JSON).entity(resultsObj.toString()).build();
                }else{
                resultsObj.put("error", true);
                resultsObj.put("Message", "Invalid credentials");
                resp=Response.status(401).type(MediaType.APPLICATION_JSON).entity(resultsObj.toString()).build();
                }
            }
        } catch (Exception ex) {
            System.out.println("Exc: " + ex.getMessage());
        }
        return resp;
    }
}
