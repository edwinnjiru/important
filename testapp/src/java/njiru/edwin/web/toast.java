/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package njiru.edwin.web;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author njiru
 */
@WebService(serviceName = "toast")
public class toast {

    /**
     * This is a sample web service operation
     *
     * @param header
     * @param operationParameters
     * @return
     */
    @WebMethod(operationName = "getdetails")
    public String getdetails(@WebParam(name = "Header") Header header, @WebParam(name = "OperationParemeters") OperationParameters operationParameters) {
        String response = "";
        boolean loggedin = login(header);
        if (loggedin) {
            response = "logged in";
        } else {
            response = "WWrong credentials";
        }
        

        return response;
    }

    private boolean login(Header header) {
        boolean loggedin = false;
        try {
            String username = header.username;
            String pass = header.password;
            System.out.println("Username: " + username + ", password: " + pass);
            loggedin = true;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return loggedin;
        }

        return loggedin;
    }
}
