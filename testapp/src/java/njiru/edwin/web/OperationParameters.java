/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package njiru.edwin.web;

/**
 *
 * @author njiru
 */
public class OperationParameters {

    public Customerdetails customerDetails;
    public Transactiondetails transactionDetails;

    public OperationParameters(Customerdetails customerDetails, Transactiondetails transactionDetails) {
        this.customerDetails = customerDetails;
        this.transactionDetails = transactionDetails;
    }
    
    public OperationParameters(){}

}
