package ESBLogEngine;

import ESBReports.QueueWriter;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SocketHandler;

/**
 * This ESBLog class implements an application that handles All logging
 * functionality, for both file
 *
 * @author Hesbon
 * @version 1.1
 * @since 2017-01-16
 */
public class ESBLog {

    private FileHandler fh = null;
    private String pathtologs = "/var/log/esblogs/ESBReports/";
    private SocketHandler sh = null;
    //for socket handler
    private String directory = "ESBReports";

    private String filename = null;
    private String msg = null;
    private Logger logger = null;
    /**
     * Hahmap to be logged to db *
     */
    public HashMap<String, String> logmap;

    /**
     * Constructor for Text messages
     *
     * @param filename
     * @param msg - Text message to be logged
     */
    public ESBLog(String filename, String msg) {
        this.filename = filename;
        this.msg = msg;
    }

    public ESBLog(String filename, Exception ex, String msg) {
        this.filename = filename;
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString() + "\n" + msg;
    }

    public ESBLog(String filename, HashMap<String, String> map) {
        this.filename = filename;
        logmap = new HashMap(map);
        this.msg = logmap.toString();
    }

    public ESBLog(String filename, Map<String, String> map) {
        this.filename = filename;
        logmap = new HashMap(map);
        this.msg = logmap.toString();
    }

    /**
     * Constructor for Error messages (Exception Stack traces)
     *
     * @param ex
     * @param msg - Text message to be logged
     */
    public ESBLog(Exception ex, String msg) {
        this.filename = "exceptions";
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString() + "\n" + msg;
    }

    public ESBLog(Exception ex, HashMap<String, String> map) {
        this.filename = "exceptions";
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString() + "\n" + msg;
    }

    public ESBLog(Exception ex, Map<String, String> map) {
        this.filename = "exceptions";
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString() + "\n" + msg;
    }

    /**
     * Writes text files to file directly
     */
    public void logfile() {
        logger = Logger.getLogger("");
        String daylog = anyDate("dd-MMM-yyyy");
        try {
            fh = new FileHandler(createDailyDirectory() + filename + "%g.txt", 26000000, 20, true);
            fh.setFormatter(new EsbFormatter());
            logger.addHandler(fh);
            logger.setLevel(Level.FINE);
            logger.fine(msg);
            fh.close();
        } catch (Exception e) {
            //System.out.println("AT ESB Class ESBLog{ log() }: " + e.getMessage());
            e.printStackTrace();
        }

    }

    /**
     * Writes text files to a TCP Socket be logged to file by another external
     * application
     */
    public void logTCPSocket() {
        logger = Logger.getLogger(filename);
        String finalmessage = "###" + directory + "$$$" + filename + "###" + msg;
        try {
            sh = new SocketHandler("NOT SUPPOTED", 5050);
            sh.setFormatter(new EsbFormatter());
            logger.addHandler(sh);
            logger.setLevel(Level.FINE);
            logger.fine(finalmessage);
            sh.flush();
            sh.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Writes text files to a Log Queue be logged to file by another external
     * application
     */
    public void log() {
        String timestamp = anyDate("dd-MMM-yyyy HH:mm:ss.SSS");
        String finalmessage = "###" + directory + "$$$" + filename + "###" + timestamp + "::" + msg;
        QueueWriter qw = new QueueWriter("jms/ESB_LOG_QUEUE");
        qw.send(finalmessage, "");
        
        System.out.println(msg);
    }
    
  

    /**
     * Writes text files to file directly
     */
    public void logToFile() {
        logger = Logger.getLogger("");
        System.out.println(msg);
        try {
            fh = new FileHandler(createDailyDirectory() + filename + "%g.txt", 26000000, 20, true);
            fh.setFormatter(new EsbFormatter());
            logger.addHandler(fh);
            logger.setLevel(Level.FINE);
            logger.fine(msg);
            fh.close();
        } catch (Exception e) {
            //System.out.println("AT ESB Class ESBLog{ log() }: " + e.getMessage());
            e.printStackTrace();
        }

    }

    /**
     * Creates a directory based on daily path
     *
     * @return Dailydirectory - Created directory
     */
    public String createDailyDirectory() {
        String Dailydirectory = "";
        String daylog = anyDate("dd-MMM-yyyy");
        Dailydirectory = pathtologs + daylog;
        try {
            new File(Dailydirectory).mkdir();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Dailydirectory + "/";
    }

    public String anyDate(String format) {
        String myDate = "";
        try {
            if ("".equals(format)) {
                format = "yyyy-MM-dd HH:mm:ss"; // default
            }
            LocalDateTime date = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            myDate = date.format(formatter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return myDate;
    }

   

}
