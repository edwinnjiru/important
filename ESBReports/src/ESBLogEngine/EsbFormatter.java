package ESBLogEngine;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Overrides the default Simple Date Formatter
 * Returns a new date format with Milliseconds
 * @author Hesbon
 */
public class EsbFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
//        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss.SSS");
//        return format.format(new Date()) + "::"+ record.getMessage() + "\n";
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss.SSS");
        String text = date.format(formatter);
        return text;
    }

}
