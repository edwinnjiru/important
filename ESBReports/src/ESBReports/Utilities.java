/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESBReports;

import ESBLogEngine.ESBLog;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author hkiptoo
 */
public class Utilities {

    final String path = "/home/mvisa/ESBAPPLICATIONS/reports/";
    static Properties mailServerProperties;
    static Session getMailSession;
    static MimeMessage generateMailMessage;

//    public void sendMail2(Map<String, String> details) {
//        try {
//            mailServerProperties = System.getProperties();
//            mailServerProperties.put("mail.smtp.port", "587");
//            mailServerProperties.put("mail.smtp.auth", "true");
//            mailServerProperties.put("mail.smtp.starttls.enable", "true");
//            getMailSession = Session.getDefaultInstance(mailServerProperties, null);
//            generateMailMessage = new MimeMessage(getMailSession);
//
//            MimeBodyPart messageBodyPart = new MimeBodyPart();
//            messageBodyPart.setContent(details.get("content"), "text/html");
//
//            MimeBodyPart messageBodyPart2 = new MimeBodyPart();
//            String filename = path + details.get("fileName");
//            DataSource source = new FileDataSource(filename);
//            messageBodyPart2.setDataHandler(new DataHandler(source));
//            messageBodyPart2.setFileName(details.get("fileName"));
//
//            Multipart multipart = new MimeMultipart();
//            multipart.addBodyPart(messageBodyPart);
//            multipart.addBodyPart(messageBodyPart2);
//
//            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("kiptoo.hesbon@ekenya.co.ke"));
//            generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("njiru.edwin@ekenya.co.ke"));
//            generateMailMessage.setSubject(details.get("subject"));
//            generateMailMessage.setContent(multipart);
//            Transport transport = getMailSession.getTransport("smtp");
//
//            transport.connect("smtp.gmail.com", "ibankdemo@gmail.com", "internet2015");
//            transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
//            transport.close();
//
//        } catch (MessagingException e) {
//            StringWriter sw = new StringWriter();
//            e.printStackTrace(new PrintWriter(sw));
//            ESBLog el = new ESBLog("SendmailException", sw.toString());
//            el.log();
//        }
//
//    }

    public void sendMail(Map<String, String> details) {
        try {
            mailServerProperties = System.getProperties();
            mailServerProperties.put("mail.debug", "false");
            mailServerProperties.put("mail.smtp.auth", "false");
            mailServerProperties.put("mail.transport.protocol", "smtp");
            mailServerProperties.put("mail.smtp.host", "192.168.0.39");
            getMailSession = Session.getDefaultInstance(mailServerProperties, null);
            generateMailMessage = new MimeMessage(getMailSession);

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(details.get("content"), "text/html");

            MimeBodyPart messageBodyPart2 = new MimeBodyPart();
            String filename = path + details.get("fileName");
            DataSource source = new FileDataSource(filename);
            messageBodyPart2.setDataHandler(new DataHandler(source));
            messageBodyPart2.setFileName(details.get("fileName"));

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            multipart.addBodyPart(messageBodyPart2);

            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("mcoopcashupgrade@ekenya.co.ke"));
            //generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("mmaina@co-opbank.co.ke "));
            generateMailMessage.setSubject(details.get("subject"));
            generateMailMessage.setContent(multipart);
            Transport transport = getMailSession.getTransport("smtp");

            transport.connect();
            transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
            transport.close();
            
            ESBLog el = new ESBLog("SentReport", details.get("fileName"));
            el.log();

        } catch (MessagingException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SendmailException", sw.toString());
            el.log();
        }

    }

}
