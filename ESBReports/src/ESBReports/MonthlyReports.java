/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESBReports;

import Database.DBFunctions;
import ESBLogEngine.ESBLog;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author hkiptoo
 */
public class MonthlyReports {

    String path = "/home/mvisa/ESBAPPLICATIONS/reports/";
    Utilities util;

    public MonthlyReports() {
        util = new Utilities();
    }

    public void processMonthlyReports() {
        Map<Integer, Map<String, String>> data = new HashMap();

        LocalDateTime date = LocalDateTime.now();
        if (date.getDayOfMonth() == 1) {
            //generate report of previous month every first day of the month
            generateReport();
        }

    }

    public void generateReport() {
        DBFunctions dbf = new DBFunctions();
        Map<Integer, Map<String, String>> data = new HashMap();
        Map<String, String> details = new HashMap();

        dbf.getMonthlySummary(data);
        LocalDateTime dateTime = LocalDateTime.now();

        if (!data.isEmpty()) {
            String filename = dateTime.minusMonths(1).getMonth() + "_MonthlyReport.xls";
            String[] header = getHeaders(data.get(0)).split(",");

            try (FileOutputStream fileOut = new FileOutputStream(path + filename);) {

                HSSFWorkbook workbook = new HSSFWorkbook();
                HSSFSheet sheet = workbook.createSheet("Sheet1");

                HSSFRow rowhead = sheet.createRow(0);
                for (int i = 0; i < header.length; i++) {
                    rowhead.createCell(i).setCellValue(header[i]);
                }

                int count = 1;
                int runningtotal = 0;
                for (int i = 0; i < data.size(); i++) {
                    Map<String, String> datarow = data.get(i);
                    HSSFRow row = sheet.createRow(count);
                    row.createCell(0).setCellValue(datarow.get(header[0]));
                    row.createCell(1).setCellValue(datarow.get(header[1]));
                    row.createCell(2).setCellValue(Integer.parseInt(datarow.get(header[2])));
                    runningtotal = runningtotal + Integer.parseInt(datarow.get(header[2]));
                    count++;
                }

                HSSFRow row = sheet.createRow(count);
                row.createCell(0).setCellValue("Total");
                row.createCell(2).setCellValue(runningtotal);

                workbook.write(fileOut);
                workbook.close();
                System.out.println("Your excel file has been generated!");

                details.put("content", LocalDateTime.now().minusMonths(1).getMonth().toString() + " REPORT");
                details.put("fileName", filename);
                details.put("subject", "Co-op Autogenerated Monthly Report");

                util.sendMail(details);

            } catch (Exception e) {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog("MonthlyReportGenerationException", sw.toString());
                el.log();
            }

        } else {
            System.out.println("No data found");
        }

    }

    public String getHeaders(Map<String, String> data) {
        String headers = "";

        for (Map.Entry<String, String> entry : data.entrySet()) {
            headers += entry.getKey() + ",";
        }
        return headers;
    }

}
