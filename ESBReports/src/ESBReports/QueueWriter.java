package ESBReports;

import ESBLogEngine.ESBLog;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class QueueWriter {

    // Defines the JNDI context factory.
    private final String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";
    // Defines the JMS context factory.
    // Defines the queue.
    private String QUEUE;
    private QueueConnectionFactory qconFactory;
    private QueueConnection qcon;
    private QueueSession qsession;
    private QueueSender qsender;
    private Queue queue;
    private TextMessage msg;
    private ObjectMessage objmsg;
    private MapMessage mapmsg;

    public QueueWriter() {
    }

    public QueueWriter(String QUEUE) {
        this.QUEUE = QUEUE;
        InitialContext ic = getInitialContext();
        init(ic, QUEUE);
    }

    public void init(Context ctx, String queueName) {
        QueueConnectionFactory qconFactory;
        Queue queue;
        try {
            qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
            qcon = qconFactory.createQueueConnection();
            qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            queue = (Queue) ctx.lookup(queueName);
            qsender = qsession.createSender(queue);
            msg = qsession.createTextMessage();
            objmsg = qsession.createObjectMessage();
            mapmsg = qsession.createMapMessage();
            qcon.start();
        } catch (Exception e) {
            ESBLog el = new ESBLog(e, "");
            el.log();
//System.out.println("init: "+e.getMessage());
        }
    }

    private InitialContext getInitialContext() {
        Hashtable env = new Hashtable();
        InitialContext ic = null;
        try {
            env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
            env.put(Context.PROVIDER_URL, Config.PROVIDER_URL.trim());
            ic = new InitialContext(env);
        } catch (NamingException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            e.printStackTrace();
        }
        return ic;
    }

    public void close() {
        try {
            qsender.close();
            qsession.close();
            qcon.close();
        } catch (JMSException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            e.printStackTrace();
        }
    }

    public boolean send(String message, String CorrelationID) {
        boolean sent = false;
        try {
            msg.setText(message);
            qsender.send(msg);
            sent = true;
        } catch (JMSException e) {
            ESBLog el = new ESBLog(e, "");
            el.log();
        }
        close();
        return sent;
    }

    public boolean sendObject(Map message, String CorrelationID, String biller) {
        boolean sent = false;
        try {

            objmsg.setStringProperty("biller", biller);
            objmsg.setJMSCorrelationID(CorrelationID);
            objmsg.setObject((HashMap) message);
            qsender.send(objmsg);
            sent = true;
            //System.out.println("Sent Successfully");
        } catch (JMSException e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));

        }
        close();
        return sent;
    }

}
