/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESBReports;

import java.io.FileInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author hkiptoo
 */
public class ESBReportsMain {

    public static Map<String, String> Configs = new HashMap();

    public static void main(String[] args) {
        //readPropertyFile(Configs);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt("06"));
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 0);

        Date alarmTime = calendar.getTime();
        if ((Calendar.getInstance()).getTime().after(alarmTime)) {
            calendar.add(Calendar.DATE, 1);
            alarmTime = calendar.getTime();
            System.out.println("Not yet run time");
        }
        (new Timer()).schedule(new dotask(), alarmTime, 1000 * 60 * 60 * 24);

    }

    public static class dotask extends TimerTask {

        @Override
        public void run() {
            try {
                (new DailyReports()).processDailyReports();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            try {
                (new MonthlyReports()).processMonthlyReports();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    public static Map<String, String> readPropertyFile(Map<String, String> configs) {
        Properties prop = new Properties();
        FileInputStream fip = null;
        try {
            //fip = new FileInputStream("/home/mvisa/ESBAPPLICATIONS/configs/config.properties");
            fip = new FileInputStream("config.properties");
            prop.load(fip);

            Set<String> propertyNames = prop.stringPropertyNames();
            for (String Property : propertyNames) {
                configs.put(Property.trim(), prop.getProperty(Property).trim());
            }
            //System.out.println(Configs.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (fip != null) {
                    fip.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return configs;
    }
}
