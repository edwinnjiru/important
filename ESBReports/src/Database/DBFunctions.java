package Database;

import ESBLogEngine.ESBLog;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import oracle.jdbc.OracleTypes;

public class DBFunctions {

    private DBconn DBconn;
    private CallableStatement callableStatement = null;

    public DBFunctions() {

    }

    private void getDBconn() {
        this.DBconn = new DBconn();
    }

    public boolean getValidateGL(String glCode) {
        getDBconn();
        boolean isGL = false;
        try {
            callableStatement = DBconn.conn.prepareCall("{call SP_VALIDATEGLACCOUNT(?,?)}");
            callableStatement.setString("iv_glCode", glCode);
            callableStatement.registerOutParameter("cv_1", OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            ResultSet rs = (ResultSet) callableStatement.getObject("cv_1");
            while (rs.next()) {
                String ifGL = rs.getString(1);
                if (("1").equalsIgnoreCase(ifGL.trim())) {
                    isGL = true;
                }
            }
            callableStatement.close();
        } catch (Exception e) {
            ESBLog el = new ESBLog(e, "");
            el.log();
            System.out.println(e.getMessage());
        } finally {
            DBconn.close();
        }
        return isGL;
    }

    public Map<Integer, Map<String, String>> getDailySummary(Map<Integer, Map<String, String>> data) {
        getDBconn();
        Map<String, String> singleRowsdata;
        String query = "SELECT count(FIELD98) AS TRANSACTIONS,FIELD98 AS TRANSACTIONTYPE,RESPONSE FROM "
                + "(SELECT FIELD98,CASE WHEN FIELD39 = '000' THEN 'SUCCESS' ELSE 'FAILURE' END AS RESPONSE,FIELD38 FROM TBMESSAGES_EXTERNAL WHERE trunc(REQUESTTIME_CHANNEL)=trunc(sysdate-1) GROUP BY FIELD98,FIELD39,FIELD38) GROUP BY FIELD98,RESPONSE";
        try (Statement stm = DBconn.conn.createStatement();
                ResultSet rs = stm.executeQuery(query)) {
            int counter = 0;
            while (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int columnCount = rsmd.getColumnCount();
                singleRowsdata = new HashMap();
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = rsmd.getColumnName(i);
                    String columnValue = rs.getString(columnName);
                    columnValue = columnValue == null ? "" : columnValue;  //JSON doesn't allow pupulating Null values
                    singleRowsdata.put(columnName.toUpperCase(), columnValue);
                }
                data.put(counter, singleRowsdata);
                counter += 1;
            }

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("getDailyException", sw.toString());
            el.log();
        } finally {
            DBconn.close();
        }

        return data;
    }

    public Map<Integer, Map<String, String>> getMonthlySummary(Map<Integer, Map<String, String>> data) {
        getDBconn();
        Map<String, String> singleRowsdata;
        String query = "SELECT count(FIELD98) AS TRANSACTIONS,FIELD98 AS TRANSACTIONTYPE,RESPONSE FROM "
                + "(SELECT FIELD98,CASE WHEN FIELD39 = '000' THEN 'SUCCESS' ELSE 'FAILURE' END AS RESPONSE,FIELD38 FROM TBMESSAGES_EXTERNAL WHERE REQUESTTIME_CHANNEL >= add_months(trunc(sysdate,'mm'),-1) \n"
                + " and REQUESTTIME_CHANNEL < trunc(sysdate, 'mm')  GROUP BY FIELD98,FIELD39,FIELD38) GROUP BY FIELD98,RESPONSE";
        try (Statement stm = DBconn.conn.createStatement();
                ResultSet rs = stm.executeQuery(query)) {
            int counter = 0;
            while (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int columnCount = rsmd.getColumnCount();
                singleRowsdata = new HashMap();
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = rsmd.getColumnName(i);
                    String columnValue = rs.getString(columnName);
                    columnValue = columnValue == null ? "" : columnValue;  //JSON doesn't allow pupulating Null values
                    singleRowsdata.put(columnName.toUpperCase(), columnValue);
                }
                data.put(counter, singleRowsdata);
                counter += 1;
            }

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("getMonthlyException", sw.toString());
            el.log();
        } finally {
            DBconn.close();
        }

        return data;
    }
}
