package Database;

import ESBLogEngine.ESBLog;
import ESBReports.Config;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;

public class DBconn {

    public Connection conn = null;
    public Statement stmt = null;
    public ResultSet rs = null;
    public Context ctx = null;

    public DBconn() {
        Hashtable ht = new Hashtable();
        ht.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        String prov_url=Config.PROVIDER_URL;
        //String prov_url="t3://localhost:7001";
        ht.put(Context.PROVIDER_URL, prov_url);
        try {
            ctx = new InitialContext(ht);
            //javax.sql.DataSource ds = (javax.sql.DataSource) ctx.lookup(Configs.get("DATASOURCE_NAME"));
//            String dsource="jdbc/ECONNECTESB";
//            javax.sql.DataSource ds = (javax.sql.DataSource) ctx.lookup(dsource);
            javax.sql.DataSource ds = (javax.sql.DataSource) ctx.lookup(Config.DATASOURCE_NAME);
            this.conn = ds.getConnection();
            this.stmt = this.conn.createStatement();
        } catch (Exception e) {
            ESBLog el = new ESBLog(e,"");
            el.logfile();
        }
    }

    /**
     * function close - Closes JDBC objects
     */
    public void close() {
        try {
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
            //Close context
            ctx.close();
        } catch (Exception e) {
            ESBLog el = new ESBLog(e,"");
            el.logfile();
            System.out.println(e.getMessage());
        }
    }

}
