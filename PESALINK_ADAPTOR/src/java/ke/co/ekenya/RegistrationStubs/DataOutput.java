
package ke.co.ekenya.RegistrationStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Register/1.0/Customer.register}registerOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "registerOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Customer/Service/Customer/Register/1.0/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Register/1.0/Customer.register")
    protected RegisterOutputType registerOutput;

    /**
     * Gets the value of the registerOutput property.
     * 
     * @return
     *     possible object is
     *     {@link RegisterOutputType }
     *     
     */
    public RegisterOutputType getRegisterOutput() {
        return registerOutput;
    }

    /**
     * Sets the value of the registerOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegisterOutputType }
     *     
     */
    public void setRegisterOutput(RegisterOutputType value) {
        this.registerOutput = value;
    }

}
