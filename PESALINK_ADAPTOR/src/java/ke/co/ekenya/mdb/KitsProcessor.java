package ke.co.ekenya.mdb;

import commonj.work.Work;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Response_Codes_ISO87;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author Ngari Josh
 */
public class KitsProcessor implements Work {

    private Message message;

    /*JMS message containing the transaction fields*/

    /**
     * Class constructor
     *
     * @param message - JMS Object Message fetched from Request Queue
     */
    public KitsProcessor(Message message) {
        this.message = message;
    }

    @Override
    public void run() {

        HashMap<String, String> requestMap = new HashMap();
        /* Hashmap of transaction fields*/
        Map<String, String> configs = KitsMDB.Configs;
        try {

            if (message instanceof ObjectMessage) {
                //Log ESB request

                requestMap = (HashMap) (((ObjectMessage) message).getObject());
                requestMap.put("CorrelationID", message.getJMSCorrelationID());
                ESBLog el22 = new ESBLog("RawRequests", "RequestMap to process:" + message);
                el22.log();

                MainValidator mainValidator = new MainValidator(requestMap, message.getJMSCorrelationID(), configs);
                mainValidator.process();

            } else {

                HashMap<String, String> messageMap = new HashMap();
                String msgStat = "FAILURE";

                messageMap.put("39", Response_Codes_ISO87.HOST_CONNECTION_DOWN);
                messageMap.put("48", "INVALID MESSAGE TYPE");
                messageMap.put("MSGSTAT", msgStat);
                messageMap.put("direction", "response");

                //Write failure response back to ESB response queue
                QueueWriter qw = new QueueWriter(KitsMDB.Configs.get("ADAPTOR_RESPONSE_QUEUE"));
                qw.sendObject(messageMap, messageMap.get("37"));

                //Log response sent back to ESB
                ESBLog el3 = new ESBLog("ReponsesToESB", Utilities.logPreString() + "Response sent back to ESB:"
                        + messageMap);
                el3.log();

            }
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("GeneralExceptions", Utilities.logPreString() + "Error:" + sw.toString());
            el.log();
        }
    }

    @Override
    public void release() {
    }

    @Override
    public boolean isDaemon() {
        return false;
    }

}
