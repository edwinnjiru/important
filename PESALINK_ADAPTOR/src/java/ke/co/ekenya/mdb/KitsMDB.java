/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.mdb;

import commonj.work.WorkManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.CommonOperations.WorkManagerUtilities;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author njiru
 */
@MessageDriven(mappedName = "jms/ADAPTOR_REQUEST_QUEUE", activationConfig = {
    @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "durable")
    ,
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
    ,
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
    ,
    @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "biller = 'PESALINK_FT' or biller='PESALINK_REG' or biller='PESALINK_INQ' or biller='PESALINK_DEL' or biller='PESALINK_SETACC' or biller='PESALINK_UPDATE' or biller='PESALINK_FT_TIMEOUTS'")
    ,
    @ActivationConfigProperty(propertyName = "connectionFactoryLookup", propertyValue = "jms/ECONNECT_JMSFACTORY")
})
public class KitsMDB implements MessageListener {

    public static Map<String, String> Configs = new HashMap();

    public KitsMDB() {
    }

    @Override
    public void onMessage(Message message) {
        
        WorkManager wm = WorkManagerUtilities.getWorkManager();
        KitsProcessor kitsProcessor = new KitsProcessor(message);

        try {
            wm.schedule(kitsProcessor);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("MDBExceptions", Utilities.logPreString()
                    + "Exception occured while initializing work manager|" + "\nError:" + sw.toString());

            el.logfile();
        }
    }

    @PostConstruct
    public void initialize() {
        
        readPropertyFile(Configs);
        ESBLog logger=new ESBLog("ReadConfigs", Configs.toString());
        logger.log();
    }

    @PreDestroy
    public void release() {
        // Destruction logic
    }

    /**
     * Reads the configuration file in file system and stores it in hash map
     *
     * @param configs - HashMap to store configuration information
     */
    public static void readPropertyFile(Map<String, String> configs) {
        InitialContext initialContext = null;
        try {
            initialContext = new InitialContext();
            NamingEnumeration<NameClassPair> list = initialContext.list("configs");
            while (list.hasMoreElements()) {
                String key = list.next().getName();
                String value = "";
                try {
                    value = (String) initialContext.lookup("configs/" + key);
                    configs.put(key.trim(), value.trim());
                } catch (Exception ex) {
                }
            }
            initialContext.close();
        } catch (Exception ex) {
            ESBLog el = new ESBLog(ex, "");
            el.log();
        } finally {
            try {
                if (initialContext != null) {
                    initialContext.close();
                }
            } catch (Exception ex) {
                ESBLog el = new ESBLog(ex, "");
                el.log();
            }
        }
    }
}
