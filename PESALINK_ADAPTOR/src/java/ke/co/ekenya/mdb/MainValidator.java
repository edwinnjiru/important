/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.mdb;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import ke.co.ekenya.AccountSet.KitsSetAccount;
import ke.co.ekenya.CommonOperations.DistributedWebLogicQueueBrowser;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Response_Codes_ISO87;
import ke.co.ekenya.Database.DBFunctions;
import ke.co.ekenya.Deletion.KitsDeletion;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.Registration.KitsRegistration;
import ke.co.ekenya.TransactionsToSOA.SendToAccount;
import ke.co.ekenya.TransactionsToSOA.SendToCard;
import ke.co.ekenya.TransactionsToSOA.SendToPhone;
import ke.co.ekenya.Update.KitsUpdate;
import ke.co.ekenya.populate.KitsCustomerPopulate;

/**
 *
 * @author Njiru
 */
public class MainValidator {

    Map<String, String> Configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    public String CorrelationID;
    DBFunctions dbFuntions;
    Map<String, String> esbMap = new HashMap();
    DistributedWebLogicQueueBrowser queueBrowser = null;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    public MainValidator(Map<String, String> xmlmap, String CorrelationID, Map<String, String> Configs) {
        this.xmlmap = xmlmap;
        this.CorrelationID = CorrelationID;
        this.Configs = Configs;
        this.dbFuntions = new DBFunctions();
        this.queueBrowser = new DistributedWebLogicQueueBrowser();
    }

    public void process() {
        String biller = xmlmap.get("98").toLowerCase();
        String corrId = "";
        StringBuilder ecBuilder = new StringBuilder();
        switch (biller) {
            case "pesalink_ft":
                if (xmlmap.containsKey("biller")) {
                    if ("PESALINK_FT_TIMEOUTS".equalsIgnoreCase(xmlmap.get("biller"))) {
                        //reversal to kba
                        xmlmap.put("MTI", "1420");
                        xmlmap.put("narration", "");
                        xmlmap.put("processed", "0");

                        transaction();

                        //update original transaction that is has timed out
                        dbFuntions.updatetran(xmlmap.get("37"), xmlmap.get("11"), "777", (new StringBuilder("Transaction Timed out at ")).append(new Date()).toString(), "1200");
                    } else {
                        ESBLog lot = new ESBLog("UnknownTimeouts", xmlmap.toString());
                        lot.log();
                    }
                } else {
                    //write to holding queue
                    LocalDateTime date = LocalDateTime.now();

                    xmlmap.put("12", date.format(formatter));
                    xmlmap.put("POSTED_TO_ADAPTER", "1");
                    xmlmap.remove("EXTRACHARGE_POSTED");
                    xmlmap.remove("DESTINATIONHOST");
                    xmlmap.put("HOLDING_QUEUE", "KITS_HOLDING_QUEUE");

                    if ((xmlmap.containsKey("ISDEBITCARD") && xmlmap.get("online").equals("0")) || ("ZZ".equals(xmlmap.get("24")))) {
                        xmlmap.put("host", "BFUB");
                    }
                    if (Integer.parseInt(xmlmap.get("4")) > 500) {
                        xmlmap.put("POST_EXTRACHARGE", "1");
                    } else {
                        xmlmap.put("EXTRACHARGE", "0");
                    }

                    if (xmlmap.containsKey("RTPS_REF")) {
                        if (xmlmap.get("RTPS_REF").length() > 0) {
                            xmlmap.put("kref", xmlmap.get("RTPS_REF"));
                            xmlmap.put("30", xmlmap.get("RTPS_REF"));
                            corrId = ecBuilder.append("EC").append(xmlmap.get("kref")).toString();
                        } else {
                            xmlmap.put("kref", CorrelationID.substring(2));
                            corrId = CorrelationID;
                        }

                    } else {
                        //to be removed once EC issue is resolved
                        xmlmap.put("kref", CorrelationID.substring(2));
                        corrId = CorrelationID;
                    }

                    QueueWriter qr = new QueueWriter("jms/KITS_HOLDING_QUEUE");//write to holding queue for timeouts
                    boolean sent = qr.sendObject(xmlmap, corrId);
                    ESBLog logger = new ESBLog("ToKitsHoldingQueue", "Sent: " + sent + " Map: " + xmlmap);
                    logger.log();

                    xmlmap.put("MTI", "1200");
                    xmlmap.put("narration", null);
                    xmlmap.put("processed", "0");

                    transaction();
                }
                break;
            case "pesalink_inq":
                KitsCustomerPopulate kpop = new KitsCustomerPopulate(xmlmap, CorrelationID, Configs);
                kpop.doProcessing();
                break;
            case "pesalink_reg":
                KitsRegistration kreg = new KitsRegistration(xmlmap, CorrelationID, Configs);
                kreg.doProcessing();
                break;
            case "pesalink_del":
                KitsDeletion kdel = new KitsDeletion(xmlmap, CorrelationID, Configs);
                kdel.doProcessing();
                break;
            case "pesalink_update":
                KitsUpdate kupd = new KitsUpdate(xmlmap, CorrelationID, Configs);
                kupd.doProcessing();
                break;
            case "pesalink_setacc":
                KitsSetAccount ksa = new KitsSetAccount(xmlmap, CorrelationID, Configs);
                ksa.doProcessing();
                break;
            default:
                ESBLog lg = new ESBLog("InvalidTran", "Unhandled field98: " + xmlmap.toString());
                lg.log();
                break;
        }

    }

    public void transaction() {
        int insert = 0;
        String trantype = xmlmap.get("74");
        switch (trantype) {
            case "SENDTOPHONE":
                insert = dbFuntions.insertTransaction(xmlmap);
                if (insert == 1) {
                    SendToPhone stp = new SendToPhone(xmlmap, CorrelationID, Configs);
                    stp.doProcessing();
                } else {
                    esbMap = queueBrowser.getWeblogicMessageFromUDQueue(CorrelationID);
                    esbMap.put("REVERSE", "1");
                    esbMap.put("39", Response_Codes_ISO87.DATABASE_ERROR);
                    esbMap.put("48", "DB insert failed");

                    QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
                    boolean sent = qr.sendObject(esbMap, CorrelationID, "PESALINK_FT");
                    ESBLog logg = new ESBLog("ResponsetoESB", "Sent: " + sent + " Map: " + esbMap);
                    logg.log();
                }
                break;
            case "SENDTOCARD":
                insert = dbFuntions.insertTransaction(xmlmap);
                if (insert == 1) {
                    SendToCard stc = new SendToCard(xmlmap, CorrelationID, Configs);
                    stc.doProcessing();
                } else {
                    esbMap = queueBrowser.getWeblogicMessageFromUDQueue(CorrelationID);
                    esbMap.put("REVERSE", "1");
                    esbMap.put("39", Response_Codes_ISO87.DATABASE_ERROR);
                    esbMap.put("48", "DB insert failed");

                    QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
                    boolean sent = qr.sendObject(esbMap, CorrelationID, "PESALINK_FT");
                    ESBLog logg = new ESBLog("ResponsetoESB", "Sent: " + sent + " Map: " + esbMap);
                    logg.log();
                }

                break;
            case "SENDTOACCOUNT":
                insert = dbFuntions.insertTransaction(xmlmap);
                if (insert == 1) {
                    SendToAccount sta = new SendToAccount(xmlmap, CorrelationID, Configs);
                    sta.doProcessing();
                } else {
                    esbMap = queueBrowser.getWeblogicMessageFromUDQueue(CorrelationID);
                    esbMap.put("REVERSE", "1");
                    esbMap.put("39", Response_Codes_ISO87.DATABASE_ERROR);
                    esbMap.put("48", "DB insert failed");

                    QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
                    boolean sent = qr.sendObject(esbMap, CorrelationID, "PESALINK_FT");
                    ESBLog logg = new ESBLog("ResponsetoESB", "Sent: " + sent + " Map: " + esbMap);
                    logg.log();
                }
                break;
            default:
                ESBLog lg = new ESBLog("InvalidTransactions", "Unhandled field74: " + xmlmap.toString());
                lg.log();
                break;
        }

    }

}
