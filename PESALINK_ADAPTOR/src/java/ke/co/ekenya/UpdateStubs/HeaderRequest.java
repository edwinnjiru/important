
package ke.co.ekenya.UpdateStubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MessageID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorrelationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreationTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ReplyTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaultTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Credentials" type="{urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader}Credentials"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "messageID",
    "correlationID",
    "creationTimestamp",
    "replyTO",
    "faultTO",
    "credentials"
})
@XmlRootElement(name = "HeaderRequest")
public class HeaderRequest {

    @XmlElement(name = "MessageID", required = true)
    protected String messageID;
    @XmlElementRef(name = "CorrelationID", namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<String> correlationID;
    @XmlElementRef(name = "CreationTimestamp", namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> creationTimestamp;
    @XmlElementRef(name = "ReplyTO", namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<String> replyTO;
    @XmlElementRef(name = "FaultTO", namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<String> faultTO;
    @XmlElement(name = "Credentials", required = true)
    protected Credentials credentials;

    /**
     * Gets the value of the messageID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageID() {
        return messageID;
    }

    /**
     * Sets the value of the messageID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageID(String value) {
        this.messageID = value;
    }

    /**
     * Gets the value of the correlationID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCorrelationID() {
        return correlationID;
    }

    /**
     * Sets the value of the correlationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCorrelationID(JAXBElement<String> value) {
        this.correlationID = value;
    }

    /**
     * Gets the value of the creationTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getCreationTimestamp() {
        return creationTimestamp;
    }

    /**
     * Sets the value of the creationTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setCreationTimestamp(JAXBElement<XMLGregorianCalendar> value) {
        this.creationTimestamp = value;
    }

    /**
     * Gets the value of the replyTO property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReplyTO() {
        return replyTO;
    }

    /**
     * Sets the value of the replyTO property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReplyTO(JAXBElement<String> value) {
        this.replyTO = value;
    }

    /**
     * Gets the value of the faultTO property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFaultTO() {
        return faultTO;
    }

    /**
     * Sets the value of the faultTO property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFaultTO(JAXBElement<String> value) {
        this.faultTO = value;
    }

    /**
     * Gets the value of the credentials property.
     * 
     * @return
     *     possible object is
     *     {@link Credentials }
     *     
     */
    public Credentials getCredentials() {
        return credentials;
    }

    /**
     * Sets the value of the credentials property.
     * 
     * @param value
     *     allowed object is
     *     {@link Credentials }
     *     
     */
    public void setCredentials(Credentials value) {
        this.credentials = value;
    }

}
