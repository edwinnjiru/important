
package ke.co.ekenya.UpdateStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Update/1.1/Customer.update}updateOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Customer/Service/Customer/Update/1.1/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Update/1.1/Customer.update")
    protected UpdateOutputType updateOutput;

    /**
     * Gets the value of the updateOutput property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateOutputType }
     *     
     */
    public UpdateOutputType getUpdateOutput() {
        return updateOutput;
    }

    /**
     * Sets the value of the updateOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateOutputType }
     *     
     */
    public void setUpdateOutput(UpdateOutputType value) {
        this.updateOutput = value;
    }

}
