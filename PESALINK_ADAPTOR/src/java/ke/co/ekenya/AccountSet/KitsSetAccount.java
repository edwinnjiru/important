/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.AccountSet;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import ke.co.ekenya.AccountSetStubs.AccountType;
import ke.co.ekenya.AccountSetStubs.CardType;
import ke.co.ekenya.AccountSetStubs.CustomerType;
import ke.co.ekenya.AccountSetStubs.DataInput;
import ke.co.ekenya.AccountSetStubs.PortType;
import ke.co.ekenya.AccountSetStubs.ServiceStarter;
import ke.co.ekenya.AccountSetStubs.SetInputType;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.messagehandlers.setAccountHandler;
import ke.co.ekenya.messagehandlers.setAccountResolver;

/**
 *
 * @author njiru
 */
public class KitsSetAccount {

    Map<String, String> Configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    public String CorrelationID;
    EncryptionUtils enc = new EncryptionUtils();

    public KitsSetAccount(Map<String, String> xmlmap, String CorrelationID, Map<String, String> Configs) {
        this.Configs = Configs;
        this.xmlmap = xmlmap;
        this.CorrelationID = CorrelationID;
    }

    public void doProcessing() {
        sendtosoa();
    }

    public void sendtosoa() {
        String response = "";
        try {
            DataInput dataInput = new DataInput();
            SetInputType inputType = new SetInputType();
            AccountType accType = new AccountType();
            CardType cardType = new CardType();
            CustomerType cstType = new CustomerType();
            SetInputType.OperationParameters op_params = new SetInputType.OperationParameters();

            cstType.setIDNumber(xmlmap.get("71"));
            cstType.setIdentificationType(xmlmap.get("72"));
            cstType.setMobileNumber(xmlmap.get("2"));

            String email = xmlmap.get("74");
            if (email.equals("")) {
                email = "customerservice@co-operativebank.co.ke";
            }
            cstType.setEmailAddress(email);
            cstType.setFullName(xmlmap.get("73"));

            op_params.setDefaultRecord("0");
            op_params.setBankSortCode("40411000");

            accType.setAccountNumber(xmlmap.get("75"));

            String cardNumber = xmlmap.get("76");
            if (!cardNumber.equals("")) {
                GregorianCalendar gCal = new GregorianCalendar();
                gCal.setTime(new Date());
                XMLGregorianCalendar gDateUnformated = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal);
                cardType.setExpiryDate(gDateUnformated);
                cardType.setNumber(cardNumber);

            } else {
                cardType.setExpiryDate(null);

            }

            inputType.setAccount(accType);
            inputType.setCard(cardType);
            inputType.setCustomer(cstType);
            inputType.setOperationParameters(op_params);
            dataInput.setSetInput(inputType);

            String headerPassword = enc.decrypt(Configs.get("KITSHEADERPASS"));
            String proxypass = enc.decrypt(Configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + Configs.get("PESA_WSDLS_PATH") + "Customer_CustomerAccounts_set_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            setAccountHandler setHandler = new setAccountHandler(Configs, headerPassword, CorrelationID);
            setAccountResolver setResolver = new setAccountResolver(setHandler);
            service.setHandlerResolver(setResolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request
            BindingProvider prov = (BindingProvider) port;
            prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, Configs.get("SOA_USERNAME"));
            prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            //time-outs
            int connecttimeout = Integer.parseInt(Configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(Configs.get("READTIMEOUT"));
            prov.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            prov.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.set(dataInput);

            response = setHandler.getMyXML();
            breakDownSOAResponse(response);

        } catch (Exception ex) {
            String f48 = "";
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SOAExceptions", "Exception occured while while sending set account request to SOA.\nError:" + sw.toString());
            el.log();

            String msg = ex.getMessage();
            if (msg.contains("SocketTimeoutException") && msg.contains("HTTP")) {
                f48 = "Connect timed out";
            } else if (msg.contains("SocketTimeoutException")) {
                f48 = "Read timed out";
            } else if (msg.contains("500")) {
                f48 = "Internal Server Error";
            } else {
                f48 = "Generic Error";
            }

            xmlmap.put("39", "333");
            xmlmap.put("48", f48);
        }finally{
        QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
        boolean sent = qr.sendObject(xmlmap, CorrelationID);

        ESBLog lg = new ESBLog("ResponsetoESB", "Sent: " + sent + ", Map: " + xmlmap);
        lg.log();
        }
    }

    private void breakDownSOAResponse(String SOAResp) {
        //change this for set account
        HashMap result = new HashMap();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAResp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("SOAP-ENV:Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("ns:HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("ns:StatusMessages");
            result.put("statusDescription", (String) headerReply.get("ns:StatusDescription"));
            result.put("statusCode", (String) headerReply.get("ns:StatusCode"));
            result.put("statusDescriptionKey", (String) headerReply.get("ns:StatusDescriptionKey"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                result.put("messageID", (String) headerReply.get("ns:MessageID"));

                //status messages breakdown for header
                HashMap statusMessage = (HashMap<String, Object>) statusMessages.get("ns:StatusMessage");
                result.put("messageType", (String) statusMessage.get("ns:MessageType"));
                result.put("applicationID", (String) statusMessage.get("ns:ApplicationID"));
                result.put("messageCode", (String) statusMessage.get("ns:MessageCode"));

                //BreakDown Of Body
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("SOAP-ENV:Body");
                HashMap<String, Object> dataOutput = (HashMap<String, Object>) body.get("ns0:DataOutput");
                HashMap<String, Object> setOutput = (HashMap<String, Object>) dataOutput.get("ns1:setOutput");
                HashMap<String, Object> operationparmeters = (HashMap<String, Object>) setOutput.get("ns1:OperationParameters");
                result.put("RegistrationNumber", (String) operationparmeters.get("ns1:RegistrationNumber"));

                //put success here
                xmlmap.put("39", "000");
                xmlmap.put("48", "Set Successfully");

            } else {
                xmlmap.put("39", "333");
                xmlmap.put("48", "Set Account Unsuccessful");
            }
        } catch (Exception ex) {
            //System.out.println("Registration breakdown error: " + ex.getMessage());
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("BreakdownExceptions", "Exception occured while breaking down set account SOA resonse.\nError:" + sw.toString());
            el.log();

            xmlmap.put("39", "333");
            xmlmap.put("48", "Exception occured");
        }
    }
}
