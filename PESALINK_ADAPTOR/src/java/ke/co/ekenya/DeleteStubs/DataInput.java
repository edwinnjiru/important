
package ke.co.ekenya.DeleteStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete}deleteInput"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deleteInput"
})
@XmlRootElement(name = "DataInput", namespace = "urn://co-opbank.co.ke/Banking/Customer/Service/Customer/Delete/1.0/DataIO")
public class DataInput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete", required = true)
    protected DeleteInputType deleteInput;

    /**
     * Gets the value of the deleteInput property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteInputType }
     *     
     */
    public DeleteInputType getDeleteInput() {
        return deleteInput;
    }

    /**
     * Sets the value of the deleteInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteInputType }
     *     
     */
    public void setDeleteInput(DeleteInputType value) {
        this.deleteInput = value;
    }

}
