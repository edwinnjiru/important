
package ke.co.ekenya.DeleteStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete}deleteOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deleteOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Customer/Service/Customer/Delete/1.0/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete")
    protected DeleteOutputType deleteOutput;

    /**
     * Gets the value of the deleteOutput property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteOutputType }
     *     
     */
    public DeleteOutputType getDeleteOutput() {
        return deleteOutput;
    }

    /**
     * Sets the value of the deleteOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteOutputType }
     *     
     */
    public void setDeleteOutput(DeleteOutputType value) {
        this.deleteOutput = value;
    }

}
