
package ke.co.ekenya.DeleteStubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ke.co.ekenya.DeleteStubs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeleteOutput_QNAME = new QName("urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete", "deleteOutput");
    private final static QName _Address_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Address/1.0", "Address");
    private final static QName _Account_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0", "Account");
    private final static QName _DeleteInput_QNAME = new QName("urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete", "deleteInput");
    private final static QName _BusinessDetails_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/BusinessDetails/1.0", "BusinessDetails");
    private final static QName _Customer_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Customer/1.0", "Customer");
    private final static QName _Card_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Card/1.0", "Card");
    private final static QName _StatusMessageMessageDescription_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageDescription");
    private final static QName _StatusMessageMessageCode_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageCode");
    private final static QName _StatusMessageApplicationID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ApplicationID");
    private final static QName _StatusMessageMessageType_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageType");
    private final static QName _HeaderReplyStatusDescription_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusDescription");
    private final static QName _HeaderReplyStatusDescriptionKey_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusDescriptionKey");
    private final static QName _HeaderReplyStatusCode_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusCode");
    private final static QName _HeaderReplyMessageID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageID");
    private final static QName _HeaderReplyElapsedTime_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ElapsedTime");
    private final static QName _HeaderReplyCorrelationID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "CorrelationID");
    private final static QName _DeleteOutputTypeOperationParametersRegistrationNumber_QNAME = new QName("urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete", "RegistrationNumber");
    private final static QName _HeaderRequestCreationTimestamp_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "CreationTimestamp");
    private final static QName _HeaderRequestReplyTO_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ReplyTO");
    private final static QName _HeaderRequestFaultTO_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "FaultTO");
    private final static QName _CredentialsUsername_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Username");
    private final static QName _CredentialsRealm_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Realm");
    private final static QName _CredentialsPassword_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Password");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ke.co.ekenya.DeleteStubs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link HeaderReply }
     * 
     */
    public HeaderReply createHeaderReply() {
        return new HeaderReply();
    }

    /**
     * Create an instance of {@link DeleteOutputType }
     * 
     */
    public DeleteOutputType createDeleteOutputType() {
        return new DeleteOutputType();
    }

    /**
     * Create an instance of {@link DeleteInputType }
     * 
     */
    public DeleteInputType createDeleteInputType() {
        return new DeleteInputType();
    }

    /**
     * Create an instance of {@link AccountType }
     * 
     */
    public AccountType createAccountType() {
        return new AccountType();
    }

    /**
     * Create an instance of {@link DataInput }
     * 
     */
    public DataInput createDataInput() {
        return new DataInput();
    }

    /**
     * Create an instance of {@link DataOutput }
     * 
     */
    public DataOutput createDataOutput() {
        return new DataOutput();
    }

    /**
     * Create an instance of {@link CardType }
     * 
     */
    public CardType createCardType() {
        return new CardType();
    }

    /**
     * Create an instance of {@link BusinessDetailsType }
     * 
     */
    public BusinessDetailsType createBusinessDetailsType() {
        return new BusinessDetailsType();
    }

    /**
     * Create an instance of {@link CustomerType }
     * 
     */
    public CustomerType createCustomerType() {
        return new CustomerType();
    }

    /**
     * Create an instance of {@link HeaderRequest }
     * 
     */
    public HeaderRequest createHeaderRequest() {
        return new HeaderRequest();
    }

    /**
     * Create an instance of {@link Credentials }
     * 
     */
    public Credentials createCredentials() {
        return new Credentials();
    }

    /**
     * Create an instance of {@link HeaderReply.StatusMessages }
     * 
     */
    public HeaderReply.StatusMessages createHeaderReplyStatusMessages() {
        return new HeaderReply.StatusMessages();
    }

    /**
     * Create an instance of {@link StatusMessage }
     * 
     */
    public StatusMessage createStatusMessage() {
        return new StatusMessage();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link DeleteOutputType.OperationParameters }
     * 
     */
    public DeleteOutputType.OperationParameters createDeleteOutputTypeOperationParameters() {
        return new DeleteOutputType.OperationParameters();
    }

    /**
     * Create an instance of {@link DeleteInputType.OperationParameters }
     * 
     */
    public DeleteInputType.OperationParameters createDeleteInputTypeOperationParameters() {
        return new DeleteInputType.OperationParameters();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteOutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete", name = "deleteOutput")
    public JAXBElement<DeleteOutputType> createDeleteOutput(DeleteOutputType value) {
        return new JAXBElement<DeleteOutputType>(_DeleteOutput_QNAME, DeleteOutputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddressType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Address/1.0", name = "Address")
    public JAXBElement<AddressType> createAddress(AddressType value) {
        return new JAXBElement<AddressType>(_Address_QNAME, AddressType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0", name = "Account")
    public JAXBElement<AccountType> createAccount(AccountType value) {
        return new JAXBElement<AccountType>(_Account_QNAME, AccountType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete", name = "deleteInput")
    public JAXBElement<DeleteInputType> createDeleteInput(DeleteInputType value) {
        return new JAXBElement<DeleteInputType>(_DeleteInput_QNAME, DeleteInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessDetailsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/BusinessDetails/1.0", name = "BusinessDetails")
    public JAXBElement<BusinessDetailsType> createBusinessDetails(BusinessDetailsType value) {
        return new JAXBElement<BusinessDetailsType>(_BusinessDetails_QNAME, BusinessDetailsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Customer/1.0", name = "Customer")
    public JAXBElement<CustomerType> createCustomer(CustomerType value) {
        return new JAXBElement<CustomerType>(_Customer_QNAME, CustomerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Card/1.0", name = "Card")
    public JAXBElement<CardType> createCard(CardType value) {
        return new JAXBElement<CardType>(_Card_QNAME, CardType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageDescription", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageDescription(String value) {
        return new JAXBElement<String>(_StatusMessageMessageDescription_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageCode", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageCode(String value) {
        return new JAXBElement<String>(_StatusMessageMessageCode_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ApplicationID", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageApplicationID(String value) {
        return new JAXBElement<String>(_StatusMessageApplicationID_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageType", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageType(String value) {
        return new JAXBElement<String>(_StatusMessageMessageType_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusDescription", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusDescription(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusDescription_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusDescriptionKey", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusDescriptionKey(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusDescriptionKey_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusCode", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusCode(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusCode_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageID", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyMessageID(String value) {
        return new JAXBElement<String>(_HeaderReplyMessageID_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ElapsedTime", scope = HeaderReply.class)
    public JAXBElement<Long> createHeaderReplyElapsedTime(Long value) {
        return new JAXBElement<Long>(_HeaderReplyElapsedTime_QNAME, Long.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CorrelationID", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyCorrelationID(String value) {
        return new JAXBElement<String>(_HeaderReplyCorrelationID_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete", name = "RegistrationNumber", scope = DeleteOutputType.OperationParameters.class)
    public JAXBElement<String> createDeleteOutputTypeOperationParametersRegistrationNumber(String value) {
        return new JAXBElement<String>(_DeleteOutputTypeOperationParametersRegistrationNumber_QNAME, String.class, DeleteOutputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CreationTimestamp", scope = HeaderRequest.class)
    public JAXBElement<XMLGregorianCalendar> createHeaderRequestCreationTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_HeaderRequestCreationTimestamp_QNAME, XMLGregorianCalendar.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ReplyTO", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestReplyTO(String value) {
        return new JAXBElement<String>(_HeaderRequestReplyTO_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "FaultTO", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestFaultTO(String value) {
        return new JAXBElement<String>(_HeaderRequestFaultTO_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CorrelationID", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestCorrelationID(String value) {
        return new JAXBElement<String>(_HeaderReplyCorrelationID_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Username", scope = Credentials.class)
    public JAXBElement<String> createCredentialsUsername(String value) {
        return new JAXBElement<String>(_CredentialsUsername_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Realm", scope = Credentials.class)
    public JAXBElement<String> createCredentialsRealm(String value) {
        return new JAXBElement<String>(_CredentialsRealm_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Password", scope = Credentials.class)
    public JAXBElement<String> createCredentialsPassword(String value) {
        return new JAXBElement<String>(_CredentialsPassword_QNAME, String.class, Credentials.class, value);
    }

}
