
package ke.co.ekenya.DeleteStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteInputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteInputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperationParameters" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="BankSortCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DefaultRecord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Customer/1.0}Customer" minOccurs="0"/>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0}Account" minOccurs="0"/>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Card/1.0}Card" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteInputType", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete", propOrder = {
    "operationParameters",
    "customer",
    "account",
    "card"
})
public class DeleteInputType {

    @XmlElement(name = "OperationParameters")
    protected DeleteInputType.OperationParameters operationParameters;
    @XmlElement(name = "Customer", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Customer/1.0")
    protected CustomerType customer;
    @XmlElement(name = "Account", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Account/1.0")
    protected AccountType account;
    @XmlElement(name = "Card", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Card/1.0")
    protected CardType card;

    /**
     * Gets the value of the operationParameters property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteInputType.OperationParameters }
     *     
     */
    public DeleteInputType.OperationParameters getOperationParameters() {
        return operationParameters;
    }

    /**
     * Sets the value of the operationParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteInputType.OperationParameters }
     *     
     */
    public void setOperationParameters(DeleteInputType.OperationParameters value) {
        this.operationParameters = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerType }
     *     
     */
    public CustomerType getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerType }
     *     
     */
    public void setCustomer(CustomerType value) {
        this.customer = value;
    }

    /**
     * Gets the value of the account property.
     * 
     * @return
     *     possible object is
     *     {@link AccountType }
     *     
     */
    public AccountType getAccount() {
        return account;
    }

    /**
     * Sets the value of the account property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountType }
     *     
     */
    public void setAccount(AccountType value) {
        this.account = value;
    }

    /**
     * Gets the value of the card property.
     * 
     * @return
     *     possible object is
     *     {@link CardType }
     *     
     */
    public CardType getCard() {
        return card;
    }

    /**
     * Sets the value of the card property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardType }
     *     
     */
    public void setCard(CardType value) {
        this.card = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="BankSortCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DefaultRecord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bankSortCode",
        "defaultRecord"
    })
    public static class OperationParameters {

        @XmlElement(name = "BankSortCode", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete")
        protected String bankSortCode;
        @XmlElement(name = "DefaultRecord", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/Customer/Delete/1.0/Customer.delete")
        protected String defaultRecord;

        /**
         * Gets the value of the bankSortCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBankSortCode() {
            return bankSortCode;
        }

        /**
         * Sets the value of the bankSortCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBankSortCode(String value) {
            this.bankSortCode = value;
        }

        /**
         * Gets the value of the defaultRecord property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefaultRecord() {
            return defaultRecord;
        }

        /**
         * Sets the value of the defaultRecord property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefaultRecord(String value) {
            this.defaultRecord = value;
        }

    }

}
