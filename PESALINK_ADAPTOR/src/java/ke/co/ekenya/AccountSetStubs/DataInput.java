
package ke.co.ekenya.AccountSetStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerAccounts/Set/1.0/CustomerAccounts.set}setInput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setInput"
})
@XmlRootElement(name = "DataInput", namespace = "urn://co-opbank.co.ke/Banking/Customer/Service/CustomerAccounts/Set/1.0/DataIO")
public class DataInput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerAccounts/Set/1.0/CustomerAccounts.set")
    protected SetInputType setInput;

    /**
     * Gets the value of the setInput property.
     * 
     * @return
     *     possible object is
     *     {@link SetInputType }
     *     
     */
    public SetInputType getSetInput() {
        return setInput;
    }

    /**
     * Sets the value of the setInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link SetInputType }
     *     
     */
    public void setSetInput(SetInputType value) {
        this.setInput = value;
    }

}
