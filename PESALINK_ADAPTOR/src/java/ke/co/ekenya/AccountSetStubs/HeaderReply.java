
package ke.co.ekenya.AccountSetStubs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MessageID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CorrelationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ElapsedTime" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusDescriptionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusMessages">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="StatusMessage" type="{urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader}StatusMessage" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "messageID",
    "correlationID",
    "elapsedTime",
    "statusCode",
    "statusDescription",
    "statusDescriptionKey",
    "statusMessages"
})
@XmlRootElement(name = "HeaderReply")
public class HeaderReply {

    @XmlElementRef(name = "MessageID", namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<String> messageID;
    @XmlElementRef(name = "CorrelationID", namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<String> correlationID;
    @XmlElementRef(name = "ElapsedTime", namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> elapsedTime;
    @XmlElementRef(name = "StatusCode", namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<String> statusCode;
    @XmlElementRef(name = "StatusDescription", namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<String> statusDescription;
    @XmlElementRef(name = "StatusDescriptionKey", namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<String> statusDescriptionKey;
    @XmlElement(name = "StatusMessages", required = true)
    protected HeaderReply.StatusMessages statusMessages;

    /**
     * Gets the value of the messageID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMessageID() {
        return messageID;
    }

    /**
     * Sets the value of the messageID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMessageID(JAXBElement<String> value) {
        this.messageID = value;
    }

    /**
     * Gets the value of the correlationID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCorrelationID() {
        return correlationID;
    }

    /**
     * Sets the value of the correlationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCorrelationID(JAXBElement<String> value) {
        this.correlationID = value;
    }

    /**
     * Gets the value of the elapsedTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getElapsedTime() {
        return elapsedTime;
    }

    /**
     * Sets the value of the elapsedTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setElapsedTime(JAXBElement<Long> value) {
        this.elapsedTime = value;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStatusCode(JAXBElement<String> value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the statusDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStatusDescription() {
        return statusDescription;
    }

    /**
     * Sets the value of the statusDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStatusDescription(JAXBElement<String> value) {
        this.statusDescription = value;
    }

    /**
     * Gets the value of the statusDescriptionKey property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStatusDescriptionKey() {
        return statusDescriptionKey;
    }

    /**
     * Sets the value of the statusDescriptionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStatusDescriptionKey(JAXBElement<String> value) {
        this.statusDescriptionKey = value;
    }

    /**
     * Gets the value of the statusMessages property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderReply.StatusMessages }
     *     
     */
    public HeaderReply.StatusMessages getStatusMessages() {
        return statusMessages;
    }

    /**
     * Sets the value of the statusMessages property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderReply.StatusMessages }
     *     
     */
    public void setStatusMessages(HeaderReply.StatusMessages value) {
        this.statusMessages = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="StatusMessage" type="{urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader}StatusMessage" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "statusMessage"
    })
    public static class StatusMessages {

        @XmlElement(name = "StatusMessage", nillable = true)
        protected List<StatusMessage> statusMessage;

        /**
         * Gets the value of the statusMessage property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the statusMessage property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getStatusMessage().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link StatusMessage }
         * 
         * 
         */
        public List<StatusMessage> getStatusMessage() {
            if (statusMessage == null) {
                statusMessage = new ArrayList<StatusMessage>();
            }
            return this.statusMessage;
        }

    }

}
