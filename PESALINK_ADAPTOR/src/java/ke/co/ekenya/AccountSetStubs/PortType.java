
package ke.co.ekenya.AccountSetStubs;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebService(name = "PortType", targetNamespace = "http://co-opbank.co.ke/Banking/Customer/Service/CustomerAccounts/Set/1.0/WSDL/setImpl/BusinessDomains/_000__CUSTOMER/Services/CustomerAccounts/_1.0/set")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface PortType {


    /**
     * 
     * @param input
     * @return
     *     returns ke.co.ekenya.AccountSetStubs.DataOutput
     */
    @WebMethod(action = "/co-opbank.co.ke/Banking/Customer/Service/CustomerAccounts/Set/1.0")
    @WebResult(name = "DataOutput", targetNamespace = "urn://co-opbank.co.ke/Banking/Customer/Service/CustomerAccounts/Set/1.0/DataIO", partName = "output")
    public DataOutput set(
        @WebParam(name = "DataInput", targetNamespace = "urn://co-opbank.co.ke/Banking/Customer/Service/CustomerAccounts/Set/1.0/DataIO", partName = "input")
        DataInput input);

}
