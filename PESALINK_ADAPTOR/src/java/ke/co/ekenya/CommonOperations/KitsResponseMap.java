/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.CommonOperations;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Njiru
 */
public class KitsResponseMap {

    public KitsResponseMap() {

    }

    public static String response(String code) {
        Map<String, String> responses = new HashMap();
        String description="";
        try {
            responses.put("915", "Reconciliation cutout");
            responses.put("914", "Not able to trace back to original transaction");
            responses.put("913", "Duplicate transmission");
            responses.put("912", "Card issuer unavailable");
            responses.put("911", "Card issuer timedout");
            responses.put("910", "Card issuer signed off");
            responses.put("909", "System malfunction");
            responses.put("908", "Transaction destination cannot be found for routing");
            responses.put("907", "Card issuer or switch inoperative");
            responses.put("906", "Cutover in process");
            responses.put("905", "Acquirer not supported by switch");
            responses.put("904", "Format Error");
            responses.put("903", "Invalid Transaction");
            responses.put("902", "Tnvalid Transaction");
            responses.put("923", "Request in progress");
            responses.put("107", "Refer to card issuer");
            responses.put("106", "Allowable PIN tries exceeded");
            responses.put("105", "Card acceptor 's security department");
            responses.put("104", "Restricted card");
            responses.put("103", "Card acceptor contact acquirer");
            responses.put("102", "Suspected fraud");
            responses.put("101", "Expired card");
            responses.put("100", "General no comments");
            responses.put("950", "Violation of business arrangement");
            responses.put("922", "Message number out of sequence");
            responses.put("921", "Security software/hardware error - no action");
            responses.put("920", "Security software/hardware error - try again");
            responses.put("919", "Encryption key sync error");
            responses.put("918", "No communication key available for use");
            responses.put("917", "MAC Key sync error");
            responses.put("916", "MAC Incorrect");
            responses.put("108", "refer to card issuer's special conditions");
            responses.put("109", "invalid merchant");
            responses.put("110", "invalid amount");
            responses.put("111", "invalid card number");
            responses.put("112", "PIN data required");
            responses.put("113", "unacceptable fee");
            responses.put("114", "no account of type requested");
            responses.put("115", "requested function not supported");
            responses.put("116", "no sufficient funds");
            responses.put("117", "incorrect PIN");
            responses.put("118", "no card record");
            responses.put("119", "transaction not permitted to cardholder");
            responses.put("120", "transaction not permitted to terminal");
            responses.put("121", "exceeds withdrawal amount limit");
            responses.put("122", "security violation");
            responses.put("123", "exceeds withdrawal frequency limit");
            responses.put("124", "violation of law");
            responses.put("125", "card not effective");
            responses.put("126", "invalid PIN block");
            responses.put("127", "PIN length error");
            responses.put("128", "PIN key synch error");
            responses.put("129", "suspected counterfeit card");
            responses.put("180", "by cardholders wish");
            responses.put("200", "general no comments");
            responses.put("201", "expired card");
            responses.put("202", "suspected fraud");
            responses.put("203", "card acceptor contact card acquirer");
            responses.put("204", "restricted card");
            responses.put("205", "card acceptor 's security department");
            responses.put("206", "allowable PIN tries exceeded");
            responses.put("207", "special conditions");
            responses.put("208", "lost card");
            responses.put("209", "stolen card");
            responses.put("210", "suspected counterfeit card");
            responses.put("301", "reserved for future use in file management messages");
            responses.put("302", "reserved for future use in file management messages");
            responses.put("303", "reserved for future use in file management messages");
            responses.put("304", "reserved for future use in file management messages");
            responses.put("305", "reserved for future use in file management messages");
            responses.put("306", "reserved for future use in file management messages");
            responses.put("307", "reserved for future use in file management messages");
            responses.put("308", "reserved for future use in file management messages");
            responses.put("309", "reserved for future use in file management messages");
            responses.put("400", "for reversal");
            responses.put("499", "for reversals");
            responses.put("500", "reconciled, in balance");
            responses.put("501", "reconciled, out of balance");
            responses.put("502", "amount not reconciled, totals provided");
            responses.put("503", "totals for reconciliation not available");
            responses.put("504", "not reconciled, totals provided");
            responses.put("600", "for administrative info, reserved for future use in administrative messages");
            responses.put("601", "impossible to trace back original transaction");
            responses.put("602", "invalid transaction reference number");
            responses.put("603", "reference number/PAN incompatible");
            responses.put("604", "POS photograph is not available");
            responses.put("605", "requested item supplied");
            responses.put("606", "request cannot be fulfilled, required documentation is not available");
            responses.put("680", "List ready");
            responses.put("681", "List not ready");
            responses.put("700", "reserved for future use in fee collection messages");
            responses.put("800", "for network management");
            responses.put("900", "Advice acknowledged, no financial liability");
            responses.put("901", "Advice acknowledged, financial liability accepted");
            responses.put("000", "Success");
            
            description=responses.get(code);
            
            if(description==null||description.equals("")){
            description="Unmapped Error";
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return description;
    }

}
