/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.CommonOperations;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Njiru
 */
public class BFUBResponseMapping {

    public BFUBResponseMapping() {
    }

    public static String respmap(String code) {
        Map<String, String> mapper = new HashMap();
        String mapcode = "";
        try {
            mapper.clear();
            mapper.put("000", "000");
            mapper.put("020", "116");
            mapper.put("536", "909");
            mapper.put("606", "601");
            mapper.put("300", "902");

            mapcode = mapper.get(code);

            if (mapcode == null || mapcode.equals("")) {
                mapcode = "909";//Generic error
            }

        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }

        return mapcode;
    }

}
