package ke.co.ekenya.CommonOperations;

/**
 *
 * @author kkarue
 */
public class Config {

    public static String DATASOURCE_NAME3="jdbc/EBANK";
    public static String DATASOURCE_NAME="jdbc/ECONNECTESB";
    //public static String PROVIDER_URL = "t3://172.16.4.42:8001,172.16.4.43:8002,172.16.4.42:8003";
    public static String MACHINE1 = "t3://172.16.4.42:8001";
    public static String MACHINE2 = "t3://172.16.4.43:8002";
    public static String MACHINE3 = "t3://172.16.4.42:8003";
    public static String SERVER1 = "ECONNECT_JMS_SERVER@ESB_SERVER1";
    public static String SERVER2 = "ECONNECT_JMS_SERVER@ESB_SERVER2";
    public static String SERVER3 = "ECONNECT_JMS_SERVER@ESB_SERVER3";
    public static String CLUSTER_SERVERS = "3";
    public static String JMS_FACTORY = "jms/ECONNECT_JMSFACTORY";
    

}
