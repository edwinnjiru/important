package ke.co.ekenya.CommonOperations;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import ke.co.ekenya.LogEngine.ESBLog;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Ngari Josh
 */
public final class Utilities {

    private String key = "@123COOP321#";
    private final String HASHING_ALG = "MD5";
    private final String ENCODING = "utf-8";
    private final String ENCRYPTION_ALG = "DESede";



    /**
     * Log pre string.
     *
     * @return the string
     */
    public static String logPreString() {
        return "PesaLink Adaptor | " + Thread.currentThread().getStackTrace()[2].getClassName() + " | "
                + Thread.currentThread().getStackTrace()[2].getLineNumber() + " | "
                + Thread.currentThread().getStackTrace()[2].getMethodName() + "() | ";
    }

    public String encrypt(String plainPass) throws Exception {
        MessageDigest md = MessageDigest.getInstance(HASHING_ALG);
        byte[] digestOfPassword = md.digest(key.getBytes(ENCODING));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        SecretKey key = new SecretKeySpec(keyBytes, ENCRYPTION_ALG);
        Cipher cipher = Cipher.getInstance(ENCRYPTION_ALG);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] plainTextBytes = plainPass.getBytes(ENCODING);
        byte[] buf = cipher.doFinal(plainTextBytes);
        //byte[] base64Bytes = Base64.encodeBase64(buf);
        byte[] base64Bytes = java.util.Base64.getEncoder().encode(buf);
        //String msgTyp = new String(encodedBytes);
        String base64EncryptedString = new String(base64Bytes);
        return base64EncryptedString;
    }

    public String decrypt(String encryptedPass) throws Exception {
        //byte[] message = Base64.decodeBase64(encryptedPass.getBytes(ENCODING));
        byte[] message = java.util.Base64.getDecoder().decode(encryptedPass.getBytes(ENCODING));
        MessageDigest md = MessageDigest.getInstance(HASHING_ALG);
        byte[] digestOfPassword = md.digest(key.getBytes(ENCODING));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        SecretKey key = new SecretKeySpec(keyBytes, ENCRYPTION_ALG);
        Cipher decipher = Cipher.getInstance(ENCRYPTION_ALG);
        decipher.init(Cipher.DECRYPT_MODE, key);
        byte[] plainText = decipher.doFinal(message);
        return new String(plainText, ENCODING);
    }
    
    public String rightPadZeros(String str, int num) {
        return String.format("%1$-" + num + "s", str).replace(' ', '0');
    }
    
    public static String leftPadZeros(String str) {
        String result = String.format("%03d", Integer.parseInt(str));

        return result;
    }
    
    public static HashMap convertNodesFromXml(String xml) throws Exception {

        InputStream is = new ByteArrayInputStream(xml.getBytes());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(is);
        return (HashMap) createMap(document.getDocumentElement());
    }

    public static Object createMap(Node node) {
        Map<String, Object> map = new HashMap<>();
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            String name = currentNode.getNodeName();
            Object value = null;
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                value = createMap(currentNode);
            } else if (currentNode.getNodeType() == Node.TEXT_NODE) {
                return currentNode.getTextContent();
            }
            if (map.containsKey(name)) {
                Object object = map.get(name);
                if (object instanceof List) {
                    ((List<Object>) object).add(value);
                } else {
                    List<Object> objectList = new LinkedList<Object>();
                    objectList.add(object);
                    objectList.add(value);
                    map.put(name, objectList);
                }
            } else {
                map.put(name, value);
            }
        }
        return map;
    }

    public static String amountInCents(String amount) {
        String amtConverted = "";
        DecimalFormat df = new DecimalFormat("#.00");
        amtConverted = df.format(Double.parseDouble(amount));

        if (amtConverted.contains(".")) {
            amtConverted = amtConverted.replace(".", "");
        }

        return amtConverted;

    }
    
    public String getsender(String fld126){
    String name="";
        try {
            int namelen=Integer.parseInt(fld126.substring(9, 11));
            System.out.println("mame len: "+namelen);
            int start=11;
            name=fld126.substring(start,(start+namelen));            
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog lg = new ESBLog("GetSenderException", "fld126: "+fld126+", Error: "+sw.toString());
            lg.log();
        }
    return name;
    }

}
