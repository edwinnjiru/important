/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Update;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.UpdateStubs.AccountType;
import ke.co.ekenya.UpdateStubs.CardType;
import ke.co.ekenya.UpdateStubs.CustomerType;
import ke.co.ekenya.UpdateStubs.DataInput;
import ke.co.ekenya.UpdateStubs.PortType;
import ke.co.ekenya.UpdateStubs.ServiceStarter;
import ke.co.ekenya.UpdateStubs.UpdateInputType;
import ke.co.ekenya.messagehandlers.updateAccountHandler;
import ke.co.ekenya.messagehandlers.updateAccountResolver;

/**
 *
 * @author Njiru
 */
public class KitsUpdate {

    Map<String, String> Configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    Map<String, String> resxmlmap = new HashMap();
    public String CorrelationID;
    EncryptionUtils enc;

    public KitsUpdate(Map<String, String> xmlmap, String CorrelationID, Map<String, String> Configs) {
        this.xmlmap = xmlmap;
        this.CorrelationID = CorrelationID;
        this.Configs = Configs;
        this.enc = new EncryptionUtils();
    }

    public void doProcessing() {
        sendtosoa();

    }

    private void sendtosoa() {
        String response = "";
        resxmlmap.clear();
        try {
            DataInput dataInput = new DataInput();
            UpdateInputType inputType = new UpdateInputType();
            UpdateInputType.OperationParameters op_params = new UpdateInputType.OperationParameters();
            AccountType accType = new AccountType();
            CardType cardType = new CardType();
            CustomerType custType = new CustomerType();

            custType.setIDNumber(xmlmap.get("71"));;
            custType.setIdentificationType(xmlmap.get("72"));
            custType.setMobileNumber(xmlmap.get("2"));
            custType.setLanguage("EN");
            custType.setFullName(xmlmap.get("73"));

            String email = xmlmap.get("74");
            if (email.equals("")) {
                email = "customerservice@co-operativebank.co.ke";
            }
            custType.setEmailAddress(email);

            accType.setAccountNumber(xmlmap.get("75"));

            String cardNumber = xmlmap.get("76");
            if (!cardNumber.equals("")) {
                GregorianCalendar gCal = new GregorianCalendar();
                gCal.setTime(new Date());
                XMLGregorianCalendar gDateUnformated = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal);
                cardType.setExpiryDate(gDateUnformated);
                cardType.setNumber(cardNumber);

            } else {
                cardType.setExpiryDate(null);

            }

            op_params.setBankSortCode("40411000");
            op_params.setDefaultRecord("1");

            inputType.setCard(cardType);
            inputType.setAccount(accType);
            inputType.setCustomer(custType);
            inputType.setOperationParameters(op_params);
            dataInput.setUpdateInput(inputType);

            String headerPassword = enc.decrypt(Configs.get("KITSHEADERPASS"));
            String proxypass = enc.decrypt(Configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + Configs.get("PESA_WSDLS_PATH") + "Customer_Customer_update_1.1.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            updateAccountHandler uph = new updateAccountHandler(Configs, headerPassword, CorrelationID);
            updateAccountResolver upr = new updateAccountResolver(uph);
            service.setHandlerResolver(upr);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request
            BindingProvider prov = (BindingProvider) port;
            prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, Configs.get("SOA_USERNAME"));
            prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            //time-outs
            int connecttimeout = Integer.parseInt(Configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(Configs.get("READTIMEOUT"));
            prov.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            prov.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.update(dataInput);

            response = uph.getMyXML();

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SOAExceptions", "Exception occured while sending customer update request to SOA.\nError:" + sw.toString());
            el.log();

            String f48 = "";
            String msg = ex.getMessage();
            if (msg.contains("SocketTimeoutException") && msg.contains("HTTP")) {
                f48 = "Connect timed out";
            } else if (msg.contains("SocketTimeoutException")) {
                f48 = "Read timed out";
            } else if (msg.contains("500")) {
                f48 = "Internal Server Error";
            } else {
                f48 = "Generic Error";
            }

            xmlmap.put("39", "333");
            xmlmap.put("48", f48);
        } finally {
            QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
            boolean sent = qr.sendObject(xmlmap, CorrelationID);

            ESBLog lg = new ESBLog("ResponsetoESB", "Sent: " + sent + ", Map: " + xmlmap);
            lg.log();
        }
    }

    //breakdown response
}
