
package ke.co.ekenya.SendTransactionStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send}sendOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Account/Service/AccountFundsTransfer/Send/1.0/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
    protected SendOutputType sendOutput;

    /**
     * Gets the value of the sendOutput property.
     * 
     * @return
     *     possible object is
     *     {@link SendOutputType }
     *     
     */
    public SendOutputType getSendOutput() {
        return sendOutput;
    }

    /**
     * Sets the value of the sendOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendOutputType }
     *     
     */
    public void setSendOutput(SendOutputType value) {
        this.sendOutput = value;
    }

}
