
package ke.co.ekenya.SendTransactionStubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ke.co.ekenya.SendTransaction package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Transaction_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Transaction/1.0", "Transaction");
    private final static QName _AccountTransaction_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/AccountTransaction/1.0", "AccountTransaction");
    private final static QName _SendInput_QNAME = new QName("urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send", "sendInput");
    private final static QName _SendOutput_QNAME = new QName("urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send", "sendOutput");
    private final static QName _Card_QNAME = new QName("urn://co-opbank.co.ke/Banking/CanonicalDataModel/Card/1.0", "Card");
    private final static QName _HeaderRequestCreationTimestamp_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "CreationTimestamp");
    private final static QName _HeaderRequestReplyTO_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ReplyTO");
    private final static QName _HeaderRequestFaultTO_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "FaultTO");
    private final static QName _HeaderRequestCorrelationID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "CorrelationID");
    private final static QName _SendOutputTypeOperationParametersOperationDate_QNAME = new QName("urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send", "OperationDate");
    private final static QName _CredentialsUsername_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Username");
    private final static QName _CredentialsRealm_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Realm");
    private final static QName _CredentialsPassword_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Password");
    private final static QName _HeaderReplyStatusDescription_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusDescription");
    private final static QName _HeaderReplyStatusDescriptionKey_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusDescriptionKey");
    private final static QName _HeaderReplyStatusCode_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "StatusCode");
    private final static QName _HeaderReplyMessageID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageID");
    private final static QName _HeaderReplyElapsedTime_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ElapsedTime");
    private final static QName _StatusMessageMessageDescription_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageDescription");
    private final static QName _StatusMessageMessageCode_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageCode");
    private final static QName _StatusMessageApplicationID_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "ApplicationID");
    private final static QName _StatusMessageMessageType_QNAME = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ke.co.ekenya.SendTransaction
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link HeaderReply }
     * 
     */
    public HeaderReply createHeaderReply() {
        return new HeaderReply();
    }

    /**
     * Create an instance of {@link SendOutputType }
     * 
     */
    public SendOutputType createSendOutputType() {
        return new SendOutputType();
    }

    /**
     * Create an instance of {@link SendInputType }
     * 
     */
    public SendInputType createSendInputType() {
        return new SendInputType();
    }

    /**
     * Create an instance of {@link AccountTransactionType }
     * 
     */
    public AccountTransactionType createAccountTransactionType() {
        return new AccountTransactionType();
    }

    /**
     * Create an instance of {@link TransactionType }
     * 
     */
    public TransactionType createTransactionType() {
        return new TransactionType();
    }

    /**
     * Create an instance of {@link DataInput }
     * 
     */
    public DataInput createDataInput() {
        return new DataInput();
    }

    /**
     * Create an instance of {@link DataOutput }
     * 
     */
    public DataOutput createDataOutput() {
        return new DataOutput();
    }

    /**
     * Create an instance of {@link HeaderRequest }
     * 
     */
    public HeaderRequest createHeaderRequest() {
        return new HeaderRequest();
    }

    /**
     * Create an instance of {@link Credentials }
     * 
     */
    public Credentials createCredentials() {
        return new Credentials();
    }

    /**
     * Create an instance of {@link HeaderReply.StatusMessages }
     * 
     */
    public HeaderReply.StatusMessages createHeaderReplyStatusMessages() {
        return new HeaderReply.StatusMessages();
    }

    /**
     * Create an instance of {@link StatusMessage }
     * 
     */
    public StatusMessage createStatusMessage() {
        return new StatusMessage();
    }

    /**
     * Create an instance of {@link CardType }
     * 
     */
    public CardType createCardType() {
        return new CardType();
    }

    /**
     * Create an instance of {@link SendOutputType.OperationParameters }
     * 
     */
    public SendOutputType.OperationParameters createSendOutputTypeOperationParameters() {
        return new SendOutputType.OperationParameters();
    }

    /**
     * Create an instance of {@link SendInputType.OperationParameters }
     * 
     */
    public SendInputType.OperationParameters createSendInputTypeOperationParameters() {
        return new SendInputType.OperationParameters();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Transaction/1.0", name = "Transaction")
    public JAXBElement<TransactionType> createTransaction(TransactionType value) {
        return new JAXBElement<TransactionType>(_Transaction_QNAME, TransactionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountTransactionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/AccountTransaction/1.0", name = "AccountTransaction")
    public JAXBElement<AccountTransactionType> createAccountTransaction(AccountTransactionType value) {
        return new JAXBElement<AccountTransactionType>(_AccountTransaction_QNAME, AccountTransactionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send", name = "sendInput")
    public JAXBElement<SendInputType> createSendInput(SendInputType value) {
        return new JAXBElement<SendInputType>(_SendInput_QNAME, SendInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendOutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send", name = "sendOutput")
    public JAXBElement<SendOutputType> createSendOutput(SendOutputType value) {
        return new JAXBElement<SendOutputType>(_SendOutput_QNAME, SendOutputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Card/1.0", name = "Card")
    public JAXBElement<CardType> createCard(CardType value) {
        return new JAXBElement<CardType>(_Card_QNAME, CardType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CreationTimestamp", scope = HeaderRequest.class)
    public JAXBElement<XMLGregorianCalendar> createHeaderRequestCreationTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_HeaderRequestCreationTimestamp_QNAME, XMLGregorianCalendar.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ReplyTO", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestReplyTO(String value) {
        return new JAXBElement<String>(_HeaderRequestReplyTO_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "FaultTO", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestFaultTO(String value) {
        return new JAXBElement<String>(_HeaderRequestFaultTO_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CorrelationID", scope = HeaderRequest.class)
    public JAXBElement<String> createHeaderRequestCorrelationID(String value) {
        return new JAXBElement<String>(_HeaderRequestCorrelationID_QNAME, String.class, HeaderRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send", name = "OperationDate", scope = SendOutputType.OperationParameters.class)
    public JAXBElement<XMLGregorianCalendar> createSendOutputTypeOperationParametersOperationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_SendOutputTypeOperationParametersOperationDate_QNAME, XMLGregorianCalendar.class, SendOutputType.OperationParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Username", scope = Credentials.class)
    public JAXBElement<String> createCredentialsUsername(String value) {
        return new JAXBElement<String>(_CredentialsUsername_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Realm", scope = Credentials.class)
    public JAXBElement<String> createCredentialsRealm(String value) {
        return new JAXBElement<String>(_CredentialsRealm_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "Password", scope = Credentials.class)
    public JAXBElement<String> createCredentialsPassword(String value) {
        return new JAXBElement<String>(_CredentialsPassword_QNAME, String.class, Credentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusDescription", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusDescription(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusDescription_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusDescriptionKey", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusDescriptionKey(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusDescriptionKey_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "StatusCode", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyStatusCode(String value) {
        return new JAXBElement<String>(_HeaderReplyStatusCode_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageID", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyMessageID(String value) {
        return new JAXBElement<String>(_HeaderReplyMessageID_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ElapsedTime", scope = HeaderReply.class)
    public JAXBElement<Long> createHeaderReplyElapsedTime(Long value) {
        return new JAXBElement<Long>(_HeaderReplyElapsedTime_QNAME, Long.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "CorrelationID", scope = HeaderReply.class)
    public JAXBElement<String> createHeaderReplyCorrelationID(String value) {
        return new JAXBElement<String>(_HeaderRequestCorrelationID_QNAME, String.class, HeaderReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageDescription", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageDescription(String value) {
        return new JAXBElement<String>(_StatusMessageMessageDescription_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageCode", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageCode(String value) {
        return new JAXBElement<String>(_StatusMessageMessageCode_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "ApplicationID", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageApplicationID(String value) {
        return new JAXBElement<String>(_StatusMessageApplicationID_QNAME, String.class, StatusMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", name = "MessageType", scope = StatusMessage.class)
    public JAXBElement<String> createStatusMessageMessageType(String value) {
        return new JAXBElement<String>(_StatusMessageMessageType_QNAME, String.class, StatusMessage.class, value);
    }

}
