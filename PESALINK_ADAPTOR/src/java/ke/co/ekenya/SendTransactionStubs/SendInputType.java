
package ke.co.ekenya.SendTransactionStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for sendInputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendInputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperationParameters" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MessageTypeIdentification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ProcessingCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="ReconciliationAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CardholderBillingAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TransmissionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="SystemTraceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="SettlementDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="POSDataCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FunctionCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="MessageReasonCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="CardAcceptorBusinessCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="ReconciliationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="ReconciliationIndicator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="OriginalAmounts" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AcquirerReferenceData" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AcquirerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ForwarderID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Track2Data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RetrievalReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ApprovalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ActionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ServiceCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="TerminalID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CardAcceptorIdentification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CardAcceptorNameLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Track1Data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FeesAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AdditionalData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ReconciliationCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CardholderBillingCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PINData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SecurityControl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ICCSystemData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OriginalDataElements" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DataRecord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DestinationCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OriginatorCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CardIssuerReferenceData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ReceivingInstitutionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AccountIdentification1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AccountIdentification2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ReservedNationalUse122" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ReservedPrivateUse123" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ReservedPrivateUse126" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="InitiatingSystem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Transaction/1.0}Transaction" minOccurs="0"/>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/AccountTransaction/1.0}AccountTransaction" minOccurs="0"/>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/CanonicalDataModel/Card/1.0}Card" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendInputType", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send", propOrder = {
    "operationParameters",
    "transaction",
    "accountTransaction",
    "card"
})
public class SendInputType {

    @XmlElement(name = "OperationParameters")
    protected SendInputType.OperationParameters operationParameters;
    @XmlElement(name = "Transaction", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Transaction/1.0")
    protected TransactionType transaction;
    @XmlElement(name = "AccountTransaction", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/AccountTransaction/1.0")
    protected AccountTransactionType accountTransaction;
    @XmlElement(name = "Card", namespace = "urn://co-opbank.co.ke/Banking/CanonicalDataModel/Card/1.0")
    protected CardType card;

    /**
     * Gets the value of the operationParameters property.
     * 
     * @return
     *     possible object is
     *     {@link SendInputType.OperationParameters }
     *     
     */
    public SendInputType.OperationParameters getOperationParameters() {
        return operationParameters;
    }

    /**
     * Sets the value of the operationParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendInputType.OperationParameters }
     *     
     */
    public void setOperationParameters(SendInputType.OperationParameters value) {
        this.operationParameters = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionType }
     *     
     */
    public TransactionType getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionType }
     *     
     */
    public void setTransaction(TransactionType value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the accountTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTransactionType }
     *     
     */
    public AccountTransactionType getAccountTransaction() {
        return accountTransaction;
    }

    /**
     * Sets the value of the accountTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTransactionType }
     *     
     */
    public void setAccountTransaction(AccountTransactionType value) {
        this.accountTransaction = value;
    }

    /**
     * Gets the value of the card property.
     * 
     * @return
     *     possible object is
     *     {@link CardType }
     *     
     */
    public CardType getCard() {
        return card;
    }

    /**
     * Sets the value of the card property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardType }
     *     
     */
    public void setCard(CardType value) {
        this.card = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MessageTypeIdentification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ProcessingCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="ReconciliationAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CardholderBillingAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TransmissionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="SystemTraceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="SettlementDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="POSDataCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FunctionCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="MessageReasonCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="CardAcceptorBusinessCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="ReconciliationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="ReconciliationIndicator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="OriginalAmounts" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AcquirerReferenceData" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AcquirerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ForwarderID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Track2Data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RetrievalReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ApprovalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ActionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ServiceCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="TerminalID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CardAcceptorIdentification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CardAcceptorNameLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Track1Data" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FeesAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AdditionalData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ReconciliationCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CardholderBillingCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PINData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SecurityControl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ICCSystemData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="OriginalDataElements" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DataRecord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DestinationCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="OriginatorCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CardIssuerReferenceData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ReceivingInstitutionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AccountIdentification1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AccountIdentification2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ReservedNationalUse122" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ReservedPrivateUse123" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ReservedPrivateUse126" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="InitiatingSystem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "messageTypeIdentification",
        "processingCode",
        "reconciliationAmount",
        "cardholderBillingAmount",
        "transmissionDate",
        "systemTraceNumber",
        "settlementDate",
        "posDataCode",
        "functionCode",
        "messageReasonCode",
        "cardAcceptorBusinessCode",
        "reconciliationDate",
        "reconciliationIndicator",
        "originalAmounts",
        "acquirerReferenceData",
        "acquirerID",
        "forwarderID",
        "track2Data",
        "retrievalReferenceNumber",
        "approvalCode",
        "actionCode",
        "serviceCode",
        "terminalID",
        "cardAcceptorIdentification",
        "cardAcceptorNameLocation",
        "track1Data",
        "feesAmount",
        "additionalData",
        "reconciliationCurrency",
        "cardholderBillingCurrency",
        "pinData",
        "securityControl",
        "iccSystemData",
        "originalDataElements",
        "dataRecord",
        "destinationCountryCode",
        "originatorCountryCode",
        "cardIssuerReferenceData",
        "receivingInstitutionID",
        "fileName",
        "accountIdentification1",
        "accountIdentification2",
        "reservedNationalUse122",
        "reservedPrivateUse123",
        "reservedPrivateUse126",
        "initiatingSystem"
    })
    public static class OperationParameters {

        @XmlElement(name = "MessageTypeIdentification", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String messageTypeIdentification;
        @XmlElement(name = "ProcessingCode", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected Integer processingCode;
        @XmlElement(name = "ReconciliationAmount", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String reconciliationAmount;
        @XmlElement(name = "CardholderBillingAmount", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String cardholderBillingAmount;
        @XmlElement(name = "TransmissionDate", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar transmissionDate;
        @XmlElement(name = "SystemTraceNumber", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected Integer systemTraceNumber;
        @XmlElement(name = "SettlementDate", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar settlementDate;
        @XmlElement(name = "POSDataCode", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String posDataCode;
        @XmlElement(name = "FunctionCode", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected Integer functionCode;
        @XmlElement(name = "MessageReasonCode", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected Integer messageReasonCode;
        @XmlElement(name = "CardAcceptorBusinessCode", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected Integer cardAcceptorBusinessCode;
        @XmlElement(name = "ReconciliationDate", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar reconciliationDate;
        @XmlElement(name = "ReconciliationIndicator", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected Integer reconciliationIndicator;
        @XmlElement(name = "OriginalAmounts", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String originalAmounts;
        @XmlElement(name = "AcquirerReferenceData", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected Integer acquirerReferenceData;
        @XmlElement(name = "AcquirerID", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String acquirerID;
        @XmlElement(name = "ForwarderID", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String forwarderID;
        @XmlElement(name = "Track2Data", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String track2Data;
        @XmlElement(name = "RetrievalReferenceNumber", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String retrievalReferenceNumber;
        @XmlElement(name = "ApprovalCode", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String approvalCode;
        @XmlElement(name = "ActionCode", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String actionCode;
        @XmlElement(name = "ServiceCode", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected Integer serviceCode;
        @XmlElement(name = "TerminalID", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String terminalID;
        @XmlElement(name = "CardAcceptorIdentification", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String cardAcceptorIdentification;
        @XmlElement(name = "CardAcceptorNameLocation", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String cardAcceptorNameLocation;
        @XmlElement(name = "Track1Data", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String track1Data;
        @XmlElement(name = "FeesAmount", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String feesAmount;
        @XmlElement(name = "AdditionalData", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String additionalData;
        @XmlElement(name = "ReconciliationCurrency", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String reconciliationCurrency;
        @XmlElement(name = "CardholderBillingCurrency", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String cardholderBillingCurrency;
        @XmlElement(name = "PINData", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String pinData;
        @XmlElement(name = "SecurityControl", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String securityControl;
        @XmlElement(name = "ICCSystemData", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String iccSystemData;
        @XmlElement(name = "OriginalDataElements", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String originalDataElements;
        @XmlElement(name = "DataRecord", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String dataRecord;
        @XmlElement(name = "DestinationCountryCode", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String destinationCountryCode;
        @XmlElement(name = "OriginatorCountryCode", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String originatorCountryCode;
        @XmlElement(name = "CardIssuerReferenceData", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String cardIssuerReferenceData;
        @XmlElement(name = "ReceivingInstitutionID", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String receivingInstitutionID;
        @XmlElement(name = "FileName", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String fileName;
        @XmlElement(name = "AccountIdentification1", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String accountIdentification1;
        @XmlElement(name = "AccountIdentification2", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String accountIdentification2;
        @XmlElement(name = "ReservedNationalUse122", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String reservedNationalUse122;
        @XmlElement(name = "ReservedPrivateUse123", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String reservedPrivateUse123;
        @XmlElement(name = "ReservedPrivateUse126", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String reservedPrivateUse126;
        @XmlElement(name = "InitiatingSystem", namespace = "urn://co-opbank.co.ke/Banking/Account/DataModel/AccountFundsTransfer/Send/1.0/AccountFundsTransfer.send")
        protected String initiatingSystem;

        /**
         * Gets the value of the messageTypeIdentification property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMessageTypeIdentification() {
            return messageTypeIdentification;
        }

        /**
         * Sets the value of the messageTypeIdentification property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMessageTypeIdentification(String value) {
            this.messageTypeIdentification = value;
        }

        /**
         * Gets the value of the processingCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getProcessingCode() {
            return processingCode;
        }

        /**
         * Sets the value of the processingCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setProcessingCode(Integer value) {
            this.processingCode = value;
        }

        /**
         * Gets the value of the reconciliationAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReconciliationAmount() {
            return reconciliationAmount;
        }

        /**
         * Sets the value of the reconciliationAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReconciliationAmount(String value) {
            this.reconciliationAmount = value;
        }

        /**
         * Gets the value of the cardholderBillingAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCardholderBillingAmount() {
            return cardholderBillingAmount;
        }

        /**
         * Sets the value of the cardholderBillingAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCardholderBillingAmount(String value) {
            this.cardholderBillingAmount = value;
        }

        /**
         * Gets the value of the transmissionDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getTransmissionDate() {
            return transmissionDate;
        }

        /**
         * Sets the value of the transmissionDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setTransmissionDate(XMLGregorianCalendar value) {
            this.transmissionDate = value;
        }

        /**
         * Gets the value of the systemTraceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getSystemTraceNumber() {
            return systemTraceNumber;
        }

        /**
         * Sets the value of the systemTraceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setSystemTraceNumber(Integer value) {
            this.systemTraceNumber = value;
        }

        /**
         * Gets the value of the settlementDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getSettlementDate() {
            return settlementDate;
        }

        /**
         * Sets the value of the settlementDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setSettlementDate(XMLGregorianCalendar value) {
            this.settlementDate = value;
        }

        /**
         * Gets the value of the posDataCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPOSDataCode() {
            return posDataCode;
        }

        /**
         * Sets the value of the posDataCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPOSDataCode(String value) {
            this.posDataCode = value;
        }

        /**
         * Gets the value of the functionCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getFunctionCode() {
            return functionCode;
        }

        /**
         * Sets the value of the functionCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setFunctionCode(Integer value) {
            this.functionCode = value;
        }

        /**
         * Gets the value of the messageReasonCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMessageReasonCode() {
            return messageReasonCode;
        }

        /**
         * Sets the value of the messageReasonCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMessageReasonCode(Integer value) {
            this.messageReasonCode = value;
        }

        /**
         * Gets the value of the cardAcceptorBusinessCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCardAcceptorBusinessCode() {
            return cardAcceptorBusinessCode;
        }

        /**
         * Sets the value of the cardAcceptorBusinessCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCardAcceptorBusinessCode(Integer value) {
            this.cardAcceptorBusinessCode = value;
        }

        /**
         * Gets the value of the reconciliationDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getReconciliationDate() {
            return reconciliationDate;
        }

        /**
         * Sets the value of the reconciliationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setReconciliationDate(XMLGregorianCalendar value) {
            this.reconciliationDate = value;
        }

        /**
         * Gets the value of the reconciliationIndicator property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getReconciliationIndicator() {
            return reconciliationIndicator;
        }

        /**
         * Sets the value of the reconciliationIndicator property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setReconciliationIndicator(Integer value) {
            this.reconciliationIndicator = value;
        }

        /**
         * Gets the value of the originalAmounts property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOriginalAmounts() {
            return originalAmounts;
        }

        /**
         * Sets the value of the originalAmounts property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOriginalAmounts(String value) {
            this.originalAmounts = value;
        }

        /**
         * Gets the value of the acquirerReferenceData property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAcquirerReferenceData() {
            return acquirerReferenceData;
        }

        /**
         * Sets the value of the acquirerReferenceData property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAcquirerReferenceData(Integer value) {
            this.acquirerReferenceData = value;
        }

        /**
         * Gets the value of the acquirerID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcquirerID() {
            return acquirerID;
        }

        /**
         * Sets the value of the acquirerID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcquirerID(String value) {
            this.acquirerID = value;
        }

        /**
         * Gets the value of the forwarderID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getForwarderID() {
            return forwarderID;
        }

        /**
         * Sets the value of the forwarderID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setForwarderID(String value) {
            this.forwarderID = value;
        }

        /**
         * Gets the value of the track2Data property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrack2Data() {
            return track2Data;
        }

        /**
         * Sets the value of the track2Data property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrack2Data(String value) {
            this.track2Data = value;
        }

        /**
         * Gets the value of the retrievalReferenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRetrievalReferenceNumber() {
            return retrievalReferenceNumber;
        }

        /**
         * Sets the value of the retrievalReferenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRetrievalReferenceNumber(String value) {
            this.retrievalReferenceNumber = value;
        }

        /**
         * Gets the value of the approvalCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApprovalCode() {
            return approvalCode;
        }

        /**
         * Sets the value of the approvalCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApprovalCode(String value) {
            this.approvalCode = value;
        }

        /**
         * Gets the value of the actionCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActionCode() {
            return actionCode;
        }

        /**
         * Sets the value of the actionCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActionCode(String value) {
            this.actionCode = value;
        }

        /**
         * Gets the value of the serviceCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getServiceCode() {
            return serviceCode;
        }

        /**
         * Sets the value of the serviceCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setServiceCode(Integer value) {
            this.serviceCode = value;
        }

        /**
         * Gets the value of the terminalID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTerminalID() {
            return terminalID;
        }

        /**
         * Sets the value of the terminalID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTerminalID(String value) {
            this.terminalID = value;
        }

        /**
         * Gets the value of the cardAcceptorIdentification property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCardAcceptorIdentification() {
            return cardAcceptorIdentification;
        }

        /**
         * Sets the value of the cardAcceptorIdentification property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCardAcceptorIdentification(String value) {
            this.cardAcceptorIdentification = value;
        }

        /**
         * Gets the value of the cardAcceptorNameLocation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCardAcceptorNameLocation() {
            return cardAcceptorNameLocation;
        }

        /**
         * Sets the value of the cardAcceptorNameLocation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCardAcceptorNameLocation(String value) {
            this.cardAcceptorNameLocation = value;
        }

        /**
         * Gets the value of the track1Data property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrack1Data() {
            return track1Data;
        }

        /**
         * Sets the value of the track1Data property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrack1Data(String value) {
            this.track1Data = value;
        }

        /**
         * Gets the value of the feesAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFeesAmount() {
            return feesAmount;
        }

        /**
         * Sets the value of the feesAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFeesAmount(String value) {
            this.feesAmount = value;
        }

        /**
         * Gets the value of the additionalData property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAdditionalData() {
            return additionalData;
        }

        /**
         * Sets the value of the additionalData property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAdditionalData(String value) {
            this.additionalData = value;
        }

        /**
         * Gets the value of the reconciliationCurrency property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReconciliationCurrency() {
            return reconciliationCurrency;
        }

        /**
         * Sets the value of the reconciliationCurrency property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReconciliationCurrency(String value) {
            this.reconciliationCurrency = value;
        }

        /**
         * Gets the value of the cardholderBillingCurrency property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCardholderBillingCurrency() {
            return cardholderBillingCurrency;
        }

        /**
         * Sets the value of the cardholderBillingCurrency property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCardholderBillingCurrency(String value) {
            this.cardholderBillingCurrency = value;
        }

        /**
         * Gets the value of the pinData property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPINData() {
            return pinData;
        }

        /**
         * Sets the value of the pinData property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPINData(String value) {
            this.pinData = value;
        }

        /**
         * Gets the value of the securityControl property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSecurityControl() {
            return securityControl;
        }

        /**
         * Sets the value of the securityControl property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSecurityControl(String value) {
            this.securityControl = value;
        }

        /**
         * Gets the value of the iccSystemData property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getICCSystemData() {
            return iccSystemData;
        }

        /**
         * Sets the value of the iccSystemData property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setICCSystemData(String value) {
            this.iccSystemData = value;
        }

        /**
         * Gets the value of the originalDataElements property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOriginalDataElements() {
            return originalDataElements;
        }

        /**
         * Sets the value of the originalDataElements property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOriginalDataElements(String value) {
            this.originalDataElements = value;
        }

        /**
         * Gets the value of the dataRecord property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDataRecord() {
            return dataRecord;
        }

        /**
         * Sets the value of the dataRecord property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDataRecord(String value) {
            this.dataRecord = value;
        }

        /**
         * Gets the value of the destinationCountryCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationCountryCode() {
            return destinationCountryCode;
        }

        /**
         * Sets the value of the destinationCountryCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationCountryCode(String value) {
            this.destinationCountryCode = value;
        }

        /**
         * Gets the value of the originatorCountryCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOriginatorCountryCode() {
            return originatorCountryCode;
        }

        /**
         * Sets the value of the originatorCountryCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOriginatorCountryCode(String value) {
            this.originatorCountryCode = value;
        }

        /**
         * Gets the value of the cardIssuerReferenceData property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCardIssuerReferenceData() {
            return cardIssuerReferenceData;
        }

        /**
         * Sets the value of the cardIssuerReferenceData property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCardIssuerReferenceData(String value) {
            this.cardIssuerReferenceData = value;
        }

        /**
         * Gets the value of the receivingInstitutionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReceivingInstitutionID() {
            return receivingInstitutionID;
        }

        /**
         * Sets the value of the receivingInstitutionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReceivingInstitutionID(String value) {
            this.receivingInstitutionID = value;
        }

        /**
         * Gets the value of the fileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileName() {
            return fileName;
        }

        /**
         * Sets the value of the fileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileName(String value) {
            this.fileName = value;
        }

        /**
         * Gets the value of the accountIdentification1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountIdentification1() {
            return accountIdentification1;
        }

        /**
         * Sets the value of the accountIdentification1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountIdentification1(String value) {
            this.accountIdentification1 = value;
        }

        /**
         * Gets the value of the accountIdentification2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountIdentification2() {
            return accountIdentification2;
        }

        /**
         * Sets the value of the accountIdentification2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountIdentification2(String value) {
            this.accountIdentification2 = value;
        }

        /**
         * Gets the value of the reservedNationalUse122 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReservedNationalUse122() {
            return reservedNationalUse122;
        }

        /**
         * Sets the value of the reservedNationalUse122 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReservedNationalUse122(String value) {
            this.reservedNationalUse122 = value;
        }

        /**
         * Gets the value of the reservedPrivateUse123 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReservedPrivateUse123() {
            return reservedPrivateUse123;
        }

        /**
         * Sets the value of the reservedPrivateUse123 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReservedPrivateUse123(String value) {
            this.reservedPrivateUse123 = value;
        }

        /**
         * Gets the value of the reservedPrivateUse126 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReservedPrivateUse126() {
            return reservedPrivateUse126;
        }

        /**
         * Sets the value of the reservedPrivateUse126 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReservedPrivateUse126(String value) {
            this.reservedPrivateUse126 = value;
        }

        /**
         * Gets the value of the initiatingSystem property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInitiatingSystem() {
            return initiatingSystem;
        }

        /**
         * Sets the value of the initiatingSystem property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInitiatingSystem(String value) {
            this.initiatingSystem = value;
        }

    }

}
