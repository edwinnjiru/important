/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.messagehandlers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author mwallet1
 */
public class ReceiveHandler implements SOAPHandler<SOAPMessageContext> {

    private String myXML;

    public String getMyXML() {
        return myXML;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        try {

            if (outbound) {
                // OUTBOUND              
//                System.out.println("Direction=outbound (handleMessage)");
                SOAPMessage msg = (context).getMessage();
                SOAPPart sp = msg.getSOAPPart();

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                msg.writeTo(out);
                String strMsg = new String(out.toByteArray());
                ESBLog lg = new ESBLog("RecieveAcknowledgement", strMsg);
                lg.log();

            } else {
//                System.out.println("Direction=inbound (handleMessage)");
                SOAPMessage msg = context.getMessage();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                msg.writeTo(out);
                String strMsg = new String(out.toByteArray());
                myXML = strMsg;
                ESBLog lg = new ESBLog("RecieveInbound", strMsg);
                lg.log();

            }

        } catch (IOException | SOAPException ex) {
            ESBLog lg = new ESBLog("RecieveException", ex.getMessage());
            lg.log();

        }
        return true;
    }

    public Set<QName> getHeaders() {
        // getHeaders();
        return Collections.EMPTY_SET;
    }

    public boolean handleFault(SOAPMessageContext messageContext) {
        System.out.println(messageContext.getMessage());
        return true;
    }

    public void close(MessageContext context) {
    }

}
