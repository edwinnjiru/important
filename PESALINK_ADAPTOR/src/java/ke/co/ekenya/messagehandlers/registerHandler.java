/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.messagehandlers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author Njiru
 */
public class registerHandler implements SOAPHandler<SOAPMessageContext> {

    private String systemCode, headerUsername;
    public String myXML;
    public String CorrelationID;
    Map<String, String> Configs = new HashMap();
    private String headerPassword = "";

    public String getMyXML() {
        return myXML;
    }

    public registerHandler() {
    }

    public registerHandler(Map<String, String> Configs, String headerPassword, String CorrelationID) {
        this.Configs = Configs;
        this.headerPassword = headerPassword;
        this.CorrelationID = CorrelationID;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext smc) {
        systemCode = Configs.get("SOA_SYSCODE");
        //headerUsername = Configs.get("KITSHEADERUNAME");
        headerUsername = "CO-OPERATIVE BANK";

        String msgid = "";
        if (CorrelationID.contains("EC")) {
            msgid = CorrelationID.substring(2);
        }

        Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (outboundProperty) {
            try {
                SOAPMessage message = smc.getMessage();
                SOAPEnvelope env = smc.getMessage().getSOAPPart().getEnvelope();

                env.addNamespaceDeclaration("soap", "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader");

                QName MsgID = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "MessageID", "soap");
                QName qname = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "HeaderRequest", "soap");
                QName Credentials = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Credentials", "soap");
                QName q_syscode = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "SystemCode", "soap");
                QName q_uname = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Username", "soap");
                QName q_pword = new QName("urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader", "Password", "soap");

                javax.xml.soap.Name kaname = new javax.xml.soap.Name() {
                    @Override
                    public String getLocalName() {
                        return "Header";
                    }

                    @Override
                    public String getQualifiedName() {
                        return "Header";
                    }

                    @Override
                    public String getPrefix() {
                        return "soap";
                    }

                    @Override
                    public String getURI() {
                        return "urn://co-opbank.co.ke/SharedResources/Schemas/SOAMessages/SoapHeader";
                    }
                };

                SOAPElement soapElement2 = env.getHeader().addHeaderElement(qname);

                SOAPElement soapElement_msgid = soapElement2.addChildElement(MsgID);
                soapElement_msgid.addTextNode(msgid);
                SOAPElement soapElement_cred = soapElement2.addChildElement(Credentials);
                SOAPElement soapElement_syscode = soapElement_cred.addChildElement(q_syscode);
                soapElement_syscode.addTextNode(systemCode);
                SOAPElement soapElement_userid = soapElement_cred.addChildElement(q_uname);
                soapElement_userid.addTextNode(headerUsername);
                SOAPElement soapElement_userpass = soapElement_cred.addChildElement(q_pword);
                soapElement_userpass.addTextNode(headerPassword);

                message.saveChanges();

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                message.writeTo(out);
                String strMsg = new String(out.toByteArray());
                //System.out.println(strMsg);

                ESBLog lg = new ESBLog("RegistrationRequest", strMsg);
                lg.log();
            } catch (SOAPException | IOException e) {
                ESBLog lg = new ESBLog("RegistrationException", e.getMessage());
                lg.log();
            }
        } else {
            try {
                SOAPMessage message = smc.getMessage();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                message.writeTo(out);
                String strMsg = new String(out.toByteArray());
                myXML = strMsg;
                ESBLog lg = new ESBLog("RegistrationResponse", strMsg);
                lg.log();
            } catch (SOAPException | IOException ex) {
                ESBLog lg = new ESBLog("RegistrationException", ex.getMessage());
                lg.log();
            }
        }
        return outboundProperty;
    }

    @Override
    public Set getHeaders() {
        return null;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {
    }

}
