/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.messagehandlers;

import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

/**
 *
 * @author Njiru
 */
public class updateAccountResolver implements HandlerResolver{
    updateAccountHandler uaH=new updateAccountHandler();
    
    public updateAccountResolver(updateAccountHandler uaH){
    this.uaH=uaH;    
    }
    
    public List<Handler> getHandlerChain(PortInfo portInfo) {
      List<Handler> handlerChain = new ArrayList<Handler>();

      handlerChain.add(uaH);

      return handlerChain;
   }
    
}
