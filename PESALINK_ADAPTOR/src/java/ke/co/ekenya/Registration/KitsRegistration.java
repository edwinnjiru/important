/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Registration;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DBFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.RegistrationStubs.AccountType;
import ke.co.ekenya.RegistrationStubs.CardType;
import ke.co.ekenya.RegistrationStubs.CustomerType;
import ke.co.ekenya.RegistrationStubs.DataInput;
import ke.co.ekenya.RegistrationStubs.PortType;
import ke.co.ekenya.RegistrationStubs.RegisterInputType;
import ke.co.ekenya.RegistrationStubs.ServiceStarter;
import ke.co.ekenya.messagehandlers.registerHandler;
import ke.co.ekenya.messagehandlers.registerResolver;

/**
 *
 * @author Njiru
 */
public class KitsRegistration {

    Map<String, String> Configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    public String CorrelationID;
    EncryptionUtils enc;
    DBFunctions databaseFunctions;

    public KitsRegistration(Map<String, String> xmlmap, String CorrelationID, Map<String, String> Configs) {
        this.xmlmap = xmlmap;
        this.CorrelationID = CorrelationID;
        this.Configs = Configs;
        this.enc = new EncryptionUtils();
        this.databaseFunctions = new DBFunctions();
    }

    public void doProcessing() {
        sendtoSoa();

    }

    private void sendtoSoa() {
        String response = "";
        try {
            DataInput dataInput = new DataInput();
            RegisterInputType regInputType = new RegisterInputType();
            AccountType accType = new AccountType();
            CardType cardType = new CardType();
            CustomerType custType = new CustomerType();
            RegisterInputType.OperationParameters op_Params = new RegisterInputType.OperationParameters();

            custType.setIDNumber(xmlmap.get("71"));
            custType.setIdentificationType(xmlmap.get("72"));
            custType.setMobileNumber(xmlmap.get("2"));
            op_Params.setBankSortCode("40411000");
            op_Params.setDefaultRecord("1");
            custType.setFullName(xmlmap.get("73"));
            accType.setAccountNumber(xmlmap.get("75"));

            String email = xmlmap.get("74");
            if (email.equals("")) {
                email = "customerservice@co-operativebank.co.ke";
                xmlmap.put("74", "customerservice@co-operativebank.co.ke");
            }
            custType.setEmailAddress(email);
            custType.setLanguage("EN");

            //carddetails
            String cardNumber = xmlmap.get("76");
            if (!cardNumber.equals("")) {
                GregorianCalendar gCal = new GregorianCalendar();
                gCal.setTime(new Date());
                XMLGregorianCalendar gDateUnformated = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal);
                cardType.setExpiryDate(gDateUnformated);
                cardType.setNumber(cardNumber);
            }

            regInputType.setAccount(accType);
            regInputType.setCard(cardType);
            regInputType.setCustomer(custType);
            regInputType.setOperationParameters(op_Params);
            dataInput.setRegisterInput(regInputType);

            String headerPassword = enc.decrypt(Configs.get("KITSHEADERPASS"));
            String proxypass = enc.decrypt(Configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + Configs.get("PESA_WSDLS_PATH") + "Customer_Customer_register_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            registerHandler regHandler = new registerHandler(Configs, headerPassword, CorrelationID);
            registerResolver regResolver = new registerResolver(regHandler);
            service.setHandlerResolver(regResolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request
            BindingProvider prov = (BindingProvider) port;
            prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, Configs.get("SOA_USERNAME"));
            prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            //time-outs
            int connecttimeout = Integer.parseInt(Configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(Configs.get("READTIMEOUT"));
            prov.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            prov.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.register(dataInput);

            response = regHandler.getMyXML();
            breakDownSOAResponse(response);

        } catch (Exception ex) {
            String f48 = "";
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SOAExceptions", "Exception occured while while sending customer registration request to SOA.\nError:" + sw.toString());
            el.log();
            String msg = ex.getMessage();
            if (msg.contains("SocketTimeoutException") && msg.contains("HTTP")) {
                f48 = "Connect timed out";
            } else if (msg.contains("SocketTimeoutException")) {
                f48 = "Read timed out";
            } else if (msg.contains("500")) {
                f48 = "Internal Server Error";
            } else {
                f48 = "Generic Error";
            }
            xmlmap.put("registered", "2");
            xmlmap.put("39", "333");
            xmlmap.put("48", f48);
        } finally {
            QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
            boolean sent = qr.sendObject(xmlmap, CorrelationID);

            ESBLog lg = new ESBLog("ResponsetoESB", "Sent: " + sent + ", Map: " + xmlmap);
            lg.log();

            if (("000".equals(xmlmap.get("39"))) && (("USSD".equalsIgnoreCase(xmlmap.get("32"))) || ("WAP".equalsIgnoreCase(xmlmap.get("32"))))) {
                boolean exists = databaseFunctions.checkRegistrationStatus(xmlmap);
                if (!exists) {
                    int insert = databaseFunctions.insertRegistration(xmlmap);
                    ESBLog l0g = new ESBLog("RegistrationInsert", "Insert: " + insert + ", Map: " + xmlmap);
                    l0g.log();
                }
            } else if (("PORTAL".equalsIgnoreCase(xmlmap.get("32")))) {
                //ni portal so update
                int update = databaseFunctions.updateReg(xmlmap);
                ESBLog l0g = new ESBLog("RegistrationUpdate", "Update: " + update + ", Map: " + xmlmap);
                l0g.log();
            }
        }

    }

    private void breakDownSOAResponse(String SOAResp) {
        HashMap result = new HashMap<>();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAResp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("SOAP-ENV:Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("ns:HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("ns:StatusMessages");
            result.put("statusDescription", (String) headerReply.get("ns:StatusDescription"));
            result.put("statusCode", (String) headerReply.get("ns:StatusCode"));
            result.put("statusDescriptionKey", (String) headerReply.get("ns:StatusDescriptionKey"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                result.put("messageID", (String) headerReply.get("ns:MessageID"));

                //status messages breakdown for header
                HashMap statusMessage = (HashMap<String, Object>) statusMessages.get("ns:StatusMessage");
                result.put("messageType", (String) statusMessage.get("ns:MessageType"));
                result.put("applicationID", (String) statusMessage.get("ns:ApplicationID"));
                result.put("messageCode", (String) statusMessage.get("ns:MessageCode"));

                //BreakDown Of Body
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("SOAP-ENV:Body");
                HashMap<String, Object> dataOutput = (HashMap<String, Object>) body.get("ns0:DataOutput");
                HashMap<String, Object> registerOutput = (HashMap<String, Object>) dataOutput.get("ns1:registerOutput");
                HashMap<String, Object> operationparmeters = (HashMap<String, Object>) registerOutput.get("ns1:OperationParameters");
                result.put("RegistrationNumber", (String) operationparmeters.get("ns1:RegistrationNumber"));

                xmlmap.put("status", "Success");
                xmlmap.put("39", "000");
                xmlmap.put("48", "Successfully Registered");
                xmlmap.put("registered", "1");

            } else {
                xmlmap.put("status", "Failure");
                xmlmap.put("39", "333");
                xmlmap.put("48", "Customer Registration Failed");
                xmlmap.put("registered", "2");
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("BreakdownExceptions", "Exception occured while breaking down customer registration SOA resonse.\nError:" + sw.toString());
            el.log();

            xmlmap.put("status", "Failure");
            xmlmap.put("39", "333");
            xmlmap.put("48", "Exception occured");
            xmlmap.put("registered", "2");
        }
    }

}
