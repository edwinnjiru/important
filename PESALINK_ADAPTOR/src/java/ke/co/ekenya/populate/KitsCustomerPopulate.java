/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.populate;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import javax.xml.ws.BindingProvider;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.PopulateStubs.DataInput;
import ke.co.ekenya.PopulateStubs.GetListInputType;
import ke.co.ekenya.PopulateStubs.PortType;
import ke.co.ekenya.PopulateStubs.ServiceStarter;
import ke.co.ekenya.messagehandlers.populateHandler;
import ke.co.ekenya.messagehandlers.populateResolver;

/**
 *
 * @author Njiru
 */
public class KitsCustomerPopulate {

    Map<String, String> Configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    Map<String, String> resxmlmap = new HashMap();
    public String CorrelationID;
    EncryptionUtils enc;

    public KitsCustomerPopulate(Map<String, String> xmlmap, String CorrelationID, Map<String, String> Configs) {
        this.xmlmap = xmlmap;
        this.CorrelationID = CorrelationID;
        this.Configs = Configs;
        this.enc = new EncryptionUtils();

    }

    public void doProcessing() {
        sendtosoa();
    }

    private void sendtosoa() {
        BigDecimal amount = new BigDecimal("788");
        String response;
        try {
            DataInput dataInput = new DataInput();
            GetListInputType accountlist = new GetListInputType();
            GetListInputType.OperationParameters op_Params = new GetListInputType.OperationParameters();

            op_Params.setAmount(amount);
            op_Params.setDestinationMsisdn(xmlmap.get("103"));
            op_Params.setSourceMsisdn(xmlmap.get("2"));

            accountlist.setOperationParameters(op_Params);
            dataInput.setGetListInput(accountlist);

            //Decrypting
            String headerPassword = enc.decrypt(Configs.get("KITSHEADERPASS"));
            String proxypass = enc.decrypt(Configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + Configs.get("PESA_WSDLS_PATH") + "Customer_CustomerServices_getList_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            populateHandler popHandler = new populateHandler(Configs, headerPassword, CorrelationID);
            populateResolver popResolver = new populateResolver(popHandler);
            service.setHandlerResolver(popResolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request
            BindingProvider prov = (BindingProvider) port;
            prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, Configs.get("SOA_USERNAME"));
            prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            //time-outs
            int connecttimeout = Integer.parseInt(Configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(Configs.get("READTIMEOUT"));
            prov.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            prov.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.getList(dataInput);
            response = popHandler.getMyXML();
            //System.out.println(response);

            breakDownSOAResponse(response);

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SOAExceptions", "Exception occured while sending customer populate request to SOA.\nError:" + sw.toString());
            el.log();

            String f48 = "";
            String msg = ex.getMessage();
            if (msg.contains("SocketTimeoutException") && msg.contains("HTTP")) {
                f48 = "Connect timed out";
            } else if (msg.contains("SocketTimeoutException")) {
                f48 = "Read timed out";
            } else if (msg.contains("500")) {
                f48 = "Internal Server Error";
            } else {
                f48 = "Generic Error";
            }

            xmlmap.put("39", "333");
            xmlmap.put("48", f48);
        } finally {
            QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
            boolean sent = qr.sendObject(xmlmap, CorrelationID);

            ESBLog lg = new ESBLog("ResponsetoESB", "Sent: " + sent + ", Map: " + xmlmap);
            lg.log();
        }

    }

    private void breakDownSOAResponse(String SOAResp) {
        HashMap result = new HashMap();
        String banks = "";
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAResp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("SOAP-ENV:Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("ns:HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("ns:StatusMessages");
            result.put("statusDescription", (String) headerReply.get("ns:StatusDescription"));
            result.put("statusCode", (String) headerReply.get("ns:StatusCode"));
            result.put("statusDescriptionKey", (String) headerReply.get("ns:StatusDescriptionKey"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                result.put("messageID", (String) headerReply.get("ns:MessageID"));

                //status messages breakdown for header
                HashMap statusMessage = (HashMap<String, Object>) statusMessages.get("ns:StatusMessage");
                result.put("messageType", (String) statusMessage.get("ns:MessageType"));
                result.put("applicationID", (String) statusMessage.get("ns:ApplicationID"));
                result.put("messageCode", (String) statusMessage.get("ns:MessageCode"));

                //BreakDown Of Body
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("SOAP-ENV:Body");
                HashMap<String, Object> dataOutput = (HashMap<String, Object>) body.get("ns0:DataOutput");
                HashMap<String, Object> ListOutput = (HashMap<String, Object>) dataOutput.get("ns1:getListOutput");
                HashMap<String, Object> operationparmeters = (HashMap<String, Object>) ListOutput.get("ns1:OperationParameters");

                //breakdown banklist
                if (checkinstance(operationparmeters.get("ns1:BankList"), "java.util.LinkedList")) {
                    LinkedList<Object> banklist = (LinkedList) operationparmeters.get("ns1:BankList");
                    StringBuilder sb = new StringBuilder();
                    Iterator<Object> itr = banklist.iterator();
                    while (itr.hasNext()) {
                        if (sb.length() > 1) {
                            sb.append("~");
                        }
                        HashMap<String, String> bk = (HashMap<String, String>) itr.next();
                        sb.append(bk.get("ns1:BankName")).append("|").append(bk.get("ns1:SortCode"));
                    }
                    banks = sb.toString();
                } else {
                    HashMap<String, Object> banklist = (HashMap<String, Object>) operationparmeters.get("ns1:BankList");
                    banks = banklist.get("ns1:BankName") + "|" + banklist.get("ns1:SortCode");
                }
                String destination = (String) operationparmeters.get("ns1:DestinationName");
                xmlmap.put("39", "000");
                xmlmap.put("48", "Success");
                xmlmap.put("72", banks);
                xmlmap.put("73", destination);
                System.out.println(xmlmap);

            } else {
                xmlmap.put("39", "333");
                xmlmap.put("48", "Customer not registered".toUpperCase());
                xmlmap.put("72", "");
                xmlmap.put("73", "");
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("BreakdownExceptions", "Exception occured while breaking down customer populate SOA resonse.\nError:" + sw.toString());
            el.log();

            xmlmap.put("39", "333");
            xmlmap.put("48", "Exception occured");
            xmlmap.put("72", "");
            xmlmap.put("73", "");
        }
    }

    private boolean checkinstance(Object obj, String cls)
            throws ClassNotFoundException {
        return Class.forName(cls).isInstance(obj);
    }

}
