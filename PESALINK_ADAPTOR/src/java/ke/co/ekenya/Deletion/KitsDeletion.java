/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Deletion;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.xml.ws.BindingProvider;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DBFunctions;
import ke.co.ekenya.Database.DatabaseModel;
import ke.co.ekenya.DeleteStubs.CustomerType;
import ke.co.ekenya.DeleteStubs.DataInput;
import ke.co.ekenya.DeleteStubs.DeleteInputType;
import ke.co.ekenya.DeleteStubs.PortType;
import ke.co.ekenya.DeleteStubs.ServiceStarter;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.messagehandlers.deleteHandler;
import ke.co.ekenya.messagehandlers.deleteResolver;

/**
 *
 * @author Njiru
 */
public class KitsDeletion {

    Map<String, String> Configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    public String CorrelationID;
    DatabaseModel databaseModel = null;
    EncryptionUtils enc = new EncryptionUtils();

    public KitsDeletion(Map<String, String> xmlmap, String CorrelationID, Map<String, String> Configs) {
        this.xmlmap = xmlmap;
        this.CorrelationID = CorrelationID;
        this.Configs = Configs;
        this.databaseModel = new DatabaseModel();
    }

    public void doProcessing() {
        sendtosoa();

    }

    private void sendtosoa() {
        String response = "";
        try {
            DataInput dataInput = new DataInput();
            DeleteInputType deleteInputType = new DeleteInputType();
            DeleteInputType.OperationParameters op_Params = new DeleteInputType.OperationParameters();
            CustomerType custType = new CustomerType();

            custType.setMobileNumber(xmlmap.get("2"));
            op_Params.setBankSortCode("40411000");
            op_Params.setDefaultRecord("1");

            deleteInputType.setCustomer(custType);
            deleteInputType.setOperationParameters(op_Params);
            dataInput.setDeleteInput(deleteInputType);

            String headerPassword = enc.decrypt(Configs.get("KITSHEADERPASS"));
            String proxypass = enc.decrypt(Configs.get("SOA_PASSWORD"));

            String wsdlLoc = "file:" + Configs.get("PESA_WSDLS_PATH") + "Customer_Customer_delete_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            deleteHandler delHandler = new deleteHandler(Configs, headerPassword, CorrelationID);
            deleteResolver delResolver = new deleteResolver(delHandler);
            service.setHandlerResolver(delResolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request
            BindingProvider prov = (BindingProvider) port;
            prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, Configs.get("SOA_USERNAME"));
            prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            //time-outs
            int connecttimeout = Integer.parseInt(Configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(Configs.get("READTIMEOUT"));
            prov.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            prov.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            port.delete(dataInput);
            response = delHandler.getMyXML();
            breakDownSOAResponse(response);

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SOAExceptions", "Exception occured while while sending customer delete request to SOA.\nError:" + sw.toString());
            el.log();
            String f48 = "";
            String msg = ex.getMessage();
            if (msg.contains("SocketTimeoutException") && msg.contains("HTTP")) {
                f48 = "Connect timed out";
            } else if (msg.contains("SocketTimeoutException")) {
                f48 = "Read timed out";
            } else if (msg.contains("500")) {
                f48 = "Internal Server Error";
            } else {
                f48 = "Generic Error";
            }

            xmlmap.put("39", "333");
            xmlmap.put("48", f48);
        } finally {
            QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
            boolean sent = qr.sendObject(xmlmap, CorrelationID);

            ESBLog lg = new ESBLog("ResponsetoESB", "Sent: " + sent + ", Map: " + xmlmap);
            lg.log();

            if ("000".equals(xmlmap.get("39"))) {
                int del = DBFunctions.deleteCustomer(xmlmap);
                ESBLog l0g = new ESBLog("DeleteCustomer", "Deleted: " + del + ", Map: " + xmlmap);
                l0g.log();
            }
        }
    }

    private void breakDownSOAResponse(String SOAResp) {
        HashMap result = new HashMap<>();
        try {
            Map<String, Object> response = Utilities.convertNodesFromXml(SOAResp);

            //Breaking Down Header
            HashMap<String, Object> header = (HashMap<String, Object>) response.get("SOAP-ENV:Header");
            HashMap<String, Object> headerReply = (HashMap<String, Object>) header.get("ns:HeaderReply");
            HashMap<String, Object> statusMessages = (HashMap<String, Object>) headerReply.get("ns:StatusMessages");
            result.put("statusDescription", (String) headerReply.get("ns:StatusDescription"));
            result.put("statusCode", (String) headerReply.get("ns:StatusCode"));
            result.put("statusDescriptionKey", (String) headerReply.get("ns:StatusDescriptionKey"));

            String MSGSTAT = result.get("statusCode").equals("S_001") ? "SUCCESS" : "FAILURE";
            result.put("MSGSTAT", MSGSTAT);
            if (("SUCCESS").equalsIgnoreCase((MSGSTAT).trim())) {
                result.put("messageID", (String) headerReply.get("ns:MessageID"));

                //status messages breakdown for header
                HashMap statusMessage = (HashMap<String, Object>) statusMessages.get("ns:StatusMessage");
                result.put("messageType", (String) statusMessage.get("ns:MessageType"));
                result.put("applicationID", (String) statusMessage.get("ns:ApplicationID"));
                result.put("messageCode", (String) statusMessage.get("ns:MessageCode"));

                //BreakDown Of Body
                HashMap<String, Object> body = (HashMap<String, Object>) response.get("SOAP-ENV:Body");
                HashMap<String, Object> dataOutput = (HashMap<String, Object>) body.get("ns0:DataOutput");
                HashMap<String, Object> deleteOutput = (HashMap<String, Object>) dataOutput.get("ns1:deleteOutput");
                HashMap<String, Object> operationparmeters = (HashMap<String, Object>) deleteOutput.get("ns1:OperationParameters");
                result.put("RegistrationNumber", (String) operationparmeters.get("ns1:RegistrationNumber"));

                xmlmap.put("39", "000");
                xmlmap.put("48", "Customer deleted successfully");
                xmlmap.put("status", "Success");
                xmlmap.put("message", "Customer deleted successfully");

            } else {

                xmlmap.put("39", "333");
                xmlmap.put("48", "Customer not deleted");
                xmlmap.put("status", "Failure");
                xmlmap.put("message", "Customer not deleted");
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("BreakdownExceptions", "Exception occured while breaking down delete customer SOA resonse.\nError:" + sw.toString());
            el.log();

            xmlmap.put("39", "333");
            xmlmap.put("48", "Exception occured");
            xmlmap.put("status", "Failure");
            xmlmap.put("message", "Exception occured");
        }
    }

}
