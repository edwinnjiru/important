
package ke.co.ekenya.PopulateStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList}getListOutput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getListOutput"
})
@XmlRootElement(name = "DataOutput", namespace = "urn://co-opbank.co.ke/Banking/Customer/Service/CustomerServices/GetList/1.0/DataIO")
public class DataOutput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
    protected GetListOutputType getListOutput;

    /**
     * Gets the value of the getListOutput property.
     * 
     * @return
     *     possible object is
     *     {@link GetListOutputType }
     *     
     */
    public GetListOutputType getGetListOutput() {
        return getListOutput;
    }

    /**
     * Sets the value of the getListOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetListOutputType }
     *     
     */
    public void setGetListOutput(GetListOutputType value) {
        this.getListOutput = value;
    }

}
