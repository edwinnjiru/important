
package ke.co.ekenya.PopulateStubs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getListInputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getListInputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperationParameters" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SourceMsisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DestinationMsisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListInputType", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList", propOrder = {
    "operationParameters"
})
public class GetListInputType {

    @XmlElement(name = "OperationParameters")
    protected GetListInputType.OperationParameters operationParameters;

    /**
     * Gets the value of the operationParameters property.
     * 
     * @return
     *     possible object is
     *     {@link GetListInputType.OperationParameters }
     *     
     */
    public GetListInputType.OperationParameters getOperationParameters() {
        return operationParameters;
    }

    /**
     * Sets the value of the operationParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetListInputType.OperationParameters }
     *     
     */
    public void setOperationParameters(GetListInputType.OperationParameters value) {
        this.operationParameters = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SourceMsisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DestinationMsisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sourceMsisdn",
        "destinationMsisdn",
        "amount"
    })
    public static class OperationParameters {

        @XmlElement(name = "SourceMsisdn", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
        protected String sourceMsisdn;
        @XmlElement(name = "DestinationMsisdn", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
        protected String destinationMsisdn;
        @XmlElement(name = "Amount", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
        protected BigDecimal amount;

        /**
         * Gets the value of the sourceMsisdn property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSourceMsisdn() {
            return sourceMsisdn;
        }

        /**
         * Sets the value of the sourceMsisdn property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSourceMsisdn(String value) {
            this.sourceMsisdn = value;
        }

        /**
         * Gets the value of the destinationMsisdn property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationMsisdn() {
            return destinationMsisdn;
        }

        /**
         * Sets the value of the destinationMsisdn property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationMsisdn(String value) {
            this.destinationMsisdn = value;
        }

        /**
         * Gets the value of the amount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Sets the value of the amount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }

}
