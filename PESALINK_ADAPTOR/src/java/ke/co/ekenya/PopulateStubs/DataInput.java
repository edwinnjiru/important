
package ke.co.ekenya.PopulateStubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList}getListInput" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getListInput"
})
@XmlRootElement(name = "DataInput", namespace = "urn://co-opbank.co.ke/Banking/Customer/Service/CustomerServices/GetList/1.0/DataIO")
public class DataInput {

    @XmlElement(namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
    protected GetListInputType getListInput;

    /**
     * Gets the value of the getListInput property.
     * 
     * @return
     *     possible object is
     *     {@link GetListInputType }
     *     
     */
    public GetListInputType getGetListInput() {
        return getListInput;
    }

    /**
     * Sets the value of the getListInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetListInputType }
     *     
     */
    public void setGetListInput(GetListInputType value) {
        this.getListInput = value;
    }

}
