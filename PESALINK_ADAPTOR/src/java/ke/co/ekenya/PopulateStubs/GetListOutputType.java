
package ke.co.ekenya.PopulateStubs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getListOutputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getListOutputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperationParameters" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DestinationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BankList" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Account" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BankName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="Pan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="SortCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getListOutputType", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList", propOrder = {
    "operationParameters"
})
public class GetListOutputType {

    @XmlElement(name = "OperationParameters")
    protected GetListOutputType.OperationParameters operationParameters;

    /**
     * Gets the value of the operationParameters property.
     * 
     * @return
     *     possible object is
     *     {@link GetListOutputType.OperationParameters }
     *     
     */
    public GetListOutputType.OperationParameters getOperationParameters() {
        return operationParameters;
    }

    /**
     * Sets the value of the operationParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetListOutputType.OperationParameters }
     *     
     */
    public void setOperationParameters(GetListOutputType.OperationParameters value) {
        this.operationParameters = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DestinationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BankList" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Account" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BankName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="Pan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="SortCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "destinationName",
        "bankList"
    })
    public static class OperationParameters {

        @XmlElement(name = "DestinationName", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
        protected String destinationName;
        @XmlElement(name = "BankList", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
        protected List<GetListOutputType.OperationParameters.BankList> bankList;

        /**
         * Gets the value of the destinationName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationName() {
            return destinationName;
        }

        /**
         * Sets the value of the destinationName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationName(String value) {
            this.destinationName = value;
        }

        /**
         * Gets the value of the bankList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the bankList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBankList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GetListOutputType.OperationParameters.BankList }
         * 
         * 
         */
        public List<GetListOutputType.OperationParameters.BankList> getBankList() {
            if (bankList == null) {
                bankList = new ArrayList<GetListOutputType.OperationParameters.BankList>();
            }
            return this.bankList;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Account" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BankName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="Pan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="SortCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "account",
            "bankName",
            "pan",
            "sortCode"
        })
        public static class BankList {

            @XmlElement(name = "Account", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
            protected String account;
            @XmlElement(name = "BankName", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
            protected String bankName;
            @XmlElement(name = "Pan", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
            protected String pan;
            @XmlElement(name = "SortCode", namespace = "urn://co-opbank.co.ke/Banking/Customer/DataModel/CustomerServices/GetList/1.0/CustomerService.getList")
            protected String sortCode;

            /**
             * Gets the value of the account property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAccount() {
                return account;
            }

            /**
             * Sets the value of the account property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAccount(String value) {
                this.account = value;
            }

            /**
             * Gets the value of the bankName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBankName() {
                return bankName;
            }

            /**
             * Sets the value of the bankName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBankName(String value) {
                this.bankName = value;
            }

            /**
             * Gets the value of the pan property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPan() {
                return pan;
            }

            /**
             * Sets the value of the pan property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPan(String value) {
                this.pan = value;
            }

            /**
             * Gets the value of the sortCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSortCode() {
                return sortCode;
            }

            /**
             * Sets the value of the sortCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSortCode(String value) {
                this.sortCode = value;
            }

        }

    }

}
