/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Database;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import ke.co.ekenya.LogEngine.ESBLog;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Njiru
 */
public class DBFunctions extends DatabaseModel {

    String star = "******";

    public DBFunctions() {
    }

    public static String validateMcoopcash(String account) {
        String mcoopAccount = null;
        if (account.trim().startsWith("254") && account.trim().length() == 12) {
            mcoopAccount = account.trim();
        } else if (account.trim().startsWith("07") && account.trim().length() == 10) {
            mcoopAccount = account.trim().replaceFirst("0", "254");
        } else if (account.trim().startsWith("+254") && account.trim().length() == 13) {
            mcoopAccount = account.trim().replaceFirst("\\+", " ").trim();
        } else if (account.trim().startsWith("7") && account.trim().length() == 9) {
            mcoopAccount = "254".concat(account.trim()).trim();
        }
        return mcoopAccount;
    }

    public static String generateField126(String mwalletno, String account, String field93) {
        String field126 = "";
        String customerName = "";
        String firstName = "";
        String secondName = "";
        String threePad = "%03d";
        String twoPad = "%02d";
        try {
            Map<String, String> customerDetails = getNames(mwalletno);
            firstName = customerDetails.get("firstname");
            secondName = customerDetails.get("lastname");

            //namebuilder
            StringBuilder nameBuilder = new StringBuilder();
            if ((firstName.length() > 0) && (secondName.length() > 0)) {
                nameBuilder.append(firstName).append(" ").append(secondName);
            } else if (firstName.length() > 0) {
                nameBuilder.append(firstName);
            } else {
                nameBuilder.append(secondName);
            }
            customerName = nameBuilder.toString();

            StringBuilder mssrsb = new StringBuilder();
            mssrsb.append("01").append(String.format(twoPad, customerName.length())).append(customerName).append("0200030011").append(String.format(twoPad, account.length())).append(account);
            String mssrTag = mssrsb.toString();

            StringBuilder sb = new StringBuilder();
            sb.append("NRTV").append(String.format(threePad, field93.length())).append(field93).append("MSSR").append(String.format(threePad, mssrTag.length())).append(mssrTag);
            field126 = sb.toString();

        } catch (Exception ex) {
            ESBLog es = new ESBLog("generate126Exception", ex.getMessage());
            es.log();
        }
        return field126;
    }

    public static String generatefield126reciever(String accountno) {
        String field126 = "";
        String customerName = "";
        String firstName = "";
        String secondName = "";
        String threePad = "%03d";
        String twoPad = "%02d";
        try {
            Map<String, String> customerDetails = getNames(accountno);
            firstName = customerDetails.get("firstname");
            secondName = customerDetails.get("lastname");
            //name string builder                
            StringBuilder nameBuilder = new StringBuilder();
            if ((firstName.length() > 0) && (secondName.length() > 0)) {
                nameBuilder.append(firstName).append(" ").append(secondName);
            } else if (firstName.length() > 0) {
                nameBuilder.append(firstName);
            } else {
                nameBuilder.append(secondName);
            }
            customerName = nameBuilder.toString();

            StringBuilder sb = new StringBuilder();
            sb.append("01").append(String.format(twoPad, customerName.length())).append(customerName).append("0200030011").append(String.format(twoPad, accountno.length())).append(accountno);
            String mssrtag = sb.toString();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("MSRV").append(String.format(threePad, mssrtag.length())).append(mssrtag);
            field126 = stringBuilder.toString();
        } catch (Exception ex) {
            ESBLog es = new ESBLog("generate126recieveException", ex.getMessage());
            es.log();
        }
        return field126;
    }

    public static Map getNames(String walletno) {
        ResultSet rs = null;
        String command = "{call SP_KITSGETCUSTOMERNAMES(?,?)}";
        Map<String, String> response = new HashMap();
        try (Connection conn = createConnection();
                CallableStatement cStmt = conn.prepareCall(command);) {

            cStmt.setString("IV_MWALLET", walletno.trim());
            cStmt.registerOutParameter("cv_1", OracleTypes.CURSOR);
            cStmt.execute();

            rs = (ResultSet) cStmt.getObject("cv_1");
            while (rs.next()) {
                String fname=(rs.getString(1) != null) ? rs.getString(1) : "";
                String firstnames[]=fname.split(" ");
                
                String lname=(rs.getString(2) != null) ? rs.getString(2) : "";
                String lastnames[]=lname.split(" ");
                
                response.put("name", firstnames[0] + " " + lastnames[0]);
                response.put("firstname", firstnames[0]);
                response.put("lastname", lastnames[0]);
            }

        } catch (SQLException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es = new ESBLog("DatabaseException", "Error getting customer details for no: " + walletno + ", error: " + sw.toString());
            es.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog es = new ESBLog("DatabaseException", "Error closing getdetails resultset, error: " + sw.toString());
                es.log();
            }
        }
        return response;
    }

    public static int deleteCustomer(Map<String, String> xmlMap) {
        int delete = 0;
        String command = "{call SP_DELETEKITSCUSTOMER(?,?)}";
        try (Connection conn = createConnection();
                CallableStatement cStmt = conn.prepareCall(command);) {
            cStmt.setString("V_FIELD2", xmlMap.get("2"));
            cStmt.registerOutParameter("cv_1", java.sql.Types.VARCHAR);
            cStmt.execute();
            delete = Integer.valueOf(cStmt.getString("cv_1"));
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Error in deletecustomer function: " + sw.toString());
            el.log();
        }
        return delete;
    }
    
    public boolean checkRegistrationStatus(Map<String, String> xmlMap){
        boolean registered=false;
        String command = "{call SP_CHECKKITS_REGISTRATION(?,?)}";
        try (Connection conn= createConnection();
                CallableStatement stmt=conn.prepareCall(command)){
            stmt.setString("IV_WALLETNO", xmlMap.get("2"));
            stmt.registerOutParameter("cv_1", java.sql.Types.VARCHAR);
            stmt.execute();
            registered=Boolean.valueOf(stmt.getString("cv_1"));
            
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Error in check customer registration function: " + sw.toString());
            el.log();
        }
    return registered;
    }

    public int updatetran(String refno, String stan, String actioncode, String narration, String mti) {
        int update = 0;
        String command = "{call SP_KITSUPDATE(?,?,?,?,?,?)}";
        try (Connection conn = createMvisaConnection();
                CallableStatement cStmt = conn.prepareCall(command)) {
            cStmt.setString("refno", refno);
            cStmt.setString("stan", stan);
            cStmt.setString("actioncode", actioncode);
            cStmt.setString("narration", narration);
            cStmt.setString("mti", mti);

            cStmt.registerOutParameter("RESP", OracleTypes.VARCHAR);
            cStmt.execute();

            String respString = (String) cStmt.getString("RESP");
            if (respString.toLowerCase().contains("success")) {
                update = 1;
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es = new ESBLog("DatabaseException", "Error updating transaction ref: " + refno + " error:" + sw.toString());
            es.log();
        }

        return update;
    }

    public int updateReg(Map<String, String> regMap) {
        int update = 0;
        String command = "{call SP_KITSREG_UPDATE(?,?,?,?)}";
        try (Connection con = createConnection();
                CallableStatement stmt = con.prepareCall(command)) {
            stmt.setString("IV_MWALLETNO", regMap.get("2"));
            stmt.setString("IV_REGISTERED", regMap.get("registered"));
            stmt.setString("IV_PROCESSED", regMap.get("1"));
            stmt.setString("IV_NARRATION", regMap.get("48"));
            stmt.registerOutParameter("RESP", OracleTypes.VARCHAR);
            stmt.execute();

            String respString = (String) stmt.getString("RESP");
            if (respString.toLowerCase().contains("success")) {
                update = 1;
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es = new ESBLog("DatabaseException", "Error updating customer registration: " + regMap.toString() + " error:" + sw.toString());
            es.log();
        }
        return update;
    }

    public String getCorrelationID() {
        String CorrelationID = "";
        String command = "{call SP_GET_ESB_NEXT_SEQ(?)}";
        try (Connection con = createMvisaConnection();
                CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.registerOutParameter("cv_1", java.sql.Types.VARCHAR);
            callableStatement.executeUpdate();
            CorrelationID = callableStatement.getString("cv_1");
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Error in correlationID function: " + sw.toString());
            el.log();
        }
        return CorrelationID;
    }

    public int insertTransaction(Map<String, String> xmlMap) { //xmlMap.get("");
        int insert = 0;
        ResultSet rs = null;
        String command = "{call SP_INSERT_KITSTRAN(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        try (Connection con = createMvisaConnection();
                CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.setString("V_MSGTYPE", xmlMap.get("MTI"));
            callableStatement.setString("V_FIELD2", xmlMap.get("2"));
            callableStatement.setString("V_FIELD3", xmlMap.get("3"));
            if ("1210".equalsIgnoreCase(xmlMap.get("MTI")) || "1430".equalsIgnoreCase(xmlMap.get("MTI"))) {
                callableStatement.setDouble("V_FIELD4", Double.valueOf(xmlMap.get("4")));
                callableStatement.setString("V_FIELD5", xmlMap.get("4"));
                callableStatement.setString("V_FIELD11", xmlMap.get("41"));
            } else {
                callableStatement.setDouble("V_FIELD4", (Double.valueOf(xmlMap.get("4")) + Double.valueOf(xmlMap.get("EXTRACHARGE"))));
                callableStatement.setString("V_FIELD5", xmlMap.get("4"));
                callableStatement.setString("V_FIELD11", xmlMap.get("11"));
            }
            callableStatement.setString("V_FIELD24", xmlMap.get("24"));
            callableStatement.setString("V_FIELD32", xmlMap.get("32"));
            callableStatement.setString("V_FIELD33", "");
            callableStatement.setString("V_FIELD35", xmlMap.get("74"));
            callableStatement.setString("V_FIELD37", xmlMap.get("37"));
            callableStatement.setString("V_FIELD38", xmlMap.get("38"));
            callableStatement.setString("V_FIELD39", xmlMap.get("39"));
            callableStatement.setString("V_FIELD56", xmlMap.get("56"));
            callableStatement.setString("V_FIELD126", xmlMap.get("126"));
            callableStatement.setString("V_FIELD93", xmlMap.get("93"));
            if (xmlMap.get("102").length() == 16) {
                callableStatement.setString("V_FIELD102", (new StringBuilder(xmlMap.get("102"))).replace(5, 11, star).toString());
            } else {
                callableStatement.setString("V_FIELD102", xmlMap.get("102"));
            }
            if (xmlMap.get("103").length() == 16) {
                callableStatement.setString("V_FIELD103", (new StringBuilder(xmlMap.get("103"))).replace(5, 11, star).toString());
            } else {
                callableStatement.setString("V_FIELD103", xmlMap.get("103"));
            }
            callableStatement.setString("V_CHANNEL", xmlMap.get("CHANNEL"));
            callableStatement.setString("V_MSGID", "");
            callableStatement.setString("V_FIELD6", "");
            callableStatement.setString("V_FIELD7", "");
            callableStatement.setString("V_FIELD10", "");
            callableStatement.setString("V_FIELD14", xmlMap.get("14"));
            callableStatement.setString("V_FIELD15", "");
            callableStatement.setString("V_FIELD19", "");
            callableStatement.setString("V_FIELD22", "");
            callableStatement.setString("V_FIELD23", "");
            callableStatement.setString("V_FIELD25", "");
            callableStatement.setString("V_FIELD26", "");
            callableStatement.setString("V_FIELD28", "");
            callableStatement.setString("V_FIELD29", "");
            if (xmlMap.get("30").length() > 0) {
                callableStatement.setString("V_FIELD30", xmlMap.get("30"));
            } else {
                callableStatement.setString("V_FIELD30", "");
            }
            callableStatement.setString("V_FIELD31", xmlMap.get("31"));
            callableStatement.setString("V_FIELD40", xmlMap.get("40"));
            callableStatement.setString("V_FIELD41", xmlMap.get("41"));
            callableStatement.setString("V_FIELD42", xmlMap.get("42"));
            callableStatement.setString("V_FIELD43", xmlMap.get("43"));
            callableStatement.setString("V_FIELD45", "");
            callableStatement.setString("V_FIELD46", "");
            callableStatement.setString("V_FIELD48", xmlMap.get("48"));
            callableStatement.setString("V_FIELD49", xmlMap.get("49"));
            callableStatement.setString("V_FIELD50", xmlMap.get("50"));
            callableStatement.setString("V_FIELD51", xmlMap.get("51"));
            if (xmlMap.containsKey("sender")) {
                callableStatement.setString("V_FIELD52", xmlMap.get("sender"));
            } else {
                callableStatement.setString("V_FIELD52", "");
            }
            callableStatement.setString("V_FIELD53", "");
            callableStatement.setString("V_FIELD55", "");
            callableStatement.setString("V_FIELD72", xmlMap.get("72"));
            callableStatement.setString("V_FIELD94", xmlMap.get("94"));
            callableStatement.setString("V_FIELD95", "");
            callableStatement.setString("V_FIELD100", xmlMap.get("100"));
            callableStatement.setString("V_FIELD101", xmlMap.get("101"));
            callableStatement.setString("V_FIELD104", "");
            callableStatement.setString("V_FIELD122", "");
            callableStatement.setString("V_FIELD123", "");
            callableStatement.setString("V_Msg", "");
            callableStatement.setString("V_COMM_MSG", "");
            if ("1420".equals(xmlMap.get("MTI"))) {
                callableStatement.setString("V_PROCESSED", "1");
                callableStatement.setString("V_NARRATION", (new StringBuilder()).append("Sent at: ").append(new Date()).toString());
            } else {
                callableStatement.setString("V_PROCESSED", xmlMap.get("processed"));
                callableStatement.setString("V_NARRATION", xmlMap.get("narration"));
            }
            callableStatement.registerOutParameter("cv_1", OracleTypes.CURSOR);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject("cv_1");
            while (rs.next()) {
                insert = rs.getInt(1);
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("InsertExceptions", "Error: " + sw.toString() + "***inserting transaction: " + xmlMap);
            el.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }

        }

        return insert;
    }

    public int insertRegistration(Map<String, String> xmlMap) {
        int insert = 0;
        ResultSet rs = null;
        String command = "{call SP_INSERTKITSREGISTRATON(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        try (Connection con = createConnection();
                CallableStatement callableStatement = con.prepareCall(command);) {
            if ("".equals(xmlMap.get("75"))) {
                callableStatement.setString("IV_COOP_AC1", xmlMap.get("2"));
            } else {
                callableStatement.setString("IV_COOP_AC1", xmlMap.get("75"));
            }
            callableStatement.setString("IV_SORT", "40411000");
            callableStatement.setString("IV_DOC_TYPE", xmlMap.get("72"));
            callableStatement.setString("IV_DOC_NO", xmlMap.get("71"));
            callableStatement.setString("IV_FIRST_NAME", xmlMap.get("73"));
            callableStatement.setString("IV_EMAIL", xmlMap.get("74"));
            callableStatement.setString("IV_LANG", "EN");
            callableStatement.setString("IV_PROCESSED", "1");
            callableStatement.setString("IV_REGISTERED", xmlMap.get("registered"));
            callableStatement.setString("IV_NARRATION", xmlMap.get("48"));
            callableStatement.setString("IV_MWALLETACCOUNT", xmlMap.get("2"));
            callableStatement.setString("IV_CHANNEL", xmlMap.get("32"));

            callableStatement.registerOutParameter("cv_1", OracleTypes.CURSOR);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject("cv_1");
            while (rs.next()) {
                insert = rs.getInt(1);
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("InsertExceptions", "Error: " + sw.toString() + "***inserting registration: " + xmlMap);
            el.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return insert;
    }

    public HashMap getSMSTemplate(String smsCode) {
        HashMap detailsMap = new HashMap<>();
        String template = "";
        String command = "{call SP_GET_SMS_TEMPLATE(?,?)}";
        ResultSet rs = null;

        try (Connection con = createMvisaConnection();
                CallableStatement stmt = con.prepareCall(command);) {
            stmt.setString("IV_SMSCODE", smsCode);
            stmt.registerOutParameter("cv_1", OracleTypes.CURSOR);
            stmt.execute();

            rs = (ResultSet) stmt.getObject("cv_1");
            while (rs.next()) {
                template = rs.getString("MESSAGE");
            }

            detailsMap.put("SMSTEMPLATE", template);

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Exception occured while calling"
                    + " SP_GET_SMS_TEMPLATE SP to get sms template for sms code:" + smsCode + "|Error:" + sw.toString() + "\n");
            el.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog("SQLExceptions", "Error occured closing resultset of get sms template function. Error: " + sw.toString());
                el.log();
            }
        }

        return detailsMap;

    }

    public String getAnyGLAccount(String glName) {
        String glAccount = "";
        ResultSet rs = null;
        String command = "{call SP_GETANYGLACCOUNT(?,?)}";
        try (Connection con = createMvisaConnection();
                CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.setString("iv_glName", glName);
            callableStatement.registerOutParameter("cv_1", OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            rs = (ResultSet) callableStatement.getObject("cv_1");
            while (rs.next()) {
                glAccount = rs.getString(1);
            }
        } catch (Exception e) {
            ESBLog el = new ESBLog(e, "");
            el.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog("SQLExceptions", "Error occured closing resultset of get gl function. Error: " + sw.toString());
                el.log();
            }
        }
        return glAccount;
    }
}
