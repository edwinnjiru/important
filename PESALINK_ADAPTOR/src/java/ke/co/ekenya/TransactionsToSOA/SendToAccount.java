/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.TransactionsToSOA;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import ke.co.ekenya.CommonOperations.DistributedWebLogicQueueBrowser;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DBFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.SendTransactionStubs.AccountTransactionType;
import ke.co.ekenya.SendTransactionStubs.CardType;
import ke.co.ekenya.SendTransactionStubs.DataInput;
import ke.co.ekenya.SendTransactionStubs.PortType;
import ke.co.ekenya.SendTransactionStubs.SendInputType;
import ke.co.ekenya.SendTransactionStubs.ServiceStarter;
import ke.co.ekenya.SendTransactionStubs.TransactionType;
import ke.co.ekenya.messagehandlers.SendTranHandler;
import ke.co.ekenya.messagehandlers.SendTranResolver;

/**
 *
 * @author Njiru
 */
public class SendToAccount {

    Map<String, String> Configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    Map<String, String> esbMap = new HashMap();
    public String CorrelationID;
    EncryptionUtils enc=new EncryptionUtils();
    DistributedWebLogicQueueBrowser queueBrowser=null;

    public SendToAccount(Map<String, String> xmlmap, String CorrelationID, Map<String, String> Configs) {
        this.xmlmap = xmlmap;
        this.CorrelationID = CorrelationID;
        this.Configs = Configs;
        this.queueBrowser=new DistributedWebLogicQueueBrowser();
    }

    public void doProcessing() {
        Map<String, String> soaRes = sendToSOA(xmlmap);
        if ("true".equals(soaRes.get("Error")) && "Connect timed out".equals(soaRes.get("response"))) {
            //browse queue and send reversal
            esbMap = queueBrowser.getWeblogicMessageFromUDQueue(CorrelationID);
            esbMap.put("REVERSE", "1");
            esbMap.put("48", "SOA Connection timeout");

            QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
            boolean sent=qr.sendObject(esbMap, CorrelationID, "PESALINK_FT");
            ESBLog logg = new ESBLog("ResponsetoESB", "Sent: " + sent + " Map: " + esbMap);
            logg.log();

        }
    }

    public Map<String, String> sendToSOA(Map<String, String> xmlmap) {
        Map<String, String> resMap = new HashMap();

        String response = "";
        String mti;
        int functioncode;
        try {
            DataInput dataInput = new DataInput();
            SendInputType inputType = new SendInputType();
            TransactionType trantype = new TransactionType();
            AccountTransactionType acc_details = new AccountTransactionType();
            CardType cardDetails = new CardType();
            SendInputType.OperationParameters Op_Params = new SendInputType.OperationParameters();

            GregorianCalendar gCal = new GregorianCalendar();
            gCal.setTime(new Date());
            XMLGregorianCalendar gDateUnformated = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal);

            if (("1200").equals(xmlmap.get("MTI"))) {
                //original transaction
                mti = "1200";
                functioncode = 200;
            } else {
                //reversals
                mti = "1420";
                functioncode = 400;
                Op_Params.setMessageReasonCode(4021);
                Op_Params.setActionCode("400");
                String acquirerlen = "06";
                String ordelements = "1200" + xmlmap.get("11") + xmlmap.get("12") + acquirerlen + "440782";
                Op_Params.setOriginalDataElements(ordelements);
            }

            Op_Params.setMessageTypeIdentification(mti);
            Op_Params.setProcessingCode(500077);
            

            String Amount = xmlmap.get("4");
            String com=xmlmap.get("EXTRACHARGE");
            Double k = Double.parseDouble(Amount) + Double.parseDouble(com);
            String AmountNoCommission = xmlmap.get("4");
            Double AmountNoCommission1 = Double.parseDouble(AmountNoCommission);
            BigDecimal comm = BigDecimal.valueOf(k).subtract(BigDecimal.valueOf(AmountNoCommission1));
            int AmountCommision = Integer.parseInt(Utilities.amountInCents(String.valueOf(comm)));
            int Amounth = (int) (AmountNoCommission1 * 100) + AmountCommision;
            Op_Params.setCardholderBillingAmount(String.valueOf(Amounth));


            Op_Params.setTransmissionDate(gDateUnformated);
            Op_Params.setSystemTraceNumber(Integer.valueOf(xmlmap.get("11")));
            Op_Params.setPOSDataCode("211101954240");
            Op_Params.setFunctionCode(functioncode);
            Op_Params.setCardAcceptorBusinessCode(5999);
            Op_Params.setAcquirerID("440782");
            Op_Params.setForwarderID("BANK11");
            
             //to be interchanged once EC issue is resolved
            Op_Params.setRetrievalReferenceNumber(xmlmap.get("kref"));
            //Op_Params.setRetrievalReferenceNumber(CorrelationID);
            Op_Params.setTerminalID("MCS04895");
            Op_Params.setCardAcceptorIdentification("MCSS00001");
            Op_Params.setCardAcceptorNameLocation("KENYA BANKERS SWITCH>NAIROBI KE");

            String feesamount = "70404D0000";
            String feesamount2 = "21000000C0000";
            String feesamount3 = "404";
            String AmountCommisionStr = String.format("%04d", AmountCommision);
            String finalFeesAmmount = (feesamount + AmountCommisionStr + feesamount2 + AmountCommisionStr + feesamount3);
            Op_Params.setFeesAmount(finalFeesAmmount);

            String sortCode = "40411000|";
            String sortcode2 = xmlmap.get("56");
            String sortLength = ("sort|" + sortCode + ";sort2|" + sortcode2 + "|;terminal_id|MCS04895|;transaction_type|credit_push|");
            String sortLenght1 = "SVCT0000204PAYD000" + sortLength.length() + "" + sortLength;
            String sortLenght2 = "EBP" + sortLenght1.length() + "" + sortLenght1;
            Op_Params.setDataRecord(sortLenght2);

            Op_Params.setDestinationCountryCode("NATIVE");
            Op_Params.setOriginatorCountryCode("NATIVE");
            Op_Params.setReceivingInstitutionID("NATIVE");

            Op_Params.setAccountIdentification1(xmlmap.get("102"));
            Op_Params.setAccountIdentification2(xmlmap.get("103"));

            Op_Params.setReservedPrivateUse126(DBFunctions.generateField126(xmlmap.get("2"), xmlmap.get("102"),xmlmap.get("93")));

            trantype.setTransactionDate(gDateUnformated);
            trantype.setTransactionAmount(BigDecimal.valueOf(Amounth));
            trantype.setTransactionCurrency("404");
            trantype.setCountryCode("404");

            acc_details.setAccountNumber("1140411000000000");

            cardDetails.setExpiryDate(gDateUnformated);

            inputType.setAccountTransaction(acc_details);
            inputType.setTransaction(trantype);
            inputType.setCard(cardDetails);
            inputType.setOperationParameters(Op_Params);
            dataInput.setSendInput(inputType);

            String headerPassword = enc.decrypt(Configs.get("KITSHEADERPASS"));
            String proxypass = enc.decrypt(Configs.get("SOA_PASSWORD"));

            //Message Handler Resolver
            String wsdlLoc = "file:" + Configs.get("PESA_WSDLS_PATH") + "Account_AccountFundsTransfer_send_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            SendTranHandler headerHandler = new SendTranHandler(CorrelationID, Configs, headerPassword);
            SendTranResolver handlerResolver = new SendTranResolver(headerHandler);
            service.setHandlerResolver(handlerResolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request with proxy username and password
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, Configs.get("SOA_USERNAME")); //Configs.get("B2CSENDUNAME")
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);
            
                        //time-outs
            int connecttimeout = Integer.parseInt(Configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(Configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            //sendtoSOA
            port.send(dataInput);
            response = headerHandler.getMyXML();

            if (response == null || response.isEmpty() || response.trim().equalsIgnoreCase("")) {
                resMap.put("Error", "true");
                resMap.put("response", "Generic error");
            } else {
                resMap.put("Error", "false");
                resMap.put("response", response);
            }

//            System.out.println(response);
        } catch (Exception ex) {
            resMap.put("Error", "true");
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SOAExceptions", "Exception occured while while sending request to SOA.\nError:" + sw.toString());
            el.log();
            String msg = ex.getMessage();
            if (msg.contains("SocketTimeoutException") && msg.contains("HTTP")) {
                response = "Connect timed out";
            } else if (msg.contains("SocketTimeoutException")) {
                response = "Read timed out";
            } else if (msg.contains("500")) {
                response = "Internal Server Error";
            } else {
                response = "Generic Error";
            }

            resMap.put("response", response);
            return resMap;
        }

        return resMap;
    }

}
