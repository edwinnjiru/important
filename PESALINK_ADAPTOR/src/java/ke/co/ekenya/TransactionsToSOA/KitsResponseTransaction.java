/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.TransactionsToSOA;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import ke.co.ekenya.CommonOperations.BFUBResponseMapping;
import ke.co.ekenya.CommonOperations.EncryptionUtils;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DBFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.SendTransactionStubs.AccountTransactionType;
import ke.co.ekenya.SendTransactionStubs.CardType;
import ke.co.ekenya.SendTransactionStubs.DataInput;
import ke.co.ekenya.SendTransactionStubs.PortType;
import ke.co.ekenya.SendTransactionStubs.SendInputType;
import ke.co.ekenya.SendTransactionStubs.ServiceStarter;
import ke.co.ekenya.SendTransactionStubs.TransactionType;
import ke.co.ekenya.mdb.KitsMDB;
import ke.co.ekenya.messagehandlers.SendTranHandler;
import ke.co.ekenya.messagehandlers.SendTranResolver;

/**
 *
 * @author Njiru
 */
public class KitsResponseTransaction {

    Map<String, String> Configs = new HashMap();
    Map<String, String> xmlmap = new HashMap();
    public String CorrelationID;
    DBFunctions dbFunctions;
    String sender = "";

    public KitsResponseTransaction(Map<String, String> xmlmap, String CorrelationID) {
        this.Configs = KitsMDB.Configs;
        this.xmlmap = xmlmap;
        this.CorrelationID = CorrelationID;
        this.dbFunctions = new DBFunctions();
    }

    public void doProcessing() {
        if(Configs.isEmpty()){
        KitsMDB.readPropertyFile(Configs);
        ESBLog conf=new ESBLog("ReadConfig", "Kits Response config: "+Configs);
        conf.log();
        }
        String msgtype = xmlmap.get("MTI");
        switch (msgtype) {
            case "1210":
                responses();
                break;
            case "1430":
                timeouts();
                break;
            default:
                break;
        }

    }

    public void responses() {
        String response = "";
        EncryptionUtils enc = new EncryptionUtils();
        try {
            DataInput dataInput = new DataInput();
            SendInputType inputType = new SendInputType();
            TransactionType trantype = new TransactionType();
            AccountTransactionType acc_details = new AccountTransactionType();
            CardType cardDetails = new CardType();
            SendInputType.OperationParameters Op_Params = new SendInputType.OperationParameters();

            String Amount = xmlmap.get("4");
            Double k = new Double(Amount) * 100;
            int Amounth = k.intValue();
            String AmountinCents = Integer.toString(Amounth);
            Op_Params.setReservedPrivateUse126(DBFunctions.generatefield126reciever(xmlmap.get("103")));
            Op_Params.setOriginatorCountryCode(xmlmap.get("123"));
            Op_Params.setDestinationCountryCode(xmlmap.get("101"));
            Op_Params.setTerminalID(xmlmap.get("53"));
            Op_Params.setPOSDataCode(xmlmap.get("52"));
            Op_Params.setInitiatingSystem(xmlmap.get("77"));

            String actioncode = BFUBResponseMapping.respmap(xmlmap.get("39"));
            Op_Params.setActionCode(actioncode);

            Op_Params.setFunctionCode(200);
            Op_Params.setMessageTypeIdentification(xmlmap.get("MTI"));
            Op_Params.setProcessingCode(260000);
            trantype.setTransactionDescription(xmlmap.get("43"));
            Op_Params.setAccountIdentification1(xmlmap.get("103"));
            Op_Params.setAcquirerID(xmlmap.get("50"));
            Op_Params.setCardAcceptorBusinessCode(Integer.parseInt(xmlmap.get("49")));
            Op_Params.setCardAcceptorIdentification(xmlmap.get("76"));
            Op_Params.setCardIssuerReferenceData(xmlmap.get("126"));

            trantype.setConversionRate(BigDecimal.valueOf(Integer.parseInt("61000000")));
            String drecord = xmlmap.get("95");
            String FLD_094 = drecord.replace("*", "|");
            Op_Params.setDataRecord(FLD_094);
            Op_Params.setRetrievalReferenceNumber(xmlmap.get("37"));
            Op_Params.setSystemTraceNumber(Integer.parseInt(xmlmap.get("41")));
            GregorianCalendar g_Cal = new GregorianCalendar();
            g_Cal.setTime(new Date());
            Date dob_new_1 = javax.xml.bind.DatatypeConverter.parseDateTime(xmlmap.get("14")).getTime();
            GregorianCalendar gCal1 = new GregorianCalendar();
            gCal1.setTime(dob_new_1);
            XMLGregorianCalendar gDateUnformated1 = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal1);
            Op_Params.setTransmissionDate(gDateUnformated1);
            trantype.setTransactionDate(gDateUnformated1);
            trantype.setTransactionAmount(BigDecimal.valueOf(Integer.parseInt(AmountinCents)));
            Op_Params.setForwarderID(xmlmap.get("51"));
            acc_details.setAccountNumber(xmlmap.get("72"));
            Op_Params.setApprovalCode(xmlmap.get("11"));
            Op_Params.setReceivingInstitutionID(xmlmap.get("73"));
            trantype.setTransactionCurrency("404");
            inputType.setAccountTransaction(acc_details);
            inputType.setCard(cardDetails);
            inputType.setOperationParameters(Op_Params);
            inputType.setTransaction(trantype);

            dataInput.setSendInput(inputType);

            //Decrypting
            String headerPassword = enc.decrypt(Configs.get("KITSHEADERPASS").trim());
            String proxypass = enc.decrypt(Configs.get("SOA_PASSWORD").trim());
            
//            ESBLog pas=new ESBLog("Passwords", "header pass: "+Configs.get("KITSHEADERPASS").trim()+" proxy pass: "+Configs.get("SOA_PASSWORD").trim()+" Proxy username: "+Configs.get("SOA_USERNAME"));
//            pas.log();

            //Message Handler Resolver
            String wsdlLoc = "file:" + Configs.get("PESA_WSDLS_PATH") + "Account_AccountFundsTransfer_send_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            SendTranHandler headerHandler = new SendTranHandler(CorrelationID, Configs, headerPassword);
            SendTranResolver handlerResolver = new SendTranResolver(headerHandler);
            service.setHandlerResolver(handlerResolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request with proxy username and password
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, Configs.get("SOA_USERNAME").trim()); //Configs.get("B2CSENDUNAME")
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);//

            int connecttimeout = Integer.parseInt(Configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(Configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);
            //sendtoSOA
            port.send(dataInput);
            response = headerHandler.getMyXML();
            if (!"".equals(response)) {
                xmlmap.put("narration", "Sent Successfully at " + (new Date()));
                xmlmap.put("processed", "1");
            } else {
                xmlmap.put("narration", "Transaction not acknowledged at " + (new Date()));
                xmlmap.put("processed", "2");
            }
            sender = (new Utilities()).getsender(xmlmap.get("96"));
            xmlmap.put("sender", sender);
            dbFunctions.insertTransaction(xmlmap);
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SOAExceptions", "Exception occured while while sending request to SOA.\nError:" + sw.toString() + " Map: " + xmlmap.toString());
            el.log();
        }

    }

    public void timeouts() {
        String response = "";
        EncryptionUtils enc = new EncryptionUtils();
        try {
            DataInput dataInput = new DataInput();
            SendInputType inputType = new SendInputType();
            TransactionType trantype = new TransactionType();
            AccountTransactionType acc_details = new AccountTransactionType();
            CardType cardDetails = new CardType();
            SendInputType.OperationParameters Op_Params = new SendInputType.OperationParameters();

            String Amount = xmlmap.get("4");
            Double k = new Double(Amount) * 100;
            int Amounth = k.intValue();
            //Integer.parseInt(Amount);
            String AmountinCents = Integer.toString(Amounth);

            Op_Params.setReservedPrivateUse126(DBFunctions.generatefield126reciever(xmlmap.get("103")));
            Op_Params.setOriginatorCountryCode(xmlmap.get("123"));
            Op_Params.setDestinationCountryCode(xmlmap.get("101"));
            Op_Params.setTerminalID(xmlmap.get("53"));
            Op_Params.setPOSDataCode(xmlmap.get("52"));
            Op_Params.setInitiatingSystem(xmlmap.get("77"));

            Op_Params.setOriginalDataElements(xmlmap.get("94"));
            Op_Params.setMessageReasonCode(4021);
            Op_Params.setActionCode("400");
            Op_Params.setFunctionCode(400);
            Op_Params.setMessageTypeIdentification(xmlmap.get("MTI"));
            Op_Params.setOriginalAmounts(AmountinCents);
            Op_Params.setProcessingCode(260000);
            trantype.setTransactionDescription(xmlmap.get("43"));
            Op_Params.setAccountIdentification1(xmlmap.get("103"));
            Op_Params.setAcquirerID(xmlmap.get("50"));
            Op_Params.setCardAcceptorBusinessCode(Integer.parseInt(xmlmap.get("49")));
            Op_Params.setCardAcceptorIdentification(xmlmap.get("76"));
            Op_Params.setCardIssuerReferenceData(xmlmap.get("126"));

            trantype.setConversionRate(BigDecimal.valueOf(Integer.parseInt("61000000")));
            String drecord = xmlmap.get("95");
            String FLD_094 = drecord.replace("*", "|");
            Op_Params.setDataRecord(FLD_094);
            Op_Params.setRetrievalReferenceNumber(xmlmap.get("37"));
            Op_Params.setSystemTraceNumber(Integer.parseInt(xmlmap.get("41")));
            GregorianCalendar g_Cal = new GregorianCalendar();
            g_Cal.setTime(new Date());
            Date dob_new_1 = javax.xml.bind.DatatypeConverter.parseDateTime(xmlmap.get("14")).getTime();
            GregorianCalendar gCal1 = new GregorianCalendar();
            gCal1.setTime(dob_new_1);
            XMLGregorianCalendar gDateUnformated1 = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCal1);
            Op_Params.setTransmissionDate(gDateUnformated1);
            trantype.setTransactionDate(gDateUnformated1);
            trantype.setTransactionAmount(BigDecimal.valueOf(Integer.parseInt(AmountinCents)));
            Op_Params.setForwarderID(xmlmap.get("51"));
            acc_details.setAccountNumber(xmlmap.get("72"));
            Op_Params.setApprovalCode(xmlmap.get("11"));
            Op_Params.setReceivingInstitutionID(xmlmap.get("73"));
            trantype.setTransactionCurrency("404");
            inputType.setAccountTransaction(acc_details);
            inputType.setCard(cardDetails);
            inputType.setOperationParameters(Op_Params);
            inputType.setTransaction(trantype);

            dataInput.setSendInput(inputType);

            //Decrypting
            String headerPassword = enc.decrypt(Configs.get("KITSHEADERPASS"));
            String proxypass = enc.decrypt(Configs.get("SOA_PASSWORD"));
            
//            ESBLog pas=new ESBLog("Passwords", "header pass: "+Configs.get("KITSHEADERPASS").trim()+" proxy pass: "+Configs.get("SOA_PASSWORD").trim()+" Proxy username: "+Configs.get("SOA_USERNAME"));
//            pas.log();

            //Message Handler Resolver
            String wsdlLoc = "file:" + Configs.get("PESA_WSDLS_PATH") + "Account_AccountFundsTransfer_send_1.0.wsdl";
            URL url = new URL(wsdlLoc);
            ServiceStarter service = new ServiceStarter(url);
            SendTranHandler headerHandler = new SendTranHandler(CorrelationID, Configs, headerPassword);
            SendTranResolver handlerResolver = new SendTranResolver(headerHandler);
            service.setHandlerResolver(handlerResolver);
            PortType port = service.getSOAPSpEventSpSource();

            //Binding request with proxy username and password
            BindingProvider provider = (BindingProvider) port;
            provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, Configs.get("SOA_USERNAME")); //Configs.get("B2CSENDUNAME")
            provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, proxypass);

            int connecttimeout = Integer.parseInt(Configs.get("CONNECTTIMEOUT"));
            int readtimeout = Integer.parseInt(Configs.get("READTIMEOUT"));
            provider.getRequestContext().put("com.sun.xml.ws.connect.timeout", connecttimeout);
            provider.getRequestContext().put("com.sun.xml.ws.request.timeout", readtimeout);

            //sendtoSOA
            port.send(dataInput);
            response = headerHandler.getMyXML();
//            System.out.println(response);
            if (!"".equals(response)) {
                xmlmap.put("narration", "Sent Successfully at " + (new Date()));
                xmlmap.put("processed", "1");
            } else {
                xmlmap.put("narration", "Transaction not acknowledged at " + (new Date()));
                xmlmap.put("processed", "2");
            }
            sender = (new Utilities()).getsender(xmlmap.get("96"));
            xmlmap.put("sender", sender);
            if (!xmlmap.get("48").contains("DUPLICATE TRANSACTION")) {
                dbFunctions.insertTransaction(xmlmap);
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SOAExceptions", "Exception occured while while sending request to SOA.\nError:" + sw.toString() + "Map: " + xmlmap.toString());
            el.log();
        }

    }
}
