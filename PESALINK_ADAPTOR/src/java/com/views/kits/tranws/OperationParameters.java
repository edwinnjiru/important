/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.views.kits.tranws;

/**
 *
 * @author fkamau
 */
public class OperationParameters {
    
    public String MessageTypeIdentification;
    public String ProcessingCode;
    public String ReconciliationAmount;
    public String CardHolderBillingAmount;
    public String TransmissionDate;
    public String SystemTraceNumber;
    public String POSDataCode;
    public String FunctionCode;
    public String MessageReasonCode;
    public String CardAcceptorBusinessCode;
    public String ReconciliationDate;
    public String ReconciliationIndicator;
    public String OriginalAmounts;//-----------
    public String AcquirerReferenceData;
    public String AcquirerID;
    public String ForwarderID;
    public String Track2Data;
    public String RetrievalReferenceNumber;
    public String ApprovalCode;
    public String ActionCode;
    public String ServiceCode;
    public String TerminalID;
    public String CardAcceptorIdentification;
    public String CardAcceptorNameLocation;
    public String Track1Data;
    public String FeesAmount;
    public String AdditionalData;
    public String ReconcialiationCurrency;
    public String CardHolderBillingCurrency;
    public String PINData;
    public String SecurityControl;
    public String ICCSystemData;
    public String OriginalDataElements;
    public String DataRecord;
    public String DestinationCountryCode;
    public String OriginatorCountryCode;
    public String CardIssuererRefernceData;
    public String ReceivingInstitutionID;
    public String FileName;
    public String AccountIdentification1;
    public String AccountIdentification2;
    public String ReservedNationalUse122;
    public String ReservedPrivateUse123;
    public String ReservedPrivateUse126;
    public String InitiatingSystem;
    public String SettlementDate;
    
    
    
    
    
    
    
    public OperationParameters( String MessageTypeIdentification,
    String ProcessingCode,
    String ReconciliationAmount,
    String CardHolderBillingAmount,
    String TransmissionDate,
    String SystemTraceNumber,
    String POSDataCode,
    String FunctionCode,
    String MessageReasonCode,
    String CardAcceptorBusinessCode,
    String ReconciliationDate,
    String ReconciliationIndicator,
    String OriginalAmounts,
    String AcquirerReferenceData,
    String AcquirerID,
    String ForwarderID,
    String Track2Data,
    String RetrievalReferenceNumber,
    String ApprovalCode,
    String ActionCode,
    String ServiceCode,
    String TerminalID,
    String CardAcceptorIdentification,
    String CardAcceptorNameLocation,
    String Track1Data,
    String FeesAmount,
    String AdditionalData,
    String ReconcialiationCurrency,
    String CardHolderBillingCurrency,
    String PINData,
    String SecurityControl,
    String ICCSystemData,
    String OriginalDataElements,
    String DataRecord,
    String DestinationCountryCode,
    String OriginatorCountryCode,
    String CardIssuererRefernceData,
    String ReceivingInstitutionID,
    String FileName,
    String AccountIdentification1,
    String AccountIdentification2,
    String ReservedNationalUse122,
    String ReservedPrivateUse123,
    String ReservedPrivateUse126,
    String InitiatingSystem,
    String SettlementDate){
          this.MessageTypeIdentification = MessageTypeIdentification;
    this.ProcessingCode = ProcessingCode;
    this.ReconciliationAmount = ReconciliationAmount;
    this.CardHolderBillingAmount = CardHolderBillingAmount;
    this.TransmissionDate = TransmissionDate;
    this.SystemTraceNumber = SystemTraceNumber;
    this.POSDataCode = POSDataCode;
    this.FunctionCode = FunctionCode;
    this.MessageReasonCode = MessageReasonCode; 
    this.CardAcceptorBusinessCode = CardAcceptorBusinessCode;
    this.ReconciliationDate = ReconciliationDate;
    this.ReconciliationIndicator = ReconciliationIndicator;
    this.OriginalAmounts = OriginalAmounts;
    this.AcquirerReferenceData = AcquirerReferenceData;
    this.AcquirerID = AcquirerID;
    this.ForwarderID = ForwarderID;
    this.Track2Data = Track2Data;
    this.RetrievalReferenceNumber = RetrievalReferenceNumber;
    this.ApprovalCode = ApprovalCode;
    this.ActionCode = ActionCode;
    this.ServiceCode =ServiceCode; 
    this.TerminalID = TerminalID;
    this.CardAcceptorIdentification = CardAcceptorIdentification;
    this.CardAcceptorNameLocation = CardAcceptorNameLocation;
    this.Track1Data = Track1Data;
    this.FeesAmount = FeesAmount;
    this.AdditionalData = AdditionalData;
    this.ReconcialiationCurrency = ReconcialiationCurrency;
    this.CardHolderBillingCurrency = CardHolderBillingCurrency;
    this.PINData = PINData;
    this.SecurityControl = SecurityControl;
    this.ICCSystemData = ICCSystemData;
    this.OriginalDataElements = OriginalDataElements;
    this.DataRecord = DataRecord;
    this.DestinationCountryCode = DestinationCountryCode;
    this.OriginatorCountryCode = OriginatorCountryCode;
    this.CardIssuererRefernceData = CardIssuererRefernceData;
    this.ReceivingInstitutionID = ReceivingInstitutionID;
    this.FileName = FileName;
    this.AccountIdentification1 = AccountIdentification1;
    this.AccountIdentification2 = AccountIdentification2;
    this.ReservedNationalUse122 = ReservedNationalUse122;
    this.ReservedPrivateUse123 = ReservedPrivateUse123;
    this.ReservedPrivateUse126 = ReservedPrivateUse126;
    this.InitiatingSystem = InitiatingSystem;
    this.SettlementDate = SettlementDate;         
     }
     
     public OperationParameters(){
         
     }
     
}
