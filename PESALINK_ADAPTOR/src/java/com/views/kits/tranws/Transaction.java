/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.views.kits.tranws;

/**
 *
 * @author fkamau
 */
public class Transaction {

    public String TransactionDate;
    public String TransactionAmount;
    public String TransactionCurrency;
    public String ConversionRate;
    public String CountryCode;
    public String TransactionDescription;

    public Transaction(String TransactionDate, String TransactionAmount, String TransactionCurrency, String ConversionRate, String CountryCode, String TransactionDescription) {
        this.TransactionDate = TransactionDate;
        this.TransactionAmount = TransactionAmount;
        this.TransactionCurrency = TransactionCurrency;
        this.ConversionRate = ConversionRate;
        this.CountryCode = CountryCode;
        this.TransactionDescription = TransactionDescription;

    }

    public Transaction() {
    }

}
