/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.views.kits.tranws;

import commonj.work.WorkException;
import commonj.work.WorkManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import ke.co.ekenya.CommonOperations.WorkManagerUtilities;

/**
 *
 * @author Njiru
 */
@WebService(serviceName = "coopReceiveTransactions_ws")
@Stateless()
@HandlerChain(file = "CoopReceiveTransactionsWs_handler.xml")
public class coopReceiveTransactions_ws {

    /**
     * This is a sample web service operation
     *
     * @param operationParameters
     * @param AccountTransaction
     * @param Card
     * @param transaction
     * @return
     */
    @WebMethod(operationName = "DataInput")
    public String DataInput(@WebParam(name = "OperationParameters") OperationParameters operationParameters, @WebParam(name = "AccountTransaction") AccountTransaction AccountTransaction, @WebParam(name = "Card") Card Card, @WebParam(name = "Transaction") Transaction transaction) {

        WorkManager wm = WorkManagerUtilities.getWorkManager();
        ProcessReceive processIncoming = new ProcessReceive(operationParameters, AccountTransaction, Card, transaction);
        try {
            wm.schedule(processIncoming);
        } catch (WorkException | IllegalArgumentException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            //log here
        }

        String response = "<?xml version=\"1.0\"?>"
                + "<Receive_KITS_A2AResponse>"
                + "<statusCode>1</statusCode>"
                + "<resultmessage>Success</resultmessage>"
                + "</Receive_KITS_A2AResponse>";
        return response;
    }

}
