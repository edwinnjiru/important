/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.views.kits.tranws;

import commonj.work.Work;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import ke.co.ekenya.CommonOperations.DistributedWebLogicQueueBrowser;
import ke.co.ekenya.CommonOperations.KitsResponseMap;
import ke.co.ekenya.CommonOperations.PostGetESBRequest;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DBFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.TransactionsToSOA.KitsResponseTransaction;
import ke.co.ekenya.mdb.KitsMDB;

/**
 *
 * @author njiru
 */
public class ProcessReceive implements Work {

    OperationParameters operationParameters;
    AccountTransaction AccountTransaction;
    Card Card;
    Transaction transaction;
    public Map<String, String> Configs = new HashMap();
    Map<String, String> esbResponse = new HashMap();
    DistributedWebLogicQueueBrowser queueBrowser = null;
    Utilities utilities = new Utilities();
    DBFunctions dbFunctions;
    String star = "******";
    StringBuilder str;
    String masked_acc = "";

    public ProcessReceive(OperationParameters operationParameters,
            AccountTransaction accountTransaction,
            Card card,
            Transaction transaction) {
        this.operationParameters = operationParameters;
        this.AccountTransaction = accountTransaction;
        this.Card = card;
        this.transaction = transaction;
        this.queueBrowser = new DistributedWebLogicQueueBrowser();
        dbFunctions = new DBFunctions();
        this.Configs = KitsMDB.Configs;
    }

    @Override
    public void run() {
        try {
            doProcessing();
        } catch (Exception ex) {
            System.out.println("Runnable error: " + ex.getMessage());
            ESBLog es = new ESBLog(ex, "Run error");
            es.log();
        }
    }

    void doProcessing() {

        if (Configs.isEmpty()) {
            KitsMDB.readPropertyFile(Configs);
        }
        if (!Configs.containsKey("KitsGlAccount")) {
            Configs.put("KitsGlAccount", (new DBFunctions()).getAnyGLAccount("MWALLETSUSPENSE"));
        }

        String MCS = operationParameters.AccountIdentification1;
        String MCS1 = "MCS";
        Map<String, String> customerDetails = new HashMap();
        //String msg = "";

        String num = "";
        int index1 = MCS.indexOf(MCS1);
        if (index1 != -1) {
            num = "" + MCS;
        } else {
            num = MCS1 + MCS;
        }
        String number22 = num;
        String[] theArray3 = number22.split("MCS");
        String finalNumber1 = theArray3[1];

        String custno = finalNumber1; //put check to validate that customer is valid

        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");
        int update = 0;

        if (operationParameters.MessageTypeIdentification.equals("1200")) {
            HashMap<String, String> resXmlMap = new HashMap();
            String fld = transaction.TransactionAmount;
            int fld4 = Integer.parseInt(fld) / 100;
            String fld68 = Configs.get("KitsGlAccount") + " " + custno + " " + operationParameters.RetrievalReferenceNumber + " " + "400000" + " " + "CM" + " " + "PESALINK";
            String ConversionRate = transaction.ConversionRate + "|" + operationParameters.ReservedPrivateUse126 + "|" + operationParameters.OriginatorCountryCode + "|" + operationParameters.DestinationCountryCode + "|" + operationParameters.TerminalID + "|" + operationParameters.POSDataCode + "|" + operationParameters.InitiatingSystem + "|" + operationParameters.OriginalDataElements;

            resXmlMap.put("0", "0200");
            resXmlMap.put("MTI", "0200");
            resXmlMap.put("2", custno);
            resXmlMap.put("3", "400000");
            resXmlMap.put("4", String.valueOf(fld4));
            resXmlMap.put("7", sdf.format(d));
            resXmlMap.put("11", operationParameters.SystemTraceNumber);
            resXmlMap.put("12", sdf2.format(d));
            resXmlMap.put("14", transaction.TransactionDate);
            resXmlMap.put("24", "MM");
            resXmlMap.put("30", "");
            resXmlMap.put("31", operationParameters.TransmissionDate);
            resXmlMap.put("32", "KBA");
            resXmlMap.put("35", "");
            resXmlMap.put("37", operationParameters.RetrievalReferenceNumber);
            resXmlMap.put("38", operationParameters.RetrievalReferenceNumber);
            resXmlMap.put("40", operationParameters.FunctionCode);
            resXmlMap.put("41", operationParameters.SystemTraceNumber);
            resXmlMap.put("42", Card.SequenceNumber);
            resXmlMap.put("43", transaction.TransactionDescription);
            resXmlMap.put("49", operationParameters.CardAcceptorBusinessCode);
            resXmlMap.put("50", operationParameters.AcquirerID);
            resXmlMap.put("51", operationParameters.ForwarderID);
            resXmlMap.put("52", operationParameters.POSDataCode);
            resXmlMap.put("53", operationParameters.TerminalID);
            resXmlMap.put("68", fld68);
            resXmlMap.put("69", transaction.TransactionCurrency);
            resXmlMap.put("70", transaction.CountryCode);
            resXmlMap.put("71", Card.ExpiryDate);
            resXmlMap.put("72", AccountTransaction.AccountNumber);
            resXmlMap.put("73", operationParameters.ReceivingInstitutionID);//fldbranch
            resXmlMap.put("74", "MWALLETRECEIVE");
            resXmlMap.put("78", "001"); //debit branch
            resXmlMap.put("75", ConversionRate);//conversion rate
            resXmlMap.put("76", operationParameters.CardAcceptorIdentification);
            resXmlMap.put("77", operationParameters.InitiatingSystem);
            resXmlMap.put("94", operationParameters.OriginalDataElements);
            resXmlMap.put("95", operationParameters.DataRecord);
            resXmlMap.put("98", "PESALINK_FT");
            resXmlMap.put("100", "MWALLET");
            resXmlMap.put("101", operationParameters.DestinationCountryCode);
            resXmlMap.put("102", Configs.get("KitsGlAccount"));
            resXmlMap.put("103", custno);
            resXmlMap.put("123", operationParameters.OriginatorCountryCode);
            resXmlMap.put("126", operationParameters.CardIssuererRefernceData);
            resXmlMap.put("host", "MWALLET");
            resXmlMap.put("CREDIT_WALLET", "1");

            PostGetESBRequest postRequet = new PostGetESBRequest();
            esbResponse = postRequet.CURLRequest(resXmlMap);

            if ("0210".equals(esbResponse.get("0"))) {
                esbResponse.put("MTI", "1210");
                esbResponse.put("CHANNEL", "MWALLETRECEIVE");
            }

            esbResponse.put("96", operationParameters.ReservedPrivateUse126);

            KitsResponseTransaction response = new KitsResponseTransaction(esbResponse, esbResponse.get("37"));
            response.doProcessing();

        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("EC").append(operationParameters.RetrievalReferenceNumber);
            String correlationID = sb.toString();
            boolean sent = false;
            switch (operationParameters.MessageTypeIdentification) {
                case "1210":

                    HashMap<String, String> toESBMap = new HashMap();
                    toESBMap = queueBrowser.getWeblogicMessageFromUDQueue(correlationID);

                    if (!toESBMap.isEmpty()) {
                        customerDetails = DBFunctions.getNames(toESBMap.get("2"));
                        String trantype = toESBMap.get("74");
                        switch (trantype) {
                            case "SENDTOPHONE":
                                customerDetails.put("receiver", toESBMap.get("66"));
                                customerDetails.put("smscode", "kits_005");
                                break;
                            case "SENDTOACCOUNT":
                                customerDetails.put("receiver", toESBMap.get("103"));
                                customerDetails.put("smscode", "kits_001");
                                break;
                            case "SENDTOCARD":
                                str = new StringBuilder(toESBMap.get("103"));
                                masked_acc = str.replace(5, 11, star).toString();
                                customerDetails.put("receiver", masked_acc);
                                customerDetails.put("smscode", "kits_002");
                                break;
                            default:
                                break;
                        }

                        String code = "";
                        String narration = "";
                        if (operationParameters.ActionCode.equals("000")) {
                            code = operationParameters.ActionCode;
                            narration = " Code " + code + " Desc " + KitsResponseMap.response(code);

                            //send to esb
                            toESBMap.put("39", "000");
                            toESBMap.put("48", "Success".toUpperCase());
                            QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
                            sent = qr.sendObject(toESBMap, correlationID, "PESALINK_FT");
                            ESBLog logg = new ESBLog("ResponsetoESB", "Sent: " + sent + " Map: " + toESBMap);
                            logg.log();

                            customerDetails.put("amount", toESBMap.get("4"));
                            customerDetails.put("2", toESBMap.get("2"));
                            customerDetails.put("bankto", toESBMap.get("72"));
                            customerDetails.put("ref", operationParameters.RetrievalReferenceNumber);
                            sendsms(customerDetails);
                        } else {
                            code = operationParameters.ActionCode;
                            String desc = KitsResponseMap.response(code);
                            narration = " Code " + code + " Desc " + desc;

                            //send to esb
                            toESBMap.put("39", code);
                            toESBMap.put("48", desc.toUpperCase());
                            toESBMap.put("REVERSE", "1");
                            QueueWriter qr = new QueueWriter("jms/ADAPTOR_RESPONSE_QUEUE");
                            sent = qr.sendObject(toESBMap, correlationID, "PESALINK_FT");
                            ESBLog logg = new ESBLog("ResponsetoESB", "Sent: " + sent + " Map: " + toESBMap);
                            logg.log();

                            customerDetails.put("amount", toESBMap.get("4"));
                            customerDetails.put("2", toESBMap.get("2"));
                            customerDetails.put("bankto", toESBMap.get("72"));
                            customerDetails.put("smscode", "kits_004");
                            customerDetails.put("ref", operationParameters.RetrievalReferenceNumber);
                            customerDetails.put("code", code);
                            customerDetails.put("desc", desc);
                            sendsms(customerDetails);
                        }
                        update = dbFunctions.updatetran(toESBMap.get("37"), operationParameters.SystemTraceNumber, code, narration, "1200");
                        if (update == 1) {
                            ESBLog lg = new ESBLog("DB_UPDATE", "Successful-Ref: " + toESBMap.get("37"));
                            lg.log();
                        } else {
                            ESBLog lg = new ESBLog("DB_UPDATE", "Failed-Ref: " + toESBMap.get("37"));
                            lg.log();
                        }
                    } else {
                        //log here
                        ESBLog lg = new ESBLog("Missing_ORG_Transaction", "Ref: " + operationParameters.RetrievalReferenceNumber);
                        lg.log();
                    }
                    break;
                case "1420":
                    //reversals hapa
                    HashMap<String, String> resXmlMap = new HashMap();
                    String fld = transaction.TransactionAmount;
                    int fld4 = Integer.parseInt(fld) / 100;
                    String fld68 = Configs.get("KitsGlAccount") + " " + custno + " " + operationParameters.RetrievalReferenceNumber + " " + "400000" + " " + "CM" + " " + "PESALINK";
                    String ConversionRate = transaction.ConversionRate + "|" + operationParameters.ReservedPrivateUse126 + "|" + operationParameters.OriginatorCountryCode + "|" + operationParameters.DestinationCountryCode + "|" + operationParameters.TerminalID + "|" + operationParameters.POSDataCode + "|" + operationParameters.InitiatingSystem + "|" + operationParameters.OriginalDataElements;

                    resXmlMap.put("MTI", "0420");
                    resXmlMap.put("0", "0420");
                    resXmlMap.put("2", custno);
                    resXmlMap.put("3", "400000");
                    resXmlMap.put("4", String.valueOf(fld4));
                    resXmlMap.put("7", sdf.format(d));
                    resXmlMap.put("11", operationParameters.SystemTraceNumber);
                    resXmlMap.put("12", sdf2.format(d));
                    resXmlMap.put("14", transaction.TransactionDate);
                    resXmlMap.put("24", "MM");
                    resXmlMap.put("30", "");
                    resXmlMap.put("31", operationParameters.TransmissionDate);
                    resXmlMap.put("32", "KBA");
                    resXmlMap.put("35", "MWALLETRECEIVE");
                    resXmlMap.put("37", (new DBFunctions()).getCorrelationID());
                    resXmlMap.put("38", operationParameters.RetrievalReferenceNumber);
                    resXmlMap.put("XREF", operationParameters.RetrievalReferenceNumber);
                    resXmlMap.put("40", operationParameters.FunctionCode);
                    resXmlMap.put("41", operationParameters.SystemTraceNumber);
                    resXmlMap.put("42", Card.SequenceNumber);
                    resXmlMap.put("43", transaction.TransactionDescription);
                    resXmlMap.put("49", operationParameters.CardAcceptorBusinessCode);
                    resXmlMap.put("50", operationParameters.AcquirerID);
                    resXmlMap.put("51", operationParameters.ForwarderID);
                    resXmlMap.put("52", operationParameters.POSDataCode);
                    resXmlMap.put("53", operationParameters.TerminalID);
                    resXmlMap.put("68", fld68);
                    resXmlMap.put("69", transaction.TransactionCurrency);
                    resXmlMap.put("70", transaction.CountryCode);
                    resXmlMap.put("71", Card.ExpiryDate);
                    resXmlMap.put("72", AccountTransaction.AccountNumber);
                    resXmlMap.put("73", operationParameters.ReceivingInstitutionID);//fldbranch
                    resXmlMap.put("74", "MWALLETRECEIVE");
                    resXmlMap.put("75", ConversionRate);//conversion rate
                    resXmlMap.put("76", operationParameters.CardAcceptorIdentification);
                    resXmlMap.put("77", operationParameters.InitiatingSystem);
                    resXmlMap.put("78", "001"); //debit branch
                    resXmlMap.put("94", operationParameters.OriginalDataElements);
                    resXmlMap.put("95", operationParameters.DataRecord);
                    resXmlMap.put("96", operationParameters.ReservedPrivateUse126);
                    resXmlMap.put("98", "PESALINK_FT");
                    resXmlMap.put("100", "MWALLET");
                    resXmlMap.put("101", operationParameters.DestinationCountryCode);
                    resXmlMap.put("102", Configs.get("KitsGlAccount"));
                    resXmlMap.put("103", custno);
                    resXmlMap.put("123", operationParameters.OriginatorCountryCode);
                    resXmlMap.put("126", operationParameters.CardIssuererRefernceData);
                    resXmlMap.put("host", "MWALLET");
                    resXmlMap.put("direction", "request");

                    //send
                    //inserted in the Kitsresponse class after sending to soa to improve turn around time
                    PostGetESBRequest postRequet = new PostGetESBRequest();
                    esbResponse = postRequet.CURLRequest(resXmlMap);

                    if ("0430".equals(esbResponse.get("0"))) {
                        esbResponse.put("MTI", "1430");
                        esbResponse.put("CHANNEL", "MWALLETRECEIVE");
                        esbResponse.put("37", esbResponse.get("XREF"));
                    }

                    esbResponse.put("96", operationParameters.ReservedPrivateUse126);
                    KitsResponseTransaction response = new KitsResponseTransaction(esbResponse, esbResponse.get("37"));
                    response.doProcessing();
                    break;
                default:
                    //1430 hapa
//                    update = dbFunctions.updatetran(toESBMap.get("37"), operationParameters.SystemTraceNumber, "000", "Success", "0420");
//                    if (update == 1) {
//                        ESBLog lg = new ESBLog("DB_UPDATE", "Successful-Ref: " + operationParameters.RetrievalReferenceNumber);
//                        lg.log();
//                    } else {
//                        ESBLog lg = new ESBLog("DB_UPDATE", "Failed-Ref: " + operationParameters.RetrievalReferenceNumber);
//                        lg.log();
//                    }
                    break;
            }
        }

    }

    @Override
    public void release() {
    }

    @Override
    public boolean isDaemon() {
        return false;
    }

    public void sendsms(Map<String, String> details) {
        String msg = "";
        Map<String, String> smsMap = new HashMap();
        boolean sent = false;
        try {
            Map<String, String> smsTemplate = dbFunctions.getSMSTemplate(details.get("smscode"));
            msg = smsTemplate.get("SMSTEMPLATE");
            if (!"".equals(msg)) {
                msg = msg.replace("SENDER", details.get("name"));
                msg = msg.replace("AMTR", details.get("amount"));
                msg = msg.replace("RECIPIENT", details.get("receiver"));
                msg = msg.replace("TRANREF", details.get("ref"));

                if (msg.contains("BANKNAME")) {
                    if (details.get("bankto").toLowerCase().contains("bank")) {
                        msg = msg.replace("BANKNAME Bank", details.get("bankto"));
                    } else {
                        msg = msg.replace("BANKNAME", details.get("bankto"));
                    }
                }

                smsMap.put("PHONENUMBER", details.get("2"));
                smsMap.put("SMSMESSAGE", msg);
                smsMap.put("NotificationType", "SMS");
                smsMap.put("98", "SMS");
                smsMap.put("37", details.get("ref"));
                smsMap.put("32", "KITS");
                smsMap.put("102", details.get("2"));
                smsMap.put("2", details.get("2"));

                QueueWriter queueWriter = new QueueWriter("jms/NOTIFICATIONS_QUEUE");
                sent = queueWriter.sendObject(smsMap, details.get("ref"));
            } else {
                ESBLog es2 = new ESBLog("missingSmsTemplate", "Template code: " + details.get("smscode"));
                es2.log();
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog lg = new ESBLog("SendsmsException", "Tran ref: " + details.get("ref") + ", error: " + sw.toString());
            lg.log();
        } finally {

            ESBLog es2 = new ESBLog("SendSms", "sent: " + sent + ", messageMap: " + smsMap);
            es2.log();
        }

    }

}
