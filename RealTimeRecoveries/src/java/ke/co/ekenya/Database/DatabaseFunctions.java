/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.Database;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.senders.SendArrears;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author njiru
 */
public class DatabaseFunctions extends DatabaseModel {

    public DatabaseFunctions() {
    }

    public void checkloanstatus(Map<String, String> details) {

        String loanaccount = "";
        String flexiRef = "";
        String custno = "";
        ResultSet rs = null;
        String command = "{call SP_GET_ALL_LOANACCOUNTS(?,?)}";
        try (Connection con = createConnection();
                CallableStatement cStmt = con.prepareCall(command);) {
            cStmt.setString(1, details.get("accountNumber"));
            cStmt.registerOutParameter(2, OracleTypes.CURSOR);
            cStmt.execute();

            rs = (ResultSet) cStmt.getObject(2);

            while (rs.next()) {
                loanaccount = rs.getString(1);
                flexiRef = rs.getString(2);
                custno = rs.getString(3);
                details.put("loanaccount", loanaccount);
                details.put("flexiRef", flexiRef);
                details.put("customerNumber", custno);

                checkarrears(details, con); //check arrears
                
                ESBLog arrearscheck=new ESBLog("ArrearsDetails", "Account: "+details.get("accountNumber")+" details: "+details.toString());
                arrearscheck.log();
                
                if ("1".equals(details.get("error").trim())) {
                    //Exception occured
                    ESBLog es = new ESBLog("DatabaseException", "Error in check arrears. error: " + details.get("exception"));
                    es.log();
                } else {
                    
                    String isinarrears = details.get("inarrears");
                    switch (isinarrears) {
                        case "0":// do nothing, customer has no loan in arrears
                            break;
                        case "1":
                            //send to loan recovery adapter
                            SendArrears sendArrears = new SendArrears(details);
                            sendArrears.doprocess();                            
                            break;
                        default:
                            break;
                    }

                }
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog es = new ESBLog("loanstatuscheckException", sw.toString());
            es.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog es = new ESBLog("ResultSetException", sw.toString());
                es.log();
            }

        }

    }

    public Map checkarrears(Map<String, String> details, Connection con) {
        ResultSet rs = null;
        String results = "";
        String command = "{call SP_CHECK_ACCOUNTS_IN_ARREARS(?,?,?)}";
        try (CallableStatement stmt = con.prepareCall(command)) {
            stmt.setString(1, details.get("loanaccount"));
            stmt.setString(2, details.get("flexiRef"));
            stmt.registerOutParameter(3, OracleTypes.CURSOR);
            stmt.execute();

            rs = (ResultSet) stmt.getObject(3);

            while (rs.next()) {
                results = rs.getString(1);

                if ("ACCOUNT NOT FOUND".equals(results.trim())) {
                    String Account_details = "account =" + details.get("loanaccount") + ": Response = " + results;
                    details.put("error", "0");//1 stands for yes, 0 for no
                    details.put("inarrears", "0");//1 stands for yes, 0 for no
                    details.put("details", Account_details);
                } else {
                    String splitDetails[] = results.split("\\|");

                    details.put("error", "0");////1 stands for yes, 0 for no
                    details.put("inarrears", "1");//1 stands for yes, 0 for no
                    details.put("loanaccount", splitDetails[0]);
                    details.put("loanamount", splitDetails[1]);
                    details.put("penalty_interest", splitDetails[2]);
                    details.put("accrual_interest", splitDetails[3]);
                    details.put("product_code", splitDetails[4]);
                    details.put("branch_code", splitDetails[5]);
                    details.put("flexi_ref", splitDetails[6]);
                    details.put("provision_amount", splitDetails[7]);
                    details.put("mwalletaccount", splitDetails[8]);

                    String accountDetails = "loanaccount: " + splitDetails[0] + ", loanamount: " + splitDetails[1] + ", penalty_interest: " + splitDetails[2]
                            + ", accrual_interest: " + splitDetails[3] + ", product_code: " + splitDetails[4] + ", branch_code: " + splitDetails[5] + ", flexi_ref: "
                            + splitDetails[6] + ", provision_amount: " + splitDetails[7] + ", mwalletaccount: " + splitDetails[8];

                    details.put("alldetails", accountDetails);

                }
            }
            //return details;
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            details.put("error", "1");//1 stands for yes, 0 for no
            details.put("exception", sw.toString());
            return details;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }

        }

        return details;
    }

    public Map checkSubscription(String accountno) {
        ResultSet rs = null;
        Map<String, String> subscribedAlerts = new HashMap();
        String command = "{call SP_CHECKALERTSREGISTRATION(?,?)}";
        try (Connection con = createConnection();
                CallableStatement stmt = con.prepareCall(command);) {
            stmt.setString("accountnumber", accountno);
            stmt.registerOutParameter("cv_1", OracleTypes.CURSOR);
            stmt.execute();

            rs = (ResultSet) stmt.getObject("cv_1");
            while (rs.next()) {
                if ("0".equals(rs.getString("REGISTERED").trim())) {
                    //Customer not registered to alerts
                    subscribedAlerts.put("registered", "0");
                } else {
                    //customer registered to alerts                  
                    subscribedAlerts.put("registered", "1");
                    subscribedAlerts.put("largecredit", (rs.getString("LARGECREDIT") != null) ? rs.getString("LARGECREDIT") : "0");
                    subscribedAlerts.put("largedebit", (rs.getString("LARGEDEBIT") != null) ? rs.getString("LARGEDEBIT") : "0");
                    subscribedAlerts.put("credit", (rs.getString("CREDIT") != null) ? rs.getString("CREDIT") : "0");
                    subscribedAlerts.put("debit", (rs.getString("DEBIT") != null) ? rs.getString("DEBIT") : "0");
                    subscribedAlerts.put("mobileno", rs.getString("MOBILENUMBER"));
                }

            }
            subscribedAlerts.put("Account", accountno);

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            ESBLog es = new ESBLog("DatabaseException", "Checking subscription error: " + sw.toString());
            es.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }

        }

        return subscribedAlerts;
    }

    public HashMap getSMSTemplate(String smsCode) {
        HashMap detailsMap = new HashMap<>();
        String template = "";
        String command = "{call SP_GET_SMS_TEMPLATE(?,?)}";
        ResultSet rs = null;

        try (Connection con = createMvisaConnection();
                CallableStatement stmt = con.prepareCall(command);) {
            stmt.setString("IV_SMSCODE", smsCode);
            stmt.registerOutParameter("cv_1", OracleTypes.CURSOR);
            stmt.execute();

            rs = (ResultSet) stmt.getObject("cv_1");
            while (rs.next()) {
                template = rs.getString("MESSAGE");
            }

            detailsMap.put("SMSTEMPLATE", template);

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Exception occured while calling"
                    + " SP_GET_SMS_TEMPLATE SP to get sms template for sms code:" + smsCode + "|Error:" + sw.toString() + "\n");
            el.log();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog("SQLExceptions", "Error occured closing resultset of get sms template function. Error: " + sw.toString());
                el.log();
            }
        }

        return detailsMap;

    }

    public String getCorrelationID() {
        String CorrelationID = "";
        String command = "{call SP_GET_ESB_NEXT_SEQ(?)}";
        try (Connection con = createMvisaConnection();
                CallableStatement callableStatement = con.prepareCall(command);) {
            callableStatement.registerOutParameter("cv_1", java.sql.Types.VARCHAR);
            callableStatement.executeUpdate();
            CorrelationID = callableStatement.getString("cv_1");
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("SQLExceptions", "Error in correlationID function: " + sw.toString());
            el.log();
        }
        return CorrelationID;
    }

}
