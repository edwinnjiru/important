/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.api;

import commonj.work.WorkException;
import commonj.work.WorkManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 * REST Web Service
 *
 * @author njiru
 */
@Path("service")
public class RealTimeResource {

    @Context
    private UriInfo context;
    WorkManager wm = null;
    public static Map<String, String> configs = new HashMap();

    /**
     * Creates a new instance of GenericResource
     */
    static {
        //Utilities.readPropertyFile(configs);
        readPropertyFile(configs);
        ESBLog es12 = new ESBLog("readconfig", configs.toString());
        es12.log();
    }

    /**
     * Retrieves representation of an instance of
     * ke.co.ekenya.api.GenericResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    public String getXml() {
        //TODO return proper representation object
        return "Real Time Recovery Resource is available";
    }

    /**
     *
     * @param content representation for the resource
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public String postXml(String content) {
        if (configs.isEmpty()) {
            readPropertyFile(configs);
            //Utilities.readPropertyFile(configs);
        }

        String response = "<?xml version=\"1.0\"?>"
                + "<IntervalSyncResponse>"
                + "<statusCode>1</statusCode>"
                + "<resultmessage>Success</resultmessage>"
                + "</IntervalSyncResponse>";

        ESBLog es = new ESBLog("IncomingMessage", content);
        es.log();

        wm = Utilities.getWorkManager();
        ProcessIncoming processIncoming = new ProcessIncoming(content);
        try {
            wm.schedule(processIncoming);
        } catch (WorkException | IllegalArgumentException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            
             ESBLog es2 = new ESBLog("ScheduleException", "Error: " + sw.toString());
            es2.log();
            System.out.println("Error initializing thread: " + ex.getMessage());
        }

        return response;
    }

    public static void readPropertyFile(Map configs) {
        InitialContext initialContext = null;
        try {
            initialContext = new InitialContext();
            NamingEnumeration<NameClassPair> list = initialContext.list("configs");
            while (list.hasMoreElements()) {
                String key = list.next().getName();
                String value = "";
                try {
                    value = (String) initialContext.lookup("configs/" + key);
                    configs.put(key.trim(), value.trim());
                } catch (Exception ex) {
                }
            }
            initialContext.close();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog(sw.toString());
            el.log();
        } finally {
            try {
                if (initialContext != null) {
                    initialContext.close();
                }
            } catch (Exception ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog(sw.toString());
                el.log();
            }
        }
    }

}
