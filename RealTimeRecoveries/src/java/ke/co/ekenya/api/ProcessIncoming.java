/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.api;

import ke.co.ekenya.senders.SendAlerts;
import commonj.work.Work;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author njiru
 */
public class ProcessIncoming implements Work {

    String message = "";
    DatabaseFunctions databaseFuntions;

    public ProcessIncoming(String message) {
        this.message = message;
        this.databaseFuntions = new DatabaseFunctions();
    }

    @Override
    public void run() {
        try {
            doprocess();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            ESBLog es = new ESBLog("RunException", "Intitialization error: " + sw.toString());
            es.log();
        }
    }

    @Override
    public void release() {
    }

    @Override
    public boolean isDaemon() {
        return false;
    }

    void doprocess() {
        Map<String, String> messageDetails = new HashMap();
        String narration = "";
        String creditType = "";
        String debitType = "";
        try {
            if (message.contains("coopaccounts")) {
                message = message.replace("<coopaccounts>", "");
                message = message.replace("</coopaccounts>", "");

                ESBLog eslog = new ESBLog("Loadedaccounts", message);
                eslog.log();
                String[] acc = message.trim().split(",");
                for (String accNo : acc) {
                    Map<String, String> linkedDetails = new HashMap();
                    linkedDetails.put("accountNumber", accNo.trim());
                    linkedDetails.put("source", "bfub");
                    databaseFuntions.checkloanstatus(linkedDetails);
                }
            } else {

                HashMap<String, Object> brokenDown = Utilities.convertNodesFromXml(message);

                messageDetails.put("source", "bfub");

                HashMap<String, Object> bfub = (HashMap<String, Object>) brokenDown.get("bfub:Content");
                HashMap<String, Object> bfubtrandetails = (HashMap<String, Object>) bfub.get("bfub:Transaction");
                HashMap<String, Object> trandetails = (HashMap<String, Object>) bfubtrandetails.get("bfub:AcctTransaction");
                HashMap<String, Object> datedetails = (HashMap<String, Object>) trandetails.get("bfub:PayDate");
                messageDetails.put("trandate", (String) datedetails.get("bfub:ValueDate"));//common to both debit and credit

                //handle debit here
                HashMap<String, Object> debitdetails = (HashMap<String, Object>) trandetails.get("bfub:Debit");
                HashMap<String, Object> debitaccountID = (HashMap<String, Object>) debitdetails.get("bfub:AcctID");
                HashMap<String, Object> debitbfubamount = (HashMap<String, Object>) debitdetails.get("bfub:Amount");
                HashMap<String, Object> debitrunningbalance = (HashMap<String, Object>) debitdetails.get("bfub:RunningBalance");
                HashMap<String, Object> debitcustmemos = (HashMap<String, Object>) debitdetails.get("bfub:CustMemo");
                narration = debitcustmemos.get("bfub:CustMemoLine1").toString().trim() + " " + debitcustmemos.get("bfub:CustMemoLine2").toString().trim() + " " + debitcustmemos.get("bfub:CustMemoLine3").toString().trim();

                messageDetails.put("narration", narration);
                messageDetails.put("transactionId", (String) debitdetails.get("bfub:PaymentRef").toString());
                messageDetails.put("accountNumber", (String) debitaccountID.get("bfub:AcctNo").toString());
                messageDetails.put("transactionAmount", (String) debitbfubamount.get("bfub:Value").toString());
                messageDetails.put("drcr", (String) debitdetails.get("bfub:PayType").toString());
                debitType = (String) debitdetails.get("bfub:PayType").toString();
                messageDetails.put("runningBalance", (String) debitrunningbalance.get("bfub:Booked").toString());

                if ((!(debitType.equals("")) || !(debitType.isEmpty())) && !(debitType.equals("{}"))) {
                    SendAlerts sendAlerts = new SendAlerts(messageDetails);
                    sendAlerts.process();
                    ESBLog es2 = new ESBLog("brokendownxml", messageDetails.toString());
                    es2.log();
                    return;
                }

                //handle credit here
                HashMap<String, Object> creditdetails = (HashMap<String, Object>) trandetails.get("bfub:Credit");
                HashMap<String, Object> creditaccountID = (HashMap<String, Object>) creditdetails.get("bfub:AcctID");
                HashMap<String, Object> creditbfubamount = (HashMap<String, Object>) creditdetails.get("bfub:Amount");
                HashMap<String, Object> creditrunningbalance = (HashMap<String, Object>) creditdetails.get("bfub:RunningBalance");
                HashMap<String, Object> creditcustmemos = (HashMap<String, Object>) creditdetails.get("bfub:CustMemo");
                narration = creditcustmemos.get("bfub:CustMemoLine1").toString().trim() + " " + creditcustmemos.get("bfub:CustMemoLine2").toString().trim() + " " + creditcustmemos.get("bfub:CustMemoLine3").toString().trim();

                messageDetails.put("narration", narration);
                messageDetails.put("transactionId", (String) creditdetails.get("bfub:PaymentRef").toString());
                messageDetails.put("accountNumber", (String) creditaccountID.get("bfub:AcctNo").toString());
                messageDetails.put("transactionAmount", (String) creditbfubamount.get("bfub:Value").toString());
                messageDetails.put("drcr", (String) creditdetails.get("bfub:PayType").toString());
                creditType = (String) creditdetails.get("bfub:PayType").toString();
                messageDetails.put("runningBalance", (String) creditrunningbalance.get("bfub:Booked").toString());
                if ((!(creditType.equals("")) || !(creditType.isEmpty())) && !(creditType.equals("{}"))) {
                    databaseFuntions.checkloanstatus(messageDetails);
                    SendAlerts sendAlerts = new SendAlerts(messageDetails);
                    sendAlerts.process();
                    ESBLog es2 = new ESBLog("brokendownxml", messageDetails.toString());
                    es2.log();
                }
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            ESBLog es = new ESBLog("SendAlertsException", "error: " + sw.toString() + ", Message: " + message);
            es.log();
        }

    }

}
