/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.senders;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import ke.co.ekenya.CommonOperations.PostGetESBRequest;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.api.RealTimeResource;

/**
 *
 * @author njiru
 */
public class SendArrears {

    String mwalletAccount;
    String loanamount;
    String penalty_interest;
    String accrual_interest;
    String product_code;
    String loanaccount;
    String flexi_ref;
    String branch_code;
    String provision_amount;
    String accountNumber;
    String customerNumber;
    String source;
    Map<String, String> configs;

    public SendArrears(Map<String, String> detailsMap) {
        this.mwalletAccount = detailsMap.get("mwalletaccount");
        this.loanamount = detailsMap.get("loanamount");
        this.penalty_interest = detailsMap.get("penalty_interest");
        this.accrual_interest = detailsMap.get("accrual_interest");
        this.product_code = detailsMap.get("product_code");
        this.loanaccount = detailsMap.get("loanaccount");
        this.flexi_ref = detailsMap.get("flexi_ref");
        this.branch_code = detailsMap.get("branch_code");
        this.provision_amount = detailsMap.get("provision_amount");
        this.accountNumber = detailsMap.get("accountNumber");
        this.customerNumber = detailsMap.get("customerNumber");
        this.source = detailsMap.get("source");
        configs = RealTimeResource.configs;
        if (configs.isEmpty()) {
            RealTimeResource.readPropertyFile(configs);
        }
    }

    public void doprocess() {
        HashMap<String, String> arrearsMap = new HashMap();
        try {
            String ref = (new DatabaseFunctions()).getCorrelationID();
            arrearsMap.put("0", "0200");
            arrearsMap.put("2", mwalletAccount);
            arrearsMap.put("3", "710000");
            arrearsMap.put("4", loanamount);
            arrearsMap.put("5", penalty_interest);
            arrearsMap.put("6", accrual_interest);
            arrearsMap.put("7", "");
            arrearsMap.put("8", "");
            arrearsMap.put("11", GenerateRandomNumber(6));
            arrearsMap.put("12", "");
            arrearsMap.put("32", "CREDIT");
            arrearsMap.put("35", product_code);
            arrearsMap.put("37", "EC" + ref);
            arrearsMap.put("38", ref);
            arrearsMap.put("49", "KES");
            arrearsMap.put("65", loanaccount);
            arrearsMap.put("68", "");
            arrearsMap.put("97", flexi_ref);
            arrearsMap.put("98", "RECOVERIES");
            arrearsMap.put("100", "MWALLET");
            arrearsMap.put("101", "");
            arrearsMap.put("102", accountNumber);
            arrearsMap.put("103", "");
            arrearsMap.put("XREF", "EC" + ref);
            arrearsMap.put("MTI", "0200");
            arrearsMap.put("online", "1");
            arrearsMap.put("recovery", "1");
            arrearsMap.put("status", "0");
            arrearsMap.put("BRANCH", branch_code);
            arrearsMap.put("PRAmt", provision_amount);
            arrearsMap.put("REALTIME", "1");
            arrearsMap.put("96", customerNumber);
            arrearsMap.put("TRIAL", "1");

            if (source.equals("wallet")) {
                arrearsMap.put("24", "MM"); //MM for wallet recoveries and CC for BFUB accounts
            } else {
                arrearsMap.put("24", "CC");
            }

            for (int i = 1; i <= 128; i++) {
                if (!arrearsMap.containsKey("" + i)) {
                    arrearsMap.put("" + i, "");
                }
            }
            
            ESBLog es = new ESBLog("SendArrearsInitiate", arrearsMap.toString());
            es.log();
            
            PostGetESBRequest post = new PostGetESBRequest();
            post.CURLRequest(arrearsMap,configs.get("LOANS_URL"),"LoanAdapter");

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            ESBLog es = new ESBLog("SendArrearsException", "Error: " + sw.toString());
            es.log();
        }

    }

    public String GenerateRandomNumber(int charLength) {
        return String.valueOf(charLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1)
                + (int) Math.pow(10, charLength - 1));
    }

}
