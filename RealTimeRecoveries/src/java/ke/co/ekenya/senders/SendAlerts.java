/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.senders;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import ke.co.ekenya.CommonOperations.PostGetESBRequest;
import ke.co.ekenya.CommonOperations.QueueWriter;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;
import ke.co.ekenya.api.RealTimeResource;

/**
 *
 * @author njiru
 */
public class SendAlerts {

    String transactionId;
    String trandate;
    String drcr;
    String accountNumber;
    String runningBalance;
    double transactionAmount;
    String narration;
    DatabaseFunctions databaseFunctions;
    Map<String, String> configs = new HashMap();

    public SendAlerts(Map<String, String> details) {
        //initialize variables here
        this.transactionId = details.get("transactionId");
        this.trandate = LocalDate.parse(details.get("trandate"), DateTimeFormatter.BASIC_ISO_DATE).toString();
        this.drcr = details.get("drcr");
        this.accountNumber = details.get("accountNumber");
        this.runningBalance = details.get("runningBalance");
        this.transactionAmount = Double.parseDouble(details.get("transactionAmount"));
        this.narration = details.get("narration");
        this.databaseFunctions = new DatabaseFunctions();
        this.configs = RealTimeResource.configs;
    }

    public void process() {
        Map<String, String> subscriptionDetails;
        try {
            subscriptionDetails = databaseFunctions.checkSubscription(accountNumber);

            if ("1".equals(subscriptionDetails.get("registered").trim())) {
                //send alert
                if (subscriptionDetails.get("mobileno").length() == 10) {
                    subscriptionDetails.put("mobileno", "254" + subscriptionDetails.get("mobileno").substring(1));
                }
                ESBLog es = new ESBLog("Subscriptiondetails", subscriptionDetails.toString());
                es.log();
                switch (drcr) {
                    case "CRED":
                        creditalerts(subscriptionDetails);
                        break;
                    case "DEBIT":
                        debitalerts(subscriptionDetails);
                        break;
                    default:
                        break;
                }
            } else {
                ESBLog es = new ESBLog("Subscriptiondetails", "Account number: " + accountNumber + " holder is not subscribed to alerts");
                es.log();
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            ESBLog es = new ESBLog("SendAlertsException", "switching  error: " + sw.toString());
            es.log();
        }

    }

    public void creditalerts(Map<String, String> subDetails) {
        HashMap<String, String> smsTemplateMap;
        Map<String, String> smsMap = new HashMap();
        String message = "";
        double creditamt = 0.0;
        boolean messageformulated = false;
        String channel="";

        try {
            smsTemplateMap = databaseFunctions.getSMSTemplate("rtr_001");
            if (!("0".equals(subDetails.get("largecredit").trim()))) {
                creditamt = Double.parseDouble(subDetails.get("largecredit").replaceAll(",", ""));
                if (transactionAmount >= creditamt) {
                    messageformulated = true; //prevents customer from getting two alerts i.e. largecredit and credit if subscribed to both
                    message = smsTemplateMap.get("SMSTEMPLATE");
                    message = message.replace("ACCOUNTNO", accountNumber);
                    message = message.replace("DATE", trandate);
                    message = message.replace("AMOUNT", String.valueOf(transactionAmount));
                    message = message.replace("BAL", runningBalance);
                    message = message.replace("TRANSACTIONREF", transactionId);
                    channel="largecredit";
                }
            }

            if (("1".equals(subDetails.get("credit").trim())) && !(messageformulated)) {
                message = smsTemplateMap.get("SMSTEMPLATE");
                message = message.replace("ACCOUNTNO", accountNumber);
                message = message.replace("DATE", trandate);
                message = message.replace("AMOUNT", String.valueOf(transactionAmount));
                message = message.replace("BAL", runningBalance);
                message = message.replace("TRANSACTIONREF", transactionId);
                channel="credit";
            }

            smsMap.put("PHONENUMBER", subDetails.get("mobileno"));
            smsMap.put("SMSMESSAGE", message);
            smsMap.put("NotificationType", "SMS");
            smsMap.put("98", "SMS");
            smsMap.put("37", transactionId);
            smsMap.put("32", channel);
            smsMap.put("102", accountNumber);
            smsMap.put("2", subDetails.get("mobileno"));

            sendtoQueue(smsMap);

        } catch (NumberFormatException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            ESBLog es = new ESBLog("creditAlertsException", "credit alerts message formulation error: " + sw.toString());
            es.log();
        }

    }

    public void debitalerts(Map<String, String> subDetails) {
        HashMap<String, String> smsTemplateMap;
        Map<String, String> smsMap = new HashMap();
        String message = "";
        double debitamt = 0.0;
        double cutoff = 250.00;
        boolean messageformulated = false;
        String channel="";
        try {
            smsTemplateMap = databaseFunctions.getSMSTemplate("rtr_002");
            if (!("0".equals(subDetails.get("largedebit").trim()))) {
                debitamt = Double.parseDouble(subDetails.get("largedebit").replaceAll(",", ""));
                if ((transactionAmount > cutoff) && (transactionAmount >= debitamt)) {
                    //customer gets alerted
                    messageformulated = true; //prevents customer from getting two alerts i.e. largedebit and debit if subscribed to both
                    message = smsTemplateMap.get("SMSTEMPLATE");
                    message = message.replace("ACCOUNTNO", accountNumber);
                    message = message.replace("DATE", trandate);
                    message = message.replace("AMOUNT", String.valueOf(transactionAmount));
                    message = message.replace("BAL", runningBalance);
                    message = message.replace("TRANSACTIONREF", transactionId);
                    channel="largedebit";
                }

            }

            if (("1".equals(subDetails.get("debit").trim())) && (transactionAmount > cutoff) && !(messageformulated)) {
                message = smsTemplateMap.get("SMSTEMPLATE");
                message = message.replace("ACCOUNTNO", accountNumber);
                message = message.replace("DATE", trandate);
                message = message.replace("AMOUNT", String.valueOf(transactionAmount));
                message = message.replace("BAL", runningBalance);
                message = message.replace("TRANSACTIONREF", transactionId);
                channel="debit";
            }

            smsMap.put("PHONENUMBER", subDetails.get("mobileno"));
            smsMap.put("SMSMESSAGE", message);
            smsMap.put("NotificationType", "SMS");
            smsMap.put("98", "SMS");
            smsMap.put("37", transactionId);
            smsMap.put("32", channel);
            smsMap.put("102", accountNumber);
            smsMap.put("2", subDetails.get("mobileno"));

            sendtoQueue(smsMap);

        } catch (NumberFormatException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            ESBLog es = new ESBLog("debitAlertsException", "debit alerts message formulation error: " + sw.toString());
            es.log();
        }
    }

    public static String GenerateRandomNumber(int charLength) {
        return String.valueOf(charLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1)
                + (int) Math.pow(10, charLength - 1));
    }

    public void sendtoQueue(Map<String, String> smsMap) {
        boolean sent = false;
        String stan = "";
        HashMap<String, String> chargeMap = new HashMap();
        try {

            if (!("".equals(smsMap.get("SMSMESSAGE")))) {
                stan = GenerateRandomNumber(6);
                chargeMap.put("0", "0200");
                chargeMap.put("2", smsMap.get("PHONENUMBER"));
                chargeMap.put("3", "350000");
                chargeMap.put("4", "0");
                chargeMap.put("7", stan);
                chargeMap.put("11", "");
                chargeMap.put("12", "");
                chargeMap.put("24", "CC");
                chargeMap.put("32", "ALERT");
                chargeMap.put("37", (new StringBuilder("EC")).append(databaseFunctions.getCorrelationID()).toString());
                chargeMap.put("38", transactionId);
                chargeMap.put("41", "FID00001");
                chargeMap.put("49", "KES");
                chargeMap.put("56", "bcac5bdc-32fb-42df-8ddb-ce8f705ba8c5");
                chargeMap.put("66", "");
                chargeMap.put("68", "Subscribed debit/credit Alerts charge entry " + transactionId);
                chargeMap.put("69", "");
                chargeMap.put("71", String.valueOf(transactionAmount));
                chargeMap.put("72", "");
                chargeMap.put("73", "");
                chargeMap.put("98", "SUBSCRIBED_ALERTS");
                chargeMap.put("100", "MWALLET");
                chargeMap.put("101", "");
                chargeMap.put("102", accountNumber);
                chargeMap.put("103", "");

                PostGetESBRequest post = new PostGetESBRequest();
                HashMap<String, String> response = post.CURLRequest(chargeMap, configs.get("ESB_SERVLET_URL"), "ESB");

                if ("000".equals(response.get("39"))) {

                    QueueWriter queueWriter = new QueueWriter("jms/NOTIFICATIONS_QUEUE");
                    sent = queueWriter.sendObject(smsMap, transactionId);

                    ESBLog es2 = new ESBLog("SendSms", "sent: " + sent + ", messageMap: " + smsMap);
                    es2.log();

                }
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));

            ESBLog es = new ESBLog("sendtoqueueException", "error: " + sw.toString());
            es.log();

        }

    }

}
