/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.CommonOperations;

import commonj.work.WorkManager;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import ke.co.ekenya.LogEngine.ESBLog;
import static ke.co.ekenya.mdb.RthMDB.Configs;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Njiru
 */
public class Utilities {

    public static final String NEW_LINE = System.getProperty("line.separator");

    public Utilities() {
    }

    public static Map readconfig() {
        Map<String, String> Configs = new HashMap();
        Properties prop = new Properties();
        FileInputStream fip = null;
        String os = "";
        try {
            os = System.getProperty("os.name").toLowerCase();
            if (os.contains("windows")) {
                fip = new FileInputStream("E:\\Configurations\\config.properties");
            } else {
                fip = new FileInputStream("/app/webservices/MWALLET/config.properties");
            }
            prop.load(fip);
            Set<String> propertyNames = prop.stringPropertyNames();
            for (String Property : propertyNames) {
                Configs.put(Property.trim(), prop.getProperty(Property).trim());
            }
        } catch (Exception ex) {
            System.out.println("Error reading config. Error: " + ex.getMessage());
        } finally {
            if (fip != null) {
                try {
                    fip.close();
                } catch (IOException ex) {
                    Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return Configs;
    }

    public static Map<String, String> readPropertyFile(Map<String, String> configs) {
        Properties prop = new Properties();
        FileInputStream fip = null;
        try {
            fip = new FileInputStream("/home/mvisa/ESBAPPLICATIONS/configs/config.properties");
//fip = new FileInputStream("C:/Users/user1/Desktop/Projects_Stuff/CoopProjects/Web_Services/MVisaIssuingPayments/configs/config.properties"); 
            prop.load(fip);
            Set<String> propertyNames = prop.stringPropertyNames();
            for (String Property : propertyNames) {
                configs.put(Property.trim(), prop.getProperty(Property).trim());
            }
            ESBLog el = new ESBLog("ReadConfigs", "ReadConfigs at " + new Date() + " values " + Configs.toString());
            el.logfile();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog("ConfigFileExceptions", "Exception occured while reading config file|" + "\nError:" + sw.toString());
            el.logfile();
        } finally {
            try {
                if (fip != null) {
                    fip.close();
                }
            } catch (Exception ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog("ConfigFileExceptions", "Exception occured while closing config file|" + "\nError:" + sw.toString());
                el.logfile();
            }
        }
        return configs;
    }

    public static WorkManager getWorkManager() {
        WorkManager workManager = null;
        if (workManager == null) {
            try {
                InitialContext ctx = new InitialContext();
                String jndiName = "java:global/wm/default";
                workManager = (WorkManager) ctx.lookup(jndiName);
            } catch (NamingException e) {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                ESBLog es = new ESBLog("WorkmanagerExceptions", "error: " + sw.toString());
                es.log();
            }
        }
        return workManager;
    }

    public static HashMap convertNodesFromXml(String xml) throws Exception {
        if (xml.contains(NEW_LINE)) {
            xml = trimmessage(xml);//remove white spaces and new lines otherwhise it fails if xml is formatted ---Edwin Njiru 22/01/2018
        }
        InputStream is = new ByteArrayInputStream(xml.getBytes());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(is);
        return (HashMap) createMap(document.getDocumentElement());
    }

    public static Object createMap(Node node) {
        Map<String, Object> map = new HashMap<>();
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            String name = currentNode.getNodeName();
            Object value = null;
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                value = createMap(currentNode);
            } else if (currentNode.getNodeType() == Node.TEXT_NODE) {
                return currentNode.getTextContent().trim();
            }
            if (map.containsKey(name)) {
                Object object = map.get(name);
                if (object instanceof List) {
                    ((List<Object>) object).add(value);
                } else {
                    List<Object> objectList = new LinkedList<Object>();
                    objectList.add(object);
                    objectList.add(value);
                    map.put(name, objectList);
                }
            } else {
                map.put(name, value);
            }
        }
        return map;
    }

    public static String trimmessage(String input) {
        BufferedReader reader = new BufferedReader(new StringReader(input));
        StringBuilder result = new StringBuilder();
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line.trim());
            }
            return result.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
