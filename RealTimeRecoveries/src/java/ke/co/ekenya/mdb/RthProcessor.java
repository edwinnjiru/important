/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.mdb;

import commonj.work.Work;
import java.util.HashMap;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import ke.co.ekenya.Database.DatabaseFunctions;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author njiru
 */
public class RthProcessor implements Work {

    private Message message;
    private HashMap<String, String> xmlmap = new HashMap();
    DatabaseFunctions databaseFuntions;

    public RthProcessor(Message message) {
        this.message = message;
        this.databaseFuntions = new DatabaseFunctions();
    }

    @Override
    public void run() {
        try {
            if (message instanceof ObjectMessage) {
                String CorrelationID = message.getJMSCorrelationID();
                xmlmap = (HashMap) (((ObjectMessage) message).getObject());
                xmlmap.put("CorrelationID", CorrelationID);

                ESBLog es = new ESBLog("mwalletIncoming", xmlmap);
                es.log();
                process();

            }

        } catch (JMSException e) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    public void release() {
    }

    @Override
    public boolean isDaemon() {
        return false;
    }

    void process() {
        try {
        xmlmap.put("accountNumber", xmlmap.get("MWALLET_CREDITACCOUNT"));
        xmlmap.put("source", "wallet");
        databaseFuntions.checkloanstatus(xmlmap);
            
        } catch (Exception ex) {
         ESBLog es = new ESBLog("MDBExceptions", ex.getMessage());
         es.log();   
        }

    }

}
