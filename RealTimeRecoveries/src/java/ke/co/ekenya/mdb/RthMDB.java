/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.ekenya.mdb;

import commonj.work.WorkException;
import commonj.work.WorkManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import ke.co.ekenya.CommonOperations.Utilities;
import ke.co.ekenya.LogEngine.ESBLog;

/**
 *
 * @author Njiru
 */
@MessageDriven(mappedName = "jms/REALTIME_QUEUE", activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
    ,
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
    ,
    @ActivationConfigProperty(propertyName = "connectionFactoryLookup", propertyValue = "jms/ECONNECT_JMSFACTORY")
})
public class RthMDB implements MessageListener {

    public static Map<String, String> Configs = new HashMap();

    public RthMDB() {
        
    }

    @Override
    public void onMessage(Message message) {
        WorkManager wm = Utilities.getWorkManager();
        RthProcessor rthprocessor = new RthProcessor(message);
        try {
            wm.schedule(rthprocessor);
        } catch (WorkException | IllegalArgumentException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            //log here
        }

    }

    @PostConstruct
    public void initialize() {
        readPropertyFile(Configs);
        //Configs=Utilities.readconfig();
        //Utilities.readPropertyFile(Configs);
    }

    @PreDestroy
    public void release() {
    }

    /**
     * Reads the configuration file in file system and stores it in hash map
     *
     * @param configs - HashMap to store configuration information
     * 
     */
        public static void readPropertyFile(Map configs) {
        InitialContext initialContext = null;
        try {
            initialContext = new InitialContext();
            NamingEnumeration<NameClassPair> list = initialContext.list("configs");
            while (list.hasMoreElements()) {
                String key = list.next().getName();
                String value = "";
                try {
                    value = (String) initialContext.lookup("configs/" + key);
                    configs.put(key.trim(), value.trim());
                } catch (Exception ex) {
                }
            }
            initialContext.close();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            ESBLog el = new ESBLog(sw.toString());
            el.log();
        } finally {
            try {
                if (initialContext != null) {
                    initialContext.close();
                }
            } catch (Exception ex) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                ESBLog el = new ESBLog(sw.toString());
                el.log();
            }
        }
    }

}
