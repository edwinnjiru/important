package ke.co.ekenya.LogEngine;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SocketHandler;
import ke.co.ekenya.CommonOperations.QueueWriter;


public class ESBLog {

    private FileHandler fh = null;
    private String pathtologs = "/var/log/esblogs/RealTimeHunter/";
    private SocketHandler sh = null;
    //for socket handler
    private String directory = "RealTimeHunter";

    private String filename = null;
    private String msg = null;
    private Logger logger = null;
     public HashMap<String, String> logmap;

    public ESBLog(String filename, String msg) {
        this.filename = filename;
        this.msg = msg;
    }
    
    public ESBLog(Exception e,String msg) {
        this.filename = "exceptions";
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        this.msg = sw.toString() +msg;
    }

    public ESBLog(String filename, HashMap map) {
        this.filename = filename;
        logmap = new HashMap(map);
        if (logmap.containsKey("RawMessage")) {
            logmap.put("RawMessage", "");
        }
        this.msg = logmap.toString();
    }

    public ESBLog(String msg) {
        this.filename = "exceptions";
        this.msg = msg;
    }

    public void logfile() {
        logger = Logger.getLogger("");
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        String daylog = format.format(new Date());
        try {
            fh = new FileHandler(createDailyDirectory() + filename + "%g.txt", 26000000, 20, true);
            fh.setFormatter(new EsbFormatter());
            logger.setLevel(Level.FINE);
            logger.addHandler(fh);
            logger.fine(msg);
            fh.close();
        } catch (Exception e) {            
            e.printStackTrace();
        }

    }

    public void logfilex() {
        logger = Logger.getLogger(filename);
        String finalmessage = "###" + directory + "$$$" + filename + "###" + msg;

        try {
            sh = new SocketHandler("192.168.114.211", 5050);
            sh.setFormatter(new EsbFormatter());
            logger.addHandler(sh);
            logger.setLevel(Level.FINE);
            logger.fine(finalmessage);
            sh.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public void log() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
        String timestamp = format.format(new Date());
        String finalmessage = "###" + directory + "$$$" + filename + "###" +timestamp+"::"+ msg;
        QueueWriter qw = new QueueWriter("jms/ESB_LOG_QUEUE");
        qw.send(finalmessage, "");
    }
    

    
     public String createDailyDirectory() {
        String Dailydirectory = "";
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        String daylog = format.format(new Date());
        Dailydirectory = pathtologs +  daylog;
        new File(Dailydirectory).mkdir();
        return Dailydirectory + "/";
    }
}
