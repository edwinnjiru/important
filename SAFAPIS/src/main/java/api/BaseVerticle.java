package api;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class BaseVerticle extends AbstractVerticle{
    /**
     * Initialize base verticle
     *
     * @throws Exception
     */
    @Override
    public void start() throws Exception {
        this.initialize();
    }

    @Override
    public void start(Future<Void> startFuture) {
        this.initialize();
    }

    /**
     * Initialize this instance and dependent variables
     */
    private void initialize() {
        try {

            vertx.deployVerticle(DarajaApi.class.getName());
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
