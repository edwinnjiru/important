package api;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class DarajaApi extends BaseVerticle {

    @Override
    public void start() {
        // Create a Vert.x web router
        Router router = Router.router(vertx);

        //Register a simple router on /SafApi
        router.get("/SafApi").handler(rc -> {
            rc.response().end("service is available " + Thread.currentThread().getName());
        });

        // Register a body handler indicating that other routes need
        // to read the request body
        router.route().handler(BodyHandler.create());

        //Register a second route(POST) to fetch requests
        router.post("/SafApi").handler(rc -> {
            JsonObject body=rc.getBodyAsJson();
            rc.response()
                    .putHeader("content-type", "application/json")
                    .end(body.toString());
        });

        vertx.createHttpServer()
                // Pass the router's accept method as request handler
                .requestHandler(router::accept)
                .listen(8888);
        System.out.println("server up and running");
    }
}
