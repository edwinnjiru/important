/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encrpter;

import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author hmungai
 */
public class EncryptionUtils {
    private static final String key = "@123COOP321#";
    private static final String HASHING_ALG = "MD5";
    private static final String ENCODING = "utf-8";
    private static final String ENCRYPTION_ALG = "DESede";
    
    public EncryptionUtils(){
        
    }
    
    public static String encrypt(String plainPass) throws Exception {
        MessageDigest md = MessageDigest.getInstance(HASHING_ALG);
        byte[] digestOfPassword = md.digest(key.getBytes(ENCODING));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        SecretKey key = new SecretKeySpec(keyBytes, ENCRYPTION_ALG);
        Cipher cipher = Cipher.getInstance(ENCRYPTION_ALG);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] plainTextBytes = plainPass.getBytes(ENCODING);
        byte[] buf = cipher.doFinal(plainTextBytes);
        byte[] base64Bytes = Base64.encodeBase64(buf);
        String base64EncryptedString = new String(base64Bytes);
        return base64EncryptedString;
    }

    public static String decrypt(String encryptedPass) throws Exception {
        byte[] message = Base64.decodeBase64(encryptedPass.getBytes(ENCODING));
        MessageDigest md = MessageDigest.getInstance(HASHING_ALG);
        byte[] digestOfPassword = md.digest(key.getBytes(ENCODING));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        SecretKey key = new SecretKeySpec(keyBytes, ENCRYPTION_ALG);
        Cipher decipher = Cipher.getInstance(ENCRYPTION_ALG);
        decipher.init(Cipher.DECRYPT_MODE, key);
        byte[] plainText = decipher.doFinal(message);
        return new String(plainText, ENCODING);
    }
}
