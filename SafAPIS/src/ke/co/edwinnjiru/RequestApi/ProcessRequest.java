package ke.co.edwinnjiru.RequestApi;

import ke.co.edwinnjiru.CommonOperations.LogWriter;
import org.json.JSONObject;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class ProcessRequest {
    public JSONObject requestObject;

    public ProcessRequest(JSONObject requestObject) {
        this.requestObject = requestObject;
    }

    public Map doProcess(){
        Map<String,String>responseMap=new HashMap();
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        try {
            String date=LocalDateTime.now().format(formatter);
            String shortCode="174379";
            String passKey="bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919";
            String password=(new StringBuilder(shortCode)).append(passKey).append(date).toString();

            JSONObject safObject=new JSONObject();
            safObject.put("BusinessShortCode",shortCode);
            safObject.put("Password",(new String(Base64.getEncoder().encode(password.getBytes()))));
            safObject.put("Timestamp",date);
            safObject.put("TransactionType","CustomerPayBillOnline");
            safObject.put("Amount",requestObject.getString("amount"));
            safObject.put("PartyA",requestObject.getString("phonenumber"));
            safObject.put("PartyB",shortCode);
            safObject.put("PhoneNumber",requestObject.getString("phonenumber"));
            safObject.put("CallBackURL","https://323db954.ngrok.io/SafAPIS/resources/CallBackApi");
            safObject.put("AccountReference",requestObject.getString("ref"));
            safObject.put("TransactionDesc","pay edu");



            URL url=new URL("https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest");
            HttpURLConnection safURLConnection = (HttpURLConnection)url.openConnection();
            safURLConnection.setRequestProperty ("Authorization", "Bearer "+generateToken());
            safURLConnection.setRequestMethod("POST");
            safURLConnection.setRequestProperty("Content-Type", "application/json");
            safURLConnection.setRequestProperty("Content-length", String.valueOf(safObject.toString().length()));
            safURLConnection.setDoOutput(true);
            safURLConnection.setDoInput(true);

            try (DataOutputStream wr = new DataOutputStream(safURLConnection.getOutputStream())) {
                wr.writeBytes(safObject.toString());
                wr.flush();
            }

            JSONObject ackObject=new JSONObject(getInputStream(safURLConnection));
            System.out.println(ackObject);
            if(!ackObject.getString("ResponseCode").equalsIgnoreCase("0")){
                responseMap.put("error","true");
                responseMap.put("message",ackObject.getString("CustomerMessage"));
            }else{
                responseMap.put("error","false");
                //responseMap.put("message","STK Push Initiated successfully");
                responseMap.put("message",ackObject.getString("CustomerMessage"));
            }


        }catch (Exception ex){
            (new LogWriter("EXceptions","stkExc",ex.getMessage())).writeLogs();
        }

        return responseMap;
    }

    public String generateToken(){
        String token="";
        String response="";
        try {
            String key="tZXMFaGfOE5DkZakDrJ7esPVH9Kod3U1";
            String secret="GrX0VUtnVsx9KYHK";
            String saltPrepare =(new StringBuilder(key)).append(":").append(secret).toString();

            URL url=new URL("https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials");
            HttpURLConnection safURLConnection = (HttpURLConnection)url.openConnection();
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(saltPrepare.getBytes()));

            safURLConnection.setRequestProperty ("Authorization", basicAuth);
            safURLConnection.setRequestMethod("GET");
            safURLConnection.setUseCaches(false);
            safURLConnection.setDoInput(true);
            safURLConnection.setDoOutput(true);

            JSONObject resObject=new JSONObject(getInputStream(safURLConnection));
            token=resObject.getString("access_token");


        }catch (Exception ex){
            (new LogWriter("EXceptions","tokenExc",ex.getMessage())).writeLogs();
        }
        return token;
    }


   public String getInputStream(HttpURLConnection safURLConnection){
       String response="";
       StringBuilder responseBuilder;
       try {

       try (BufferedReader in = new BufferedReader(
               new InputStreamReader(safURLConnection.getInputStream()))) {
           String inputLine;
           responseBuilder = new StringBuilder();
           while ((inputLine = in.readLine()) != null) {
               responseBuilder.append(inputLine);
           }
       }
       response=responseBuilder.toString();
       }catch (Exception ex){
           (new LogWriter("EXceptions","inputExc",ex.getMessage())).writeLogs();
       }
       return response;
   }
}
