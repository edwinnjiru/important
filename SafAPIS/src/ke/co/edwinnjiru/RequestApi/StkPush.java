package ke.co.edwinnjiru.RequestApi;


import ke.co.edwinnjiru.CommonOperations.LogWriter;
import org.json.JSONObject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import java.util.Map;

// The Java class will be hosted at the URI path "/helloworld"
@Path("/safapi")
public class StkPush {
    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media type "text/plain"
    @Produces("text/plain")
    public String getClichedMessage() {
        // Return some cliched textual content
        return "Hello World";
    }

    /**
     * @param content
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String processRequest(String content) {
        String response = "";
        try {
            (new LogWriter("APICALLS","initstk",content)).writeLogs();
            JSONObject requestObject = new JSONObject(content);
            Map<String, String> responseMap = (new ProcessRequest(requestObject)).doProcess();
            JSONObject responseObject=new JSONObject(responseMap);
            response=responseObject.toString();
        } catch (Exception ex) {
            (new LogWriter("EXceptions","initExc",ex.getMessage())).writeLogs();
        }

        return response;
    }


}
