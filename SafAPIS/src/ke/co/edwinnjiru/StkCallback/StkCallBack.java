package ke.co.edwinnjiru.StkCallback;


import ke.co.edwinnjiru.CommonOperations.LogWriter;
import ke.co.edwinnjiru.CommonOperations.Utilities;
import org.json.JSONObject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import java.util.Map;

// The Java class will be hosted at the URI path "/helloworld"
@Path("/CallBackApi")
public class StkCallBack {
    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media type "text/plain"
    @Produces("text/plain")
    public String getClichedMessage() {
        // Return some cliched textual content
        return "Hello World";
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String ProcessCallBack(String content){
        String response="";
        try {
            (new LogWriter("APICALLS","CallBack",content)).writeLogs();

            Map<String,String> callBackMap=(new Utilities()).convertJsonToMap(content);
            (new ProcessCallBack(callBackMap)).doProcess();
            JSONObject resObject=new JSONObject();
            resObject.put("message","success");
            resObject.put("MerchantRequestID", callBackMap.get("MerchantRequestID"));
            response=resObject.toString();

        }catch (Exception ex){
            (new LogWriter("EXceptions","CallExc",ex.getMessage())).writeLogs();
        }
        return response;

    }

}
