package ke.co.edwinnjiru.CommonOperations;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

public class LogWriter {

    String dirname;
    String file;
    String message;

    public LogWriter(String dirname, String file, String message) {
        this.dirname = dirname;
        this.file = file;
        this.message = message;
    }

    public void writeLogs() {
        FileWriter aWriter = null;
        String logsDirectoryPath = "";
        try {
            logsDirectoryPath = "/media/njiru/Work/Logs/SAFAPI";
            Date todaysLog = new Date();
            SimpleDateFormat ftTodaysLog = new SimpleDateFormat("dd-MMM-yyyy");

            File todaysDir = new File(logsDirectoryPath + "/" + ftTodaysLog.format(todaysLog));
            if (!todaysDir.exists()) {
                todaysDir.mkdir();
            }
            File foldername = new File(todaysDir + "/" + dirname);
            if (!foldername.exists()) {
                foldername.mkdir();
            }
            File filename = new File(foldername + "/" + file);
            if (!filename.exists()) {
                filename.mkdir();
            }
            Date dateByHour = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddhh");

            Date now = new Date();
            DateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
            String currentTime = df.format(now);

            aWriter = new FileWriter(filename + "/" + ft.format(dateByHour) + ".log", true);
            aWriter.write(currentTime + " " + message + "\n");
            aWriter.flush();
            aWriter.close();

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(LogWriter.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                aWriter.close();

            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(LogWriter.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
