package ke.co.edwinnjiru.CommonOperations;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Utilities {

    public Utilities(){}

    public Map convertJsonToMap(String callbackRequest){
        Map<String,String>responseMap=new HashMap();
        try {
            JSONObject callBackObject=new JSONObject(callbackRequest);
            System.out.println(callBackObject);
            JSONObject bodyObject=callBackObject.getJSONObject("Body");
            System.out.println(bodyObject);
            JSONObject stkCallBackObject=bodyObject.getJSONObject("stkCallback");
            System.out.println(stkCallBackObject);
            JSONObject callBackMetadata = stkCallBackObject.getJSONObject("CallbackMetadata");
            JSONArray metaArray = callBackMetadata.getJSONArray("Item");

            responseMap.put("resultcode", String.valueOf(stkCallBackObject.getInt("ResultCode")));
            responseMap.put("MerchantRequestID", stkCallBackObject.getString("MerchantRequestID"));
            responseMap.put("checkoutrqstid", stkCallBackObject.getString("CheckoutRequestID"));
            responseMap.put("resultdesc", stkCallBackObject.getString("ResultDesc"));
            for (int i = 0; i < metaArray.length(); i++) {
                JSONObject jsObect = metaArray.getJSONObject(i);
                if (!jsObect.has("Value")) {
                    jsObect.put("Value", "");
                }
                String value="";
                if(checkInstance(jsObect.get("Value"),"java.lang.Integer")){
                    value=String.valueOf(jsObect.getInt("Value"));
                }

                if(checkInstance(jsObect.get("Value"),"java.lang.String")){
                    value=jsObect.getString("Value");
                }


                if(checkInstance(jsObect.get("Value"),"java.lang.Double")){
                    value=String.valueOf(jsObect.getDouble("Value"));
                }

                if(checkInstance(jsObect.get("Value"),"java.lang.Long")){
                    value=String.valueOf(jsObect.getLong("Value"));
                }

                responseMap.put(jsObect.getString("Name").toLowerCase(), value);
            }

        }catch (Exception ex){
            System.out.println("Exc: "+ex.getMessage());
        }
        return responseMap;
    }

    private boolean checkInstance(Object obj, String cls)
            throws ClassNotFoundException {
        return Class.forName(cls).isInstance(obj);
    }
}
