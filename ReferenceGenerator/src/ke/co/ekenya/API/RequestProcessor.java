package ke.co.ekenya.API;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Map;
import java.util.Stack;

public class RequestProcessor {
    String request;
    private static Stack<String> ussdStack = new Stack<>();
    private static Stack<String> wapStack = new Stack<>();
    private static Stack<String> c2bStack = new Stack<>();
    static String previousDate = "";

    public RequestProcessor(String request) {
        this.request = request;
    }

    public String doProcess() {
        String ref = "";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
        try {
            JSONObject requestObject = new JSONObject(request);
            if (ussdStack.empty()) {
                //generate Stack
                previousDate = LocalDate.now().format(formatter);
                generateStack();
                wapStack = (Stack) ussdStack.clone();
                c2bStack = (Stack) ussdStack.clone();

            } else {
                if (Integer.valueOf(LocalDate.now().format(formatter)) > Integer.valueOf(previousDate)) {
                    previousDate = LocalDate.now().format(formatter);
                    generateStack();
                    wapStack = (Stack) ussdStack.clone();
                    c2bStack = (Stack) ussdStack.clone();
                }
            }
            switch (requestObject.getString("channel").toLowerCase()) {
                case "ussd":
                    ref = (new StringBuilder(previousDate)).append("A").append(ussdStack.pop()).toString();
                    break;
                case "wap":
                    ref = (new StringBuilder(previousDate)).append("B").append(wapStack.pop()).toString();
                    break;
                case "c2b":
                    ref = (new StringBuilder(previousDate)).append("C").append(c2bStack.pop()).toString();
                    break;
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
        }

        return ref;
    }


    private static String getCharForNumber(int i) {
        return i > 0 && i < 27 ? String.valueOf((char) (i + 'A' - 1)) : null;
    }

    public static void reverse(Stack<String> arr) {
        arr.sort(Collections.reverseOrder());
    }

    public void generateStack() {
        String padNumerals = "%03d";
        try {
            if (!ussdStack.empty()) {
                ussdStack.clear();
            }

            for (int i = 1; i <= 26; i++) {
                for (int j = 1; j <= 26; j++) {
                    for (int k = 1; k <= 999; k++) {
                        ussdStack.push((new StringBuilder(getCharForNumber(i)).append(getCharForNumber(j)).append(String.format(padNumerals, k))).toString());
                    }
                }
            }
            reverse(ussdStack);

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
        }
    }
}
