package ke.co.ekenya.API;

import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

@WebServlet(name = "RefGenerator", urlPatterns = {"/coop/references"})
public class RefGenerator extends HttpServlet {

    //public static final Map<String, String> CONFIGS = new HashMap();

   /* @Override
    public void init() {
        (new Utilities()).readPropertyFile(CONFIGS);
        (new ESBLog("readconfigs", CONFIGS.toString())).log();
    }*/

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            String content = getServletInputStream(request);
            String res = (new RequestProcessor(content.replaceAll("&", ""))).doProcess();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("ref", res);
            out.println(jsonObject.toString());
            out.flush();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Available", true);
            out.println(jsonObject.toString());
            out.flush();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
        }
    }

    public String getServletInputStream(HttpServletRequest request) {
        String response = "";
        StringBuilder jb = new StringBuilder();
        String line = "";
        try (BufferedReader reader = request.getReader()) {
            while ((line = reader.readLine()) != null) {
                jb.append(line);
            }
            response = jb.toString();
        } catch (IOException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
        }
        return response;
    }
}
