
# This code tests the point class.
PointA=Point(3,5) # Create a point with coordinates (3,5)
PointB=Point(-4,-4)
# How far is it from point A to point B?
print "A to B:",PointA.DistanceToPoint(PointB)
# What"if I go backwards?
print "B to A:",PointB.DistanceToPoint(PointA)
# Who lives inside the circle of radius 5 centered at (3,3)?
CircleCenter=Point(3,3)
print "A in circle:",PointA.IsInsideCircle(CircleCenter,5)
print "B in circle:",PointB.IsInsideCircle(CircleCenter,5)