from math import sqrt

class Point(object):

	def __init__(self,X,Y):
		self.X=X
		self.Y=Y

	def DistanceToPoint(self, OtherPoint):
		"Returns the distance from this point to another"
		SumOfSquares = ((self.X-OtherPoint.X)**2) +	((self.Y-OtherPoint.Y)**2)
		#return math.sqrt(SumOfSquares)
		return sqrt(SumOfSquares)

	def IsInsideCircle(self, Center, Radius):
		"Return 1 if this point is inside the circle, 0 otherwise"
		if (self.DistanceToPoint(Center)<Radius):
			return 1
		else:
			return 0
	# try:
	# 	# This code tests the point class.
	# 	PointA=Point(3,5) # Create a point with coordinates (3,5)
	# 	PointB=Point(-4,-4)
	# 	# How far is it from point A to point B?
	# 	print "A to B:",PointA.DistanceToPoint(PointB)
	# 	# What if I go backwards?
	# 	print "B to A:",PointB.DistanceToPoint(PointA)
	# 	# Who lives inside the circle of radius 5 centered at (3,3)?
	# 	CircleCenter=Point(3,3)
	# 	print "A in circle:",PointA.IsInsideCircle(CircleCenter,5)
	# 	print "B in circle:",PointB.IsInsideCircle(CircleCenter,5)
	# except Exception as e:
	# 	print "hahaha" +str(e)
	