import string

def CountWords(Text):
	"Count how many times each word occurs in a text"
	WordCount={}

	CurrentWord=""
	Text=Text+"."
	PiecesOfWords=string.letters +"'-"

	for CharacterIndex in range(0,len(Text)):
		CurrentCharacter=Text[CharacterIndex]
		#print str(Text)
		if(PiecesOfWords.find(CurrentCharacter)!=-1):
			CurrentWord=CurrentWord+CurrentCharacter
		else:
			if(CurrentWord!=""):
				CurrentWord=string.lower(CurrentWord)

				CurrentCount=WordCount.get(CurrentWord,0)
				WordCount[CurrentWord]=CurrentCount+1
				CurrentWord=""
	return (WordCount)

if (__name__=="__main__"):
	TextFile=open("poem.txt","r")
	Text=TextFile.read()
	TextFile.close()

	WordCount=CountWords(Text)
	#print "Work count"+str(WordCount)
	SortedWords=WordCount.keys()
	SortedWords.sort()
	for Word in SortedWords:
		print Word,WordCount[Word]